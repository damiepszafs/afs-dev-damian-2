@IsTest
global class RemoteSiteManagerMock implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        //response.setBody('{"response":[{"statusCode":"200","pagesTotal":"1300","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}]}');
        response.setBody('{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                         '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                         '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                         	'"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                         	'"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                         	'"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                         '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                         	'"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                         	'"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                         '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[]}');
        response.setStatusCode(200);
        return response; 
    }
}