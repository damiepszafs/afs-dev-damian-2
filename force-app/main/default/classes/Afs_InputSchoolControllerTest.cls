/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Afs_InputSchoolControllerTest {
	
	

    @isTest static void testGetSchoolAutoComplete() {
        // TO DO: implement unit test
        List<Account> schools = Afs_InputSchoolController.getSchoolAutoComplete('Columbia');
        System.assert(schools.size() >= 0);
    }
 	
 	 @isTest static void testGetMySchool() {
    	Account school = Afs_InputSchoolController.getMySchool('');
    	Account newSchool = new Account();
    	System.assert(school.Id == newSchool.id);
    }
    
    @isTest static void testGetMySchoolWithId() {
    	Account existentSchool = new Account(Name='test');
    	insert existentSchool;
    	Account testSchool = Afs_InputSchoolController.getMySchool(existentSchool.Id);
    	System.assertEquals(existentSchool.Id, testSchool.Id);
    }
    
    @isTest static void testGetAddresAutoComplete() {
    	String response = Afs_InputSchoolController.getAddressAutoComplete('Columbia', '(region)', 'en');
    	System.assert(response.length() > 2);
    }
    
    @isTest static void testGetAdressAutoCompleteMerge() {
    	String response = Afs_InputSchoolController.getAddressAutoCompleteMerge('Harv', '(region)', 'en');
    	Test.startTest();
    	system.debug(response);
    	Test.stopTest();
    	System.assert(response.length() > 2);
    }
    
    @isTest static void testGetAddresDetails() {
    	String resp = Afs_InputSchoolController.getAddressDetails('ChIJyQ3Tlj72wokRUCflR_kzeVc', 'en');
        System.assert(resp.length() > 2);
    }
    
    @isTest static void testPredictions() {
    	Account test = new Account(Name='Test School', BillingStreet='Santiago', BillingPostalCode='111', BillingState='Santa Fe', BillingCountry='Argentina', BillingCity='Rosario', GMPlace_Id__c='211312312');
    	Afs_InputSchoolController.Predictions t = new Afs_InputSchoolController.Predictions(test);
    	System.assertEquals(t.description, 'Test School, Santa Fe, Argentina'); 
    }
   
}