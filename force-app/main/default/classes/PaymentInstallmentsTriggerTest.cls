@isTest
public class PaymentInstallmentsTriggerTest {
    @testSetup
    public static void setup_method(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }
    static testMethod void Test1() {
    
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dynamic' limit 1];
        Date myDate = Date.today();
        Application__c myApplication = new Application__c(Create_Installments__c = false , Amount__c = 1000);
        insert myApplication;

        Payment_Entry__c payment = new Payment_Entry__c(Application__c = myApplication.Id, Amount__c = 100);
        
        Test.StartTest();
        insert payment;
        PaymentInstallmentsTriggerHandler.dummy();
        Test.StopTest();
        
    }
    
}