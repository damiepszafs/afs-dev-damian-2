@isTest(SeeAllData=false)
global class Afs_SettingControllerTest {
	
    @isTest
    static void getPicklistValuesTest() {  
        Test.startTest();
        	system.assertEquals(true,Afs_SettingController.getPicklistValues('Contact','Gender__c,How_did_you_hear_about_AFS__c,Country_of_Legal_Residence__c,NationalityCitizenship__c').get('NationalityCitizenship__c').Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void saveRecordTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                Contact  con = [SELECT id,FirstName,LastName,Email FROM Contact];
                con.FirstName = 'New New';
                con.LastName = 'New LastName';
                con.Email = 'user@appohm.com';
                Afs_SettingController.saveRecord(con);
            }
        try{
            Afs_SettingController.saveRecord(null);
        }catch(Exception e){
            system.assertEquals(true, e != null);
        }
        Test.stopTest();
        system.assertEquals(true,[SELECT id,email FROM User][0].email != null);
    }
    
    @isTest
    static void changePasswordTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                try{
                    String passWrd = 'Welcome#12';
                Afs_SettingController.changePassword(passWrd);
                }catch(Exception e){
                    system.assertEquals(true,e != null);
                }
                
            }
        Test.stopTest();
        system.assertEquals(false,[SELECT id,ContactId FROM User][0].ContactId != null);
    }
    
    @isTest
    static void changePasswordExceptionTest() {  
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createUserWithContact();
        Test.startTest();
        	list<User> us = [SELECT id,ContactId FROM User WHERE ContactId != null AND isActive = true AND ContactId IN: [SELECT id,FirstName,LastName,Email FROM Contact]];
            System.runAs(us[0]){
                try{
                    String passWrd = 'Wel';
                    Afs_SettingController.changePassword(passWrd);
                }catch(Exception e){
                    system.assertEquals(true,e != null);
                }
            }
        Test.stopTest();
    }
    
    @isTest
    static void getParentsTest(){
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='Test', email='test@test.com', mobilePhone = '1234567889'));
        cons.add(new Contact(LastName='TestFather', email='test2@test.com', Phone = '2234567889'));
        insert cons;
        npe4__Relationship__c rel = new npe4__Relationship__c(npe4__Contact__c = cons[0].Id, npe4__RelatedContact__c = cons[1].Id, npe4__Type__c = 'Father');
        insert rel;
        
        Test.startTest();
        map<String,List<String>> ret = Afs_SettingController.getParents(String.valueOf(cons[0].Id));
        Test.stopTest();
        
        system.assertEquals('test2@test.com', ret.get('Email')[0]);
        system.assertEquals('2234567889', ret.get('Phone')[0]);
    }
}