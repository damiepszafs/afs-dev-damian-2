
global  without sharing class utilCommunity {
@AuraEnabled
    global static String getConsent() {
        String communityId = Network.getNetWorkId();
        String consent = [SELECT Policy_Text__c  FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId][0].Policy_Text__c ;
        return consent;
    }

    @AuraEnabled
    global static String getParentalConsent() {
        String communityId = Network.getNetWorkId();
        String consent = [SELECT Parental_Consent__c FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId][0].Parental_Consent__c ;
        return consent;
    }
    @AuraEnabled
    global static Boolean checkIfUserexist(String email){
        User userObj; 
        List<User> usrs = [SELECT Id FROM User WHERE Email =: email LIMIT 1];
        if(usrs.size() > 0){
                return true;
            }else {
                return false;
            }
    }
    
}
