@isTest
public class AccountTriggerHandler_Test {
    @isTest public static void generateNewUUID_Test(){
        Account acc = new Account(Name = 'TestName');
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('HH_Account').getRecordTypeId();
        Test.startTest();
        Insert acc;
        Test.stopTest();
        
        acc = [SELECT Global_Link_Family_ID__c FROM Account Limit 1];
        System.assert(acc.Global_Link_Family_ID__c != null);
    }
}