global class GL_SF_Integrations_Schedulable implements Schedulable{
    
    global void execute(SchedulableContext sc) {
                
        RemoteSiteManager rsm = new RemoteSiteManager();
        rsm.createHostingFactSheets('1', '50', rsm.integrationCustomSettings.Year__c);
        for(Integer i=2; i <= Integer.valueof(rsm.remoteSiteModel.response[0].pagesTotal) && Limits.getCallouts() < Limits.getLimitCallouts();i++){
            rsm.createHostingFactSheets(String.valueOf(i),'50', rsm.integrationCustomSettings.Year__c);
        }        
        
        GL_SF_Integrations_Batch batchable = new GL_SF_Integrations_Batch(rsm.mapObjects.values(), null, null, rsm.integrationCustomSettings.Year__c);
        System.scheduleBatch(batchable, 'GL-SF Integrations', 1);
        
        //next year
        String nextYearString = String.valueOf(Integer.valueOf(rsm.integrationCustomSettings.Year__c) + 1);
        rsm.createHostingFactSheets('1', '50', nextYearString);
        for(Integer i=2; i <= Integer.valueof(rsm.remoteSiteModel.response[0].pagesTotal) && Limits.getCallouts() < Limits.getLimitCallouts();i++){
            rsm.createHostingFactSheets(String.valueOf(i),'50', nextYearString);
        }        
        
        GL_SF_Integrations_Batch batchableNextYear = new GL_SF_Integrations_Batch(rsm.mapObjects.values(), null, null, nextYearString);
        System.scheduleBatch(batchableNextYear, 'GL-SF Integrations 2', 1);
       
    }
    
}