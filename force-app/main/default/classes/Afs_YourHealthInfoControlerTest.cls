@isTest(SeeAllData=false)
global class Afs_YourHealthInfoControlerTest {
	@isTest
    static void getPicklistValuesTest() {  
        Test.startTest();
        	system.assertEquals(true,Afs_YourHealthInfoControler.getPicklistValues('Contact','Country_of_Legal_Residence__c,NationalityCitizenship__c',false).Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void getHealthInfoRecordTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
             system.assertEquals(true,Afs_YourHealthInfoControler.getHealthInfoRecord([SELECT ID FROM Contact LIMIT 1].id) != null);
        Test.stopTest();
    }
    
    @isTest
    static void saveRecordTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        
        Test.startTest();
            Health_Information__c objHealth = new Health_Information__c();
            objHealth.Contact__c = [SELECT ID FROM Contact LIMIT 1].id;
        
        	List<Application__c> application = [SELECT id FROM Application__c] ; 
            To_Do_Item__c obj = new To_Do_Item__c();
            obj.Name = 'sd'; 
            obj.Type__c = 'Filling Field'; 
            obj.Stage_of_portal__c = '1. Stage: Pre-application'; 
            obj.Application__c  = application[0].id; 
            obj.Status__c = 'Pending'; 
            obj.Due_Date__c = Date.today().addDays(10); 
            insert obj;
            system.assertEquals(true,Afs_YourHealthInfoControler.saveRecord(objHealth,obj.id,false) != null);
            system.assertEquals(true,Afs_YourHealthInfoControler.saveRecord(objHealth,obj.id,false) != null);
        Test.stopTest();
    }
    
}