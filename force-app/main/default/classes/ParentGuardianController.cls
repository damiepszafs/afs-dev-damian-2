public without sharing class ParentGuardianController {
    public ParentGuardianController() {

    }
    
    @AuraEnabled
    public static List<npe4__Relationship__c> getParents(String contactId) {
        return [SELECT Id, npe4__Type__c, npe4__Description__c, They_live_together__c, npe4__RelatedContact__c FROM npe4__Relationship__c WHERE npe4__Contact__c =: contactId];
    }


    @AuraEnabled
    public static Contact getContact(String contactId){
        return [SELECT Id, FirstName, LastName, Email, Birthdate, Place_of_Birth__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Phone FROM Contact WHERE Id =: contactId][0];
    }

    @AuraEnabled
    public static Contact getCurrentContact(String userId){
        User[] contactIds = [SELECT ContactId FROM User WHERE Id =: userId];
        if(contactIds[0].ContactId != null){
            Id contactId = contactIds[0].ContactId;
            return [SELECT Id, OwnerId, FirstName, LastName, Email, AccountId FROM Contact WHERE Id=: contactId][0];
        }
        return new Contact(FirstName='Test');
        
    }
    
    @AuraEnabled
    public static List<CustomSelectOptions> getTypeOptions(){ 
        List<CustomSelectOptions> options = new List<CustomSelectOptions>();

        Schema.DescribeFieldResult fieldResult = npe4__Relationship__c.npe4__Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple) {
            options.add(new CustomSelectOptions(f.getLabel(), f.getValue()));
        }

        return options;
         
    }

    @AuraEnabled
    public static List<CustomSelectOptions> getCountryCode(){
        List<CustomSelectOptions> options = new List<CustomSelectOptions>();

        Schema.DescribeFieldResult fieldResult = Contact.Phone_Country_Codes__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple) {
            options.add(new CustomSelectOptions(f.getLabel(), f.getValue()));
        }

        return options;
        
    }

    public class CustomSelectOptions {
        String label;
        String value;

        @AuraEnabled
        public String getLabel() {
            return this.label;
        }

        public String setLabel(String lbl) {
            this.label = lbl;
            return this.label;
        }

        @AuraEnabled
        public String getValue() {
            return this.value;
        }
        public String setValue(String vl) {
            this.value = vl;
            return this.value;
        }

        public CustomSelectOptions() {
            this.label = '';
            this.value = '';
        }

        public CustomSelectOptions(String lab, String val) {
            this.label = lab;
            this.value = val;
        }
    }   

    @AuraEnabled
    public static Contact createContact(Contact contactRecord){
        try{
            insert contactRecord;
            return contactRecord;
        } catch( Exception ex) {
            Afs_UtilityApex.logger(ex.getstacktracestring() + '###' + ex);  
            throw ex;
        }
        
    }

    @AuraEnabled
    public static npe4__Relationship__c createRelationship(npe4__Relationship__c relationshipRecord){
         try{
            insert relationshipRecord;
            return relationshipRecord;
        } catch( Exception ex) {
            Afs_UtilityApex.logger(ex.getstacktracestring() + '###' + ex);  
            throw ex;
        }
    }

    @AuraEnabled
    public static Contact updateContact(Contact contactRecord){
        try{
            update contactRecord;
            return contactRecord;
        } catch( Exception ex) {
            Afs_UtilityApex.logger(ex.getstacktracestring() + '###' + ex);  
            throw ex;
        }
        
    }

    @AuraEnabled
    public static npe4__Relationship__c updateRelationship(npe4__Relationship__c relationshipRecord){
         try{
            update relationshipRecord;
            return relationshipRecord;
        } catch( Exception ex) {
            Afs_UtilityApex.logger(ex.getstacktracestring() + '###' + ex);  
            throw ex;
        }
    }
}