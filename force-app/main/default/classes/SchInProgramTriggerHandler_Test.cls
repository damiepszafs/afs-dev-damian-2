@isTest
public class SchInProgramTriggerHandler_Test {
 @isTest static void updateScholarshipPreApplicationFeeByProgramOffer() {
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true); // Se coloca esta linea ya que sin este metodo arroja un error al querer crear un application__c
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        Test.startTest();
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        insert newSchAppList;
        
		delete newSchInPro1;
        Test.stopTest();
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id];
        
        system.assertEquals(0, finalApp.Scholarship_Pre_Application_Fee__c);
    }
}