@isTest
public class AfsChapter_Upsert_Queueable_Test {
    public static testMethod void execute_test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode": "200","pagesTotal": "2","itemsTotal": "2","pageIndex": "1","pageSize": "1"}],"organizations":[{"id": "A0929740-88FC-4B1B-830D-6961FD63E23F","ioc_code": "ARG","organization_name": "Buenos Aires","org_status": "Active","country": "Argentina"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        
        AfsChapter_Upsert_Queueable que = new AfsChapter_Upsert_Queueable(1);
        Test.startTest();
        que.execute(null);
        Test.stopTest();
        List<Account> lstAccount = [SELECT Id FROM Account Limit 1];
        system.assert(lstAccount.size() == 1);
    }
	
}