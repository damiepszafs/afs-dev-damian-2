@isTest(SeeAllData=false)
global class AfsComp_ProfileHeaderControllerTest {
    
	@isTest
    static void getSelectedProgramListTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Test.startTest();
            system.assertEquals(true, AfsComp_ProfileHeaderContentController.getSelectedProgramList([SELECT id FROM Application__c LIMIT 1].id).size() > 0);
        Test.stopTest();
    }
    
}