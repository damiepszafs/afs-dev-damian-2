@isTest(SeeAllData=false)
global class Afs_PhotoNavigationControllerTest {
    @isTest
    static void getUploadedPhotoTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                system.assertEquals(true,Afs_PhotoNavigationController.getUploadedPhoto().size() > 0);
            }  
        Test.stopTest();
    }
    
    @isTest
    static void getProfilePhotoTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                system.assertEquals(true,Afs_PhotoNavigationController.getProfilePhoto().size() > 0);
            }  
        Test.stopTest();
    }
    
    @isTest
    static void getEncodedStringTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(1);
        Afs_TestDataFactory.createProfilePhoto(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<ContentDocumentLink> ContentDocumentLinkList = [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: con.id];
                try{
                    system.assertEquals(true,String.isNotBlank(Afs_PhotoNavigationController.getEncodedString(ContentDocumentLinkList[0].ContentDocumentId,true)));
                }catch(Exception e){
                    system.assertEquals(true, e != null);
                }

                
            }  
        Test.stopTest();
    }
    
    @isTest
    static void deleteCDLTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(1);
        //Afs_TestDataFactory.createProfilePhoto(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<ContentDocumentLink> ContentDocumentLinkList = [SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: con.id];
                
                try{
                    list<String> fileId = new list<String> ();
                    fileId.add(ContentDocumentLinkList[0].ContentDocumentId);
                    Afs_PhotoNavigationController.getEncodedStringList(fileId,false);
                    Afs_PhotoNavigationController.deleteCDL(ContentDocumentLinkList[0].ContentDocumentId);
                }catch(Exception e){
                    system.assertEquals(true, e != null);
                }
                
                system.assertEquals(true,true);
            }  
        Test.stopTest();
    }
    
    public static testmethod void getEncodedStringListException(){
        try{
            Afs_PhotoNavigationController.getEncodedStringList(null,null);
        } catch(Exception e){
        }
    }
    
    public static testmethod void deleteCDLException(){
        try{
            Afs_PhotoNavigationController.deleteCDL(null);
        } catch(Exception e){
        }
    }
}