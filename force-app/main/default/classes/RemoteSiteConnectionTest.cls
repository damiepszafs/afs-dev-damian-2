@IsTest
public class RemoteSiteConnectionTest {
    @isTest static void pullObjectsTest(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new RemoteSiteConnectionMock()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse response = RemoteSiteConnection.pullObjects('Test');
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = '{"testResponses": ["test 1", "test 2", "test 3", "test 4", "test 5"]}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
    }
    @isTest static void pullObjectsTestStatus201(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new RemoteSiteConnectionMock201()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse response = RemoteSiteConnection.pullObjects('Test');
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = '{"testResponses": ["test 1", "test 2", "test 3", "test 4", "test 5"]}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(201, response.getStatusCode());
    }
    @isTest static void pullObjectsTestStatusException(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new RemoteSiteConnectionMockFailed()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse response = RemoteSiteConnection.pullObjects('Test');
        // Verify that the response received contains fake values
    }
    
    @isTest static void postObjectTest(){
        // Set mock callout class 
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        RemoteSiteConnection.postObject('Test', 'Test', null);
    }
    @isTest static void postObjectTestException(){
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new RemoteSiteConnectionMockFailed()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        RemoteSiteConnection.postObject('Test', 'Test', null);
    }
}