@isTest
public class GetTravelData_Queueable_Test {
    @isTest static void executeLess50_Test(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        Application__c app = Util_Test.createApplication(null,null); 
        app.Get_Travel_Data_From_Global_Link__c = true;
        Insert app; 
        List<Application__c> lstApps = new List<Application__c>();
        lstApps.add(app);
        
        Test.startTest();            
 		GetTravelData_Queueable que = new GetTravelData_Queueable(lstApps);
       	System.enqueueJob(que);
		Test.stopTest();        
		
        //app = [SELECT Get_Travel_Data_From_Global_Link__c FROM Application__c WHERE Id = :app.Id];
        //System.assert(!app.Get_Travel_Data_From_Global_Link__c);        
    }
    
    @isTest static void executeMore50_Test(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        List<Application__c> lstApps = new List<Application__c>();
        for(Integer i=0;i<51;i++){
            Application__c app = Util_Test.createApplication(null,null); 
        	app.Get_Travel_Data_From_Global_Link__c = true;
            lstApps.add(app);
        }        
        Insert lstApps; 
        
        Test.startTest();            
 		GetTravelData_Queueable que = new GetTravelData_Queueable(lstApps);
       	System.enqueueJob(que);
		Test.stopTest();        
		
        Integer countUpdated = 0;
    	//In test cant stack queue
        Integer countNotUpdated = 0;
        for(Application__c app : [SELECT Get_Travel_Data_From_Global_Link__c FROM Application__c]){
            if(!app.Get_Travel_Data_From_Global_Link__c){
                countUpdated++;
            }else{
                countNotUpdated++;        
            } 
        }
        //System.assert(countUpdated == 50);
		//System.assert(countNotUpdated == 1);         
    }
}