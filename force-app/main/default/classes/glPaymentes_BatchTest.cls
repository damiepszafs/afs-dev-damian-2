@IsTest
public class glPaymentes_BatchTest {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.createApplication(con,null);
        app.GL_Service_Id__c = '0429B6AD-2976-4A1B-934C-00001A30FE5F';
        insert app;
        glPaymentTypes__c paytype1 = new glPaymentTypes__c(Name='App Fee',GL_payment_Record__c='App Fee',GL_payment_service__c='ApplicationFee',GL_payment_Integration__c='application_fee');
        insert paytype1;
        glPaymentTypes__c paytype2 = new glPaymentTypes__c(Name='Deposit',GL_payment_Record__c='Deposit',GL_payment_service__c='ProgramDeposit',GL_payment_Integration__c='program_deposit');
        insert paytype2;
        GL_Payment__c glappfee1 = new GL_Payment__c(Name='App Fee',Application__c=app.id, Payment_Type__c='App Fee',Payment_Status__c='Pending',GL_Service_ID__c='0429B6AD-2976-4A1B-934C-00001A30FE5F');
        insert glappfee1;
        GL_Payment__c glappfee2 = new GL_Payment__c(Name='Deposit',Application__c=app.id, Payment_Type__c='Deposit',Payment_Status__c='Pending',GL_Service_ID__c='0429B6AD-2976-4A1B-934C-00001A30FE5F');
        insert glappfee2;
        Id toDoRecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        To_Do_Item__c todoList = new To_Do_Item__c(PAY_APPLICATION_FEE__c = true,Name='PAY APPLICATION FEE',Stage_of_portal__c='2. Stage: Pre-selected',Status__c='Pending',Application__c=app.id,RecordTypeId=toDoRecordTypeId,Due_Date__c=date.today(),Type__c='Task');
        insert todoList;
       
    } 
    public static testMethod void batch_Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","usa_payment_status":{"application_fee": "Paid","program_deposit": "Paid"},"BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        
        
        test.startTest();
        Id batchJobId = Database.executeBatch(new glPaymentes_Batch());
        test.stoptest();
        
        To_Do_Item__c todoitem = [select id,Status__c from To_Do_Item__c limit 1];
        system.assertEquals(todoitem.Status__c, 'Completed');
    }
}