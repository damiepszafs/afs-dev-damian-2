@isTest
public class Afs_SavePersonalInformation_Test {
 @testSetup
    public static void setup_test(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger', false, false);
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        contact contactParent = Util_Test.create_Conact(acc, 'Applicant','Test contact');
        contact relationContact =  Util_Test.create_Conact(acc, 'General','Test Relation');
        npe4__Relationship__c relation = Util_Test.create_RelationRec(contactParent , relationContact);
        application__c application = Util_Test.create_Applicant( contactParent,po);
        // Insert configuration object
		AFS_Community_Configuration__c configuraionObject = new AFS_Community_Configuration__c();
        configuraionObject.Afs_SendingPartner__c = acc.id;
        configuraionObject.Name = 'Afs Applicant Community Test';
        configuraionObject.Community_URL__c = 'https://dev02-afstest.cs66.force.com/applicant';
        configuraionObject.Unique_Id__c = UserInfo.getUserId().subString(0,15);
        configuraionObject.Community_App_Queue__c = UserInfo.getUserId();
        configuraionObject.CRMUserOwner__c = UserInfo.getUserId();
        configuraionObject.Community_Guest_User_Owner__c = '0051I000002MkUbQAK';
        insert configuraionObject;
    }
   /*  public static testMethod void getParentGuardianListTest(){
     	Test.startTest(); 
        List<contact> testContactList = [select Id from contact where lastName = 'Test contact'];
       	List<Afs_ParentGuardianCtrl.ParentGuardianWrapper> ParentWrpper = Afs_ParentGuardianCtrl.getParentGuardianList(testContactList[0].Id);
        Test.stopTest(); 
        System.assertEquals(1,ParentWrpper.size());
    
    }*/
    public static testMethod void savePersonalInfoTest (){
    	Test.startTest(); 
        List<contact> testContactList = [select Id from contact where lastName = 'Test contact'];
        contact  contactTest = testContactList[0] ;
        contactTest.FirstName ='test 1';
       	Afs_SavePersonalInformation.savePersonalInfo(contactTest);
        Test.stopTest(); 
        System.assertEquals('test 1',contactTest.FirstName);
    
    }
    
     public static testMethod void getPicklistValuesTest (){
        Test.startTest();
        	System.assertEquals(true,Afs_SavePersonalInformation.getPicklistValues('contact', 'NationalityCitizenship__c').containsKey('NationalityCitizenship__c'));
        Test.stopTest(); 
        //System.assert(false,picklsitmap);
        
    }
    public static testMethod void getPicklistValuesNoSortTest (){
        Test.startTest();
        	System.assertEquals(true,Afs_SavePersonalInformation.getPicklistValuesNoSort('contact', 'NationalityCitizenship__c').containsKey('NationalityCitizenship__c'));
        Test.stopTest(); 
        //System.assert(false,picklsitmap);
        
    }
 
    public static testMethod void saveCompleteProfileTest (){
         Test.startTest();
        List<contact> testContactList = [select Id,LGBTQ_Member__c,Ethnicity__c,AccountId, OwnerId from contact  where  lastName = 'Test contact' ];
        List<Application__c> testApplicationList = [select Id from Application__c limit 1];
        Application__c applicationRec  = 	testApplicationList[0];
        applicationRec.Religious_affiliation__c ='-None-';
       	List<Afs_ParentGuardianCtrl.ParentGuardianWrapper> ParentWrpper = Afs_ParentGuardianCtrl.getParentGuardianList(testContactList[0].Id);
        String parentGuardianWrapperJSON = JSON.serialize(ParentWrpper); 	
        String schoolAppObj = '{"Applicant__c":"'+ testContactList[0].Id +'","School__c":null,"Graduation_Date__c":"2019-04-11","Grade_Average__c":"9","Id":null}';
        Account school = new Account(Name='Test School');
        insert school;
        Afs_SavePersonalInformation.saveCompleteProfile(testContactList[0],applicationRec , schoolAppObj, school, parentGuardianWrapperJSON);
        Test.stopTest(); 
        system.assertEquals(1,testApplicationList.size());
    
	}
     public static testMethod void getSchoolsTest(){
         Test.startTest();
     	 list<Afs_SavePersonalInformation.schoolWrapper>  schoolList = Afs_SavePersonalInformation.getSchools();
         Test.stopTest(); 
         system.assertEquals(1,schoolList.size());
     }
     
     public static testMethod void getSchoolApplicationTest() {
     	String schoolId = '';
     	String applicantId = [select Id from Application__c limit 1][0].Id;
     	Applicant_School_Association__c test = Afs_SavePersonalInformation.getSchoolApplication(applicantId, schoolId);
     	system.assertEquals(applicantId, test.Applicant__c);
     }
     
     public static testMethod void getSchoolIdwithValidationTest() {
     	String testUser = [Select Id from User limit 1][0].Id;
     	Account school = new Account(Name='Test School', BillingLatitude=38.8951, BillingLongitude=-77.0364);
     	String schoolId = Afs_SavePersonalInformation.getSchoolIdwithValidation(school, testUser);
     	System.assert(schoolId.length() > 0);
     }
     
     public static testMethod void parseTest(){
     	Contact testContactList = [select Id,LGBTQ_Member__c,Ethnicity__c,AccountId from contact  where  lastName = 'Test contact' ][0];
     	Applicant_School_Association__c schoolApp = Afs_SavePersonalInformation.parse('{"Applicant__c":"'+ contact.Id +'","School__c":null,"Graduation_Date__c":"2019-04-11","Grade_Average__c":"9","Id":null}');
     	system.assertEquals(String.valueOf(contact.Id), schoolApp.Applicant__c);
     }
    
    @isTest public static void getHasFlagshipPrograms(){
        Application__c app = [SELECT Program_Offer__c, Applicant__c FROM Application__c Limit 1];
        Program_Offer_in_App__c poa = Util_Test.createProgramOfferInApp(app.Id,app.Applicant__c,app.Program_Offer__c);
        Insert poa;
        Test.startTest();
        Boolean result = Afs_SavePersonalInformation.getHasFlagshipPrograms(app.Applicant__c, app.Id);
        Test.stopTest();
        System.assert(result);
    }
}