/**
 * Created by jotategui on 09/05/2018.
 */

public with sharing class SholarshipTriggerHandler {
    public static void createMasterToDos(Map<Id, Scholarship__c> programs, boolean isUpdate){

        if(isUpdate)
            TodoListHelper.deleteOldMasterToDos(programs.keySet());

        set<id> templatesId = new Set<Id>();

        for(Scholarship__c sch : programs.values()){
            templatesId.add(sch.To_Do_Template__c);
        }

        Map<Id,List<Master_To_Do__c>> MasterTodoGrouped = new Map<Id,List<Master_To_Do__c>>();
        for(Master_To_Do__c masterToDo : [SELECT Name, Days__c,  Dependant_Task__c, Program_Offer__c, Help__c,
                Scholarship__c, Template__r.Id, When__c,Description__c, Type__c, Link__c, Stage_of_portal__c, RecordType.Name, To_Do_Label__c from Master_To_Do__c WHERE Template__r.Id IN :templatesId]){
            Id TemplateId = masterToDo.Template__r.Id;
            if(!MasterTodoGrouped.containsKey(TemplateId))
                MasterTodoGrouped.put(TemplateId, new List<Master_To_Do__c>());
            MasterTodoGrouped.get(TemplateId).add(masterToDo);
        }

        List<Master_To_Do__c> MasterToDoInsert = new List<Master_To_Do__c>();
        for(Scholarship__c sch : programs.values()){
            List<Master_To_Do__c> programMaster_to_dos = new List<Master_To_Do__c>();
            programMaster_to_dos = TodoListHelper.cloneMasterToDo(MasterTodoGrouped.get(sch.To_Do_Template__c),sch.id,'Scholarship');
            for(Master_To_Do__c todo : programMaster_to_dos){
                MasterToDoInsert.add(todo);
            }
        }
        insert masterToDoInsert;
    }
    
    public static void isUpdatePreAppFeeAmount(List<Scholarship__c> listUpdateScholarship,Map<Id, Scholarship__c> mapScholarshipOldValues){
        system.debug('SholarshipTriggerHandler.isUpdatePreAppFeeAmount');
        try{
            Set<Id> listScholarship = new Set<Id>();
            for(Scholarship__c objScholarship : listUpdateScholarship){
                if(objScholarship.Rre_Application_Fee_Amount__c != mapScholarshipOldValues.get(objScholarship.Id).Rre_Application_Fee_Amount__c){
                    listScholarship.add(objScholarship.Id);
                }
            }
            
            
            
            if(listScholarship.size() > 0){
                List<Id> listApp = new List<Id>();
                system.debug('listScholarship='+listScholarship);
                List<Scholarship__c> newListScholarship = [SELECT Id,(SELECT Id,Application__c FROM Scholarship_Applications__r) FROM Scholarship__c WHERE Id IN: listScholarship];
                List<Id> listApplicationSelect = new List<Id>();
                for(Scholarship__c objScholarship : newListScholarship){
                    for(Scholarship_Application__c objSchApp : objScholarship.Scholarship_Applications__r){
                        if(listApplicationSelect.contains(objSchApp.Application__c) == false){
                            listApplicationSelect.add(objSchApp.Application__c);
                        	listApp.add(objSchApp.Application__c);
                        }
                    }
                }
          
                ApplicationTriggerHandler.sumPreApplicationFeeByListApp(listApp);
           }
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
    
    public static void ifScholarshipIsDeleted(List<Scholarship__c> listOldsScholarship){
        system.debug('SholarshipTriggerHandler.ifScholarshipIsDeleted');
        try{
            List<Id> listScholarshipId = new List<Id>();
            for(Scholarship__c objScholarship : listOldsScholarship){
                listScholarshipId.add(objScholarship.Id);
            }
            
            List<Scholarship__c> newListScholarship = [SELECT Id,(SELECT Id,Application__c FROM Scholarship_Applications__r) FROM Scholarship__c WHERE Id IN: listScholarshipId];
            List<Scholarship_Application__c> listSchAppOld = new List<Scholarship_Application__c>();
            for(Scholarship__c objScholarship : newListScholarship){
                for(Scholarship_Application__c objSchApp : objScholarship.Scholarship_Applications__r){
                    listSchAppOld.add(objSchApp);
                }
            }
            
            delete listSchAppOld;
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
}