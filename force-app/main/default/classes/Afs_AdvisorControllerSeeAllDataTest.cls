@isTest(SeeAllData=true)
public class Afs_AdvisorControllerSeeAllDataTest {
    public static testMethod void insertFeedTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);        
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        List<Afs_Triggers_Switch__c> ats = Afs_Triggers_Switch__c.getall().values();
		if(ats.isEmpty()){			
			Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
		}
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        Test.startTest();  
        Afs_AdvisorController.insertFeed(app.id,'Demo Body');
        System.assertEquals(true,[SELECT id FROM FeedItem WHERE ParentId =: app.id] != null);
        
        Test.stopTest(); 
    }
    
    public static testMethod void insertFeedExpTest(){
        Test.startTest(); 
        trY{
            Afs_AdvisorController.insertFeed(null,null);
        }catch(Exception e){
            System.assertEquals(true,e != null);
        }
        Test.stopTest(); 
    }
    
    public static testMethod void insertCommentTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
		List<Afs_Triggers_Switch__c> ats = Afs_Triggers_Switch__c.getall().values();
		if(ats.isEmpty()){			
			Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
		}
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        Test.startTest(); 
        List<application__c>  application  = [select Id from application__c limit 1] ; 
        Afs_AdvisorController.insertFeed(application[0].id,'Demo Body');
        Afs_AdvisorController.insertComment([SELECT id,InsertedById FROM FeedItem WHERE ParentId =: application[0].id limit 1].id,' DEMO Comment');
        System.assertEquals(true,[SELECT id FROM FeedItem WHERE ParentId =: application[0].id] != null);
        Afs_AdvisorController.feedWrapper feed = new Afs_AdvisorController.feedWrapper([SELECT id,Body,InsertedById,LikeCount,
                                                                                        (SELECT id,CommentBody,InsertedById FROM FeedComments 
                                                                                         ORDER BY CreatedDate) FROM FeedItem
                                                                                        WHERE ParentId =: application[0].id limit 1]
                                                                                       , true,'NES',new Map<id,String> (),'');
        Test.stopTest(); 
    }
    
    public static testMethod void insertCommentExpTest(){
        Test.startTest(); 
        trY{
            Afs_AdvisorController.insertComment(null,null);
        }catch(Exception e){
            System.assertEquals(true,e != null);
        }
        Test.stopTest(); 
    }
    
    public static testMethod void getFeedExpTest(){
        Test.startTest(); 
        trY{
            Afs_AdvisorController.getFeed(null,null,null);
        }catch(Exception e){
            System.assertEquals(true,e != null);
        }
        Test.stopTest(); 
    }
    
    public static testMethod void getFeedTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Contact con2 = Util_Test.createContact('Test Contact',null);
        Insert con2;
        List<Afs_Triggers_Switch__c> ats = Afs_Triggers_Switch__c.getall().values();
		if(ats.isEmpty()){			
			Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
		}
        Application__c app = Util_Test.createApplication(con2,null);
        insert app;
        
        Test.startTest(); 
        
        application__c  application  = [select Id,Ownerid from application__c limit 1] ; 
        Afs_AdvisorController.insertFeed(application.Id,'Demo Body');
        Afs_AdvisorController.insertComment([SELECT id FROM FeedItem WHERE ParentId =: application.Id limit 1].Id,' DEMO Comment');
        Id profile = [select id from profile where name='Applicant Community Profile Login' limit 1].id;
        id rId = [SELECT id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'Applicant' limit 1].id;
        
        
        id aRid = [Select Id From RecordType Where DeveloperName = 'AFS_Partner' limit 1].id;
        Account acct = new Account(Name='TestFeedAccount',RecordTypeId = aRid);
        insert acct;
        
        Contact con = new Contact(firstname='TestFeed',
                                  lastname='TestFeed',
                                  AccountId=acct.Id,email='TestFeed@HenderCross.com',RecordTypeId = rId);
        insert con;
        
        User user = new User(alias = 'TestFeed', email='TestFeed@HenderCross.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='TestFeed@HenderCross.com');
        
        //User who implements the change set must have a role assigned
        insert user;
        
        
        system.runAs(user) {
            Afs_AdvisorController.getFeed(application.Id,application.Ownerid,UserInfo.getUserId());
        }
        
        Test.stopTest(); 
    }
}