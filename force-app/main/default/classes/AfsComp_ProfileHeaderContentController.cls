global with sharing class AfsComp_ProfileHeaderContentController {	
    /**
     * @Desc : Return selected program list for a given application id 
     * @param applicant_Id : Current logged in contact active application id
     */
   	@AuraEnabled
    global static List<SelectedProgramWrapper> getSelectedProgramList(string applicant_Id){
        map<String,String> valueMap = Afs_UtilityApex.getAccountCountryLabel();
        List<Program_Offer_in_App__c> programOfferList	 = [SELECT id,Tolabel(Program_Offer__r.Length__c) ,
                                                            Program_Offer__r.Destinations__c,Program_Offer__r.Name,
                                                            Program_Offer__r.From__c,Program_Offer__r.To__c,
                                                            Program_Offer__r.Program_Title__c, Program_Offer__r.Program_Durations__c,
                                                            Program_Offer__r.Departure_Dates__c
                 											FROM Program_Offer_in_App__c 
                                                            WHERE Application__c = :  applicant_Id];
       		
      
        List<SelectedProgramWrapper> SelectedProgramWrapperList = new  List<SelectedProgramWrapper>();
        for( Program_Offer_in_App__c programOfferRec : programOfferList){
            SelectedProgramWrapper wrapperObj = new SelectedProgramWrapper(programOfferRec);
            wrapperObj.Destination = valueMap.get(programOfferRec.Program_Offer__r.Destinations__c);
            SelectedProgramWrapperList.add(wrapperObj);
        }
             
        return SelectedProgramWrapperList;
    }
    
    /**
     * @Desc : Wrapper class for wrappling program offered 
     */
    global class SelectedProgramWrapper{
        @AuraEnabled
        global string Program {get;set;}
    	@AuraEnabled
        global string Destination  {get;set;}
        @AuraEnabled
        global string Duration {get;set;}
        @AuraEnabled
        global string ProgramDates {get;set;}
        
         
       global SelectedProgramWrapper(Program_Offer_in_App__c programOffer ){
           if(programOffer.Program_Offer__r.Program_Durations__c != Null){
               this.Duration = label.AfsLbl_Multiple_Durations;
           }
           else{
               this.Duration = programOffer.Program_Offer__r.Length__c;  
           }
           this.Destination = programOffer.Program_Offer__r.Destinations__c;
           this.Program = programOffer.Program_Offer__r.Program_Title__c;
           string programDate='';
          
           if(programOffer.Program_Offer__r.To__c != null && programOffer.Program_Offer__r.From__c !=null ){
               Datetime startDate = datetime.newInstance(programOffer.Program_Offer__r.From__c.year(), programOffer.Program_Offer__r.From__c.month(),programOffer.Program_Offer__r.From__c.day());
               Datetime endDate = datetime.newInstance(programOffer.Program_Offer__r.To__c.year(), programOffer.Program_Offer__r.To__c.month(),programOffer.Program_Offer__r.From__c.day());
                
              programDate  =  string.valueOf(startDate.format('MMMMM')).left(3) +' ' + string.valueOf(startDate.year()); 
               
              programDate += ' - ' + string.valueOf(endDate.format('MMMMM')).left(3) +' ' + string.valueOf(endDate.year()); 
           }
           if(programOffer.Program_Offer__r.Departure_Dates__c != NULL){
               this.ProgramDates = label.AfsLbl_Multiple_Departure_Dates;
           }
           else{
               this.ProgramDates = programDate; // static
           }
        }
    }
    
       
}