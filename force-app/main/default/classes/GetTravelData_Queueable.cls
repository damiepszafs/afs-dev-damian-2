//Class to process get TravelData asynchronous 
public class GetTravelData_Queueable implements Queueable,  Database.AllowsCallouts{
    List<Application__c> lstApplication = new List<Application__c>();
    
    public GetTravelData_Queueable(List<Application__c> lst){
        this.lstApplication = lst;
    }
    
    public void execute(QueueableContext context){
        List<Application__c> lstAppToProcess = new List<Application__c>();
        RemoteSiteManager rsm = new RemoteSiteManager();
        //If size is less than 50 process list complete
        if(lstApplication.size() <= 50){            
            lstAppToProcess = lstApplication;
        }else{
			//If not process first 50 to avoid Limit Time            
            for(Integer i = 0; i < 50 && i < lstApplication.size(); i++){
                lstAppToProcess.add(lstApplication[i]);
                lstApplication.remove(i);
            }
            //Prepare for the next 50
            GetTravelData_Queueable que = new GetTravelData_Queueable(lstApplication);
            if(!Test.isRunningTest()){
            	System.enqueueJob(que);
            }
        }
        //Callout
        rsm.createServiceAndOA(lstAppToProcess);
    }
}