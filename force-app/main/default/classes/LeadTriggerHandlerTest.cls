@isTest
public class LeadTriggerHandlerTest {
    @IsTest static void updateCompanyField_Test(){  
        Lead newLead = new Lead(LastName = 'test lastname',Status = 'Open - Not Contacted');
        
        test.startTest();
        insert newLead;
        test.stopTest();
        
        Lead finalLead = [select id,Company from Lead where id=: newLead.id];
        system.assertEquals('test lastname', finalLead.Company);
        
    }
}