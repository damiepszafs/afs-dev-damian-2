@isTest
public class Afs_ParentGuardianCtrlTest {

    @testSetup
    public static void setup_test(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        contact contactParent = Util_Test.create_Conact(acc, 'Applicant','Test contact');
        contact relationContact =  Util_Test.create_Conact(acc, 'General','Test Relation');
        npe4__Relationship__c relation = Util_Test.create_RelationRec(contactParent , relationContact);
    }
    
    public static testMethod void getParentGuardianListTest(){
     	Test.startTest(); 
        List<contact> testContactList = [select Id from contact where lastName = 'Test contact'];
       	List<Afs_ParentGuardianCtrl.ParentGuardianWrapper> ParentWrpper = Afs_ParentGuardianCtrl.getParentGuardianList(testContactList[0].Id);
        Test.stopTest(); 
        System.assertEquals(1,ParentWrpper.size());
    
    }
     public static testMethod void deleteParentGuardianTest(){
        Test.startTest(); 
        List<contact> testContactList = [select Id from contact where lastName = 'Test contact'];
        List<npe4__Relationship__c> relation =  [select Id from npe4__Relationship__c where  npe4__Type__c ='Father'];
       	Afs_ParentGuardianCtrl.deleteParentGuardian(relation[0].Id);
        List<Afs_ParentGuardianCtrl.ParentGuardianWrapper> ParentWrpper = Afs_ParentGuardianCtrl.getParentGuardianList(testContactList[0].Id);
        Test.stopTest(); 
        System.assertEquals(0,ParentWrpper.size() );   
     }
    
    
}