@isTest
public class SholarshipTriggerHandler_Test {
    @isTest static void createMasterToDos_Test() {
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        insert newScholarshipList;
        
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        
        Test.startTest();
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);
        List<Master_To_Do_Template__c> newMasterToDoTem = Util_Test.createListMasterToDoTemplate(1);
        Master_To_Do__c newMasterToDo = Util_Test.create_Master_To_Do(newMasterToDoTem[0],newScholarship1,newProgramOffer1,'Custom_for_Program_Offer');
        insert newMasterToDo;
        
        Map<Id, Scholarship__c> programs = new Map<Id, Scholarship__c>();
        programs.put(newScholarshipList[0].Id, newScholarshipList[0]);
        
        
        SholarshipTriggerHandler.createMasterToDos(programs, false);
        Test.stopTest();
    }
    
    @isTest static void  isUpdatePreAppFeeAmountSholarship_Test() {
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true); // Se coloca esta linea ya que sin este metodo arroja un error al querer crear un application__c
        
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        Test.startTest();
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        insert newSchAppList;
        
        newScholarshipList[0].Rre_Application_Fee_Amount__c = 10;
        update newScholarshipList;
        Test.stopTest();
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id];
        
        system.assertEquals(10, finalApp.Scholarship_Pre_Application_Fee__c);
    }
    
    @isTest static void  ifScholarshipIsDeleted_Test() {
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        Account newAccount2 = Util_Test.create_AFS_Partner2('Test account 2'); //It's already insert
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true); // Se coloca esta linea ya que sin este metodo arroja un error al querer crear un application__c
        
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        Hosting_Program__c newHostingProgram2 = Util_Test.create_Hosting_Program(newAccount2); //It's already insert
        Test.startTest();
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);
        //Program_Offer__c newProgramOffer2 = Util_Test.create_Program_Offer2(newAccount2,newHostingProgram2,'Test-programoff-2',idtestproofff);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        Scholarship__c newScholarship2 = Util_Test.create_Scholarship2(newAccount2,20,'Test Scholarship 2');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        newScholarshipList.add(newScholarship2);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        Sch_in_Program__c newSchInPro2 = Util_Test.create_ScholarshipProgram(newScholarship2,newProgramOffer1);
        Test.stopTest();
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        Scholarship_Application__c newSchApp2 = Util_Test.create_ScholarshipApplication2(newScholarship2,newContact,newApplication,'Processing');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        newSchAppList.add(newSchApp2);
        insert newSchAppList;
        
        
        delete newScholarship1;
        
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id];
        
        system.assertEquals(20, finalApp.Scholarship_Pre_Application_Fee__c);
    }
}