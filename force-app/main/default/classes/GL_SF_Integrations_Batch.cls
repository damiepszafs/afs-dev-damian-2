global class GL_SF_Integrations_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    public List<sObject> lst = new List<sObject>();
    public Integer max;
    public Integer page;
    public String year;
	public static boolean isExecutingBatchIntegrations = false;
    
   
    public GL_SF_Integrations_Batch(List<sObject> pList,Integer pPage,Integer pMax, String pYear){
        lst = pList;
        page = pPage;
        if (page == null){
            page = 1;
        }
        max = pMax;    	
        year = pYear;
     }
    
    global Iterable<sObject> start(Database.BatchableContext BC){
		return lst;

    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){        
		List<Database.UpsertResult> lstUR = new List<Database.UpsertResult>();
        map<String,Hosting_Program__c> mapHostingPrograms = new map<String,Hosting_Program__c>();
        List<Program_Offer__c> lstProgramOffer = new List<Program_Offer__c>();

        
        if(scope[0].getSObjectType() == Schema.Hosting_Program__c.getSObjectType()){
            lstUR = Database.upsert((List<Hosting_Program__c>)scope, Schema.Hosting_Program__c.Name_External_Id__c,false);
        }else if(scope[0].getSObjectType() == Schema.Program_Offer__c.getSObjectType()){
            for(Hosting_Program__c hp : [SELECT Id, Name_External_Id__c FROM Hosting_Program__c WHERE Name_External_Id__c <> null]){
            	mapHostingPrograms.put(hp.Name_External_Id__c,hp);
        	}
			for(Program_Offer__c po : (List<Program_Offer__c>)scope){
                if(po.Name != null){
                    String key = po.Name.right(12);
                    if(mapHostingPrograms.containsKey(key)){
                        po.Hosting_Program__c = mapHostingPrograms.get(key).Id;
                        lstProgramOffer.add(po);
                    }
                }
            }
            if(!lstProgramOffer.isEmpty()){
                GL_SF_Integrations_Batch.isExecutingBatchIntegrations = true;
            	lstUR = Database.upsert(lstProgramOffer, Schema.Program_Offer__c.Name_External_Id__c,false);
            }
        }
    

        for(Integer i = 0;i < lstUR.size();i++){
            Database.UpsertResult ur = lstUR.get(i);
            if(!ur.isSuccess()){
                if(scope[0].getSObjectType() == Schema.Hosting_Program__c.getSObjectType()){
                    system.debug('Hosting Program: '+scope.get(i).get('Name_External_Id__c'));
                }else{
                	system.debug('Program Offer: '+lstProgramOffer.get(i).Name_External_Id__c);
                }
                system.debug(ur.getErrors());
            }            
        }
        

    }
    
    global void finish(Database.BatchableContext BC){
        if(max == null || page < max ){
            RemoteSiteManager rsm = new RemoteSiteManager();
            rsm.createMatrix(String.ValueOf(page),'50', year);
            max = Integer.ValueOf(rsm.remoteSiteModel.response[0].pagesTotal);
            page++;
            for(; page <= max && Limits.getCallouts() < Limits.getLimitCallouts();page++){
            	rsm.createMatrix(String.valueOf(page),'50', year);
            }
            GL_SF_Integrations_Batch batchable = new GL_SF_Integrations_Batch(rsm.mapObjects.values(), page, max, year);
            System.scheduleBatch(batchable, 'GL-SF Integrations ' + year + ' ' + String.valueOf(page), 1);
    	}
    }
}