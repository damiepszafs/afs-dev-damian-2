@isTest
private class ApplicationTriggerHandler_Test {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }    
     
    public static testMethod void calculateScore_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        List<Scoring_Settings__c> lstSS = new List<Scoring_Settings__c>();
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Application__c', 'Tell_us_about_yourself__c', 'Length', '5', '10',null, 10);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS2', 'Application__c', 'What_do_you_want_out_of_this_experience__c', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS3', 'Contact', 'Name', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS4', 'Application__c', 'Interests__r', 'Child', '1', null,null, 5);
        lstSS.add(ss);
        Insert lstSS;
         
        List<Application__c> lstApplication = new List<Application__c>();
        Application__c app = Util_Test.createApplication(con,null);
        lstApplication.add(app);       
        
        Test.startTest();
        Insert lstApplication;
        app = [SELECT Score__c FROM Application__c Limit 1];
        System.assertEquals(95,app.Score__c);
        
        Program_Offer_in_App__c poa = new Program_Offer_in_App__c(Application__c = app.Id, Program_offer__c = po.Id, Contact__c = con.Id);
        Insert poa;
        app.Tell_us_about_yourself__c = 'Test 123';
        update app;
        app = [SELECT Score__c FROM Application__c Limit 1];
        Test.stopTest();
        
        System.assertEquals(110,app.Score__c);
        
    }
    
    @isTest static void updateScholarshipPreApplicationFeeByProgramOffer() {
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        Account newAccount2 = Util_Test.create_AFS_Partner2('Test account 2'); //It's already insert

        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        system.debug('newContact.RecordType:' + newContact.RecordType);
        Test.startTest();
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        Hosting_Program__c newHostingProgram2 = Util_Test.create_Hosting_Program(newAccount2); //It's already insert
        
        
        
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        Scholarship__c newScholarship2 = Util_Test.create_Scholarship2(newAccount2,20,'Test Scholarship 2');
        Scholarship__c newScholarship3 = Util_Test.create_Scholarship2(newAccount2,10,'Test Scholarship 3');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        newScholarshipList.add(newScholarship2);
        newScholarshipList.add(newScholarship3);
        insert newScholarshipList;
        
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        list<Program_Offer__c> listProg = new list<Program_Offer__c>();
        
        Program_Offer__c newProgramOffer1 = new Program_Offer__c();
        newProgramOffer1.name = 'Test-programoff-1';
        newProgramOffer1.Durations__c = 'SM';
        newProgramOffer1.Hosting_Program__c = newHostingProgram1.Id;
        newProgramOffer1.From__c = Date.newInstance(2020, 2, 17);
        newProgramOffer1.To__c = Date.newInstance(2020, 6, 17);
        newProgramOffer1.Sending_Partner__c = newAccount1.Id;
        newProgramOffer1.Flexible_Age_Range__c = 'Yes';
        newProgramOffer1.Applications_Received_To_local__c = System.today() + 30;
        newProgramOffer1.RecordTypeId = idtestproofff; //flagship
        listProg.add(newProgramOffer1);
        Program_Offer__c newProgramOffer2 = new Program_Offer__c();
        newProgramOffer2.name = 'Test-programoff-2';
        newProgramOffer2.Durations__c = 'SM';
        newProgramOffer2.Hosting_Program__c = newHostingProgram2.Id;
        newProgramOffer2.From__c = Date.newInstance(2020, 2, 17);
        newProgramOffer2.To__c = Date.newInstance(2020, 6, 17);
        newProgramOffer2.Sending_Partner__c = newAccount2.Id;
        newProgramOffer2.Flexible_Age_Range__c = 'Yes';
        newProgramOffer2.Applications_Received_To_local__c = System.today() + 30;
        newProgramOffer2.RecordTypeId = idtestproofff; //flagship
        listProg.add(newProgramOffer2);
		insert listProg;
        
        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer2,RecordTypeIdApp);
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        Sch_in_Program__c newSchInPro2 = Util_Test.create_ScholarshipProgram(newScholarship2,newProgramOffer1);
        Sch_in_Program__c newSchInPro3 = Util_Test.create_ScholarshipProgram(newScholarship3,newProgramOffer2);
        Test.stopTest();
                system.debug('newContact.recordTypeId:' + newContact.RecordType);

        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        Scholarship_Application__c newSchApp2 = Util_Test.create_ScholarshipApplication2(newScholarship2,newContact,newApplication,'Processing');
        Scholarship_Application__c newSchApp3 = Util_Test.create_ScholarshipApplication2(newScholarship3,newContact,newApplication,'Won');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        newSchAppList.add(newSchApp2);
        newSchAppList.add(newSchApp3);
        system.debug('newSchApp1.Applicant__r.recordTypeId:' +newSchApp1.Applicant__r.RecordType);
        system.debug('newSchApp2.Applicant__r.recordTypeId:' + newSchApp2.Applicant__r.RecordType);
        system.debug('newSchApp3.Applicant__r.recordTypeId:' +newSchApp3.Applicant__r.RecordType);
        insert newSchAppList;
        
        newApplication.Program_Offer__c = newProgramOffer1.Id;
        update newApplication;
        
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id];
        
        system.assertEquals(70, finalApp.Scholarship_Pre_Application_Fee__c);
    }   
    
    public static testMethod void generateNewUUID_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Application__c app = Util_Test.createApplication(con,null);
        Insert app;
        app = [SELECT Global_Link_Participant_App_ID__c, GL_Service_Id__c FROM Application__c WHERE Id = :app.Id];
        System.assert(app.Global_Link_Participant_App_ID__c != NULL);
		System.assert(app.GL_Service_Id__c != NULL);  
    }
    
    public static testMethod void sendAppContactToGlobalLink_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        Application__c app = Util_Test.createApplication(con,po);
        Insert app;
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Applicant Community Member Profile' Limit 1];
		User user1 = new User(
    	Username = System.now().millisecond() + 'test12345@test.com',
    	ContactId = con.Id,
    	ProfileId = portalProfile.Id,
    	Alias = 'test123',
    	Email = 'test12345@test.com',
    	EmailEncodingKey = 'UTF-8',
    	LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US');
        insert user1;
		app.Status__c = 'Decision';
		Update app;    
        test.startTest();
        	SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{}');
            Test.setMock(HttpCalloutMock.class,fakeResponse);
        	app.Status__c = 'Cancelled';
        	app.Cancellation_Reason__c = 'Became Non-Responsive';
			Update app;
        test.stopTest();
        system.assertEquals('Applicant Community Profile Login',[SELECT Profile.Name FROM User WHERE id =: user1.id].Profile.Name);
    }
    
    @isTest public static void getTravelDataFromGL_Test(){        
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        Test.startTest();
        Application__c app = Util_Test.createApplication(con,po);   		
        Insert app;       
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
       
        app.Get_Travel_Data_From_Global_Link__c = false;
        Update app;
        Test.stopTest();
		
        app = [SELECT Get_Travel_Data_From_Global_Link__c FROM Application__c WHERE Id = :app.Id];
        System.assert(!app.Get_Travel_Data_From_Global_Link__c);
        
    }
    
    @isTest public static void emptyInsert(){
        Application__c app = new Application__c(Interview_Outcome__c = 'Accepted');
        insert app;
        system.debug([SELECT id, recordtypeid from Application__c where id = :app.id]);
    }
    /*
    @isTest public static void updateRecordTypeTest(){
        Hosting_Program__c hp = [SELECT Id FROM Hosting_Program__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        pos.add(new Program_Offer__c());
        pos[0].name = 'Test Program Offer Minors';
        pos[0].Hosting_Program__c = hp.Id;
        pos[0].Sending_Partner__c = acc.Id;
        pos[0].Applications_Received_To_local__c = System.today() + 30;
        pos[0].RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Minors').getRecordTypeId();
        pos.add(new Program_Offer__c());
        pos[1].name = 'Test Program Offer Adults';
        pos[1].Hosting_Program__c = hp.Id;
        pos[1].Sending_Partner__c = acc.Id;
        pos[1].Applications_Received_To_local__c = System.today() + 30;
        pos[1].RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Adults').getRecordTypeId();
        pos.add(new Program_Offer__c());
        pos[2].name = 'Test Program Offer Other_Programs';
        pos[2].Hosting_Program__c = hp.Id;
        pos[2].Sending_Partner__c = acc.Id;
        pos[2].Applications_Received_To_local__c = System.today() + 30;
        pos[2].RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Other_Programs').getRecordTypeId();
        Insert pos;
        
        Test.startTest();
        List<Application__c> apps = new List<Application__c>();
        apps.add(new Application__c(Program_Offer__c = pos[0].Id, Interview_Outcome__c = 'Accepted'));
        apps.add(new Application__c(Program_Offer__c = pos[1].Id, Interview_Outcome__c = 'Accepted'));
        apps.add(new Application__c(Program_Offer__c = pos[2].Id, Interview_Outcome__c = 'Accepted'));
        insert apps;
        Test.stopTest();
    }
    
    @isTest public static void modifyOnStatusTest(){
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        System.runAs(new User(Id=UserInfo.getUserId())){
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Application__c');
            insert testQueue;
        }
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Contact con = [SELECT Id FROM CONTACT LIMIT 1];
        Account acc = new Account(Name = 'Tset', Flagship_Queue__c = Id.valueOf(testGroup.Id), 
                                  Sentio_Queue__c = Id.valueOf(testGroup.Id), Other_Program_Queue__c = Id.valueOf(testGroup.Id), recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('AFS Partner').getRecordTypeId());
        insert acc;
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        Id hp = [SELECT Id FROM Hosting_Program__c LIMIT 1].Id;
        pos.add(new Program_Offer__c(name = 'Test Program Offer', Hosting_Program__c = hp, Sending_Partner__c = acc.Id, Applications_Received_To_local__c = System.today() + 30, 
                                     RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Minors').getRecordTypeId()));
        pos.add(new Program_Offer__c(name = 'Test Program Offer', Hosting_Program__c = hp, Sending_Partner__c = acc.Id, Applications_Received_To_local__c = System.today() + 30, 
                                     RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Other_Programs').getRecordTypeId()));
        pos.add(new Program_Offer__c(name = 'Test Program Offer', Hosting_Program__c = hp, Sending_Partner__c = acc.Id, Applications_Received_To_local__c = System.today() + 30, 
                                     RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Adults').getRecordTypeId()));
        insert pos;
        List<Application__c> apps = new List<Application__c>();
        apps.add(new Application__c(ownerId = testGroup.Id, Applicant__c = con.Id, Status__c = 'Interest', Interview_Outcome__c = 'Accepted'));
        apps.add(new Application__c(ownerId = testGroup.Id, Applicant__c = con.Id, Status__c = 'Interest', Program_Offer__c = pos[1].Id, Interview_Outcome__c = 'Accepted'));
        apps.add(new Application__c(ownerId = testGroup.Id, Applicant__c = con.Id, Status__c = 'Interest', Program_Offer__c = pos[2].Id, Interview_Outcome__c = 'Accepted'));
        insert apps;
        List<Program_Offer_in_App__c> poas = new List<Program_Offer_in_App__c>();
        poas.add(new Program_Offer_in_App__c(Application__c = apps[0].Id, Program_Offer__c = pos[0].Id, Contact__c = con.Id, Selected__c = true));
        poas.add(new Program_Offer_in_App__c(Application__c = apps[1].Id, Program_Offer__c = pos[1].Id, Contact__c = con.Id, Selected__c = true));
        poas.add(new Program_Offer_in_App__c(Application__c = apps[2].Id, Program_Offer__c = pos[2].Id, Contact__c = con.Id, Selected__c = true));
        insert poas;
        apps[0].Status__c = 'Participation Desire';
        apps[1].Status__c = 'Decision';
        apps[2].Status__c = 'Confirmation';
        update apps;
        Test.stopTest();
    }
	*/
   
    public static testMethod void insertTwoGLPaymentRecords_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Group g1 = new Group(Name='group name', type='Queue');
        insert g1;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
        QueuesObject q2 = new QueueSObject(QueueID = g1.id, SobjectType = 'AFS_Community_Configuration__c');
        insert q2;
        }
        
        QueueSobject q1 = [Select Id,q.Queue.Name,q.Queue.ID from QueueSobject q LIMIT 1] ; 
        
        Account acc = [select id from account limit 1];
       
        AFS_Community_Configuration__c afscmm = new AFS_Community_Configuration__c(Community_Guest_User_Owner__c=UserInfo.getUserId(),Community_URL__c='test',Name='Afs test', CRMUserOwner__c=UserInfo.getUserId(), Community_App_Queue__c=q1.id, Afs_SendingPartner__c=acc.id, Unique_Id__c='0DB2h000000041c', Afs_ToDoStatusComplete__c='Canceled', Default_Phone_Country_Code__c='Chile (+56) - CL',Enable_Global_Link_Payment__c=false );
        insert afscmm;
        Application__c app = Util_Test.createApplication(con,null);
        Test.startTest();
        Insert app;
        Test.stopTest();
        Application__c app2 = [select Id,(select id from GL_Payments__r) from Application__c where Id =: app.Id];
        
        system.assertEquals(app2.GL_Payments__r.size(),0);
		/***/
        
    }
    
    public static testmethod void assignOwnerContactsTest(){
        User usr = Util_Test.createUser([SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id, null, null);
        insert usr;
        Account chapter = new Account(Specialist__c = usr.Id,
                                      Country__c = 'Argentina', Zip_From__c = 1233, Zip_To__c = 1239,
                                      name = 'Test Account',
                                      IOC_Code__c = 'ARG',
                                      RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Chapters').getRecordTypeId());
        insert chapter;
        Account sendPartner = new Account(name = 'Test Account',IOC_Code__c = 'ARG',RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Partner').getRecordTypeId());
        Insert sendPartner;
        Contact con = Util_Test.createContact('Test Contact',null);
        con.Country_of_Legal_Residence__c = 'Argentina';
        con.MailingPostalCode = '1234';
        con.Sending_Partner__c = sendPartner.Id;
        insert con;
        
        Test.startTest();
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        app.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        update app;
        Test.stopTest();
        
        system.assertEquals(usr.Id, [SELECT Id, OwnerId FROM Contact WHERE Id = :con.Id LIMIT 1].OwnerId);
    }
}