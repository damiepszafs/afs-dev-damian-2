global class CountryCodesPicklist extends VisualEditor.DynamicPickList{
    
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('United States + 1 - US', 'United States + 1 - US');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {
        List<ParentGuardianController.CustomSelectOptions> options = ParentGuardianController.getCountryCode();
        VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();


        for(ParentGuardianController.CustomSelectOptions option: options) {
            VisualEditor.DataRow row = new VisualEditor.DataRow(option.getLabel(), option.getValue());
            myValues.addRow(row);
        }
        return myValues;
    }
}
