@isTest
public class DailyAll_SchedulableTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testMethod void Test1() { 
        Test.startTest();
        DailyAll_Schedulable reminder = new DailyAll_Schedulable();
        String jobID = System.schedule('Remind Opp Owners', CRON_EXP, reminder);
        Test.stopTest();
    }
}