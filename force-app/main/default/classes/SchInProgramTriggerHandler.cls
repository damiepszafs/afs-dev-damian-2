public class SchInProgramTriggerHandler {
    
    public static void whenSchInProgramOfferIsDeleted(List<Sch_in_Program__c> oldSchInProgramList){
        system.debug('SchInProgramTriggerHandler.whenSchInProgramOfferIsDeleted');
        try {
            
            Set<Id> scholarshipRelatedId = new Set<Id>();
            List<Id> listApplicationSelect = new List<Id>();
            for(Sch_in_Program__c objSchPro : oldSchInProgramList){
                scholarshipRelatedId.add(objSchPro.Scholarship__c);
            }
            
            List<Scholarship__c> newScholarshipList = [SELECT Id,(SELECT Id,Application__c FROM Scholarship_Applications__r) FROM Scholarship__c WHERE Id IN: scholarshipRelatedId];
            
            for(Scholarship__c objSch : newScholarshipList){
                for(Scholarship_Application__c objSchApp : objSch.Scholarship_Applications__r){
                    listApplicationSelect.add(objSchApp.Application__c);
                }
            }
            
            ApplicationTriggerHandler.sumPreApplicationFeeByListApp(listApplicationSelect);
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
    
}