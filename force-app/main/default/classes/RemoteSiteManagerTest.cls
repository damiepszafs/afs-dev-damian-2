@IsTest
private class RemoteSiteManagerTest {
    @testSetup static void testSetup(){
        /*GlobalAPI__c ga = Util_Test.createGlobalAPI('TestAdm','TestEnd','TestSC','2019');
        Insert ga;*/
        Account acc = Util_Test.create_AFS_Partner();
        Contact con = Util_Test.createContact('Test', acc);
        insert con;
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po =  Util_Test.create_Program_Offer(acc, hp);
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.create_Applicant(con, po);
        Attachment att = new Attachment(Name = 'Test', ParentId = acc.Id);
        att.ContentType = 'Test';
        att.Body = Blob.valueof('Test');
        insert att;
    }
    
    @IsTest static void initializeTest(){
        RemoteSiteManager remote = new RemoteSiteManager();
    }
    @IsTest static void sendAttachmentToGlobalLinkTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        RemoteSiteManager.sendApplicationToGlobalLinkFuture(app.Id);
    }
    @IsTest static void sendPersonToGlobalLinkFutureTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        RemoteSiteManager.sendPersonToGlobalLinkFuture(con.Id);
    }
    @IsTest static void createMatrixTest(){
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                                                                                '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                                                                                '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                                                                                '"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                                                                                '"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                                                                                '"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                                                                                '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                                                                                '"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                                                                                '"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                                                                                '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[{"BV_RECORD_LOCATOR": "Test", "BV_AIRLINE": "Test","BV_FLIGHT_NUMBER": "Test","BV_ARRIVAL_AIRPORT": "Test","BV_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","BV_GATEWAY_CITY": "Test","BV_ARRIVE_IN_PRIOR_TO_THE_START": "Test","BV_ADDITIONAL_COMMENTS": "Test","BV_CONTACT_FIRST_NAME": "Test","BV_CONTACT_LAST_NAME": "Test","BV_RECORD_LOCATOR": "Test","BV_CONTACT_PHONE": "Test","BV_CONTACT_PHONE_TYPE": "Test",'+
                                                                                '"BV_CONTACT_RELATIONSHIP": "Test","CBH_RECORD_LOCATOR": "Test","CBH_AIRLINE": "Test","CBH_FLIGHT_NUMBER": "Test","CBH_DEPARTURE_AIRPORT": "Test","CBH_ARRIVAL_AIRPORT": "Test","CBH_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO": "Test","CBH_GATEWAY_CITY": "Test","CBH_ARRIVE_IN_PRIOR_TO_THE_START": "Test","CBH_ADDITIONAL_COMMENTS": "Test","BV_RECORD_LOCATOR": "Test","CBH_CONTACT_FIRST_NAME": "Test","CBH_CONTACT_LAST_NAME": "Test",'+
                                                                                 '"CBH_CONTACT_PHONE": "Test","CBH_CONTACT_PHONE_TYPE": "Test","CBH_CONTACT_RELATIONSHIP": "Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        
        RemoteSiteManager remote = new RemoteSiteManager();
        
        remote.createMatrix('pageIndex', 'pageSize', 'programYear');
        Test.stopTest();
    }
    @IsTest static void processMatrixTest(){
        RemoteSiteManager remote = new RemoteSiteManager();
        remote.remoteSiteModel = new RemoteSiteModel();
        remote.remoteSiteModel.matrix = new List<RemoteSiteModel.Matrix>();
        RemoteSiteModel.Matrix mat = New remoteSiteModel.matrix();
        mat.send_ioc = 'Test';
        mat.program_code = 'Test';
        mat.host_ioc = 'Test';
        mat.host_planned_number = 'Test';
        mat.send_projected_actual = 'Test';
        remote.remoteSiteModel.matrix.add(mat);
        Account acc = [SELECT Id FROM Account LIMIT 1];
        remote.mapAccountsByIOCCode.put('Test', acc);
        
        remote.processMatrix();
    }
    @IsTest static void processHostingFactSheetsTest(){
        RemoteSiteManager remote = new RemoteSiteManager();
        remote.remoteSiteModel = new RemoteSiteModel();
        remote.remoteSiteModel.hostingFactSheets = new List<RemoteSiteModel.HostingFactSheets>();
        RemoteSiteModel.HostingFactSheets hos = New remoteSiteModel.HostingFactSheets();
        hos.host_ioc = 'Test';
        hos.program_code = 'Test';
        hos.program_title = 'Test';
        hos.program_duration = 'Test';
        hos.program_cycle = 'Test';
        hos.program_content = 'Test';
        hos.program_type = 'Test';
        hos.program_description = 'Test';
        hos.program_language = 'Test';
        hos.program_year = 'Test';
        hos.age_range_year_start = 'Test';
        hos.age_range_year_end = 'Test';
        hos.age_range_month_start = 'Test';
        hos.age_range_month_end = 'Test';
        hos.app_received_start = '11111111';
        hos.app_received_end = 'Test';
        hos.from_departure_date = 'Test';
        hos.to_departure_date = 'Test';
        hos.from_arrival_date = 'Test';
        hos.to_arrival_date = 'Test';
        hos.from_arrival_date2 = 'Test';
        hos.to_arrival_date2 = 'Test';
        hos.from_arrival_date3 = 'Test';
        hos.from_departure_date3 = 'Test';
        hos.graduate_accept = 'Test';
        remote.remoteSiteModel.hostingFactSheets.add(hos);
        hos.program_language = 'Swiss German';
        remote.remoteSiteModel.hostingFactSheets.add(hos);
        Account acc = [SELECT Id FROM Account LIMIT 1];
        remote.mapAccountsByIOCCode.put('Test', acc);
        
        remote.processHostingFactSheets();
    }
    @IsTest static void tryToDeserializeResponseTest(){
        RemoteSiteManager remote = new RemoteSiteManager();
        remote.tryToDeserializeResponse();
        
        remote.httpResponseBody = '{"response":[{"statusCode":"200","pagesTotal":"1300","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}]}';
        remote.tryToDeserializeResponse();
    }
    @IsTest static void createHostingFactSheetsTest(){
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                                                                                '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                                                                                '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                                                                                '"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                                                                                '"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                                                                                '"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                                                                                '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                                                                                '"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                                                                                '"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                                                                                '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[{"BV_RECORD_LOCATOR": "Test", "BV_AIRLINE": "Test","BV_FLIGHT_NUMBER": "Test","BV_ARRIVAL_AIRPORT": "Test","BV_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","BV_GATEWAY_CITY": "Test","BV_ARRIVE_IN_PRIOR_TO_THE_START": "Test","BV_ADDITIONAL_COMMENTS": "Test","BV_CONTACT_FIRST_NAME": "Test","BV_CONTACT_LAST_NAME": "Test","BV_RECORD_LOCATOR": "Test","BV_CONTACT_PHONE": "Test","BV_CONTACT_PHONE_TYPE": "Test",'+
                                                                                '"BV_CONTACT_RELATIONSHIP": "Test","CBH_RECORD_LOCATOR": "Test","CBH_AIRLINE": "Test","CBH_FLIGHT_NUMBER": "Test","CBH_DEPARTURE_AIRPORT": "Test","CBH_ARRIVAL_AIRPORT": "Test","CBH_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO": "Test","CBH_GATEWAY_CITY": "Test","CBH_ARRIVE_IN_PRIOR_TO_THE_START": "Test","CBH_ADDITIONAL_COMMENTS": "Test","BV_RECORD_LOCATOR": "Test","CBH_CONTACT_FIRST_NAME": "Test","CBH_CONTACT_LAST_NAME": "Test",'+
                                                                                 '"CBH_CONTACT_PHONE": "Test","CBH_CONTACT_PHONE_TYPE": "Test","CBH_CONTACT_RELATIONSHIP": "Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        
        remote.createHostingFactSheets('Test', 'Test', 'Test');
        Test.stopTest();
    }
    @IsTest static void sendAttachmentToGlobalLink(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        Attachment att = [SELECT Id FROM Attachment LIMIT 1];
        remote.sendAttachmentToGlobalLink(att.Id);
    }
    @IsTest static void createAfs_ChaptersTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                                                                                '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                                                                                '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                                                                                '"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                                                                                '"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                                                                                '"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                                                                                '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                                                                                '"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                                                                                '"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                                                                                '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[{"BV_RECORD_LOCATOR": "Test", "BV_AIRLINE": "Test","BV_FLIGHT_NUMBER": "Test","BV_ARRIVAL_AIRPORT": "Test","BV_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","BV_GATEWAY_CITY": "Test","BV_ARRIVE_IN_PRIOR_TO_THE_START": "Test","BV_ADDITIONAL_COMMENTS": "Test","BV_CONTACT_FIRST_NAME": "Test","BV_CONTACT_LAST_NAME": "Test","BV_RECORD_LOCATOR": "Test","BV_CONTACT_PHONE": "Test","BV_CONTACT_PHONE_TYPE": "Test",'+
                                                                                '"BV_CONTACT_RELATIONSHIP": "Test","CBH_RECORD_LOCATOR": "Test","CBH_AIRLINE": "Test","CBH_FLIGHT_NUMBER": "Test","CBH_DEPARTURE_AIRPORT": "Test","CBH_ARRIVAL_AIRPORT": "Test","CBH_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO": "Test","CBH_GATEWAY_CITY": "Test","CBH_ARRIVE_IN_PRIOR_TO_THE_START": "Test","CBH_ADDITIONAL_COMMENTS": "Test","BV_RECORD_LOCATOR": "Test","CBH_CONTACT_FIRST_NAME": "Test","CBH_CONTACT_LAST_NAME": "Test",'+
                                                                                 '"CBH_CONTACT_PHONE": "Test","CBH_CONTACT_PHONE_TYPE": "Test","CBH_CONTACT_RELATIONSHIP": "Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        remote.remoteSiteModel = new RemoteSiteModel();
        
        remote.createAfs_Chapters('pageIndex', 'pageSize');
    }
    @IsTest static void createMTPDatesTest(){
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                                                                                '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                                                                                '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                                                                                '"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                                                                                '"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                                                                                '"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                                                                                '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                                                                                '"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                                                                                '"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                                                                                '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[{"BV_RECORD_LOCATOR": "Test", "BV_AIRLINE": "Test","BV_FLIGHT_NUMBER": "Test","BV_ARRIVAL_AIRPORT": "Test","BV_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","BV_GATEWAY_CITY": "Test","BV_ARRIVE_IN_PRIOR_TO_THE_START": "Test","BV_ADDITIONAL_COMMENTS": "Test","BV_CONTACT_FIRST_NAME": "Test","BV_CONTACT_LAST_NAME": "Test","BV_RECORD_LOCATOR": "Test","BV_CONTACT_PHONE": "Test","BV_CONTACT_PHONE_TYPE": "Test",'+
                                                                                '"BV_CONTACT_RELATIONSHIP": "Test","CBH_RECORD_LOCATOR": "Test","CBH_AIRLINE": "Test","CBH_FLIGHT_NUMBER": "Test","CBH_DEPARTURE_AIRPORT": "Test","CBH_ARRIVAL_AIRPORT": "Test","CBH_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO": "Test","CBH_GATEWAY_CITY": "Test","CBH_ARRIVE_IN_PRIOR_TO_THE_START": "Test","CBH_ADDITIONAL_COMMENTS": "Test","BV_RECORD_LOCATOR": "Test","CBH_CONTACT_FIRST_NAME": "Test","CBH_CONTACT_LAST_NAME": "Test",'+
                                                                                 '"CBH_CONTACT_PHONE": "Test","CBH_CONTACT_PHONE_TYPE": "Test","CBH_CONTACT_RELATIONSHIP": "Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        
        remote.createMTPDates('pageIndex', 'pageSize');
        remote.processMTPDates();
        Test.stopTest();
    }
    @IsTest static void createZipcodeAssignmentsTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pagesTotal":"2","itemsTotal":"64980","pageIndex":"1","pageSize":"50"}],'+
                                                                                '"Matrix":[{"send_ioc":"Test","program_code":"Test","host_ioc":"Test","host_planned_number":"Test","send_projected_actual":"Test"}],'+
                                                                                '"HostingFactSheets":[{"host_ioc":"Test","program_code":"Test","program_title":"Test","program_duration":"Test","program_cycle":"Test","program_content":"Test","program_type":"Test","program_description":"Test",'+
                                                                                '"program_language":"Test","program_year":"Test","age_range_year_start":"Test","age_range_year_end":"Test","age_range_month_start":"Test","age_range_month_end":"Test",'+
                                                                                '"app_received_start":"Test","app_received_end":"Test","from_departure_date":"Test","to_departure_date":"Test","from_arrival_date":"Test","to_arrival_date":"Test",'+
                                                                                '"from_arrival_date2":"Test","to_arrival_date2":"Test","from_arrival_date3":"Test","from_departure_date3":"Test","graduate_accept":"Test"}],'+
                                                                                '"Organization":[{"sf_org_id":"Test","id":"Test","owner_ioc":"Test","ioc_code":"Test","organization_name":"Test","native_name":"Test","organization_ref":"Test","org_type":"Test","org_sub_type":"Test","org_status":"Test","chapter_id":"Test",'+
                                                                                '"area_team_id":"Test","region_id":"Test","web_site":"Test","english_address1":"Test","english_address2":"Test","english_city":"Test","english_state":"Test",'+
                                                                                '"english_zip":"Test","native_address1":"Test","native_address2":"Test","native_city":"Test","native_state":"Test","native_zip":"Test","country":"Test","telnum1":"Test","faxnum":"Test"}],'+
                                                                                '"MTPDates":[], "ZipcodeAssignments":[], "ServiceAndOA":[{"BV_RECORD_LOCATOR": "Test", "BV_AIRLINE": "Test","BV_FLIGHT_NUMBER": "Test","BV_ARRIVAL_AIRPORT": "Test","BV_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","BV_GATEWAY_CITY": "Test","BV_ARRIVE_IN_PRIOR_TO_THE_START": "Test","BV_ADDITIONAL_COMMENTS": "Test","BV_CONTACT_FIRST_NAME": "Test","BV_CONTACT_LAST_NAME": "Test","BV_RECORD_LOCATOR": "Test","BV_CONTACT_PHONE": "Test","BV_CONTACT_PHONE_TYPE": "Test",'+
                                                                                '"BV_CONTACT_RELATIONSHIP": "Test","CBH_RECORD_LOCATOR": "Test","CBH_AIRLINE": "Test","CBH_FLIGHT_NUMBER": "Test","CBH_DEPARTURE_AIRPORT": "Test","CBH_ARRIVAL_AIRPORT": "Test","CBH_DOMESTIC_TRAVEL_ARRANGEMENTS": "Test","CBH_ARRIVAL_TO_GATEWAY_CITY_WITH_WHO": "Test","CBH_GATEWAY_CITY": "Test","CBH_ARRIVE_IN_PRIOR_TO_THE_START": "Test","CBH_ADDITIONAL_COMMENTS": "Test","BV_RECORD_LOCATOR": "Test","CBH_CONTACT_FIRST_NAME": "Test","CBH_CONTACT_LAST_NAME": "Test",'+
                                                                                 '"CBH_CONTACT_PHONE": "Test","CBH_CONTACT_PHONE_TYPE": "Test","CBH_CONTACT_RELATIONSHIP": "Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        
        remote.createZipcodeAssignments('iocCode');
    }
    @IsTest static void createServiceAndOATest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        Application__c app = Util_Test.createApplication(null,null); 
        app.Get_Travel_Data_From_Global_Link__c = true;
        Insert app; 
        List<Application__c> lstApps = new List<Application__c>();
        lstApps.add(app);
        
        Test.startTest();            
 		remote.createServiceAndOA(lstApps);
		Test.stopTest();        
		
        app = [SELECT Get_Travel_Data_From_Global_Link__c FROM Application__c WHERE Id = :app.Id];
        System.assert(!app.Get_Travel_Data_From_Global_Link__c);  
    }
    
    @IsTest static void createServiceGLpaymentsTest(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","usa_payment_status":{"application_fee": "Paid","program_deposit": "Paid"},"BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        RemoteSiteManager remote = new RemoteSiteManager();
        glPaymentTypes__c paytype = new glPaymentTypes__c(Name='App Fee',GL_payment_Record__c='App Fee',GL_payment_service__c='ApplicationFee',GL_payment_Integration__c='application_fee');
        insert paytype;
        Application__c app = Util_Test.createApplication(null,null); 
        app.GL_Service_Id__c = '0429B6AD-2976-4A1B-934C-00001A30FE5F';
        Insert app; 
        GL_Payment__c glappfee = new GL_Payment__c(Name='App Fee',Application__c=app.id, Payment_Type__c='App Fee',Payment_Status__c='Pending',GL_Service_ID__c='0429B6AD-2976-4A1B-934C-00001A30FE5F');
        insert glappfee;
        GL_Payment__c glappfee45 = new GL_Payment__c(Name='Deposit',Application__c=app.id, Payment_Type__c='Deposit',Payment_Status__c='Pending',GL_Service_ID__c='0429B6AD-2976-4A1B-934C-00001A30FE5F');
        insert glappfee45;
        List<Application__c> lstApps = new List<Application__c>();
        Application__c app2 = [SELECT Id,GL_Service_Id__c,(SELECT Id,Name,Payment_Type__c,Payment_Status__c FROM GL_Payments__r) FROM  Application__c WHERE Id =: app.id LIMIT 1];
        lstApps.add(app2);
        
        Test.startTest();            
 		List<Application__c> lstApps2 = remote.createServiceGLpayments(lstApps);
		Test.stopTest();        
        GL_Payment__c glappfee2 = [select id,Payment_Status__c from GL_Payment__c where Payment_Type__c='App Fee' and Application__c=:lstApps2[0].id limit 1];
        system.assertEquals(glappfee2.Payment_Status__c, 'Paid');
    }
    
    @isTest static void postEventsLogs_Test(){
        GlobalAPI__c ga = Util_Test.createGlobalAPI('TestAdm','TestEnd','TestSC','2019');
        Insert ga;
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Application__c app = [SELECT ID, Applicant__c From Application__c Limit 1];

        Test.startTest();
        app.Status__c = 'Participation Desire';
        Update app;
        Test.stopTest();
        
        IntegrationLog__c log  = [SELECT Status__c, Trigger__c FROM IntegrationLog__c where Trigger__c = 'Participation Desire'];
        System.assert(log.Status__c == 'Processed');
    }
}