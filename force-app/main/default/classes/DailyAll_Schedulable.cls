global class DailyAll_Schedulable implements Schedulable {
    global void execute(SchedulableContext ctx){
        glPaymentes_Batch glBatch = new glPaymentes_Batch();
        Id batchId = Database.executeBatch(glBatch,1);
    }
    
}