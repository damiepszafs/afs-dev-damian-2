@isTest
public class IntegrationLogTriggerHandler_Test {
    
    @TestSetup
    public static void test_setup(){
        GlobalAPI__c ga = Util_Test.createGlobalAPI('TestAdm','TestEnd','TestSC','2019');
        Insert ga;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true); 
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
    }
    
    
    @isTest public static void processIntegration_Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"status":"Ok"}');
        Test.setMock(HttpCalloutMock.class,fakeResponse); 
        Program_Offer__c po = [SELECT Id FROM Program_Offer__c LIMIT 1];
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        
        Test.startTest();
        Application__c app = Util_Test.createApplication(con,po);
        Insert app;
        Test.stopTest();
        
        IntegrationLog__c log = [SELECT Status__c FROM IntegrationLog__c LIMIT 1];
        
        System.Assert(log.Status__c == 'Processed');
    }
    
}