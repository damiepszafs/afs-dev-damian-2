@isTest(SeeAllData=false)
global class Afs_AboutYouControllerTest {
    @testSetup
    public static void setup_test(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger', false, false);
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        contact contactParent = Util_Test.create_Conact(acc, 'Applicant','Test contact');
        contact relationContact =  Util_Test.create_Conact(acc, 'General','Test Relation');
        npe4__Relationship__c relation = Util_Test.create_RelationRec(contactParent , relationContact);
        application__c application = Util_Test.create_Applicant( contactParent,po);
    }
    
	@isTest
    static void getPicklistValuesTest() {  
        Test.startTest();
        	system.assertEquals(true,Afs_AboutYouController.getPicklistValues('Contact','Country_of_Legal_Residence__c,NationalityCitizenship__c').get('NationalityCitizenship__c').Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void doSubmittApplicationTest() {  
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
             system.assertEquals('Processed',Afs_AboutYouController.doSubmittApplication([SELECT ID FROM Application__c LIMIT 1],'AboutYou'));
        Test.stopTest();
    }
    
    @isTest
    static void doSubmittApplicationException() {  
        Test.startTest();
            try{
                Afs_AboutYouController.doSubmittApplication(null,'AboutYou');
            }catch(exception e){
                system.assertEquals(true,e != null);
            }
                
        Test.stopTest();
    }
    
    @isTest public static void getHasFlagshipPrograms(){
        Application__c app = [SELECT Program_Offer__c, Applicant__c FROM Application__c Limit 1];
        Program_Offer_in_App__c poa = Util_Test.createProgramOfferInApp(app.Id,app.Applicant__c,app.Program_Offer__c);
        Insert poa;
        Test.startTest();
        Boolean result = Afs_AboutYouController.getHasFlagshipPrograms(app.Applicant__c, app.Id);
        Test.stopTest();
        System.assert(result);
    }
}