global class glPaymentes_Batch implements Database.Batchable<SObject>,Database.AllowsCallouts,Database.Stateful{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id,GL_Service_Id__c,(SELECT Id,Name,Payment_Type__c,Payment_Status__c FROM GL_Payments__r) FROM  Application__c where Id IN (select Application__c from GL_Payment__c where Payment_Status__c = \'Pending\')');
    }
    
    global void execute(Database.BatchableContext bc, List<Application__c> applicationList) {
        
        RemoteSiteManager createdServiceGL = new RemoteSiteManager();
        List<Application__c> newAppList = createdServiceGL.createServiceGLpayments(applicationList);
        
        if(newAppList.size() > 0) GLpaymentsHandler.verifyToDoItemPaymentsMethod((new Map<Id,Application__c>(newAppList)).keySet());
        
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
}