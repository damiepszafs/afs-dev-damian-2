@isTest
public class Afs_SearchProgramControllerTest {
    
    @isTest
    static void getselectOptionsTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        list<Program_Offer__c> prList = new list<Program_Offer__c>();
        date dt = Date.today().addDays(40);
        date dt1 = Date.today().addDays(-40);
        for(Program_Offer__c prObj : [Select id,Applications_Received_To_local__c,Applications_Received_From_local__c, Length__c from Program_Offer__c]){
            prObj.Applications_Received_To_local__c = dt;
            prObj.Applications_Received_From_local__c = dt1;
            prObj.Length__c = '3 weeks';
            prList.add(prObj);
        }
        prList[1].Length__c= '1 month';
        prList[2].Length__c= '2 weeks';
        

        update prList;
        system.assertEquals(true,Afs_SearchProgramController.getselectOptions().get('Destinations__c').Size() > 0);
        Test.stopTest();
    }
    
    @isTest
    static void durationWrapperCompareToTest(){
        Afs_SearchProgramController.DurationWrapper d1 = new Afs_SearchProgramController.DurationWrapper('1 Day');
        Afs_SearchProgramController.DurationWrapper d20 = new Afs_SearchProgramController.DurationWrapper('20 Days');
        Afs_SearchProgramController.DurationWrapper w1 = new Afs_SearchProgramController.DurationWrapper('1 Week');
        Afs_SearchProgramController.DurationWrapper w20 = new Afs_SearchProgramController.DurationWrapper('20 Weeks');
        Afs_SearchProgramController.DurationWrapper m1 = new Afs_SearchProgramController.DurationWrapper('1 Month');
        Afs_SearchProgramController.DurationWrapper m20 = new Afs_SearchProgramController.DurationWrapper('20 Months');
        Afs_SearchProgramController.DurationWrapper ot = new Afs_SearchProgramController.DurationWrapper('Other');
        
        System.assert(d20.compareTo(d1) == 1);
        System.assert(d20.compareTo(m1) == -1);
        System.assert(w20.compareTo(w1) == 1);
        System.assert(w20.compareTo(d1) == 1);
        System.assert(w20.compareTo(m1) == -1);
        System.assert(m1.compareTo(w1) == 1);
        System.assert(m1.compareTo(m20) == -1);
		System.assert(ot.compareTo(d1) == 1);
    }

    @isTest
    static void orderDurationToTest(){
        Set<String> setDuration = new Set<String>{'1 Month','20 Days','1 Week'};
        List<String> lstDuration =  new List<String>(Afs_SearchProgramController.orderDuration(setDuration)); 
        
		System.assert(lstDuration[0] == '20 Days');
        System.assert(lstDuration[1] == '1 Week');
        System.assert(lstDuration[2] == '1 Month');
    }
}