//Class handler for Integration Log Trigger
public class IntegrationLogTriggerHandler {
    //Method contains logic for after insert event
    public static void afterInsert(Map<Id,IntegrationLog__c> mapNew){
        processIntegration(mapNew);
    }
    
    public static void processIntegration(Map<Id,IntegrationLog__c> mapNew){        
        RemoteSiteManager rsm = new RemoteSiteManager(false);
        //Validate if integrations is enabled
        if(rsm.integrationCustomSettings != null && !rsm.integrationCustomSettings.Enabled_Integration__c){
            return;
        }
        Set<String> setTrigger = new Set<String>{'Interest','Participation Desire','Decision','Manual'};
        Map<Id,IntegrationLog__c> mapLogs = new Map<Id,IntegrationLog__c>();
        Map<Id,Contact> mapContacts = new Map<Id,Contact>();
        Map<Id,Application__c> mapApplications = new Map<Id,Application__c>();
        Map<Id,List<npe4__Relationship__c>> mapRelationships = new Map<Id,List<npe4__Relationship__c>>();
        Map<Id,Contact> mapFamilyMember = new Map<Id,Contact>();
        
        //Filter
        for(IntegrationLog__c log : mapNew.values()){           
            if(setTrigger.contains(log.Trigger__c) && log.Status__c != null && log.System__c == 'Global Link' && log.Contact__c != null && log.Application__c != null){
                mapLogs.put(log.Id,log);
                mapContacts.put(log.Contact__c,new Contact(id=log.Contact__c));
                mapApplications.put(log.Application__c, new Application__c(id=log.Application__c));
            }
        }
        
        //Get Contacts
        if(!mapContacts.isEmpty()){
            List<String> lstFieldsRelat = new List<String>(Schema.getGlobalDescribe().get('npe4__Relationship__c').getDescribe().fields.getMap().keySet());
            String subQuery = '(SELECT ' + String.join(lstFieldsRelat,',') + ' FROM npe4__Relationships__r)';
            List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
            Set<Id> contactsId = mapContacts.keyset();
            String query = 'SELECT ' + String.join(lstFields,',') + ', Account.Global_Link_Family_ID__c, '+ subQuery +' FROM Contact WHERE Id in :contactsId';
            for(Contact con : Database.Query(query)){
                mapContacts.put(con.Id,con);
                if(!con.npe4__Relationships__r.isEmpty()){
                    mapRelationships.put(con.Id,con.npe4__Relationships__r);
                    for(npe4__Relationship__c rel : con.npe4__Relationships__r){
                        mapFamilyMember.put(rel.npe4__RelatedContact__c,null);
                    } 
                }                
            }
            //If have Family Member retrieve the contactcs
            if(!mapFamilyMember.isEmpty()){
                Set<Id> setId = mapFamilyMember.keySet();
                query = 'SELECT ' + String.join(lstFields,',') + ', Account.Global_Link_Family_ID__c FROM Contact WHERE Id in :setId';
                for(Contact con : Database.Query(query)){
                    mapFamilyMember.put(con.Id,con);
                }
            }

        }
        
        //Get Applications
        if(!mapApplications.isEmpty()){
            List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('Application__c').getDescribe().fields.getMap().keySet());
            Set<Id> appsId = mapApplications.keyset();
            String query = 'SELECT ' + String.join(lstFields,',') + ', Sending_Partner__r.IOC_Code__c FROM Application__c WHERE Id in :appsId';
            List<Application__c> lstApps = Database.Query(query);
            mapApplications = new Map<Id,Application__c>(lstApps);
        }
        
        //Process event Interests
        if(!mapLogs.isEmpty()){
        	rsm.postEventsLogs(mapLogs,mapContacts,mapApplications,mapRelationships,mapFamilyMember);    
        }

		return;        
    }
}