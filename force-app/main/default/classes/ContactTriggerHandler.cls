public class ContactTriggerHandler {
    //Method contains logic for afterUpdate Event
    public static void afterUpdate(Map<Id,Contact> mapOld,Map<Id,Contact> mapNew){
    	calculateScore(mapNew.values(),mapOld);
    }

	//Method contains logic for beforeInsert Event
    public static void beforeInsert(List<Contact> lstNew){
		generateNewUUID(lstNew);
        assignChapter(lstNew, null);
    }  
    
    //Method contains logic for beforeUpdate Event
    public static void beforeUpdate(Map<Id,Contact> mapOld,Map<Id,Contact> mapNew){
        assignChapter(mapNew.values(), mapOld);
    } 
   
    public static void calculateScore(List<Contact> newList, Map<Id,Contact> mapOld){
        List<Contact> lstConToProcess = new List<Contact>();
        Map<Id,List<Application__c>> mapListApplicationByConId = new Map<Id,List<Application__c>>();
        List<Scoring_Settings__c> lstContactScoringSettings = ScoringHandler.getListScoringSettingByObject('Contact');     
        
        if(!lstContactScoringSettings.isEmpty()){
        for(Contact con : newList){
            Contact oldCon = mapOld.get(con.Id);
               if(ScoringHandler.anyFieldChange(con,oldCon,'Contact')){
                       lstConToProcess.add(con);
                       mapListApplicationByConId.put(con.Id,new List<Application__c>());
               }
            }
            
            if(lstConToProcess.Size() > 0){
                
                List<Application__c> lstApplicationsToUpdate = new List<Application__c>();
                List<String> lstApplicationFields = ScoringHandler.getFieldsOnScoringSettingByObjectName('Application__c');
                List<Scoring_Settings__c> lstApplicationScoringSettings = ScoringHandler.getListScoringSettingByObject('Application__c'); 
                
                if(!lstApplicationFields.isEmpty()){
                    Set<Id> setContactIds = mapListApplicationByConId.keySet();
					String queryApplications = 'SELECT Applicant__c, ' + String.join(lstApplicationFields,',') + ' FROM Application__c WHERE Applicant__c in :setContactIds';
                    for(Application__c app : DataBase.query(queryApplications)){
                        mapListApplicationByConId.get(app.Applicant__c).add(app);
                        lstApplicationsToUpdate.add(app);
                    }
                }
                
                //Recalculte the Score
                for(Contact con : lstConToProcess){
                    if(!mapListApplicationByConId.get(con.Id).isEmpty()){
                        //Recalculate the score from Con Record
                        Decimal contactScore = ScoringHandler.recalculateScore(con, lstContactScoringSettings,100,'Contact',null);
                        //Add score from the Application record
                        for(Application__c app : mapListApplicationByConId.get(con.Id)){
                            app.Score__c = ScoringHandler.recalculateScore(app, lstApplicationScoringSettings,contactScore,'Application__c',null);
                        }
                    }
                }
                
                if(!lstApplicationsToUpdate.isEmpty()){
                    Database.Update(lstApplicationsToUpdate);
                }
            }
        }
    }
    
    public static void generateNewUUID(List<Contact> lstNew){
        for(Contact con : lstNew){
            if(con.Global_Link_Person_ID__c == null){
                con.Global_Link_Person_ID__c = RemoteSiteParser.generateNewUUID();    
            }
        }
    }
    
    public static void assignChapter(List<Contact> newList, Map<Id,Contact> mapOld){
        List<Contact> contacts = new List<Contact>();
        Set<String> iocCodes = new Set<String>();
        Map<String,List<Account>> mapChaptersByIocCode = new Map<String,List<Account>>();
        
        for(Contact c:newList){
            if(c.MailingPostalCode != null && c.Sending_Partner__c !=null && (mapOld ==  null || mapOld.get(c.Id).Sending_Partner__c != c.Sending_Partner__c ||  mapOld.get(c.Id).MailingPostalCode != c.MailingPostalCode)){
                iocCodes.add(c.IOC_Code_Sending_Partner__c);
                contacts.add(c);
            }
        }
        
        if(!iocCodes.isEmpty()){            
            for(Account chap : [SELECT Id, Zip_From__c, Zip_To__c, IOC_Code__c FROM Account WHERE recordtype.DeveloperName = 'AFS_Chapters' AND IOC_Code__c IN :iocCodes ORDER BY Zip_From__c]){
                if(!mapChaptersByIocCode.containsKey(chap.IOC_Code__c)){
                    mapChaptersByIocCode.put(chap.IOC_Code__c,new List<Account>());
                }
                mapChaptersByIocCode.get(chap.IOC_Code__c).add(chap);
            }
            
            for(Contact c :contacts){
                try{
                    Decimal zipCode = Decimal.valueOf(c.MailingPostalCode);
                    for(Account chap : mapChaptersByIocCode.get(c.IOC_Code_Sending_Partner__c)){
                        if(zipCode >= chap.Zip_From__c && zipCode <= chap.Zip_To__c){
                            c.Chapter__c = chap.Id;
                            System.debug(c.LastName + ' ' + c.Chapter__c);
                            break;                    
                        }                        
                    }
                }catch(Exception e){
                    system.debug(e.getMessage() +' '+ c);
                }
            }      
        }        
    }
}