public class PaymentInstallemntUpload_Queueable implements Queueable{
    public PaymentInstallemntUpload_Queueable() {

    }
    public void execute(QueueableContext context){
        date filterDate = date.today() + 7;
        List<Opportunity> oppList = new List<Opportunity>();
        for(Payment_Installment__c pins : [SELECT Id, Number__c, Balance__c, Due_Date__c, Amount__c, Application__r.Opportunity__c FROM Payment_Installment__c 
                                           WHERE Balance__c > 0 and Due_Date__c <= :filterDate and due_date__c >= today]){
            Opportunity opp = new Opportunity(Id = pins.Application__r.Opportunity__c, 
                                              Due_Date_Payment_Installment__c = Date.today(),                                                            
                                              Number_of_Payment_Installment__c = pins.Number__c,
                                              Ammount_of_Payment_Installment__c = pins.Balance__c);               
                oppList.add(opp);
        }
        List<Database.SaveResult> lstSR = database.update(oppList, false);
        for(Integer i = 0;i < lstSR.Size(); i++){                
            Database.SaveResult sr = lstSR[i];
            if(!sr.isSuccess()){
                system.debug(logginglevel.error,oppList[i] + ' --> ' + String.join(sr.getErrors(),' / '));
            }
        } 
    }
}