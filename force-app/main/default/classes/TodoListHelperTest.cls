@IsTest
public class TodoListHelperTest {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }    
    
    public static testmethod void deleteOldToDosTest(){
        Set<Id> ids = new Set<Id>();
        Contact con = [SELECT Id From Contact Limit 1];
        /***/
        Test.startTest();
        integer toDoNumber = [SELECT COUNT() from To_Do_Item__c];
        TodoListHelper.deleteOldToDos(ids);
        System.assertEquals(toDoNumber, [SELECT COUNT() from To_Do_Item__c]);
        
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        To_Do_Item__c toDo = new To_Do_Item__c(Application__c = app.Id);
        insert toDo;
        ids.add(app.Id);
        
        toDoNumber = [SELECT COUNT() from To_Do_Item__c];
        TodoListHelper.deleteOldToDos(ids);
        System.assertEquals(toDoNumber - 1, [SELECT COUNT() from To_Do_Item__c]);
        Test.stopTest();
    }
    
    public static testmethod void deleteOldMasterToDos(){
        
    }
    
    public static testmethod void fakeTest(){
        TodoListHelper.fakeMethod();
    }
}