@isTest
public class RemoteSiteParserTest {
    @testSetup
    public static void setup_test(){
		Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        Afs_TestDataFactory.createProfilePhoto(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        Afs_TestDataFactory.createProfilePhoto(1);
    }
    @isTest static void ApplicationToParticipantAppTest() {
        Test.startTest(); 
        Application__c application = [select Id, Applicant__r.AAre_you_attending_University_College_o__c, Additional_destination_1__c, Additional_destination_2__c, Additional_destination_3__c, Applicant__c, Applicant__r.AccountId, Applicant__r.Account.Global_Link_Family_ID__c, Applicant__r.Account.IOC_Code__c, Applicant__r.Describe_yourself_as_a_student__c, 
                                            Applicant__r.Global_Link_Person_ID__c, Applicant__r.How_did_you_hear_about_AFS__c, Applicant__r.Keep_me_informed_Opt_in__c, Applicant__r.MailingPostalCode, Applicant__r.MobilePhone,
                                            Applicant__r.Parent_Legal_Guardian_Other__c, Applicant__r.Terms_Service_Agreement__c, Applicant__r.What_school_do_you_attend__c, Applicant__r.When_would_did_you_graduate__c,  Global_Link_Participant_App_ID__c, Applicant__r.Name_of_University_College_School__c,
                                            Applicant__r.Language_1__c,Applicant__r.Language_1_Proficiency__c,Applicant__r.Language_2__c,Applicant__r.Language_2_Proficiency__c,Applicant__r.Language_3__c,Applicant__r.Language_3_Proficiency__c,Applicant__r.Language_4__c,Applicant__r.Language_4_Proficiency__c,
                                            Applicant__r.Criminal_Convictions__c,Applicant__r.No_restrictions_or_allergies__c,Applicant__r.Limited_in_the_activities_I_can_do__c,Applicant__r.Have_allergies__c,Applicant__r.Take_medications__c,Applicant__r.Can_t_live_with_pets_s__c,Applicant__r.Can_t_live_with_a_smoker__c,
                                            Applicant__r.Medical_Condition_Comments__c,Applicant__r.No_dietary_restrictions__c,Applicant__r.Have_food_allergies__c,Applicant__r.Vegetarian__c,Applicant__r.Vegan__c,Applicant__r.Kosher__c,Applicant__r.Halal__c,Applicant__r.Celiac__c,Applicant__r.Dietary_Comments__c,
                                            Applicant__r.Willing_to_change_diet_during_program__c,Applicant__r.treated_for_any_health_issues_physical__c,Applicant__r.Health_Issue_Description__c,What_are_you_looking_for__c,What_do_you_want_out_of_this_experience__c,How_would_friends_family_describe_you__c,Tell_us_about_yourself__c,
                                            Program_you_participated__c,Applicant__r.Dual_citizenship__c,Applicant__r.SecondCitizenship__c,Applicant__r.Home_Mother__c,Applicant__r.Home_Father__c,Applicant__r.Home_Stepmother__c,Applicant__r.Home_Stepfather__c,Applicant__r.Home_Sister_Stepsister__c,Applicant__r.Home_Brother_Stepbrother__c,
                                            Applicant__r.Home_Sister_Stepsisters_Age__c,Applicant__r.Home_Brother_Stepbrothers_Age__c,Applicant__r.Home_Grandmother__c,Applicant__r.Home_Grandfather__c,Applicant__r.Home_Other__c,Applicant__r.Home_Other_Relationship__c,Applicant__r.Instagram__c,Applicant__r.LinkedIn__c,Applicant__r.Facebook__c,
                                            Applicant__r.Twitter__c,Applicant__r.Snapchat__c,Applicant__r.YouTube__c,Access_to_religious_services__c,Ability_to_live_with_household_pets__c,Open_to_Same_Sex_Host_Parents__c,Open_to_Single_Host_Parent__c,Open_to_Sharing_a_host_family__c,Applicant__r.LGBTQ_Member__c,Why_is_Global_Competence_important_for_s__c,
                                            How_to_be_a_Global_Citizen__c,Things_to_share__c,Describe_a_normal_day_in_your_life__c,What_you_bring_to_your_AFS_experience__c,Food_thoughts__c,Favorite_travel_destinations_books__c,Video_for_Host_Family__c,Applicant__r.Tshirt__c,BV_Record_Locator__c,BV_Airline__c,BV_Flight_Number__c,BV_Departure_Time__c,
                                            BV_Arrival_Time__c,BV_Departure_Airport__c,BV_Arrival_Airport__c,BV_Domestic_Travel_Arrangements__c,BV_Arrival_to_gateway_city_who_accompany__c,BV_Gateway_City__c,BV_Arrive_in_prior_to_the_start__c,BV_Additional_Comments__c,BV_Contact_First_Name__c,BV_Contact_Last_Name__c,BV_Contact_Phone__c,
                                            BV_Contact_Relationship__c,CBH_Record_Locator__c,CBH_Airline__c,CBH_Flight_Number__c,CBH_Departure_Time__c,CBH_Arrival_Time__c,CBH_Departure_Airport__c,CBH_Arrival_Airport__c,CBH_Domestic_Travel_Arrangements__c,CBH_Arrival_to_gateway_city_with_who__c,CBH_Gateway_City__c,
                                            CBH_Arrive_in_prior_to_the_start__c,CBH_Additional_Comments__c,CBH_Contact_First_Name__c,CBH_Contact_Last_Name__c,CBH_Contact_Phone__c,CBH_Contact_Relationship__c
                                            from Application__c LIMIT 1] ;
       /* application.Applicant__r.AAre_you_attending_University_College_o__c = 'Test';
        application.Additional_destination_1__c ='Test';
        application.Additional_destination_2__c ='Test';
        application.Additional_destination_3__c ='Test';
        application.Applicant__c ='Test';
        application.Applicant__r.AccountId ='Test';
        application.Applicant__r.Account.Global_Link_Family_ID__c ='Test';
        application.Applicant__r.Account.IOC_Code__c ='Test';
        application.Applicant__r.Describe_yourself_as_a_student__c ='Test';
        application.Applicant__r.Global_Link_Person_ID__c ='Test';
        application.Applicant__r.How_did_you_hear_about_AFS__c ='Test';
        application.Applicant__r.Keep_me_informed_Opt_in__c =false;
        application.Applicant__r.MailingPostalCode ='Test';
        application.Applicant__r.MobilePhone ='Test';
        application.Applicant__r.Parent_Legal_Guardian_Other__c  ='Test';
        application.Applicant__r.Terms_Service_Agreement__c =false;
        application.Applicant__r.What_school_do_you_attend__c ='Test';
        application.Applicant__r.When_would_did_you_graduate__c  =date.today();
        application.Global_Link_Participant_App_ID__c ='Test';
        application.Applicant__r.Name_of_University_College_School__c ='Test';
        application.Applicant__r.Language_1__c ='Test';
        application.Applicant__r.Language_1_Proficiency__c ='Test';
        application.Applicant__r.Language_2__c ='Test';
        application.Applicant__r.Language_2_Proficiency__c ='Test';
        application.Applicant__r.Language_3__c ='Test';
        application.Applicant__r.Language_3_Proficiency__c ='Test';
        application.Applicant__r.Language_4__c ='Test';
        application.Applicant__r.Language_4_Proficiency__c ='Test';
        application.Applicant__r.Criminal_Convictions__c ='Test';
        application.Applicant__r.No_restrictions_or_allergies__c =false;
        application.Applicant__r.Limited_in_the_activities_I_can_do__c =false;
        application.Applicant__r.Have_allergies__c =false;
        application.Applicant__r.Take_medications__c =false;
        application.Applicant__r.Can_t_live_with_pets_s__c =false;
        application.Applicant__r.Can_t_live_with_a_smoker__c =false;
        application.Applicant__r.Medical_Condition_Comments__c ='Test';
        application.Applicant__r.No_dietary_restrictions__c =false;
        application.Applicant__r.Have_food_allergies__c =false;
        application.Applicant__r.Vegetarian__c =false;
        application.Applicant__r.Vegan__c =false;
        application.Applicant__r.Kosher__c =false;
        application.Applicant__r.Halal__c =false;
        application.Applicant__r.Celiac__c =false;
        application.Applicant__r.Dietary_Comments__c ='Test';
        application.Applicant__r.Willing_to_change_diet_during_program__c ='Test';
        application.Applicant__r.treated_for_any_health_issues_physical__c ='Test';
        application.Applicant__r.Health_Issue_Description__c ='Test';
        application.What_are_you_looking_for__c ='Test';
        application.What_do_you_want_out_of_this_experience__c ='Test';
        application.How_would_friends_family_describe_you__c ='Test';
        application.Tell_us_about_yourself__c ='Test';
        application.Program_you_participated__c ='Test';
        application.Applicant__r.Dual_citizenship__c ='Test';
        application.Applicant__r.SecondCitizenship__c ='Test';
        application.Applicant__r.Home_Mother__c =1.5;
        application.Applicant__r.Home_Father__c =1.5;
        application.Applicant__r.Home_Stepmother__c =1.5;
        application.Applicant__r.Home_Stepfather__c =1.5;
        application.Applicant__r.Home_Sister_Stepsister__c =1.5;
        application.Applicant__r.Home_Brother_Stepbrother__c =1.5;
        application.Applicant__r.Home_Sister_Stepsisters_Age__c ='1.5';
        application.Applicant__r.Home_Brother_Stepbrothers_Age__c ='1.5';
        application.Applicant__r.Home_Grandmother__c =1.5;
        application.Applicant__r.Home_Grandfather__c =1.5;
        application.Applicant__r.Home_Other__c =1.5;
        application.Applicant__r.Home_Other_Relationship__c ='Test';
        application.Applicant__r.Instagram__c ='Test';
        application.Applicant__r.LinkedIn__c ='Test';
        application.Applicant__r.Facebook__c ='Test';
        application.Applicant__r.Twitter__c ='Test';
        application.Applicant__r.Snapchat__c ='Test';
        application.Applicant__r.YouTube__c ='Test';
        application.Access_to_religious_services__c ='Test';
        application.Ability_to_live_with_household_pets__c ='Test';
        application.Open_to_Same_Sex_Host_Parents__c =false;
        application.Open_to_Single_Host_Parent__c =false;
        application.Open_to_Sharing_a_host_family__c =false;
        application.Applicant__r.LGBTQ_Member__c ='Test';
        application.Why_is_Global_Competence_important_for_s__c ='Test';
        application.How_to_be_a_Global_Citizen__c ='Test';
        application.Things_to_share__c ='Test';
        application.Describe_a_normal_day_in_your_life__c ='Test';
        application.What_you_bring_to_your_AFS_experience__c ='Test';
        application.Food_thoughts__c ='Test';
        application.Favorite_travel_destinations_books__c ='Test';
        application.Video_for_Host_Family__c ='Test';
        application.Applicant__r.Tshirt__c ='Test';
        application.BV_Record_Locator__c ='Test';
        application.BV_Airline__c='Test';
        application.BV_Flight_Number__c ='Test';
        application.BV_Departure_Time__c =date.today();
        application.BV_Arrival_Time__c =date.today();
        application.BV_Departure_Airport__c ='Test';
        application.BV_Arrival_Airport__c ='Test';
        application.BV_Domestic_Travel_Arrangements__c ='Test';
        application.BV_Arrival_to_gateway_city_who_accompany__c ='Test';
        application.BV_Gateway_City__c ='Test';
        application.BV_Arrive_in_prior_to_the_start__c ='Test';
        application.BV_Additional_Comments__c ='Test';
        application.BV_Contact_First_Name__c ='Test';
        application.BV_Contact_Last_Name__c ='Test';
        application.BV_Contact_Phone__c ='Test';
        application.BV_Contact_Relationship__c ='Test';
        application.CBH_Record_Locator__c ='Test';
        application.CBH_Airline__c ='Test';
        application.CBH_Flight_Number__c ='Test';
        application.CBH_Departure_Time__c =date.today();
        application.CBH_Arrival_Time__c =date.today();
        application.CBH_Departure_Airport__c ='Test';
        application.CBH_Arrival_Airport__c ='Test';
        application.CBH_Domestic_Travel_Arrangements__c ='Test';
        application.CBH_Arrival_to_gateway_city_with_who__c ='Test';
        application.CBH_Gateway_City__c ='Test';
        application.CBH_Arrive_in_prior_to_the_start__c ='Test';
        application.CBH_Additional_Comments__c ='Test';
        application.CBH_Contact_First_Name__c ='Test';
        application.CBH_Contact_Last_Name__c ='Test';
        application.CBH_Contact_Phone__c ='Test';
        application.CBH_Contact_Relationship__c ='Test';
		update application;*/
        RemoteSiteParser.Person testPerson = new RemoteSiteParser.Person();
        testPerson.sf_family_id  ='Test';
        testPerson.family_id  ='Test';
        testPerson.ioc_code  ='Test';
        testPerson.owner_ioc  ='Test';
        testPerson.current_address_id  ='Test';
        testPerson.title  ='Test';
        testPerson.native_title  ='Test';
        testPerson.english_firstname  ='Test';
        testPerson.english_lastname  ='Test';
        testPerson.native_firstname  ='Test';
        testPerson.native_lastname  ='Test';
        testPerson.sex  ='Test';
        testPerson.date_of_birth =date.today();
        testPerson.preferred_email ='Test';
        
        Contact contactToSend = [select Id, AccountId, Account.Global_Link_Family_ID__c, Account.Id, Account.IOC_Code__c, BirthDate, Email, FirstName, Gender__c, Gender_Other__c, Global_Link_Person_ID__c, LastName, Title From Contact LIMIT 1];
        
        String testContacttoPerson = RemoteSiteParser.ContactToPerson(contactToSend);
        
        RemoteSiteParser.ApplicationToParticipantApp(application);
        Test.stopTest(); 
    }
}