@isTest
public class RESTPaymentConnector_Test {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        Id toDoRecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        To_Do_Item__c todoList = new To_Do_Item__c(PAY_APPLICATION_FEE__c = true,Name='PAY APPLICATION FEE',Stage_of_portal__c='2. Stage: Pre-selected',Status__c='Pending',Application__c=app.id,RecordTypeId=toDoRecordTypeId,Due_Date__c=date.today(),Type__c='Task');
        insert todoList;
        glPaymentTypes__c paytype1 = new glPaymentTypes__c(Name='Deposit',GL_payment_Record__c='Deposit',GL_payment_service__c='ProgramDeposit');
        insert paytype1;
        glPaymentTypes__c paytype2 = new glPaymentTypes__c(Name='App Fee',GL_payment_Record__c='App Fee',GL_payment_service__c='ApplicationFee');
        insert paytype2;
        
    }
    static testMethod void testWebService1() {
        Application__c app = [select id from Application__c limit 1];
        GL_Payment__c glappfee = new GL_Payment__c(Name='App Fee',Application__c=app.id, Payment_Type__c='App Fee',Payment_Status__c='Paid',GL_Service_ID__c='123456789');
        insert glappfee;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/PaymentConnector/paymentPOST' ;
        request.httpMethod = 'POST';
        request.params.put('status', 'Working');
        request.requestBody = Blob.valueOf('{"payment_type" : "ApplicationFee","payment_status" : "Paid","gl_service_id" : "123456789"}');
        request.headers.put('Content-Type', 'application/json');
        
        RestContext.request = request;
        RestContext.response = res;
        RESTPaymentConnector.paymentPOST();
    }
    
    static testMethod void testWebService2() {
        
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/PaymentConnector/paymentPOST' ;
        request.httpMethod = 'POST';
        request.params.put('status', 'Working');
        request.requestBody = Blob.valueOf('{"payment_type" : "ApplicationFee","payment_status" : "Paid","gl_service_id" : "123456789"}');
        request.headers.put('Content-Type', 'application/json');
        
        RestContext.request = request;
        RestContext.response = res;
        RESTPaymentConnector.paymentPOST();
    }
    
    static testMethod void testWebService3() {
        
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/PaymentConnector/paymentPOST' ;
        request.httpMethod = 'POST';
        request.params.put('status', 'Working');
        request.requestBody = Blob.valueOf('{"payment_type" : "","payment_status" : "Paid","gl_service_id" : "123456789"}');
        request.headers.put('Content-Type', 'application/json');
        
        RestContext.request = request;
        RestContext.response = res;
        RESTPaymentConnector.paymentPOST();
        /****/
    }
}