/**
* Created by jotategui on 08/05/2018.
*/

public with sharing class ProgramOfferTriggerHandler {
    public static void beforeInsert(List<Program_Offer__c> pos){
        updateNames(pos, null);
        updateCost(pos, null);
        updateFromHostingProgram(pos);
        updateProyectedSendingNumber(pos);
    }
    
    public static void beforeUpdate(Map<Id, Program_Offer__c> mapNew,Map<Id, Program_Offer__c> mapOld){
        updateNames(mapNew.values(), mapOld);
        updateCost(mapNew.values(), mapOld);
        updateProyectedSendingNumber(mapNew.values());
    }
    
    public static void afterInsert(Map<Id, Program_Offer__c> mapNew,Map<Id, Program_Offer__c> mapOld){
        createMasterToDos(mapNew.values(),mapOld,false);
    }
    
    public static void afterUpdate(Map<Id, Program_Offer__c> mapNew,Map<Id, Program_Offer__c> mapOld){
        createMasterToDos(mapNew.values(),mapOld,true);
    }
        
    public static void updateNames(List<Program_Offer__c> pos, Map<Id, Program_Offer__c> mapOld){
        List<Program_Offer__c> posToProcess = new List<Program_Offer__c>();
        if(mapOld == null){
            //Is insert
            posToProcess = pos;
        }else{
            //Is update
            for(Program_Offer__c po :pos){
                Program_Offer__c poOld = mapOld.get(po.Id);
                if(po.Durations__c != poOld.Durations__c || po.Cycle__c != poOld.Cycle__c || 
                   po.Year__c != poOld.Year__c || po.Host_IOC__c != poOld.Host_IOC__c || 
                   po.Send_IOC__c != poOld.Send_IOC__c || po.Con__c != poOld.Con__c){
                      posToProcess.add(po);
                }
            }
        }
        
        for(Program_Offer__c po :posToProcess){
            string newName = '';
            if(po.Send_IOC__c != null){
                newName += po.Send_IOC__c+'-';
            }
            if(po.Durations__c != null){
                newName += po.Durations__c;
            }
            if(po.Con__c != null){
                newName += po.Con__c;
            }
            if(po.Cycle__c != null){
                newName += po.Cycle__c;
            }
            if(po.Year__c != null){
                newName += po.Year__c.substring(2)+'-';
            }
            if(po.Host_IOC__c != null){
                newName += po.Host_IOC__c;
            }
            if(newName != ''){
            	po.Name = newName;
            }
        }
    }
    public static void updateCost(List<Program_Offer__c> pos, Map<Id, Program_Offer__c> mapOld){
        List<Program_Offer__c> posToProcess = new List<Program_Offer__c>();
        if(mapOld == null){
            //Is insert
            posToProcess = pos;
        }else{
            //Is update
            for(Program_Offer__c po :pos){
                Program_Offer__c poOld = mapOld.get(po.Id);
                if(po.Pre_application_Fee_numeric__c != poOld.Pre_application_Fee_numeric__c || po.Visa_Fee_numeric__c != poOld.Visa_Fee_numeric__c || 
                   po.Program_Price_numeric__c != poOld.Program_Price_numeric__c){
                      posToProcess.add(po);
                }
            }
        }
        
        for(Program_Offer__c po : posToProcess){
            double newCost = 0;
            newCost += po.Program_Price_numeric__c != null? po.Program_Price_numeric__c : 0;
            newCost += po.Visa_Fee_numeric__c  != null? po.Visa_Fee_numeric__c  : 0;
            newCost += po.Pre_application_Fee_numeric__c != null? po.Pre_application_Fee_numeric__c : 0;
            if(newCost != 0){
                po.Total_Cost_numeric__c = newCost;
            }
        }
    }
    public static void updateFromHostingProgram(List<Program_Offer__c> pos){
        Map<Id, List<Program_Offer__c>> MapHpXPos = new Map<Id, List<Program_Offer__c>>();
        for(Program_Offer__c po :pos){
            if(po.Hosting_Program__c != null){
                if(MapHpXPos.get(po.Hosting_Program__c)!=null){
                    MapHpXPos.get(po.Hosting_Program__c).add(po);
                }else{
                    MapHpXPos.put(po.Hosting_Program__c, new List<Program_Offer__c>{po});
                }
            }
        }
        if(MapHpXPos.size() == 0){
            return;
        }
        
        List<Hosting_Program__c> hps = [SELECT Id, App_Received_From__c, App_Received_To__c, Return_Date__c, Date_of_Birth_From__c, Date_of_Birth_To__c, 
                                               Program_Content__c, Program_Type__c, Hemisphere__c, Duration__c, Language_of_Placement__c, Year__c
                                          FROM Hosting_Program__c WHERE Id IN :MapHpXPos.KeySet()];
        for(Hosting_Program__c hp :hps){
            for(Program_Offer__c po:MapHpXPos.get(hp.Id)){
                po.Applications_Received_From_local__c = hp.App_Received_From__c;
                po.Applications_Received_To_local__c = hp.App_Received_To__c;
                po.To__c = hp.Return_Date__c;
                po.Date_of_Birth_From__c = hp.Date_of_Birth_From__c;
                po.Date_of_Birth_To__c = hp.Date_of_Birth_To__c;
                po.Con__c = hp.Program_Content__c;
                po.Program_Type__c = hp.Program_Type__c;
                po.Cycle__c = hp.Hemisphere__c;
                po.Durations__c = hp.Duration__c;
                po.Multiple_Languages_of_Placement__c = hp.Language_of_Placement__c;
                po.Year__c = hp.Year__c;
            }
        }
    }
    public static void updateProyectedSendingNumber(List<Program_Offer__c> pos){
        for(Program_Offer__c po :pos){
            if(po.Updated_Projected_Sending_Number__c == null && po.Projected_Sending_Number__c != null){
                po.Updated_Projected_Sending_Number__c = po.Projected_Sending_Number__c;
            }
        }
    }
    public static void createMasterToDos(List<Program_Offer__c> lstNew, Map<Id, Program_Offer__c> mapOld, boolean isUpdate){
        
        Map<Id, Program_Offer__c> programs = new Map<Id, Program_Offer__c>();
        
        for(Program_Offer__c PO : lstNew){
            if((!isUpdate && PO.To_Do_Template__c != null ) || (isUpdate && PO.To_Do_Template__c != mapOld.get(PO.Id).To_Do_Template__c)){
                programs.put(PO.Id,PO);
            }
        }
        
        if(programs.size() > 0){
            if(isUpdate)
                TodoListHelper.deleteOldMasterToDos(programs.keySet());
            
            set<id> templatesId = new Set<Id>();
            
            for(Program_Offer__c PO : programs.values()){
                if(PO.To_Do_Template__c != null){
                    templatesId.add(PO.To_Do_Template__c);
                }
            }    
            
            Map<Id,List<Master_To_Do__c>> MasterTodoGrouped = new Map<Id,List<Master_To_Do__c>>();
            if(templatesId.size() > 0){
                for(Master_To_Do__c masterToDo : [SELECT Name, Days__c,  Dependant_Task__c, Program_Offer__c, Help__c, Comments__c,
                                                  Scholarship__c, Template__r.Id, When__c,Description__c, Type__c, Link__c, Stage_of_portal__c, RecordType.Name, To_Do_Label__c from Master_To_Do__c WHERE Template__r.Id IN :templatesId]){
                                                      Id TemplateId = masterToDo.Template__r.Id;
                                                      if(!MasterTodoGrouped.containsKey(TemplateId))
                                                          MasterTodoGrouped.put(TemplateId, new List<Master_To_Do__c>());
                                                      MasterTodoGrouped.get(TemplateId).add(masterToDo);
                                                  }
            }
            
            if(MasterTodoGrouped.size() > 0){
                List<Master_To_Do__c> MasterToDoInsert = new List<Master_To_Do__c>();
                for(Program_Offer__c PO : programs.values()){
                    if(PO.To_Do_Template__c != null){
                        List<Master_To_Do__c> programMaster_to_dos = TodoListHelper.cloneMasterToDo(MasterTodoGrouped.get(PO.To_Do_Template__c),PO.id,'Program');
                        for(Master_To_Do__c todo : programMaster_to_dos){
                            MasterToDoInsert.add(todo);
                        }
                    }    
                }
                if(masterToDoInsert.size() > 0){
                    insert masterToDoInsert;
                }
            }            
            
        }
    }
}