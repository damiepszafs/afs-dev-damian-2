public class createLeadFromCall {
    @AuraEnabled
    public static void goToNewLead(Id recordId){
        PageReference pg = new PageReference('/lightning/r/Lead/'+recordId+'/view');
        pg.setRedirect(true);
    }
    
    @AuraEnabled
    public static void goToLeadList(){
        PageReference pg = new PageReference('/lightning/o/Lead/list');
        pg.setRedirect(true);
    }
}