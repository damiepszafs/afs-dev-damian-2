global without sharing class Afs_YourHealthInfoControler {

    
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld, Boolean isRadioList) {
        list<String> fieldAPINames = fld.split(',');
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklsitValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            if(!isRadioList){
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourHealthInformation_Select,false,''));   
            }
            picklsitValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(),picklsitValues);
        }
        
        return mapFieldPicklistVal; 
        
    }
    
    @AuraEnabled
    global static Health_Information__c getHealthInfoRecord(String strContactId) {
        Health_Information__c objHealth = new Health_Information__c ();
        objHealth.Contact__c = strContactId;
        String strQuery = 'SELECT id,';
        for(String fieldAPIName : Afs_UtilityApex.getFields('Health_Information__c',true)){
            strQuery += fieldAPIName + ' ,';
        }
        strQuery = strQuery.removeEnd(',');
        strQuery += ' FROM Health_Information__c WHERE Contact__c =: strContactId ORDER BY LastModifiedDate DESC';
        list<Health_Information__c> lstHealthInformation = Database.query(strQuery);
        return lstHealthInformation.isEmpty() ? objHealth : lstHealthInformation[0]; 
    }
    
    @AuraEnabled
    global static String saveRecord(Health_Information__c objHealth,String recId,Boolean isDraft) {
        try{
            if(objHealth.Id == null){
               insert objHealth;
            }else{
               update objHealth; 
            }
            if(!isDraft){
                List<To_Do_Item__c> rec = [SELECT Id, Status__c 
                                           FROM To_Do_Item__c 
                                           WHERE Id  =: recId];
                if(rec.size() > 0){
                    rec[0].status__c = 'In Review';
                    update rec[0];
                }
            }
            return 'Success';
        }catch(Exception e){
            system.debug(e.getStackTraceString()+ ' @@ ' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
}