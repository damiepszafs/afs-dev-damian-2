/** 
 * Description : Controller class for th search filter 
 * 				 lightning component
 * 
 */
global class Afs_SearchProgramController {
    
    @AuraEnabled
    global static Map<String, List<DisplayPicklistValueWrapper>> getselectOptions( ) {
        String communityId =  Network.getNetworkId(); 
        String queryStr = 'SELECT id, Name, Afs_SendingPartner__c';
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
        }
        list<AFS_Community_Configuration__c>  listCommunityConfig;
        if(String.isNotBlank(communityId) || Test.isRunningTest()){
            
            listCommunityConfig  =  Database.query(queryStr);
            
        }
        Map<String, List<DisplayPicklistValueWrapper>> MapOfPickListValues = new Map<String, List<DisplayPicklistValueWrapper>>();
        list<String> DestinationValues = new list<String>();
        list<String> DurationsValues = new list<String>();
        list<String> ProgramTypeValues = new list<String>();
        list<String> AreaofInteresValues = new list<String>();  
        list<DisplayPicklistValueWrapper> DestwrapperList = new list<DisplayPicklistValueWrapper>();
        list<DisplayPicklistValueWrapper> DurtnwrapperList = new list<DisplayPicklistValueWrapper>();
		list<DisplayPicklistValueWrapper> PrgmtpwrapperList = new list<DisplayPicklistValueWrapper>();
		list<DisplayPicklistValueWrapper> AreaIntwrapperList = new list<DisplayPicklistValueWrapper>();
        set<String> fieldsSet = new set<String>();
        fieldsSet.add('Length__c');
        fieldsSet.add('Program_Type__c');
        fieldsSet.add('Area_of_interest__c');
        fieldsSet.add('Destinations__c');
        fieldsSet.add('Show_in_portal__c');
        fieldsSet.add('Program_Durations__c');
        system.debug(' -- >' + communityId);
        
        Afs_SecurityEnforceUtility.checkObjFieldsAccess(fieldsSet,'Program_Offer__c',false);
        Date todayDt = Date.today(); 
        for (Program_Offer__c pro : [SELECT id,Length__c,Program_Type__c, Area_of_interest__c,Destinations__c, Program_Durations__c 
    								FROM Program_Offer__c 
    								WHERE Sending_Partner__c = :listCommunityConfig[0].Afs_SendingPartner__c
                                     AND Show_in_portal__c = true 
                                     AND Applications_Received_From_local__c <=: todayDt
                                     AND Applications_Received_To_local__c >=: todayDt ORDER BY Length__c]) { 
            if(pro.Destinations__c != null){
            	DestinationValues.add(pro.Destinations__c);            
            } 
            if(pro.Length__c != null){
            	DurationsValues.add(pro.Length__c);            
            }
            if(pro.Program_Durations__c != null){
                for(String len : pro.Program_Durations__c.split(';')){
                   DurationsValues.add(len); 
                }            	            
            } 
            if(pro.Program_Type__c != null){    
            	ProgramTypeValues.add(pro.Program_Type__c);            
            }  
            if(pro.Area_of_interest__c != null){                   
                for(String arr : pro.Area_of_interest__c.split(';')){
                	AreaofInteresValues.add(arr); 
                }  
            } 
        }
        
        // Get Picklist labels and create wrapper
        SObject objObject = Schema.getGlobalDescribe().get('Account').newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        map<String,map<String,String>> fieldMapNew = new map<String,map<String,String>>();
        
        map<String,String> valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Country__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()),String.valueOf(picklistVal.getLabel()));
        }
        fieldMapNew.put('Destinations__c',valueMap);
        system.debug( ' -- > ' + fieldMapNew );
        // Program OFfered Fields
		objObject = Schema.getGlobalDescribe().get('Program_Offer__c').newSObject() ;
		objType = objObject.getSObjectType();               
		objDescribe = objType.getDescribe();                
		fieldMap = objDescribe.fields.getMap(); 
        
        valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Length__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()),String.valueOf(picklistVal.getLabel()));
        }
        fieldMapNew.put('Length__c',valueMap);
        system.debug( ' -- > ' + fieldMapNew );
        valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Program_Type__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()),String.valueOf(picklistVal.getLabel()));
        }
        fieldMapNew.put('Program_Type__c',valueMap);
        system.debug( ' -- > ' + fieldMapNew );
        valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Area_of_interest__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()),String.valueOf(picklistVal.getLabel()));
        }
        fieldMapNew.put('Area_of_interest__c',valueMap);
        system.debug( ' -- > ' + fieldMapNew );
        
        integer DestCount = 1;
        for(String Pickval : (new Set<String>(DestinationValues))){     
           system.debug( fieldMapNew.get('Destinations__c') + ' -- > ' );
             system.debug(' -- > ' + Pickval );
           DestwrapperList.add(new DisplayPicklistValueWrapper('Destinations__c' , Pickval , false ,DestCount,fieldMapNew.get('Destinations__c').get(Pickval)));
           DestCount++;            
        }
        
        integer DurtCount = 1;
        Set<String> setDurations = new Set<String>(DurationsValues);
        setDurations = orderDuration(setDurations);
        for(String Pickval : setDurations){ 
           DurtnwrapperList.add(new DisplayPicklistValueWrapper('Length__c' , Pickval , false ,DurtCount,fieldMapNew.get('Length__c').get(Pickval)));
           DurtCount++;            
        }
        
        integer ProgmCount = 1;
        for(String Pickval : (new Set<String>(ProgramTypeValues))){ 
            system.debug( fieldMapNew.get('Program_Type__c') + ' -- > ' );
             system.debug(' -- > ' + Pickval );
           PrgmtpwrapperList.add(new DisplayPicklistValueWrapper('Program_Type__c' , Pickval , false ,ProgmCount,fieldMapNew.get('Program_Type__c').get(Pickval)));
           ProgmCount++;            
        }
        
        integer AreaCount = 1;
        for(String Pickval : (new Set<String>(AreaofInteresValues))){   
           system.debug( fieldMapNew.get('Area_of_interest__c') + ' -- > ' );
             system.debug(' -- > ' + Pickval );
             system.debug(' -- > ' + fieldMapNew.get('Area_of_interest__c').get(Pickval) );
           AreaIntwrapperList.add(new DisplayPicklistValueWrapper('Area_of_interest__c' , Pickval , false ,AreaCount,fieldMapNew.get('Area_of_interest__c').get(Pickval)));
           AreaCount++;            
        }
        system.debug( ' ## ' + AreaIntwrapperList); 
        
        //sorting done on labels instead of values from query
        DestwrapperList.sort();
        //DurtnwrapperList.sort();
        PrgmtpwrapperList.sort();
        AreaIntwrapperList.sort();
        
        
        // Looping added to revise indexing of the labels
        // after sorting is done
        Integer countIndex = 1;
        for(DisplayPicklistValueWrapper wrapperObj : DestwrapperList){
            wrapperObj.indexId = countIndex;
            countIndex++;
        }
        countIndex = 1;
        for(DisplayPicklistValueWrapper wrapperObj : DurtnwrapperList){
            wrapperObj.indexId = countIndex;
            countIndex++;
        }
        countIndex = 1;
        for(DisplayPicklistValueWrapper wrapperObj : PrgmtpwrapperList){
            wrapperObj.indexId = countIndex;
            countIndex++;
        }
        countIndex = 1;
        for(DisplayPicklistValueWrapper wrapperObj : AreaIntwrapperList){
            wrapperObj.indexId = countIndex;
            countIndex++;
        }
        
        MapOfPickListValues.put('Destinations__c', DestwrapperList);
        MapOfPickListValues.put('Length__c', DurtnwrapperList);
        MapOfPickListValues.put('Program_Type__c', PrgmtpwrapperList);
        MapOfPickListValues.put('Area_of_interest__c', AreaIntwrapperList);                   
        
        return MapOfPickListValues;
    }  
    
    public static Set<String> orderDuration(Set<String> setDuration){
        Set<String> finalSet = new Set<String>();
        List<DurationWrapper> lstDurWrap = new List<DurationWrapper>();
        
        for(String str : setDuration){
            lstDurWrap.add(new DurationWrapper(str));
        }
        
        lstDurWrap.sort();
        
        for(DurationWrapper durW : lstDurWrap){
            finalSet.add(durW.strValue);
        }
        
        return finalSet;
    }
    
    global class DurationWrapper implements Comparable{
        String strValue{get;set;}//Full String
        Integer num{get;set;}
        
        public DurationWrapper(String val){
            strValue = val;
            try{ num = Integer.valueOf(val.split(' ')[0]); }catch(Exception ex){ num = 0;}
        }
            
        public Integer compareTo(Object compareTo)
        {
            DurationWrapper compareToDuration = (DurationWrapper)compareTo;
            String value2 = compareToDuration.strValue.toUpperCase();
            Integer num2 = compareToDuration.num;
            
            if(strValue.toUpperCase().contains('DAY')){
                if(value2.contains('DAY') && num > num2){
                    return 1;
                }else{
                    return -1;
                }
            }else if(strValue.toUpperCase().contains('WEEK')){
                if(value2.contains('DAY') || (value2.contains('WEEK') && num > num2)){
                    return 1;
                }else{
                    return -1;
                }
            }else if(strValue.toUpperCase().contains('MONTH')){
                if(value2.contains('MONTH') && num < num2){
                    return -1;
                }else{
                    return 1;
                }
            }else{
                return 1;
            }         
        }
    }
    
    global class DisplayPicklistValueWrapper implements Comparable{
    
        @auraEnabled
        global Boolean isSelected {get;set;}
        @auraEnabled
        global String Name{get;set;}
        @auraEnabled
        global String FieldName {get;set;}
        @auraEnabled
        global Integer indexId {get;set;} 
        @auraEnabled
        global String FieldLbl {get;set;}
        
        global DisplayPicklistValueWrapper(String strFldName, String strNam, Boolean selected, Integer intNum,String strFieldLbl){
            FieldName = strFldName;
            Name = strNam;
            isSelected = selected;
            indexId = intNum;
            FieldLbl = strFieldLbl;
        }  
        
        public Integer compareTo(Object compareTo)
        {
            DisplayPicklistValueWrapper compareToLabel = (DisplayPicklistValueWrapper)compareTo;
            
            if(FieldLbl > compareToLabel.FieldLbl)
                return 1;
            else if (FieldLbl < compareToLabel.FieldLbl)
                return -1;
            else
                return 0;
        }
    }                   
}