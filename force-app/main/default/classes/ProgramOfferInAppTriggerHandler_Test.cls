@isTest
public class ProgramOfferInAppTriggerHandler_Test {
	@TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.createApplication(con,null);
        Insert app;
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Application__c', 'Interests__r', 'Child', '1', null, null,10);
        Insert ss;
    }
    
    public static testMethod void recalculateAppScore_Test(){
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        Program_Offer__c po = [SELECT Id FROM Program_Offer__c LIMIT 1];
		Program_Offer_in_App__c poa = new Program_Offer_in_App__c(Application__c = app.Id,Program_Offer__c = po.Id, Contact__c = con.Id);
        
        Test.startTest();
        Insert poa;
        app = [SELECT Score__c FROM Application__c LIMIT 1];
        System.assertEquals(110, app.Score__c);
        
        Delete poa;
        app = [SELECT Score__c FROM Application__c LIMIT 1];

        Test.stopTest();
        System.assertEquals(100, app.Score__c);
    }
    
    public static testMethod void populateProgramEligibilityStatusOfApplication_Test(){
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Application__c app = [SELECT Id FROM Application__c LIMIT 1];
        Program_Offer__c po = [SELECT Id FROM Program_Offer__c LIMIT 1];
		Program_Offer_in_App__c poa = new Program_Offer_in_App__c(Application__c = app.Id,Program_Offer__c = po.Id, Contact__c = con.Id);
        
        Test.startTest();
        Insert poa;
        app = [SELECT Program_Eligibility_Status__c FROM Application__c LIMIT 1];
        System.assert(app.Program_Eligibility_Status__c == 'Not Decided');
        
        poa.Is_the_applicant_Eligible__c = 'Eligible';
        Update poa;
        app = [SELECT Program_Eligibility_Status__c FROM Application__c LIMIT 1];
        System.assert(app.Program_Eligibility_Status__c == 'Eligible');
        
        poa.Is_the_applicant_Eligible__c = 'Ineligible';
        Update poa;
        app = [SELECT Program_Eligibility_Status__c FROM Application__c LIMIT 1];
        System.assert(app.Program_Eligibility_Status__c == 'Ineligible');
        
        Test.stopTest();
    }
}