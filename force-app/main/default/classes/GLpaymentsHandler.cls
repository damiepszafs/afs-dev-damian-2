public class GLpaymentsHandler {
    public static String updateGLpayments(String GLserviceId, String paymentType, String paymentStatus){
        String response;
        try{
            String paymentTypePP;
            Set<Id> applicationIdList = new Set<Id>();
            List<GLpaymentTypes__c> glPaymentTypes = GLpaymentTypes__c.getall().values();
            for(GLpaymentTypes__c glp : glPaymentTypes){
                if(glp.GL_payment_service__c == paymentType){
                    paymentTypePP = glp.GL_payment_Record__c;
                }
            }
            if(paymentTypePP != null){
                List<GL_Payment__c> glPaymentList = [SELECT Id,Application__c, Payment_Type__c,Payment_Status__c FROM GL_Payment__c WHERE Payment_Type__c =: paymentTypePP AND  GL_Service_ID__c =: GLserviceId];
                List<String> pickListValuesList= new List<String>();
                Schema.DescribeFieldResult fieldResult = GL_Payment__c.Payment_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    pickListValuesList.add(pickListVal.getLabel());
                }  
                if(glPaymentList.size()>0 && glPaymentList.size() == 1){
                    if(pickListValuesList.contains(paymentStatus)){
                        
                        glPaymentList[0].Payment_Status__c = paymentStatus;
                        update glPaymentList;
                        applicationIdList.add(glPaymentList[0].Application__c);
                        verifyToDoItemPaymentsMethod(applicationIdList);
                        response = 'Success';
                    }else{
                        response = 'Warning: Invalid Payment Status'; 
                    }
                }else{
                    response = 'Warning: GL Service not found';
                }
            }else{
                response = 'Warning: Invalid Payment Type';
            }
            
            
        }catch (Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            response = 'Alert';
        }
        return response;
    }
    
    public static void verifyToDoItemPaymentsMethod(Set<Id> applicationIds){
        try{ 
            List<SObject> recordsToUpdate = new List<SObject>();
            List<Application__c> applicationListMoreInfo = [SELECT Id,GL_Service_Id__c,Status__c,
                                                            (SELECT Id,Status__c,Application__r.Status__c,Stage_of_portal__c,PAY_PROGRAM_DEPOSIT__c,PAY_APPLICATION_FEE__c 
                                                             FROM To_Do_List__r WHERE Stage_of_portal__c like '%Pre-selected%'),
                                                            (SELECT Id,Payment_Status__c,Payment_Type__c FROM GL_Payments__r)
                                                            FROM Application__c WHERE ID IN: applicationIds];
            Integer toDsReady;
            String toDoStage = 'none';
            for(Application__c ap : applicationListMoreInfo){
                toDsReady = 0;
                for(To_Do_Item__c td : ap.to_Do_List__r){
                    for(GL_Payment__c gl : ap.GL_Payments__r){
                        if(td.PAY_PROGRAM_DEPOSIT__c == true  && td.Status__c == 'Pending' && gl.Payment_Type__c == 'Deposit' && gl.Payment_Status__c == 'Paid'){
                            td.Status__c = 'Completed';
                            recordsToUpdate.add(td);
                            break;
                        }else if(td.PAY_APPLICATION_FEE__c == true  && td.Status__c == 'Pending' && gl.Payment_Type__c == 'App Fee' && gl.Payment_Status__c == 'Paid'){
                            td.Status__c = 'Completed';
                            toDoStage = td.Stage_of_portal__c;
                            recordsToUpdate.add(td);
                            break;
                        }
                    }
                    if(td.Status__c == 'Completed' && td.PAY_PROGRAM_DEPOSIT__c == false){
                        toDsReady++; 
                    } 
                }
                if(toDsReady == (ap.to_Do_List__r.size()-1) && ap.Status__c == 'Decision'){
                    ap.Status__c = 'Confirmation';
                    recordsToUpdate.add(ap);
                }
            }
            
            recordsToUpdate.sort();
            
            if(!recordsToUpdate.isEmpty()){
                List<Database.SaveResult> lstSR = Database.Update(recordsToUpdate,false);
                for(Integer i = 0;i < lstSR.size() ; i++){
                    Database.SaveResult sr = lstSR[i];
                    if(!sr.isSuccess()){
                        System.debug(logginglevel.error,recordsToUpdate[i].Id + ' --> ' + String.join(sr.getErrors(),' / '));
                    }
                }
            }
            
            
        }catch (Exception e) {
            System.debug('The following exception in GLpaymentsHadlerverifyToDoItemPaymentsMethod has occurred: ' + e.getMessage());
        }
    }
}