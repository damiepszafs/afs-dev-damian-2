@isTest
public class GLpaymentsHandler_Test {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.createApplication(con,null);
        insert app;
        GL_Payment__c glappfee = new GL_Payment__c(Name='App Fee',Application__c=app.id, Payment_Type__c='App Fee',Payment_Status__c='Not Paid',GL_Service_ID__c='123456789');
        insert glappfee;
        Id toDoRecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        To_Do_Item__c todoList1 = new To_Do_Item__c(PAY_APPLICATION_FEE__c = true,Name='PAY APPLICATION FEE',Stage_of_portal__c='2. Stage: Pre-selected',Status__c='Pending',Application__c=app.id,RecordTypeId=toDoRecordTypeId,Due_Date__c=date.today(),Type__c='Task');
        To_Do_Item__c todoList2 = new To_Do_Item__c(PAY_PROGRAM_DEPOSIT__c = true,Name='PAY PROGRAM DEPOSIT',Stage_of_portal__c='2. Stage: Pre-selected',Status__c='Pending',Application__c=app.id,RecordTypeId=toDoRecordTypeId,Due_Date__c=date.today(),Type__c='Task');
        insert todoList1;
        insert todoList2;
       
        
    } 
    
    public static testMethod void updateGLpaymentsAppFee_Test(){
        glPaymentTypes__c paytype = new glPaymentTypes__c(Name='App Fee',GL_payment_Record__c='App Fee',GL_payment_service__c='ApplicationFee');
        insert paytype;
        
        Test.startTest();
        GLpaymentsHandler.updateGLpayments('123456789','ApplicationFee','Paid');
        Test.stopTest();
        
        
        GL_Payment__c glappfee2 = [select id,Payment_Status__c from GL_Payment__c where Payment_Type__c='App Fee' limit 1];
        system.assertEquals(glappfee2.Payment_Status__c, 'Paid');
        
    }
    
    public static testMethod void updateGLpaymentsDeposit_Test(){
        glPaymentTypes__c paytype = new glPaymentTypes__c(Name='Deposit',GL_payment_Record__c='Deposit',GL_payment_service__c='ProgramDeposit');
        insert paytype;
        Application__c app = [select id from Application__c limit 1];
        GL_Payment__c glDeposit = new GL_Payment__c(Name='Deposit',Application__c=app.id, Payment_Type__c='Deposit',Payment_Status__c='Not Paid',GL_Service_ID__c='123456789');
        insert glDeposit;
       
        Test.startTest();
        GLpaymentsHandler.updateGLpayments('123456789','ProgramDeposit','Paid');
        Test.stopTest();
        
        
        GL_Payment__c glappfee3 = [select id,Payment_Status__c from GL_Payment__c where Payment_Type__c='Deposit' limit 1];
        system.assertEquals(glappfee3.Payment_Status__c, 'Paid');
        
    }
}