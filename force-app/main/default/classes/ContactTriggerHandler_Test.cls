@isTest
public class ContactTriggerHandler_Test { 
    @testSetup
    public static void setup_method(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }
    
    public static testMethod void calculateScore_Test(){
        List<Scoring_Settings__c> lstSS = new List<Scoring_Settings__c>();
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Contact', 'Do_you_smoke__c', 'Value', 'Yes',null,null, -20);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS2', 'Application__c', 'Name', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        Insert lstSS;
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        
        Contact con = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact');     

        Application__c app = Util_Test.createApplication(con,null);
       	Insert app;
        
        Test.startTest();
		con.Do_you_smoke__c = 'Yes';
        DataBase.Update(con);
        Test.stopTest();
        
        app = [SELECT Score__c FROM Application__c Limit 1];
        System.assertEquals(80, app.Score__c);

    }
    
    public static testmethod void assignChapter(){
        List<Account> lstAccounts = new List<Account>();
        Account sendPartner = new Account(name = 'Test Account',IOC_Code__c = 'ARG',RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Partner').getRecordTypeId());
        lstAccounts.add(sendPartner);
        Account chapter = new Account(Zip_From__c = 1233, Zip_To__c = 1239, IOC_Code__c = 'ARG', Name = 'Test', recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('AFS Chapters').getRecordTypeId());
        lstAccounts.add(chapter);
        insert lstAccounts;
        Contact con = new Contact(LastName = 'Test1', MailingPostalCode = '1234', Sending_Partner__c = sendPartner.Id);
        Contact con2 = new Contact(LastName = 'Test2', MailingPostalCode = '1', Sending_Partner__c = sendPartner.Id);
        Contact con3 = new Contact(LastName = 'Exception', MailingPostalCode = 'hola', Sending_Partner__c = sendPartner.Id);
        List<Contact> inserts = new List<Contact>{con, con2, con3};
        Test.startTest();
        insert inserts;
        con2.MailingPostalCode = '1239';
        update con2;
        Test.stopTest();

        Map<Id,Contact> mapActuals = new Map<Id,Contact>([SELECT Id,Chapter__c FROM Contact WHERE Id = :con.Id OR Id = :con2.Id]);
        system.assertEquals(chapter.Id, mapActuals.get(con.Id).Chapter__c);
        system.assertEquals(chapter.Id, mapActuals.get(con2.Id).Chapter__c);
    }
}