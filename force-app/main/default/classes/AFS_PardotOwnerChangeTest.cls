@isTest(SeeAllData=true) 
public class AFS_PardotOwnerChangeTest {
	
    static testMethod void positiveTest(){
        List<AFS_Community_Configuration__c> lst = 
            [SELECT Community_Guest_User_Owner__c,CRMUserOwner__c 
             FROM AFS_Community_Configuration__c
             WHERE Community_Guest_User_Owner__c != '' 
             AND CRMUserOwner__c != ''];
        
        List<User> lstUser = [SELECT Id FROM User WHERE Name LIKE 'AFS Social Sign On%'];
        if(lstUser.isEmpty()){
            Profile prf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
            User usr = Util_Test.createUser(prf.Id,'afssocialsignon@afs.orgTest','AFS Social Sign On');
            Insert usr;
            lstUser.add(usr);
        }
        List<Contact> lstContacts = [SELECT Id From Contact];
        if(lstContacts.isEmpty()){
			Contact con = Util_Test.createContact('ContactTest',null);
            Insert con;
            lstContacts.add(con);
        }
        
        if(lst.size() > 0){
            List<pi__ObjectChangeLog__c> lstObjs = new List<pi__ObjectChangeLog__c>();
            
            pi__ObjectChangeLog__c obj =  new pi__ObjectChangeLog__c(
            ownerid= lst[0].Community_Guest_User_Owner__c,
            pi__ObjectEmail__c='abc@abc.com', 
            pi__ObjectFid__c='0030v00000G2d3t', 
            pi__ObjectState__c = 1, 
            pi__ObjectType__c = 1);            
            lstObjs.add(obj);
            obj =  new pi__ObjectChangeLog__c(
            ownerid= lstUser[0].Id,
            pi__ObjectEmail__c='abc@abc.com', 
            pi__ObjectFid__c=lstContacts[0].Id, 
            pi__ObjectState__c = 1, 
            pi__ObjectType__c = 1);            
            lstObjs.add(obj);
            
            insert lstObjs;                                                                
        }
    }
}