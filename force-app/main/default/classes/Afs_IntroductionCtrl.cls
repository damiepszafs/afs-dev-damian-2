global without sharing class Afs_IntroductionCtrl {
    @AuraEnabled
    global static void saveApplicantIntroduction(Application__c application, String recId){
        try{
        	update application; 
            List<To_Do_Item__c> rec = [SELECT Id, Status__c 
                                              FROM To_Do_Item__c 
                                       WHERE Id  = :recId];
            if(rec.size() > 0){
                rec[0].status__c = 'In Review';
                update rec[0];
            }
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }
    }
    @AuraEnabled
    global static  List<ContentDocumentLink> deleteAttachment(string AttchmentId,string Id){
        	list<ContentDocumentLink>  attchamentList  = new list<ContentDocumentLink>();  
    	try{
          	attchamentList =  [SELECT ContentDocumentId, ContentDocument.Title FROM ContentDocumentLink  where Id =: AttchmentId];
        	delete attchamentList; 
           
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }	
     return getAttachment(Id);
    }
    @AuraEnabled
    global static List<ContentDocumentLink> getAttachment(string Id){
    		list<ContentDocumentLink>  attchamentList =  [SELECT ContentDocumentId, ContentDocument.Title FROM ContentDocumentLink WHERE LinkedEntityId =: Id];
            return attchamentList;
    }
    
    
    
}