global without sharing class Afs_SettingController {
	/**
	 * @Desc : Method to get picklist values
	 * @param objObject : Object name
	 * @param fld : field names seperated by comma
	 */
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld) {
        list<String> fieldAPINames = fld.split(',');
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklsitValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            if(fieldAPIName.trim() == 'Country_of_Legal_Residence__c'){
                picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectCountry,false,System.label.AfsLbl_YourApplication_SelectCountry));
            }else if(fieldAPIName.trim() == 'NationalityCitizenship__c'){
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectNationality,false,System.label.AfsLbl_YourApplication_SelectNationality));
            }else if(fieldAPIName.trim() == 'Gender__c'){
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_YourApplication_SelectGender,false,System.label.AfsLbl_YourApplication_SelectGender));
            }else{
            	picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_General_None,false,System.label.AfsLbl_General_None));
            }
            
            picklsitValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(),picklsitValues);
        }
        
        return mapFieldPicklistVal; 
        
    }
    
    /**
	 * @Desc : Method to get parent/guardians
	 * @param contactId : id of contact
	 */
    @AuraEnabled
    global static map<String,List<String>> getParents(String contactId) {
        map<String,List<String>> ret = new map<String,List<String>>();
        List<String> pickListValuesList= new List<String>();
		for(Schema.PicklistEntry pickListVal : npe4__Relationship__c.npe4__Type__c.getDescribe().getPicklistValues()){
			pickListValuesList.add(pickListVal.getValue());
		}   
        
        Set<String> mailsSet = new Set<String>();
        Set<String> phonesSet = new Set<String>();
        String query = 'SELECT id, npe4__RelatedContact__r.Email, npe4__RelatedContact__r.Phone FROM npe4__Relationship__c ';
        query += 'WHERE npe4__Contact__c = \'' + contactId +'\'';
        for(npe4__Relationship__c related :Database.query(query)){
            mailsSet.add(related.npe4__RelatedContact__r.Email);
            phonesSet.add(related.npe4__RelatedContact__r.Phone);
        }
        ret.put('Email', new List<String>());
        for(String e : mailsSet){
            if(!String.isEmpty(e)){
                ret.get('Email').add(e);
            }
        }
        ret.put('Phone', new List<String>());
        for(String p : phonesSet){
            if(!String.isEmpty(p)){
                ret.get('Phone').add(p);
            }
        }
        
        return ret;
    }
    
    
    /**
	 * @Desc : Method to change password for user
	 * @param password : password string 
	 */
    @AuraEnabled
    global static void changePassword(String password) {
        try{            
            Id userId = UserInfo.getUserId();
        	System.setPassword(userId, password);
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            String errMsg = e.getMessage();
            if(e.getMessage().equalsIgnoreCase(System.label.AfsLbl_Error_PasswordSystem)){
                errMsg += ' '+System.label.AfsLbl_Error_PasswordAddOn;
            }else{
                errMsg = errMsg.split(':')[1];
            }
            
            throw new AuraHandledException( errMsg );
        }
        
    }
    
    /**
	 * @Desc : Method to save contact
	 * @param contactObj : Contact object instance to update
	 */
    @AuraEnabled
    global static void saveRecord(Contact contactObj) {
        try{            
            update contactObj; 
            Afs_UtilityApex.updateUser(UserInfo.getUserId());
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }
    }
}