global without sharing class Afs_InputSchoolController{
    
/**
* @description: Auto suggestion from SF Accounts with School Record Type
* @param: input: School Name
* @return: List of School Accounts  
**/
	@AuraEnabled
	global static List<Account> getSchoolAutoComplete(String input) {
		List<List<SObject>> schoolPredictions = [FIND :input IN NAME FIELDS
		 RETURNING Account(Id, Name, GMPlace_Id__c ,BillingStreet, BillingPostalCode, BillingState, BillingCountry, BillingCity, BillingLatitude, BillingLongitude WHERE RecordType.Name = 'School') LIMIT 3];
											
		Account[] schools = ((List<Account>) schoolPredictions[0]);
		return schools;
	}
	
	@AuraEnabled
    global static string getAddressAutoComplete(String input, String types,String langug) {
       	String url = 'https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input='
            + EncodingUtil.urlEncode(input, 'UTF-8')
            + '&language=' + langug
            + '&key=' + Afs_InputSchoolController.getGoogleMapsAPIKey();            
        return Afs_InputSchoolController.getHttp(url);
    }
	
	
	@AuraEnabled
	global static Account getMySchool(String schoolId) {
		Account mySchool;
		if(schoolId.length() > 0) {
			mySchool = [SELECT Id, Name, BillingStreet, BillingPostalCode, BillingState, BillingCountry, BillingCity, BillingLatitude, BillingLongitude, GMPlace_Id__c FROM Account WHERE Id = :schoolId];
		} else {
			mySchool = new Account();
		}
		return mySchool;
	}
	
 
	@AuraEnabled
    global static string getAddressAutoCompleteMerge(String input, String types,String langug) {
    	try{
	    	List<Account> schoolsInSF = getSchoolAutoComplete(input);
	       	String jsonResponse = getAddressAutoComplete(input, types, langug);
			List<Predictions> predFromGoogle = JSON2Apex.parsero(jsonResponse).getPredictions();
			for(Account  school: schoolsInSF) {
				Predictions newSch = new Predictions(school);
				Integer size = predFromGoogle.size();
				for(Integer i = 0; i < size; i++) {
					if(predFromGoogle[i].place_id == newSch.place_id) {
						predFromGoogle.remove(i);
						size--;
					}
				}
				predFromGoogle.add(0, newSch);
				
			} 
			
			return System.JSON.serialize(predFromGoogle);
		} catch (Exception e) {
				system.debug('esta linea' + e.getMessage());
				return e.getMessage();
		} 
    }
  
    
	global class JSON2Apex {
		public List<Predictions> predictions;
		public String status;
		
		public List<Predictions> getPredictions() {
			return this.predictions;
		}
	}
	
	public static JSON2Apex parsero(String json) {
		return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
	}
	
	public class Predictions {
		public String description;
		public String id;
		public List<Matched_substrings> matched_substrings;
		public String place_id;
		public String reference;
		public Structured_formatting structured_formatting;
		public List<Terms> terms;
		public List<String> types;
		public String SF_ID;
		
		
		public Predictions (Account acct) {
			this.description = acct.Name + ', ' + acct.BillingState + ', ' + acct.BillingCountry;
			this.place_id = acct.GMPlace_Id__c;
			this.SF_ID = acct.Id;
			List<String> types = new List<String>();
			types.add('establishment');
			this.types = types;
			
		}
	}
	
	public class Terms {
		public Integer offset;
		public String value;
	}
	
	public class Structured_formatting {
		public String main_text;
		public List<Matched_substrings> main_text_matched_substrings;
		public String secondary_text;
	}
	
	public class Matched_substrings {
		public Integer length;
		public Integer offset;
	}


















    /**
* @description : Auto suggestion Web Service 
* @param : input: SearchAddress , types: Results Types , langug : language for getting the results
* @return : string
 
    @AuraEnabled
    global static string getAddressAutoComplete(String input, String types,String langug) {
    
       	String url = 'https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input='
            + EncodingUtil.urlEncode(input, 'UTF-8')
            + '&language=' + langug
            + '&key=' + Afs_InputSchoolController.getGoogleMapsAPIKey();
            //String url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key=AIzaSyD4wJv-AqzQvK3evnHsSzMZg0ezGAbJ2cA&input=Прехрамбено-угоститељска-школа-Чачак&inputtype=textquery';
		String response = Afs_InputSchoolController.getHttp(url);            
        return response;
    }
    **/
    /**
* @description : Place Details Web Service 
* @param : PlaceId: Unique Place Id , langug : language for getting the results
* @return : string
**/ 
    @AuraEnabled
    global static string getAddressDetails(String PlaceId,String lang) {
        String url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='
            +PlaceId+'&language='+lang+'&key='+ Afs_InputSchoolController.getGoogleMapsAPIKey();
        return Afs_InputSchoolController.getHttp(url);
    }
    
    /**
* @description : To get the google Api key from custom label
* @param : 
* @return : string
**/
    global static String getGoogleMapsAPIKey(){
        
        String GMapkey= Label.Afs_Google_Key; 
        return GMapkey;
    }
    /**
* @description : Common Utility method for making call out
* @param : String
* @return : string
**/
    
    global static string getHttp(String url){
        try{
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(url);
            request.setMethod('GET');
            HttpResponse response = http.send(request);
            return response.getBody();
            
        }catch(Exception e){  
        	//system.debug(e.getMessage());  
            return 'sample string';
        }
    } 
  
  
   

    
    /**
* @description : Wrapper class for getting latitude and longtitude (Second Web Service)
* @param : 
* @return : 
**/
    
    global class GeoLoca{
        global Double lat;  //6.9121796
        global Double lng;  //79.8828828
    }
    
    /**
* @description : Wrapper class for getting Address (First Web Service)
* @param : 
* @return : 
**/
    global class AddressJsonInfo {
        global String long_name;
        global String short_name;
        global List<String> types;
    }
    /**
* @description : Wrapper class for getting latitude and longtitude (Second Web Service)
* @param : 
* @return : 
**/
    
    
    public class Geometry {
        public Location location;
        public Viewport viewport;
    }
    
    public class Viewport {
        public Location northeast;
        public Location southwest;
    }
    
    public class Location {
        public Double lat;
        public Double lng;
    }
    
    
  	/**
  	
  	added code
  	
  	
  	
  	**/
  	
  	
    
    /**
* @description : Wrapper class for Place Details  (Third and fourth Web Service)
* @param : 
* @return : 
**/
    
    public class TXP_JSON2Apex {
        public List<Html_attributions> html_attributions;
        public Result result;
        public String status;
    }
    public class Html_attributions {
    }
    
    
    public class Result {
        public List<Address_components> address_components;
        public String adr_address;
        public String formatted_address;
        public Geometry geometry;
        public String icon;
        public String id;
        public String name;
        public String place_id;
        public String reference;
        public String scope;
        public List<String> types;
        public String url;
        public Integer utc_offset;
        public String vicinity;
    }
    public class Address_components {
        public String long_name;
        public String short_name;
        public List<String> types;
    }
    
    
    
}