@RestResource(urlMapping='/PaymentConnector/*')
global with sharing class RESTPaymentConnector {
    @HttpPost
    global static void paymentPOST() {
        RestResponse res = RestContext.response;
        
        String paymentJSON = RestContext.request.requestBody.toString();
        RemoteSiteModel.PaymentInfo paymentInfoMap = (RemoteSiteModel.PaymentInfo) JSON.deserialize(paymentJSON,RemoteSiteModel.PaymentInfo.class);
        Map<String,String> responseMessageMap = new Map<String,String>();
        String responseMessage;
        String responseApex;
        
        if(paymentInfoMap.gl_service_id == '' || paymentInfoMap.payment_type == '' || paymentInfoMap.payment_status == ''){
            responseMessageMap.put('status:','404');
            responseMessageMap.put('message:','Left gl_service_id,payment_type or payment_status parameter');
            responseMessage = JSON.serialize(responseMessageMap);
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(responseMessage);
        }else{
            responseApex = GLpaymentsHandler.updateGLpayments(paymentInfoMap.gl_service_id,paymentInfoMap.payment_type,paymentInfoMap.payment_status);
            if(responseApex == 'Success'){
                responseMessageMap.put('status:','200');
                responseMessageMap.put('message:','Payment received successfully!');
                responseMessage = JSON.serialize(responseMessageMap);
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf(responseMessage);
            }else if(responseApex.contains('Warning')){
                responseMessageMap.put('status:','404');
                responseMessageMap.put('message:',responseApex.substringAfter(':'));//'GL Service not found');
                responseMessage = JSON.serialize(responseMessageMap);
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf(responseMessage);
            }else if(responseApex == 'Alert'){
                responseMessageMap.put('status:','500');
                responseMessageMap.put('message:','Something went wrong in Salesforce!');
                responseMessage = JSON.serialize(responseMessageMap);
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf(responseMessage);
            }
        }
        /***/
    }
}