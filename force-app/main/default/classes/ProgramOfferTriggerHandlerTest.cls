@IsTest
private class ProgramOfferTriggerHandlerTest {
   
    public static testmethod void afterInsertUpdateTest(){
        Map<Id, Program_Offer__c> emptyMap = new Map<Id, Program_Offer__c>();
        ProgramOfferTriggerHandler.afterInsert(emptyMap, emptyMap);
        ProgramOfferTriggerHandler.afterUpdate(emptyMap, emptyMap);
    }
    
    public static testmethod void createMasterToDosTest(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hP = Util_Test.create_Hosting_Program(acc);
        Master_To_Do_Template__c toDoTemplate = Util_Test.createListMasterToDoTemplate(1)[0];
        insert toDoTemplate;
        Master_To_Do__c masterToDo = new Master_To_Do__c(Name = 'Test',
                                                         Days__c = 4,
                                                         Template__c = toDoTemplate.Id);
        insert masterToDo;
        
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.To_Do_Template__c = toDoTemplate.Id;
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Minors').getRecordTypeId(); //flagship
        Insert po;
        po.To_Do_Template__c = Util_Test.createListMasterToDoTemplate(1)[0].Id;
        update po;
        masterToDo.Program_Offer__c = po.Id;
        update masterToDo;
    }
    
    public static testmethod void emptyInsert(){
        Program_Offer__c po = new Program_Offer__c();
        Test.startTest();
        insert po;
        Test.stopTest();
    }
    
    public static testmethod void updateCost(){
        Program_Offer__c po = new Program_Offer__c(Program_Price_numeric__c = 10, Visa_Fee_numeric__c = 10, Pre_application_Fee_numeric__c = 10);
        Test.startTest();
        insert po;
        Test.stopTest();
        
        system.assertEquals(30, [SELECT Total_Cost_numeric__c FROM Program_Offer__c WHERE Id = :po.Id].Total_Cost_numeric__c);
        
        po.Program_Price_numeric__c = 20;
        update po;
        system.assertEquals(40, [SELECT Total_Cost_numeric__c FROM Program_Offer__c WHERE Id = :po.Id].Total_Cost_numeric__c);
    }
    
    public static testmethod void updateNames(){
        Account acc = new Account(Name = 'Test', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('AFS Partner').getRecordTypeId(), 
                                  IOC_Code__c = 'ABCDE');
        insert acc;
        Hosting_Program__c hp = new Hosting_Program__c(Host_Partner__c = acc.Id, Duration__c = 'YP', Program_Content__c = 'ag');
        insert hp;
        Program_Offer__c po = new Program_Offer__c(Name = 'TEST', Sending_Partner__c = acc.Id, Durations__c = 'YP', Con__c = 'ag', 
                                                   Cycle__c = 'SH', Year__c = '2010', Hosting_Program__c = hp.Id);
        Test.startTest();
        insert po;
        Test.stopTest();
        system.assertEquals('ABCDE-YPagSH10-ABCDE', [SELECT Name FROM Program_Offer__c WHERE Id = :po.Id].Name);
        
        acc.IOC_Code__c = 'TEST';
        update acc;
        update po;
        system.assertEquals('TEST-YPagSH10-TEST', [SELECT Name FROM Program_Offer__c WHERE Id = :po.Id].Name);
    }
    
    public static testmethod void updateFromHostingProgram(){
        Account acc = new Account(Name = 'Test', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('AFS Partner').getRecordTypeId(), 
                                  IOC_Code__c = 'ABCDE');
        insert acc;
        Hosting_Program__c hp = new Hosting_Program__c(Host_Partner__c = acc.Id, Duration__c = 'YP', Program_Content__c = 'ag', App_Received_From__c = Date.today(), App_Received_To__c = Date.today()+5, 
                                                       Return_Date__c = Date.today()+10, Date_of_Birth_From__c = Date.today()-10, Date_of_Birth_To__c = Date.today()-5, Program_Type__c = 'Volunteering',
                                                       Hemisphere__c = 'SH', Language_of_Placement__c = 'Arabic', Year__c = '2010');
        insert hp;
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        pos.add(new Program_Offer__c(Name = 'TEST', Sending_Partner__c = acc.Id, Durations__c = 'YP', Con__c = 'ag', 
                                                   Cycle__c = 'SH', Year__c = '2010', Hosting_Program__c = hp.Id, Flexible_Age_Range__c = 'No'));
        pos.add(new Program_Offer__c(Name = 'TEST', Sending_Partner__c = acc.Id, Durations__c = 'YP', Con__c = 'ag', 
                                                   Cycle__c = 'SH', Year__c = '2010', Hosting_Program__c = hp.Id, Flexible_Age_Range__c = 'No'));
        Test.startTest();
        insert pos;
        Test.stopTest();
        Program_Offer__c poNew = [SELECT Applications_Received_From_local__c, Applications_Received_To_local__c, To__c, Date_of_Birth_From__c, Date_of_Birth_To__c, Con__c, Program_Type__c, 
                                   	 	 Cycle__c, Durations__c, Multiple_Languages_of_Placement__c, Year__c
                                  FROM Program_Offer__c WHERE Id = :pos[0].Id];
        system.assertEquals(poNew.Applications_Received_From_local__c, hp.App_Received_From__c);
        system.assertEquals(poNew.Applications_Received_To_local__c, hp.App_Received_To__c);
        system.assertEquals(poNew.To__c, hp.Return_Date__c);
        system.assertEquals(poNew.Date_of_Birth_From__c, hp.Date_of_Birth_From__c);
        system.assertEquals(poNew.Date_of_Birth_To__c, hp.Date_of_Birth_To__c);
        system.assertEquals(poNew.Con__c, hp.Program_Content__c);
        system.assertEquals(poNew.Program_Type__c, hp.Program_Type__c);
        system.assertEquals(poNew.Cycle__c, hp.Hemisphere__c);
        system.assertEquals(poNew.Durations__c, hp.Duration__c);
        system.assertEquals(poNew.Multiple_Languages_of_Placement__c, hp.Language_of_Placement__c);
        system.assertEquals(poNew.Year__c, hp.Year__c);
        
        
    }
    
    public static testmethod void updateProyectedSendingNumber(){
        List<Program_Offer__c> pos = new List<Program_Offer__c>();
        for(integer i = 0; i<10; i++){
        	pos.add(new Program_Offer__c(Projected_Sending_Number__c = 20));
        }
        test.startTest();
        insert pos;
        test.stopTest();
        
        Set<Id> ids = new Set<Id>();
		for(Program_Offer__c po:pos){
            ids.add(po.Id);
        }        
        List<Program_Offer__c> posNew = [SELECT Updated_Projected_Sending_Number__c FROM Program_Offer__c WHERE Id IN :ids];
        for(Program_Offer__c po :posNew){
            system.assertEquals(20, po.Updated_Projected_Sending_Number__c);
        }
    }
}