trigger SchInProgramTrigger on Sch_in_Program__c (before insert, before update, before delete, after insert, after update, after delete) {
    
    if(Trigger.isAfter){
        if(Trigger.isDelete) {
            SchInProgramTriggerHandler.whenSchInProgramOfferIsDeleted(Trigger.old);
        }        
    }else if(Trigger.isBefore){
            
    }
    
}