trigger ProgramOfferInAppTrigger on Program_Offer_in_App__c (after insert, after update, after delete) {
    if(Trigger.isAfter && Trigger.isInsert){
        ProgramOfferInAppTriggerHandler.afterInsert(Trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        ProgramOfferInAppTriggerHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
    }
    
     if(Trigger.isAfter && Trigger.isDelete){
        ProgramOfferInAppTriggerHandler.afterDelete(Trigger.oldMap);
    }
}