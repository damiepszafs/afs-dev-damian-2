trigger AFS_PardotLogOwnerManager on pi__ObjectChangeLog__c (before insert) {
    try{
        Afs_Triggers_Switch__c swi = Afs_Triggers_Switch__c.getInstance('Afs_Trigger');
        
        //only execute, admin has asked
        if(swi  != null && !swi.By_Pass_Pardot_Object_Log_Trigger__c){    	
            Map<Id, String> mapGuestCRM = new Map<Id, String>();
            Set<String> setIdsContacts = new Set<String>();       
            Map<Id, Contact> mapContacts = new Map<Id, Contact>();
            List<User> lstUser = [SELECT Id FROM User WHERE Name LIKE 'AFS Social Sign On%'];
            Set<String> setIOCCodes = new Set<String>{'ITA'};
            Map<Id,AFS_Community_Configuration__c> mapSendPartnerIdConfig = new Map<Id,AFS_Community_Configuration__c>();
            Map<String,Id> mapIOCCode_PardotUserId = new Map<String,Id>();  
            Set<Id> setOwnerIds = new Set<Id>();
            Map<Id,User> mapUserOwner = new Map<Id,User>();
            
            /***Retrieve required INFO - START***/
            for(AFS_Community_Configuration__c config : [SELECT Id, Community_Guest_User_Owner__c,CRMUserOwner__c, Pardot_IOC_Default_User_Id__c, Afs_SendingPartner__c
                                                         FROM AFS_Community_Configuration__c
                                                         WHERE (Community_Guest_User_Owner__c != '' 
                                                                AND CRMUserOwner__c != '')
                                                         OR (Pardot_IOC_Default_User_Id__c != '' AND
                                                            Afs_SendingPartner__c != '')]){
                                                             
                if(config.Community_Guest_User_Owner__c != '' && config.CRMUserOwner__c != '' && !mapGuestCRM.containsKey((Id)(config.Community_Guest_User_Owner__c))){
                     mapGuestCRM.put((Id)(config.Community_Guest_User_Owner__c), config.CRMUserOwner__c);
                }
                //Fill map Sending Partner Id --> Community Config Record                                             
                if(config.Pardot_IOC_Default_User_Id__c != '' && config.Afs_SendingPartner__c != ''){
                    mapSendPartnerIdConfig.put((Id)config.Afs_SendingPartner__c,config);                                                 
                }
            }
            system.debug('#####mapGuestCRM: ' + mapGuestCRM + ', Record: ' + trigger.New[0].ownerId);
            
            //Retrieve IOC Code of Sending Partner of the specific IOC
            if(!mapSendPartnerIdConfig.isEmpty()){
                for(Account acc : [SELECT IOC_Code__c FROM Account WHERE Id in :mapSendPartnerIdConfig.keyset() AND IOC_Code__c in :setIOCCodes]){
                    AFS_Community_Configuration__c cc = mapSendPartnerIdConfig.get(acc.Id);
                    //Fill map IOC_Code --> Pardot User Id
                    mapIOCCode_PardotUserId.put(acc.IOC_Code__c,(Id)cc.Pardot_IOC_Default_User_Id__c);
                }
            }            
           	/***Retrieve required INFO - END***/
            
            /***Filter - START***/            
            for(pi__ObjectChangeLog__c log : trigger.New){
                 //Get Contacts Ids of records created by 'AFS Social Sign On' User
                if(lstUser.size() > 0){
                    if(log.OwnerId == lstUser[0].Id && ((String)log.pi__ObjectFid__c).startsWith('003')){
                        setIdsContacts.add(log.pi__ObjectFid__c);
                    }
                }
                //Fill Set setOwnerIds
                setOwnerIds.add(log.OwnerId);
            }
            /***Filter - END***/
            
            /***Retrieve more INFO - START***/
            //Get Contacts
            if(!setIdsContacts.isEmpty()){
                mapContacts = new Map<Id,Contact>([SELECT OwnerId FROM Contact WHERE Id in :setIdsContacts]);
            }
            
            if(!setOwnerIds.isEmpty()){
                mapUserOwner = new map<Id,User>([SELECT Id, IOC_Code__c FROM User WHERE Id in :setOwnerIds AND IOC_Code__c in :setIOCCodes]);
            }
          	/***Retrieve more INFO - END***/
            
    		/***Process - START***/
            for(pi__ObjectChangeLog__c log : trigger.New){
                //If Created User is on specific IOC Code change owner to Default
                if(mapUserOwner.ContainsKey(log.OwnerId)){
                    User usr = mapUserOwner.get(log.OwnerId);
                    if(mapIOCCode_PardotUserId.containsKey(usr.IOC_Code__c)){
                        log.ownerId = mapIOCCode_PardotUserId.get(usr.IOC_Code__c);
                    }                
                //change owner of pardot log from guest to crm owner    
                }else if(mapGuestCRM.containsKey(log.ownerId)){
                    log.ownerId = mapGuestCRM.get(log.ownerId);
                //change owner of pardot log from Afs Social Sign On to contact owner  
                }else if(mapContacts.containsKey(log.pi__ObjectFid__c)){
                    log.ownerId = mapContacts.get(log.pi__ObjectFid__c).OwnerId;
                }
            }
            /***Process - END***/
            system.debug('#####Records: ' + trigger.New[0]);
        }
    }catch(Exception ex){
        system.debug(logginglevel.error, ex.getMessage());
    }
}