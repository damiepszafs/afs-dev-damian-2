trigger PaymentEntryTriggerMaster on Payment_Entry__c (after insert) {

  PaymentInstallmentsTriggerHandler.process();

}