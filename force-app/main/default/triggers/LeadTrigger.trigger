trigger LeadTrigger on Lead (before insert) {
    If(Trigger.isBefore){
        if(Trigger.isInsert) LeadTriggerHandler.beforeInsert(Trigger.new);
    }
}