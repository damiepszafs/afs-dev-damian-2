trigger Afs_Application_Trigger on Application__c (after insert,after update ,after delete) {
	
 	Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
    if(!afsTriggerSwitch.By_Pass_Application_Trigger__c){
        //Variable passes for static list of trigger
        Afs_App_Opportunity_SyncTrigger_Helper.applicationNewList = trigger.new;
        Afs_App_Opportunity_SyncTrigger_Helper.applicationOldList = trigger.old;
        Afs_App_Opportunity_SyncTrigger_Helper.applicationNewMap = trigger.newMap;
        Afs_App_Opportunity_SyncTrigger_Helper.applicationOldMap = trigger.oldMap;
        if(!Afs_App_Opportunity_SyncTrigger_Helper.isTriggerRunning){
            if(trigger.isAfter){
                if( trigger.isInsert || trigger.isUpdate){
                    Afs_App_Opportunity_SyncTrigger_Helper.Afs_App_Opportunity_SyncTrigger_SyncOpp();
                }else if(trigger.isDelete){
                        Afs_App_Opportunity_SyncTrigger_Helper.Afs_App_Opportunity_SyncTrigger_SyncOpp_Del();
                } 
            }
        }else{
            Afs_UtilityApex.logger('Trigger is already running');
        }    
    }else{
         Afs_UtilityApex.logger('B=Trigger is by passesd by  custom setting');
    }
    
}