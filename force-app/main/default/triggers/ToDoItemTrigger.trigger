trigger ToDoItemTrigger on To_Do_Item__c (after insert, after update, after delete) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            ToDoItemTriggerHelper.afterInsert(Trigger.newMap);
        }else if(Trigger.isUpdate){
            ToDoItemTriggerHelper.afterUpdate(Trigger.newMap,Trigger.oldMap);
        }else if(Trigger.isDelete){
           ToDoItemTriggerHelper.afterDelete(Trigger.oldMap); 
        }
    }
}