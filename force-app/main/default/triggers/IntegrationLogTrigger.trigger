trigger IntegrationLogTrigger on IntegrationLog__c (after Insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        IntegrationLogTriggerHandler.afterInsert(Trigger.newMap);
    }
}