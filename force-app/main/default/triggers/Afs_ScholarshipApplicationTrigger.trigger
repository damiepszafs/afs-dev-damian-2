trigger Afs_ScholarshipApplicationTrigger on Scholarship_Application__c (after insert,after update ,after delete) {
    // Get Trigger Switch record to check whether trigger is disbaled or not
	Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
    if(!afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c ){
        //Variable passes for static list of trigger
        Afs_ScholarshipApplicationTriggerHandler.triggerNew = trigger.new;
        Afs_ScholarshipApplicationTriggerHandler.triggerOld = trigger.old;
        Afs_ScholarshipApplicationTriggerHandler.triggerNewMap = trigger.newMap;
        Afs_ScholarshipApplicationTriggerHandler.triggerOldMap = trigger.oldMap;
        if(!Afs_ScholarshipApplicationTriggerHandler.isTriggerRunning){
            if(trigger.isAfter){
                if( trigger.isInsert){
                    Afs_ScholarshipApplicationTriggerHandler.afterInsert();
                }else if(trigger.isUpdate){
                    Afs_ScholarshipApplicationTriggerHandler.afterUpdate();
                }else if(trigger.isDelete){
                    Afs_ScholarshipApplicationTriggerHandler.afterDelete();
                } 
            }
        }else{
            Afs_UtilityApex.logger('Trigger is already running');
        }    
    }else{
         Afs_UtilityApex.logger('ByPassed Trigger is by passed by  custom setting');
    }
}