trigger ContactTrigger on Contact (before insert, after update, before update) {
    if(Trigger.isBefore && Trigger.isInsert){
        ContactTriggerHandler.beforeInsert(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isUpdate){
        ContactTriggerHandler.afterUpdate(Trigger.oldMap,Trigger.newMap);
    }else if(Trigger.isBefore && Trigger.isUpdate){
        ContactTriggerHandler.beforeUpdate(Trigger.oldMap,Trigger.newMap);
    }
}