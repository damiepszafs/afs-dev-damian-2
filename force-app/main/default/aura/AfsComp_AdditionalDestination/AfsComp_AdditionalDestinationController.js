({
    searchdestination1 : function(component, event, helper) {        
        var wrapperList = component.get("v.destinationWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.destinationSearch1");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            /*else
            {
                component.set("v.destinationWrapper",wrapperList); 
            }*/
        }
        component.set("v.destinationWrappercopy1",newwrapperList);        
    },
    
    searchdestination2 : function(component, event, helper) {        
        var wrapperList = component.get("v.destinationWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.destinationSearch2");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            /*else
            {
                component.set("v.destinationWrapper",wrapperList); 
            }*/
        }
        component.set("v.destinationWrappercopy2",newwrapperList);        
    },
    
    searchdestination3 : function(component, event, helper) {        
        var wrapperList = component.get("v.destinationWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.destinationSearch3");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            /*else
            {
                component.set("v.destinationWrapper",wrapperList); 
            }*/
        }
        component.set("v.destinationWrappercopy3",newwrapperList);        
    },
    
    toggleDestinationView1 : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandDestination1");
        var wrapperList = component.get("v.destinationWrapper");
        var counterSelected = 0
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].isSelected) {                
                counterSelected++;     
            }
            
        }
        if(counterSelected != wrapperList.length){
            if(toggleView === "true"){
                component.set("v.expandDestination1","false");
            }else{
                component.set("v.expandDestination1","true");
                component.set("v.expandDestination2","false");
				component.set("v.expandDestination3","false");
            }
        }else{
            component.set("v.expandDestination1","false");
            component.set("v.expandDestination2","false");
            component.set("v.expandDestination3","false");
        }
        
    },
    
    toggleDestinationView2 : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandDestination2");
        var wrapperList = component.get("v.destinationWrapper");
        var counterSelected = 0
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].isSelected) {                
                counterSelected++;     
            }
            
        }
        if(counterSelected != wrapperList.length){
            if(toggleView === "true"){
                component.set("v.expandDestination2","false");
            }else{
                component.set("v.expandDestination2","true");
                
                component.set("v.expandDestination1","false");
				component.set("v.expandDestination3","false");
            }
        }else{
            component.set("v.expandDestination2","false");
        }
        
    },
    
    toggleDestinationView3 : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandDestination3");
        var wrapperList = component.get("v.destinationWrapper");
        var counterSelected = 0
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].isSelected) {                
                counterSelected++;     
            }
            
        }
        if(counterSelected != wrapperList.length){
            if(toggleView === "true"){
                component.set("v.expandDestination3","false");
            }else{
                component.set("v.expandDestination3","true");
                
                component.set("v.expandDestination2","false");
				component.set("v.expandDestination1","false");
            }
        }else{
            component.set("v.expandDestination3","false");
        }
        
    },
    
    onDestinationChange1: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.destinationSearch1");
        var wrapperList = component.get("v.destinationWrapper");
        wrapperList[index-1].isSelected = true; 
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.expandDestination1","false");
        component.set("v.addDestination1",wrapperList[index-1]);                      
        component.set("v.destinationSearch1", '');
    },
    
    onDestinationChange2: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.destinationSearch2");
        var wrapperList = component.get("v.destinationWrapper");
        wrapperList[index-1].isSelected = true; 
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.expandDestination2","false");
        component.set("v.addDestination2",wrapperList[index-1]);                      
        component.set("v.destinationSearch2", '');
    },
    
    onDestinationChange3: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.destinationSearch3");
        var wrapperList = component.get("v.destinationWrapper");
        wrapperList[index-1].isSelected = true; 
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.expandDestination3","false");
        component.set("v.addDestination3",wrapperList[index-1]);;                      
        component.set("v.destinationSearch3", '');
    },
    
    onRemoveDestinationPill1 : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");  
        var wrapperList = component.get("v.destinationWrapper"); 
        wrapperList[pillId-1].isSelected = false;
        component.set("v.expandDestination1","true");
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.addDestination1",null);
    },
    
    onRemoveDestinationPill2 : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId"); 
        var wrapperList = component.get("v.destinationWrapper"); 
        wrapperList[pillId-1].isSelected = false;
        component.set("v.expandDestination2","true");
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.addDestination2",null);
    },
    
    onRemoveDestinationPill3 : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");
        var wrapperList = component.get("v.destinationWrapper"); 
        wrapperList[pillId-1].isSelected = false;
        component.set("v.expandDestination3","true");
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.addDestination3",null);
    },
    
    checkSaveValid : function(component,evt) {
        
        var addDestination1 = component.get("v.addDestination1");
        var addDestination2 = component.get("v.addDestination2");
        var addDestination3 = component.get("v.addDestination3");
        var isSave = true;
        
        if(addDestination1 && addDestination2 && addDestination3){
            isSave = false;
        }else{
            isSave = true; 
        }
        
        // disable enable save button based on criteria
        // 1. Required values are selected
        component.set("v.isSaveValid",isSave);
    } 
})