({
	getDynamicCustomLabelVal :  function (labelStr,component){
        if(labelStr != undefined && labelStr != "" ){
            var labelReference = $A.getReference("$Label.c." + labelStr);
        	component.set("v.LabelTempVal", labelReference);
        }else{
            component.set("v.LabelTempVal", "");
        }
    },
    
    getFiles : function(component, event) {
		var action = component.get("c.getFiles");
        var record = component.get("v.applicationWrapper");
        action.setParams({ recId :  record.applicationObj.Id});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var lstFiles = response.getReturnValue();
                component.set("v.files",lstFiles);
                if(lstFiles === undefined || lstFiles.length == 0){
        			component.set("v.isFileAdded",false);                
           	 	}else{
                	component.set("v.isFileAdded",true);  //file present
                }
                 component.set("v.Spinner", false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);                       
    },
    
    getDownloadFormUrl : function(component, event,fileId) {
		
		var action = component.get("c.getDownloadForm");
        action.setParams({ recId :  fileId});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(fileId != undefined && fileId != ""){
                    component.set("v.fileDownloadUrl", response.getReturnValue()[0]);
                     component.set("v.fileDownloadName", response.getReturnValue()[1]);
                }else{
                    component.set("v.fileDownloadUrl", "");
                }
                component.set("v.Spinner", false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);                       
    },
    
    deleteFileAndUpdateList : function(component, event) {
        var fId = event.currentTarget.getAttribute('data-fileId');
        //console.log(fId);
        var action = component.get("c.deleteFileSF");        
        action.setParams(
            { fileId : fId,
              recId : component.get("v.record.item")
            }
        );
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var lstFiles = [];
                var uploadedFiles = component.get("v.files");
                for(var i  = 0 ; i< uploadedFiles.length ; i ++){
                    if(uploadedFiles[i].ContentDocumentId != fId){
                        lstFiles.push(uploadedFiles[i]);
                    }
                }
                if(lstFiles.length == 0){
                    component.set("v.isFileAdded",false);                
                }else{
                    component.set("v.isFileAdded",true);  //file present
                }
                component.set("v.files",lstFiles);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File deleted."
                });
                toastEvent.fire();    
                component.set("v.Spinner", false);
                
                var record = component.get("v.record");
                record.item = response.getReturnValue();
                component.set("v.record",record);
            }else{                
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner", false);
            }           
        });
        $A.enqueueAction(action);
    },
    
    doUpdateDocumentLink : function(component, event,fileId) {
		var record = component.get("v.record");
		var action = component.get("c.updateDocumentLink");
        action.setParams({ 
            contentDocumentIds :  fileId,
            toDoItem : record.item
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(fileId != undefined && fileId != ""){
                    record.item.File_link__c = response.getReturnValue();
                    component.set("v.record",record);
                }
                component.set("v.Spinner", false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);                       
    }
})