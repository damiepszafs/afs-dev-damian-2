({
	OnInit : function(component,event) {
		var action =  component.get("c.getFeed");
        var  applicationObj = component.get("v.applicationWrapper.applicationObj");
        
        action.setParams({
            "ApplicationId" : applicationObj.Id,
            "ownerApplication" : applicationObj.OwnerId,
            "contactId" : applicationObj.Applicant__c
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {   
                var value = response.getReturnValue();
                component.set('v.feedWrapperList',value);
                component.set("v.Spinner",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }
        });  
        $A.enqueueAction(action);
    },
    
    OnInsertFeed : function(component,event) {
		var action =  component.get("c.insertFeed");
        var  applicationObj = component.get("v.applicationWrapper.applicationObj");
        
        action.setParams({
            "ApplicationId" : applicationObj.Id,
            "body" : component.get("v.feedItem"),
            "contactId" : applicationObj.Applicant__c
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.feedItem","");
                this.OnInit(component,event);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }
        });  
        $A.enqueueAction(action);
    },
    
    OnInsertComment : function(component,event,feedId,comment) {
		var action =  component.get("c.insertComment");
        action.setParams({
            "feedId" : feedId,
            "body" : comment
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {
                this.OnInit(component,event);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }
        });  
        $A.enqueueAction(action);
    },
    
    OnLikeFeed : function(component,event,feedId) {
		var action =  component.get("c.likeFeed");
        
        action.setParams({
            "feedId" : feedId
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {
                this.OnInit(component,event);
            }else{
                component.set("v.Spinner",false);
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                
            }
        });  
        $A.enqueueAction(action);
    }
	
})