({
	doInit : function(component, event, helper) {
        component.set("v.Spinner","true");
        var wrapperObj = component.get("v.applicationWrapper");
        component.set("v.None_of_the_above",wrapperObj.applicationObj.None_of_the_above__c);
        component.set("v.I_ve_lived_abroad",wrapperObj.applicationObj.I_ve_lived_abroad__c);
        component.set("v.I_ve_traveled_abroad",wrapperObj.applicationObj.I_ve_traveled_abroad__c);
        component.set("v.I_ve_participated_in_another_exchange_p",wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c);
        component.set("v.My_family_hosted_an_AFS_student",wrapperObj.applicationObj.My_family_hosted_an_AFS_student__c);
        component.set("v.A_family_member_went_abroad_with_AFS",wrapperObj.applicationObj.A_family_member_went_abroad_with_AFS__c);
        var selectedVal = wrapperObj.applicationObj.What_are_you_looking_for__c ;
        component.set("v.selectedValuesExperience",selectedVal);
        var selectedValNew = wrapperObj.applicationObj.How_would_friends_family_describe_you__c;
        component.set("v.selectedValuesFamilyDesc",selectedValNew);
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_DescribeYourself__c  ,component);
        component.set("v.AfsLbl_AboutYou_DescribeYourself",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_ExperienceHeader__c   ,component);
        component.set("v.AfsLbl_AboutYou_ExperienceHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_ExperienceTextAreaHeader__c    ,component);
        component.set("v.AfsLbl_AboutYou_ExperienceTextAreaHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_ExperienceTxtAreaHeadHlpTxt__c    ,component);
        component.set("v.AfsLbl_AboutYou_ExperienceTextAreaHeaderHelpTxt",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_ExperienceHeaderHelpTxt__c    ,component);
        component.set("v.AfsLbl_AboutYou_ExperienceHeaderHelpTxt",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_FamilyDecribeYouHeader__c    ,component);
        component.set("v.AfsLbl_AboutYou_FamilyDecribeYouHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_PageHeader__c    ,component);
        component.set("v.AfsLbl_AboutYou_PageHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_PageSubHeader__c    ,component);
        component.set("v.AfsLbl_AboutYou_PageSubHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_FamilyDescYouHeaderHelpTxt__c     ,component);
        component.set("v.AfsLbl_AboutYou_FamilyDecribeYouHeaderHelpTxt",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_ExperienceWithAfsHeader__c     ,component);
        component.set("v.AfsLbl_AboutYou_ExperienceWithAfsHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.AboutYou_Txt_SubmitButton__c,component);
        component.set("v.AfsLbl_AboutYou_SubmitBtn",component.get("v.LabelTempVal"));
        window.scrollTo(0,0);
        component.set("v.Spinner","false");
        helper.evaluateFlagshipPrograms(component);
	},
    
    updateExperience : function (component, event, helper) {
        var selectedVal = component.get("v.selectedValuesExperience");
        if(selectedVal === undefined){
            selectedVal = "";
        }
        if(selectedVal.length > 0 && selectedVal[selectedVal.length - 1] != ";"){
            selectedVal += ";";
        }
        if(event.currentTarget.className == "testDummy"){
            if(selectedVal.indexOf(event.currentTarget.id) <= -1){
                selectedVal += event.currentTarget.id + ";";
            } 
            event.currentTarget.className = "active";
        }else{
            selectedVal = selectedVal.replace(event.currentTarget.id + ";","");
            event.currentTarget.className = "testDummy";
        }
        component.set("v.selectedValuesExperience",selectedVal);
    },
    
    updateFamilyDesc : function (component, event, helper) {
        
        var selectedVal = component.get("v.selectedValuesFamilyDesc");
        if(selectedVal === undefined){
            selectedVal = "";
        }
        if(selectedVal.length > 0 && selectedVal[selectedVal.length - 1] != ";"){
            selectedVal += ";";
        }
        if(event.currentTarget.className == "testDummy"){
            if(selectedVal.indexOf(event.currentTarget.id) <= -1){
                selectedVal += event.currentTarget.id + ";";
            } 
            event.currentTarget.className = "active";
        }else{
            selectedVal = selectedVal.replace(event.currentTarget.id + ";","");
            event.currentTarget.className = "testDummy";
        }
        component.set("v.selectedValuesFamilyDesc",selectedVal);
    },
    
    submitApplicaiton : function (component, event, helper) {
        component.set("v.Spinner","true");
        var selectedValFamily = component.get("v.selectedValuesFamilyDesc");
        var selectedVal = component.get("v.selectedValuesExperience");
        var wrapperObj = component.get("v.applicationWrapper");
        var configWrapper = component.get("v.configWrapper");
        var ContactInfo = component.get("v.ContactInfo");
        wrapperObj.applicationObj.None_of_the_above__c = component.get("v.None_of_the_above");
        wrapperObj.applicationObj.I_ve_lived_abroad__c = component.get("v.I_ve_lived_abroad");
        wrapperObj.applicationObj.I_ve_traveled_abroad__c = component.get("v.I_ve_traveled_abroad");
        wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c = component.get("v.I_ve_participated_in_another_exchange_p");
        wrapperObj.applicationObj.My_family_hosted_an_AFS_student__c = component.get("v.My_family_hosted_an_AFS_student");
        wrapperObj.applicationObj.A_family_member_went_abroad_with_AFS__c = component.get("v.A_family_member_went_abroad_with_AFS");
        if(wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c == false || wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c == "false"){
            wrapperObj.applicationObj.Program_you_participated__c = "";
        }
        var hasflagshipProgram = component.get("v.flagshipProgramPreSelected");
        var messageTemplateStr;
        var messageTemplateStrPD;
        var messageTemplateStrAY;
        // Code added to add all validation for 
        // Select A Program , Your Application and About You 
        // Together
        // --------------------------------------------------
        // SELECT A PROGRAM Validations
        // --------------------------------------------------
        // Check for Require Condtion
        if(!component.get("v.atLeastOneSelectedProgram")){
            messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectAPrgmSectionHeader") + "\n";
            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SelectOnePrgmMessage");
        }
        
        if(configWrapper.SelectAProgram_Fld_AdditionalDestn1__c == "Required" && (component.get("v.addDestination1") == null || component.get("v.addDestination1") === undefined)){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination1Req");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectAPrgmSectionHeader") + "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination1Req");
            }
        }
        
        if(configWrapper.SelectAProgram_Fld_AdditionalDestn2__c == "Required" && (component.get("v.addDestination2") == null || component.get("v.addDestination2") === undefined)){
			if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination2Req");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectAPrgmSectionHeader") + "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination2Req");
            }
        }
        
        if(configWrapper.SelectAProgram_Fld_AdditionalDestn3__c == "Required" && (component.get("v.addDestination3") == null || component.get("v.addDestination3") === undefined)){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination3Req");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectAPrgmSectionHeader") + "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination3Req");
            }
        }
        // --------------------------------------------------
        // PERSONAL DETAILS Validations
        // --------------------------------------------------
        // Check for Require Condtion
        var isAddressErrorAdded =  false;
        if(configWrapper.PersonalDetail_Fld_StreetAddress__c == 'Required'){
        	if((component.get("v.addressLine1") === undefined || component.get("v.addressLine1") == "")){
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_AddressRequire");
                isAddressErrorAdded = true;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Apartment__c == 'Required' && !isAddressErrorAdded){
        	if((component.get("v.addressLine2") === undefined || component.get("v.addressLine2") == "")){
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_AddressRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressCity__c == 'Required'){
        	if(ContactInfo.MailingCity === undefined || ContactInfo.MailingCity == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressState__c == 'Required'){
            if(ContactInfo.MailingState === undefined || ContactInfo.MailingState == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PinCode__c == 'Required'){
        	if(ContactInfo.MailingPostalCode === undefined || ContactInfo.MailingPostalCode == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }
            }
        }
        
        
        
        if(configWrapper.PersonalDetail_Fld_MobileNumber__c == 'Required'){
            if(ContactInfo.MobilePhone === undefined || ContactInfo.MobilePhone == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }
            }
        }
        
        if(ContactInfo.MobilePhone === 'invalid') {
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PhoneNumber__c == 'Required'){
        	if(ContactInfo.Phone === undefined || ContactInfo.Phone == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }
            }
        }
        
        if(ContactInfo.Phone === 'invalid') {
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_LegalCountry__c == 'Required'){
            if(ContactInfo.Country_of_Legal_Residence__c === undefined ||
               ContactInfo.Country_of_Legal_Residence__c == "" ||
               ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }
            }
        }
        
        if(ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
            ContactInfo.Country_of_Legal_Residence__c = "";
        }
        
        
        
        if(configWrapper.PersonalDetail_Fld_DOB__c == 'Required'){
            if(ContactInfo.Birthdate === undefined || ContactInfo.Birthdate == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }
            }
        }
        
        if(ContactInfo.Birthdate != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(ContactInfo.Birthdate) > currentDate){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }
            }
            
            var ContactInfo = component.get("v.ContactInfo");
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            var birthdate = new Date(ContactInfo.Birthdate);
            var diff =(currentDate.getTime() - birthdate.getTime()) / 1000;
            diff /= (60 * 60 * 24);
            var result = (diff/365.25);
            component.set("v.isAdult", result > 18);
        }
        
        
        
        if(ContactInfo.Gender__c === undefined || 
           ContactInfo.Gender__c == "" ||
           ContactInfo.Gender__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectGender")){
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Nationality__c == 'Required'){
            if(ContactInfo.NationalityCitizenship__c === undefined ||
               ContactInfo.NationalityCitizenship__c == "" ||
               ContactInfo.NationalityCitizenship__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectNationality")){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Nationality");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Nationality");
                }
            }
        }
        
        var isAdult = component.get("v.isAdult");
        var isParentError = false;
        if(configWrapper.PersonalDetail_Fld_ParentGuardian__c == 'Required'){
            if(messageTemplateStrPD != undefined){
                var tempMsg = helper.validatedParentComp(component, event);
                if(tempMsg != undefined && tempMsg != ""){
                    messageTemplateStrPD += "\n";
                    isParentError = true;
                    messageTemplateStrPD += helper.validatedParentComp(component, event);
                }                
            }else{
                var tempMsg = helper.validatedParentComp(component, event);
                if(tempMsg != undefined && tempMsg != ""){
                    isParentError = true;
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                	messageTemplateStrPD += tempMsg;
                }
            }  
        }
        
        /*if(configWrapper.PersonalDetail_Fld_WhatSchoolAttended__c == 'Required'){    
            if(ContactInfo.What_school_do_you_attend__c === undefined || ContactInfo.What_school_do_you_attend__c == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_SchoolTypeRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_SchoolTypeRequired");
                }
            }
        }
        
        if(ContactInfo.What_school_do_you_attend__c == $A.get("$Label.c.AfsLbl_YourApplication_HighSchool")){
            if(configWrapper.PersonalDetail_Fld_School__c == 'Required'){
                if(component.get("v.SchoolAttendedWrapperNew") === undefined || component.get("v.SchoolAttendedWrapperNew") == "" || component.get("v.SchoolAttendedWrapperNew") == null){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
                    }
                } 
            }
        }
        
        /*if(configWrapper.PersonalDetail_Fld_WhenGraduate__c == 'Required'){ 
            if(ContactInfo.When_would_did_you_graduate__c === undefined || ContactInfo.When_would_did_you_graduate__c == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_GraduationDateRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_GraduationDateRequired");
                }
            }
        }
        
        if(ContactInfo.When_would_did_you_graduate__c != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(ContactInfo.When_would_did_you_graduate__c) > currentDate){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_GraduateDateNoFuture");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_GraduateDateNoFuture");
                }
            }
        }*/
        
        if(configWrapper.PersonalDetail_Fld_DescribeStudent__c == 'Required' && hasflagshipProgram){ 
            if(ContactInfo.Describe_yourself_as_a_student__c === undefined || ContactInfo.Describe_yourself_as_a_student__c == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DescriptionStudentRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DescriptionStudentRequired");
                }
            }
        }
        
        if(isAdult){
            /*if(ContactInfo.AAre_you_attending_University_College_o__c === undefined || ContactInfo.AAre_you_attending_University_College_o__c == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_AttendingCOllgeRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AttendingCOllgeRequired");
                }
            }*/
            if(ContactInfo.AAre_you_attending_University_College_o__c == 'Yes'){
                if(configWrapper.PersonalDetail_Fld_NameUniversity__c == 'Required'){
                    if(ContactInfo.Name_of_University_College_School__c === undefined || ContactInfo.Name_of_University_College_School__c == ""){
                        if(messageTemplateStrPD != undefined){
                            messageTemplateStrPD += "\n";
                            messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CollegeNameRequired");
                        }else{
                            messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                            messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_CollegeNameRequired");
                        }
                    }
                }
            }
        }
        
        
            
            
        
        
        
        if(ContactInfo.Language_1__c === undefined || ContactInfo.Language_1__c == "" || ContactInfo.Language_1__c == "None"){
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language1Required");
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language1Required");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Language1Prof__c == 'Required'){
            if(ContactInfo.Language_1_Proficiency__c === undefined || ContactInfo.Language_1_Proficiency__c == ""){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language1ProfRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language1ProfRequired");
                }
            }
        }
        
        if(component.get("v.SecondLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language2Speak__c == 'Required'){
                if(ContactInfo.Language_2__c === undefined || ContactInfo.Language_2__c == "" || ContactInfo.Language_2__c == "None"){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language2Required");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language2Required");
                    }
                }
            }
            if(configWrapper.PersonalDetail_Fld_Language2Prof__c == 'Required'){
                if(ContactInfo.Language_2_Proficiency__c === undefined || ContactInfo.Language_2_Proficiency__c == ""){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language2ProfRequired");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language2ProfRequired");
                    }
                }
            }
        }
        
        if(component.get("v.ThirdLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language3Speak__c == 'Required'){
                if(ContactInfo.Language_3__c === undefined || ContactInfo.Language_3__c == "" || ContactInfo.Language_3__c == "None"){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language3Required");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language3Required");
                    }
                }
            }
            
            if(configWrapper.PersonalDetail_Fld_Language3Prof__c == 'Required'){
                if(ContactInfo.Language_3_Proficiency__c === undefined || ContactInfo.Language_3_Proficiency__c == ""){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language3ProfRequired");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language3ProfRequired");
                    }
                }
            }
        }
        
        if(component.get("v.FourthLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language4Speak__c == 'Required'){
                if(ContactInfo.Language_4__c === undefined || ContactInfo.Language_4__c == "" || ContactInfo.Language_4__c == "None"){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language4Required");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language4Required");
                    }
                }
            }
            if(configWrapper.PersonalDetail_Fld_Language4Prof__c == 'Required'){
                if(ContactInfo.Language_4_Proficiency__c === undefined || ContactInfo.Language_4_Proficiency__c == ""){
                    if(messageTemplateStrPD != undefined){
                        messageTemplateStrPD += "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language4ProfRequired");
                    }else{
                        messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                        messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_Language4ProfRequired");
                    }
                }
            }
        }
        
        if(isAdult == true || isAdult == "true"){
           /* if(ContactInfo.Criminal_Convictions__c === undefined || ContactInfo.Criminal_Convictions__c == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CriminalConictionRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CriminalConictionRequired");
                }
            } */
        }
        
        
        if(configWrapper.PersonalDetail_Txt_PhysicalRestrictions__c == 'Required'){
            var flag = false;
            if(configWrapper.PersonalDetail_Fld_NoRestrictionsOrAlerg__c != 'Hide'){
                flag = ContactInfo.No_restrictions_or_allergies__c;
            }
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_HaveAlergies__c != 'Hide'){
                    flag = ContactInfo.Have_allergies__c;
                } 
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_CantLiveWithPets__c != 'Hide'){
                    flag = ContactInfo.Can_t_live_with_pets_s__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_LimitedIntheActivity__c != 'Hide'){
                    flag = ContactInfo.Limited_in_the_activities_I_can_do__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_TakeMedications__c != 'Hide'){
                    flag = ContactInfo.Take_medications__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_CantLiveWithSmokers__c != 'Hide'){
                    flag = ContactInfo.Can_t_live_with_a_smoker__c;
                } 
            }
            
            
            if(flag === undefined || flag == false){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PhysicalRestrictionsRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_PhysicalRestrictionsRequired");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Txt_DieteryRestrictions__c == 'Required'){
            var flag = false;
            
            
            if(configWrapper.PersonalDetail_Fld_NoDieteryRestrictions__c != 'Hide'){
                flag = ContactInfo.No_dietary_restrictions__c;
            }
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Vegetarian__c != 'Hide'){
                    flag = ContactInfo.Vegetarian__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Kosher__c != 'Hide'){
                    flag = ContactInfo.Kosher__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Celiac__c != 'Hide'){
                    flag = ContactInfo.Celiac__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_HaveFoodAllergies__c != 'Hide'){
                    flag = ContactInfo.Have_food_allergies__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Vegan__c != 'Hide'){
                    flag = ContactInfo.Vegan__c;
                } 
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Halal__c != 'Hide'){
                    flag = ContactInfo.Halal__c;
                }
            }
            
            
            if(flag === undefined || flag == false){
                if(messageTemplateStrPD != undefined){
                    messageTemplateStrPD += "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DieteryRestrictRequired");
                }else{
                    messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                    messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DieteryRestrictRequired");
                }
            }
        }
        
        if(ContactInfo.treated_for_any_health_issues_physical__c === undefined || ContactInfo.treated_for_any_health_issues_physical__c == ""){
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_HealthIssueRequired");
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_HealthIssueRequired");
            }
        }
        
        if(ContactInfo.Do_you_smoke__c === undefined || ContactInfo.Do_you_smoke__c == ""){
            if(messageTemplateStrPD != undefined){
                messageTemplateStrPD += "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DoYouSmokeRequired");
            }else{
                messageTemplateStrPD = "\n" + $A.get("$Label.c.AfsLbl_Error_PersonalSectionHeader") + "\n";
                messageTemplateStrPD += $A.get("$Label.c.AfsLbl_Error_DoYouSmokeRequired");
            }
        }
        
        // --------------------------------------------------
        // ABOUT YOU Validations
        // --------------------------------------------------
        //Check for require condtions
        if(configWrapper.AboutYou_Fld_ExperienceHeader__c == "Required"){
            if(selectedVal === undefined){
                    messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SelectOneExpirenceVal");
            }
        }
        
        if(configWrapper.AboutYou_Fld_ExperienceTextAreaHeader__c == "Required"){
            if(wrapperObj.applicationObj.What_do_you_want_out_of_this_experience__c === undefined || wrapperObj.applicationObj.What_do_you_want_out_of_this_experience__c == "")
            {
                if(messageTemplateStrAY != undefined){
                    messageTemplateStrAY += "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_WriteAboutExperience");
                }else{
                    messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_WriteAboutExperience");
                }
               	
            }
        }
        
        if(selectedValFamily === undefined && hasflagshipProgram){
            if(messageTemplateStrAY != undefined){
                messageTemplateStrAY += "\n";
                messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SelectOneFamilySayAboutVal");
            }else{
                messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SelectOneFamilySayAboutVal");
            }
            
        }
        
        if(configWrapper.AboutYou_Fld_DescribeYourself__c == "Required"){
            if(wrapperObj.applicationObj.Tell_us_about_yourself__c === undefined || wrapperObj.applicationObj.Tell_us_about_yourself__c == ""){
                if(messageTemplateStrAY != undefined){
                    messageTemplateStrAY += "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_WriteAboutYourself");
                }else{
                    messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_WriteAboutYourself");
                }
               	
            }
        }
        
        
        
        if(configWrapper.AboutYou_Fld_ExperienceWithAfsHeader__c == "Required"){
            if(wrapperObj.applicationObj.A_family_member_went_abroad_with_AFS__c == false && wrapperObj.applicationObj.My_family_hosted_an_AFS_student__c == false && wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c == false && wrapperObj.applicationObj.I_ve_traveled_abroad__c == false && wrapperObj.applicationObj.I_ve_lived_abroad__c == false && wrapperObj.applicationObj.None_of_the_above__c == false)
            {
                if(messageTemplateStrAY != undefined){
                    messageTemplateStrAY += "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SelectOneASFExperience");
                }else{
                    messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SelectOneASFExperience");
                }
               	
            }
        }
        
        if(wrapperObj.applicationObj.I_ve_participated_in_another_exchange_p__c == true){
            if(wrapperObj.applicationObj.Program_you_participated__c === undefined 
               || wrapperObj.applicationObj.Program_you_participated__c == ""){
                if(messageTemplateStrAY != undefined){
                    messageTemplateStrAY += "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SpecifyProgramParticipated");
                }else{
                    messageTemplateStrAY ="\n" + $A.get("$Label.c.AfsLbl_Error_AboutYouSectionHeader") + "\n";
                    messageTemplateStrAY += $A.get("$Label.c.AfsLbl_Error_SpecifyProgramParticipated");
                }
                
            }
        }
        if(messageTemplateStr === undefined){
            messageTemplateStr = "";
        }
        
        if(messageTemplateStrPD != undefined){
           messageTemplateStr += "\n" + messageTemplateStrPD;
        }
        
        if(messageTemplateStrAY != undefined){
           if(!isParentError){
                messageTemplateStr += "\n";
           }
           messageTemplateStr += messageTemplateStrAY;
        }
        
        if(messageTemplateStr != undefined && messageTemplateStr != ""){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            component.set("v.Spinner","false");
            return;
        }
        
        wrapperObj.applicationObj.What_are_you_looking_for__c = selectedVal;
        wrapperObj.applicationObj.How_would_friends_family_describe_you__c = selectedValFamily;
        helper.doSubmitApplication(component,wrapperObj);
    },
    
    checkFamilyAbroad : function (component, event, helper) {
        var flag = component.get("v.A_family_member_went_abroad_with_AFS");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.A_family_member_went_abroad_with_AFS",flag);
    },
    
    checkFamilyHosted : function (component, event, helper) {
        var flag = component.get("v.My_family_hosted_an_AFS_student");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.My_family_hosted_an_AFS_student",flag);
    },
    
    checkExchangeProgram : function (component, event, helper) {
        var flag = component.get("v.I_ve_participated_in_another_exchange_p");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.I_ve_participated_in_another_exchange_p",flag);
    },
    
    checkITravelAbroad : function (component, event, helper) {
        var flag = component.get("v.I_ve_traveled_abroad");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.I_ve_traveled_abroad",flag);
    },
    
    checkIAbroad : function (component, event, helper) {
        var flag = component.get("v.I_ve_lived_abroad");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.I_ve_lived_abroad",flag); 
    },
    
    checkNone : function (component, event, helper) {
        var flag = component.get("v.None_of_the_above");
        if(flag == true || flag == "true" ){
            flag = false;
        }else{
            flag = true;
        }
        component.set("v.None_of_the_above",flag);        
    }
    
})