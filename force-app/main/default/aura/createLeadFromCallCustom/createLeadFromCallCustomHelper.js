({
    onSuccess: function(component,payload){  
        sforce.one.navigateToURL('/lightning/r/Lead/'+payload.id+'/view');
        
    },
    onError: function(component){
        var payload = event.getParams().error;
        console.log('ERROR message: ',payload.body.message);
        
    },
    cancel: function(component){
        sforce.one.navigateToURL('/lightning/o/Lead/list');
        
    }
})