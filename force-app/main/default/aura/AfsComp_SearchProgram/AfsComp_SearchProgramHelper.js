({
	fetchPickListVal: function(component) {
        var action = component.get("c.getselectOptions");              
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {                                
                var MapValues = response.getReturnValue();              
				component.set("v.destinationWrapper", MapValues['Destinations__c']);
				component.set("v.durationWrapper", MapValues['Length__c'] );  
                component.set("v.programtypeWrapper", MapValues['Program_Type__c'] );
                component.set("v.areaofinterestnWrapper", MapValues['Area_of_interest__c']);
            }  
            
         });
        $A.enqueueAction(action);
    },
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    }
})