({
    doInit: function(component, event, helper) {
        // ASSIGN LABELS AND BUTTON TEXT USING CONFIG VAR
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_SearchPrgHeader__c ,component);
        component.set("v.SelectProg_SearchLabel",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_SearchPrgHelpTxt__c,component);
        component.set("v.SelectProg_HelpTextSearchCmp",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Btn_SearchProgram__c,component);
        component.set("v.Button_SearchPrograms",component.get("v.LabelTempVal"));
        
        helper.fetchPickListVal(component); //ToDo: need to be changed for sandbox
    },
    
    sortlist : function(component, event, helper)
    {
        var wrapperList = component.get("v.destinationWrapper");        
        wrapperList.sort(function(a,b) {
            
            var tF1 = a["Name"].toLowerCase() === b["Name"].toLowerCase(),
                tF2 = (a["Name"].toLowerCase() < b["Name"].toLowerCase() ? -1 : a["Name"].toLowerCase() > b["Name"].toLowerCase() ? 1 : 0);
            return tF1? 0: (tF2?1:-1); 
            
        });
        
        component.set("v.destinationWrapper",wrapperList);
    },
    
    onDestinationChange: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.destinationSearch");
        var valuesNew =component.get("v.selecteddestinationwrappper");       
        var wrapperList = component.get("v.destinationWrapper");
        var newwrapperList = component.get("v.destinationWrappercopy");               
        if(wrapperList[index-1].isSelected === true){
            wrapperList[index-1].isSelected = false;   
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = false;
                    break;
                }
            }                                                     
            
            for (var i = 0; i < valuesNew.length; i++) {
                if (wrapperList[index-1].Name === valuesNew[i].Name) {
                    valuesNew.splice(i, 1);
                    break;
                }
            } 
            
        }else{
            wrapperList[index-1].isSelected = true; 
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = true;
                    break;
                }
            }                                                             
            var wrapper = { 'IndexId' : index-1 ,
                           'Name' : wrapperList[index-1].Name,
                           'FieldLbl' : wrapperList[index-1].FieldLbl
                          };
            valuesNew.push(wrapper);             
        }                        
        component.set("v.destinationWrapper",wrapperList);
        component.set("v.destinationWrappercopy",newwrapperList);
        component.set("v.selecteddestinationwrappper",valuesNew);                      
        component.set("v.destinationSearch", '');
    },
    
    onDurationChange: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.durationSearch");
        var valuesNew =component.get("v.selecteddurationwrappper");       
        var wrapperList = component.get("v.durationWrapper");
        var newwrapperList = component.get("v.durationWrappercopy");              
        if(wrapperList[index-1].isSelected === true){
            wrapperList[index-1].isSelected = false;   
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = false;
                    break;
                }
            }                                                     
            
            for (var i = 0; i < valuesNew.length; i++) {
                if (wrapperList[index-1].Name === valuesNew[i].Name) {
                    valuesNew.splice(i, 1);
                    break;
                }
            } 
            
        }else{
            wrapperList[index-1].isSelected = true; 
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = true;
                    break;
                }
            }                                                             
            var wrapper = { 'IndexId' : index-1 ,
                           'Name' : wrapperList[index-1].Name,
                           'FieldLbl' : wrapperList[index-1].FieldLbl
                          };
            valuesNew.push(wrapper);             
        }                 
        component.set("v.durationWrapper",wrapperList);
        component.set("v.durationWrappercopy",newwrapperList);
        component.set("v.selecteddurationwrappper",valuesNew);                      
        component.set("v.durationSearch", '');
    },
    
    onProgramTypeChange: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.programSearch");
        var valuesNew =component.get("v.selectedprogramtypewrappper");       
        var wrapperList = component.get("v.programtypeWrapper");
        var newwrapperList = component.get("v.programtypeWrappercopy");  
        
        if(wrapperList[index-1].isSelected === true){
            wrapperList[index-1].isSelected = false;   
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = false;
                    break;
                }
            }                                                     
            
            for (var i = 0; i < valuesNew.length; i++) {
                if (wrapperList[index-1].Name === valuesNew[i].Name) {
                    valuesNew.splice(i, 1);
                    break;
                }
            } 
            
        }else{
            wrapperList[index-1].isSelected = true; 
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = true;
                    break;
                }
            }                                                             
            var wrapper = { 'IndexId' : index-1 ,
                           'Name' : wrapperList[index-1].Name,
                           'FieldLbl' : wrapperList[index-1].FieldLbl
                          };
            valuesNew.push(wrapper);             
        }                 
        component.set("v.programtypeWrapper",wrapperList);
        component.set("v.programtypeWrappercopy",newwrapperList);
        component.set("v.selectedprogramtypewrappper",valuesNew);                      
        component.set("v.programSearch", '');
    },
    
    onAreaOfintChange: function(component,event) {
        
        var index = event.currentTarget.getAttribute("data-recId");                
        var searchValue = component.get("v.areaofintSearch");
        var valuesNew =component.get("v.selectedareaofintereswrappper");       
        var wrapperList = component.get("v.areaofinterestnWrapper");
        var newwrapperList = component.get("v.areaofinterestnWrappercopy");  
        
        if(wrapperList[index-1].isSelected === true){
            wrapperList[index-1].isSelected = false;   
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = false;
                    break;
                }
            }                                                     
            
            for (var i = 0; i < valuesNew.length; i++) {
                if (wrapperList[index-1].Name === valuesNew[i].Name) {
                    valuesNew.splice(i, 1);
                    break;
                }
            } 
            
        }else{
            wrapperList[index-1].isSelected = true; 
            for(var j=0; j< newwrapperList.length ; j++)
            {    
                if (newwrapperList[j].Name === wrapperList[index-1].Name) {
                    newwrapperList[j].isSelected = true;
                    break;
                }
            }                                                             
            var wrapper = { 'IndexId' : index-1 ,
                           'Name' : wrapperList[index-1].Name,
                           'FieldLbl' : wrapperList[index-1].FieldLbl
                          };
            valuesNew.push(wrapper);             
        }                 
        component.set("v.areaofinterestnWrapper",wrapperList);
        component.set("v.areaofinterestnWrappercopy",newwrapperList);
        component.set("v.selectedareaofintereswrappper",valuesNew);                      
        component.set("v.areaofintSearch", '');
    },
    
    onRemoveDestinationPill : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");    
        var pillLabel = evt.currentTarget.getAttribute("data-recLabel"); 
        var wrapperList = component.get("v.destinationWrapper"); 
        var newwrapperList = component.get("v.destinationWrappercopy");
        var valuesNew =component.get("v.selecteddestinationwrappper");        
        wrapperList[pillId].isSelected = false;
        //newwrapperList[pillId].isSelected = false;
        component.set("v.destinationWrappercopy",newwrapperList);
        component.set("v.destinationWrapper",wrapperList);        
        
        for (var i = 0; i < valuesNew.length; i++) {
            if (pillLabel === valuesNew[i].Name) {
                valuesNew.splice(i, 1);
                break;
            }
        }               
        component.set("v.selecteddestinationwrappper",valuesNew);      
    },
    
    onRemoveDurationPill : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");    
        var pillLabel = evt.currentTarget.getAttribute("data-recLabel");
        var wrapperList = component.get("v.durationWrapper"); 
        var newwrapperList = component.get("v.durationWrappercopy");
        var valuesNew =component.get("v.selecteddurationwrappper");        
        wrapperList[pillId].isSelected = false;
        //newwrapperList[pillId].isSelected = false;
        component.set("v.durationWrappercopy",newwrapperList);
        component.set("v.durationWrapper",wrapperList);        
        
        for (var i = 0; i < valuesNew.length; i++) {
            if (pillLabel === valuesNew[i].Name) {
                valuesNew.splice(i, 1);
                break;
            }
        }               
        component.set("v.selecteddurationwrappper",valuesNew);                             
    },
    
    onRemoveProgramTypePill : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");    
        var pillLabel = evt.currentTarget.getAttribute("data-recLabel");
        var wrapperList = component.get("v.programtypeWrapper"); 
        var newwrapperList = component.get("v.programtypeWrappercopy");
        var valuesNew =component.get("v.selectedprogramtypewrappper");        
        wrapperList[pillId].isSelected = false;
        //newwrapperList[pillId].isSelected = false;
        component.set("v.programtypeWrappercopy",newwrapperList);
        component.set("v.programtypeWrapper",wrapperList);        
        
        for (var i = 0; i < valuesNew.length; i++) {
            if (pillLabel === valuesNew[i].Name) {
                valuesNew.splice(i, 1);
                break;
            }
        }               
        component.set("v.selectedprogramtypewrappper",valuesNew);                           
    },
    
    onRemoveAreaofinterestPill : function(component,evt) {
        var pillId = evt.currentTarget.getAttribute("data-recId");    
        var pillLabel = evt.currentTarget.getAttribute("data-recLabel");
        var wrapperList = component.get("v.areaofinterestnWrapper"); 
        var newwrapperList = component.get("v.areaofinterestnWrappercopy");
        var valuesNew =component.get("v.selectedareaofintereswrappper");        
        wrapperList[pillId].isSelected = false;
        //newwrapperList[pillId].isSelected = false;
        component.set("v.areaofinterestnWrappercopy",newwrapperList);
        component.set("v.areaofinterestnWrapper",wrapperList);        
        
        for (var i = 0; i < valuesNew.length; i++) {
            if (pillLabel === valuesNew[i].Name) {
                valuesNew.splice(i, 1);
                break;
            }
        }               
        component.set("v.selectedareaofintereswrappper",valuesNew);                             
    },
    
    searchdestination : function(component, event, helper) {        
        var wrapperList = component.get("v.destinationWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.destinationSearch");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            else
            {
                component.set("v.destinationWrapper",wrapperList); 
            }
        }
        component.set("v.destinationWrappercopy",newwrapperList);        
    },
    
    searchduration : function(component, event, helper) {        
        var wrapperList = component.get("v.durationWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.durationSearch");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            else
            {
                component.set("v.durationWrapper",wrapperList); 
            }
        }
        component.set("v.durationWrappercopy",newwrapperList);        
    },
    
    searchTypeOfProgram : function(component, event, helper) {        
        var wrapperList = component.get("v.programtypeWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.programSearch");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            else
            {
                component.set("v.programtypeWrapper",wrapperList); 
            }
        }
        component.set("v.programtypeWrappercopy",newwrapperList);        
    },
    
    searchAreaOfInt : function(component, event, helper) {        
        var wrapperList = component.get("v.areaofinterestnWrapper");       
        var newwrapperList = [];
        var toSearch = component.get("v.areaofintSearch");
        
        for(var i=0; i<wrapperList.length; i++) {           
            if(wrapperList[i].FieldLbl.toLowerCase().match(toSearch.toLowerCase()) ) {                
                newwrapperList.push(wrapperList[i]);              
            }
            else
            {
                component.set("v.areaofinterestnWrapper",wrapperList); 
            }
        }
        component.set("v.areaofinterestnWrappercopy",newwrapperList);        
    },
    
    Searchprogram : function(component, event, helper)
    {                
        var MapValues = {};  
        var destPickValue =component.get("v.selecteddestinationwrappper");  
        if(destPickValue.length > 0)
        {
            var listofval = [];
            for (var i = 0; i < destPickValue.length; i++) {  
                var nameVar = destPickValue[i].Name + '';
                listofval.push(nameVar);                           
            } 
            if(listofval.length > 0) {
                MapValues['Destinations__c'] = listofval;         	
            }
        }
        
        var durtntPickValue =component.get("v.selecteddurationwrappper");
        if(durtntPickValue.length > 0)
        {
            var listofval = [];
            for (var i = 0; i < durtntPickValue.length; i++) {  
                var nameVar = durtntPickValue[i].Name + '';
                listofval.push(nameVar);                           
            } 
            if(listofval.length > 0) {
                MapValues['Length__c'] = listofval;
            }
        }
        
        var ProgrmtypPickValue =component.get("v.selectedprogramtypewrappper");
        if(ProgrmtypPickValue.length > 0)
        {
            var listofval = [];
            for (var i = 0; i < ProgrmtypPickValue.length; i++) {  
                var nameVar = ProgrmtypPickValue[i].Name + '';
                listofval.push(nameVar);                           
            } 
            if(listofval.length > 0) {
                MapValues['Program_Type__c'] = listofval;         	
            }
        }
        
        var AreaofintPickValue =component.get("v.selectedareaofintereswrappper");
        if(AreaofintPickValue.length > 0)
        {
            var listofval = [];
            for (var i = 0; i < AreaofintPickValue.length; i++) {  
                var nameVar = AreaofintPickValue[i].Name + '';
                listofval.push(nameVar);                           
            } 
            if(listofval.length > 0) {
                MapValues['Area_of_interest__c'] = listofval;         	
            }
        }
        component.set("v.mapOfValues", MapValues);       
        document.body.scrollTop = 0;
    	document.documentElement.scrollTop = 0;
        var cmpEvent = component.getEvent("programsearchevent");        
        cmpEvent.setParams({
            "MapofPiclistValues" : component.get("v.mapOfValues")});
        cmpEvent.fire();        
    },
    
    toggleDestinationView : function(component, event, helper)
    {               
        var toggleView = component.get("v.expandDestination");
        if(toggleView === "true"){
            component.set("v.expandDestination","false");
        }else{
            component.set("v.expandDestination","true");
        }
        
        toggleView = component.get("v.expandDuration");
        if(toggleView === "true"){
            component.set("v.expandDuration","false");
        }else{
            component.set("v.expandDuration","true");
        }
        
        
        toggleView = component.get("v.expandProgramType");
        if(toggleView === "true"){
            component.set("v.expandProgramType","false");
        }else{
            component.set("v.expandProgramType","true");
        }
        
        toggleView = component.get("v.expandAreaofInteres");
        if(toggleView === "true"){
            component.set("v.expandAreaofInteres","false");
        }else{
            component.set("v.expandAreaofInteres","true");
        }
        
    },
    
    toggleDurationView : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandDuration");
        if(toggleView === "true"){
            component.set("v.expandDuration","false");
        }else{
            component.set("v.expandDuration","true");
        }
        
    },
    
    toggleProgramTypeView : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandProgramType");
        if(toggleView === "true"){
            component.set("v.expandProgramType","false");
        }else{
            component.set("v.expandProgramType","true");
        }
        
    },
    
    toggleAreaOfInerestView : function(component, event, helper)
    {        
        var toggleView = component.get("v.expandAreaofInteres");
        if(toggleView === "true"){
            component.set("v.expandAreaofInteres","false");
        }else{
            component.set("v.expandAreaofInteres","true");
        }       
    },      
})