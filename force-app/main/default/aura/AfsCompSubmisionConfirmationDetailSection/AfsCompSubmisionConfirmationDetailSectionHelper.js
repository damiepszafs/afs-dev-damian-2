({
    routeConfig : function(component,event){
        var config = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        if(event.currentTarget.id !=undefined){
            var  sourceId = event.currentTarget.id;
            var view ='';
            switch(sourceId) {
                case "homeItem":
                    view = "0";
                    break;
                case "nationalityItem":
                    view = "1";
                    break;
                case "scholarItem":
                    view = "2";
                    break;
                case "paymentItem":
                    view = "3";
                    break;
                case "settingItem":
                    view = "4";
                    break;    
                case "complete_Profile_Link":
                    view = "1";
                    break;
                case "apply_here_Link":
                    view = "2";
                    break;   
                case "global_Compentence_Link":
                    view = "3";
                    break;    
                default:
                    view = "0";
            }     
            
            if(view == "1" && config.Your_Profile_Available_Stages__c != undefined && config.Your_Profile_Available_Stages__c != ''){
                if((config.Your_Profile_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1)){
                    component.set("v.detailNavigationWrapper",view);
                }
            }else if(view == "2" && config.Scholarship_Available_Stages__c != undefined && config.Scholarship_Available_Stages__c != ''){
                if((config.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1)){
                    component.set("v.detailNavigationWrapper",view);
                }
            }else if(view == "3" && config.GCC_Available_Stages__c != undefined && config.GCC_Available_Stages__c != ''){
                if((config.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1) && !applicationWrapper.applicationObj.Is_GCC_Section_Completed__c){
                    component.set("v.detailNavigationWrapper",view);
                }
            }else{
                component.set("v.detailNavigationWrapper",view);
            }
            
        }
    },
    stopTimer :function (component,event){
        component.set("v.Spinner","false");
    },
    
    isAppUnderProcess : function (component,event){
        
        var action = component.get("c.isAppProcessed");
        action.setParams({
            "contactId": component.get("v.contactInfo").Id,
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var navigateWrapper = component.get("v.navigateWrapper");
                var applicationWrapper = component.get("v.applicationWrapper");
                if(response.getReturnValue() == true && applicationWrapper.applicationObj.Status__c == 'Participation Desire' && applicationWrapper.applicationObj.Status_App__c == 'Pre-Applied'){
                    navigateWrapper.isProgramDetail = false;
                    navigateWrapper.isProgramDetailSideBar = true;
                    navigateWrapper.isAboutYou = false;
                    navigateWrapper.isAboutYouSideBar = true;
                    navigateWrapper.isPortalSubmissionConfirmation =false;
                    navigateWrapper.isPortalSubmissionConfirmationSideBar = true;
                    navigateWrapper.isPreSelected = true;
                    navigateWrapper.isPreSelectedSideBar = true;
                    navigateWrapper.isSelectProgram = false;
                    navigateWrapper.isSelectProgramSideBar = true;
                    component.set("v.navigateWrapper",navigateWrapper);
                }
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    }
    
    
})