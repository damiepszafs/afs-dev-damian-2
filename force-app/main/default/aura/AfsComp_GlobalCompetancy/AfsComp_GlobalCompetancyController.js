({
	doInit : function (component, event, helper) {
        component.set("v.Spinner","true"); 
    	helper.onInit(component,event,'Why_is_Global_Competence_important_for_s__c,How_to_be_a_Global_Citizen__c');
    },
    
    setGlobalCompetanceImp : function (component, event, helper) {
        var appWrapper = component.get("v.applicationWrapper");
        appWrapper.applicationObj.Why_is_Global_Competence_important_for_s__c = event.currentTarget.id;
        component.set("v.applicationWrapper",appWrapper);
    },
    
    setHowGlobalCompetanceImp : function (component, event, helper) {
        var appWrapper = component.get("v.applicationWrapper");
        appWrapper.applicationObj.How_to_be_a_Global_Citizen__c = event.currentTarget.id;
        component.set("v.applicationWrapper",appWrapper);
    },
    
    updateApplicaiton : function (component, event, helper) {
        
        var wrapperObj = component.get("v.applicationWrapper");
        var configWrapper = component.get("v.configWrapper");
        var messageTemplateStr;
        //Check for require condtions
        if(configWrapper.GlobalCompetancy_Fld_WhyisGlobal__c == "Required"){
            if(wrapperObj.applicationObj.Why_is_Global_Competence_important_for_s__c === undefined){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectOneWhyGlobalCompetance");
            }
        }
        
        if(configWrapper.GlobalCompetancy_Fld_HowisGlobal__c == "Required"){
            if(wrapperObj.applicationObj.How_to_be_a_Global_Citizen__c === undefined){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SelectOneHowGlobalCompetance");
            }
        }
        
        if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            return;
        }
        helper.doSubmitApplication(component,wrapperObj);
    }
})