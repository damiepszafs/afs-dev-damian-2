({
	onInit : function(component, event) {
		component.set("v.Spinner","true");
		var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.configWrapper",response.getReturnValue());
                var configObj = response.getReturnValue();
                
                //prepare authURLs
                var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
                var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
                var sParameterName;
                var prIdForStartUrl = '';
                for (var i = 0; i < sURLVariables.length; i++) {
	                if (sURLVariables[i].indexOf('prId') > -1){
	                    sParameterName = sURLVariables[i].split('prId='); //to split the key from the value.
	                    if (sURLVariables[i].indexOf('prId') > -1 && sParameterName.length == 2 && !$A.util.isEmpty(sParameterName[1])) {
	                        sParameterName[1] === undefined ? '' : sParameterName[1];
	                        prIdForStartUrl = sParameterName[1];
	                    }
	                }
                }
                var comHomeURL = component.get("v.configWrapper.Community_URL__c");
                var startURL = '/s/?prId=' + prIdForStartUrl + '&';
                var authFBURL = comHomeURL + '/services/auth/sso/Facebook?community=' + comHomeURL + '&startURL=' + startURL + 'from=facebook';
                var authGoogleURL = comHomeURL + '/services/auth/sso/Google?community=' + comHomeURL + '&startURL=' + startURL + 'from=google';
                
                //console.log(authFBURL);
                //console.log(authGoogleURL);
                
                component.set("v.authFBURL", authFBURL);
                component.set("v.authGoogleURL",authGoogleURL);
                
                var userSelectedLanguage;
                for (var i = 0; i < sURLVariables.length; i++) {
                    var sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                    if (sParameterName[0] === 'userselectedlanguage') {
                        sParameterName[1] === undefined ? '' : sParameterName[1];
                        if (!$A.util.isEmpty(sParameterName[1])) {
                        	userSelectedLanguage = decodeURIComponent(sParameterName[1].replace(/\+/g, ' '));
                        }
                    }
                }
                
                var labels = component.get("v.lstCustomLabels");
                labels.push(configObj.LoginComp_Txt_SignUp__c);
                labels.push(configObj.LoginComp_Txt_SignUpHelpTxt__c);
                labels.push(configObj.LoginComp_Btn_Facebook__c);
                labels.push(configObj.LoginComp_Btn_Gmail__c);
                labels.push(configObj.LoginComp_Btn_SignUpWithGmail__c);
                labels.push(configObj.LoginComp_Txt_Or__c);
                labels.push(configObj.LoginComp_Txt_AlreadyApplied__c);
                labels.push(configObj.LoginComp_Txt_Login__c);
                labels.push("AfsLbl_SignUp_SignUpWith");
				labels.push("AfsLbl_SignUp_SignUpWithHelpTxt");
				labels.push("AfsLbl_SignUp_Facebook");					
				labels.push("AfsLbl_SignUp_Gmail");        	
				labels.push("AfsLbl_SignUp_SignUpwithEmail");			
				labels.push("AfsLbl_SignUp_SignUpMe");
				labels.push("AfsLbl_SignUp_Or");
				labels.push("AfsLbl_SignUp_AlreadyApplied");
				labels.push("AfsLbl_SignUp_LogIn");	
				
				labels.push("AfsLbl_Navigation_ApplynowSideGetStarted");
				labels.push("AfsLbl_Navigation_PreApplication");
				labels.push("AfsLbl_Navigation_15minutes");
				labels.push("AfsLbl_Navigation_ApplynowSideHeader1");
				labels.push("AfsLbl_Navigation_ApplynowSideHeader2");
				labels.push("AfsLbl_Navigation_PRESELECTED");
				labels.push("AfsLbl_Navigation_ACCEPTED");
				labels.push("AfsLbl_Navigation_GETREADY");
				labels.push("AfsLbl_Navigation_BONVOYAGE");
				labels.push("AfsLbl_SignUp_FacebookOnly");
				labels.push("AfsLbl_SignUp_GmailOnly");
				labels.push("AfsLbl_Login_PolicyHeader");
				labels.push("AfsLbl_Login_Checkbox1");
				
				labels.push("AfsLbl_SignUp_Name");
				labels.push("AfsLbl_SignUp_LastName");					
				labels.push("AfsLbl_SignUp_FirstName");
				labels.push("AfsLbl_Login_MiddleName");
				labels.push("AfsLbl_SignUp_DOB");						
				labels.push("AfsLbl_SignUp_Email");
				labels.push("AfsLbl_SignUp_MobileHelpTxt");				
				labels.push("AfsLbl_SignUp_EmailPlaceHolder");
				labels.push("AfsLbl_SignUp_PromoCode");					
				labels.push("AfsLbl_SignUp_DOBHelpText");
				labels.push("AfsLbl_SignUp_PromoCodeHelpTxt");			
				labels.push("AfsLbl_SignUp_ZipCode");
				labels.push("AfsLbl_SignUp_MobileNumber");
				labels.push("AfsLbl_SignUp_ZipCodeHelpTxt");
				labels.push("AfsLbl_SignUp_HearAboutUs");
				labels.push("AfsLbl_SignUp_Password");
				labels.push("AfsLbl_SignUp_PasswordHelpTxt");
				labels.push("AfsLbl_Login_Language");
				labels.push("AfsLbl_Login_CheckBox2");
				labels.push("AfsLbl_Loading");
				
                component.set("v.lstCustomLabels", labels);
                
                this.doGetLanguageList(component,event);
                
                if (!$A.util.isEmpty(userSelectedLanguage)) {
                	userSelectedLanguage = decodeURIComponent(userSelectedLanguage.replace(/\+/g, ' '));
                    component.set("v.languageSelected", userSelectedLanguage);
                    component.set("v.islanguageSelectedByUser", true);
                    this.onLanguageChange(component, event, false);
                } else {
                    component.set("v.languageSelected", $A.get("$Locale.langLocale"));
                    this.onLanguageChange(component, event, true);
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
	
	doGetLanguageList : function(component, event) {
    	component.set("v.Spinner",true);
		var action = component.get("c.getLanguageList");
        var configObj = component.get("v.configWrapper");
        action.setParams({
            "selectedLanguageCode" : configObj.Afs_CommunityLanguages__c
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	if(response.getReturnValue() != undefined && response.getReturnValue() != []){
            		component.set("v.isLanguagesAvailable", false);
            	}
                component.set("v.languageList",response.getReturnValue());
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                component.set("v.Spinner",false);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    onLanguageChange : function(component, event, onLoad) {
        var action = component.get("c.doTranslate");
        var languageSelected = component.get("v.languageSelected");
        var lstCustomLabels = component.get("v.lstCustomLabels");
        action.setParams({
            "selectedLanguage" : languageSelected,
            "customLabelStr" : JSON.stringify(lstCustomLabels),
            "isOnLoad" : onLoad
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	var cLabelMap = new Map();
                var customLabelWrapperList = JSON.parse(response.getReturnValue());
                for(var index in customLabelWrapperList){
                	var setToAttr = 'v.' + customLabelWrapperList[index].customLabelName;
            		component.set(setToAttr, customLabelWrapperList[index].customLabelValue);
            		cLabelMap.set(setToAttr, customLabelWrapperList[index].customLabelValue);
                }
                component.set("v.customLabelMap", cLabelMap);
            	component.set("v.isShowComponent", true);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
            component.set("v.Spinner",false);
        });
        $A.enqueueAction(action);	
	},
    
    getDetails :  function (component,event){
        var action = component.get("c.getLoginWrapper");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.loginWrapper",response.getReturnValue());
                component.set("v.showDetailSection", "true");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    navigateToSocial:  function (component, url){

    	var showDetailSection = component.get("v.showDetailSection");
    	if(showDetailSection == false){
    		var AgreeSocialTerms = component.get("v.AgreeSocialTerms");
    		
    		if(AgreeSocialTerms == false){
				var messageTemplate = $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
				
				var toastEvent = $A.get("e.force:showToast");
	            
	            toastEvent.setParams({ 
	                type : "Error",
	                duration : 5000,
	                message: messageTemplate
	            });
	            
	            toastEvent.fire();
	            component.set("v.Spinner",false);
	            return;
	        }
	        
	        //window.open(url,'_top')
	        window.location.href = url;
    	}
        
    }
})