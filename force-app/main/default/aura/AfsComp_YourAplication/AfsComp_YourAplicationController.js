({
    doInit: function(component, event, helper) {
        component.set("v.Spinner","true");
        
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PageHeader__c ,component);
        component.set("v.PageHeader",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PageSubHeader__c ,component);
        component.set("v.PageSubHeader",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressHeader__c ,component);
        component.set("v.AddressHeader",component.get("v.LabelTempVal"));     
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLabel__c  ,component);
        component.set("v.AddressLabel",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressHelpTxt__c ,component);
        component.set("v.AddressLabelHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine1PlaceHold__c ,component);
        component.set("v.AddressLine1PlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine2PlaceHold__c,component);
        component.set("v.AddressLine2PlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressCityPlaceHold__c,component);
        component.set("v.AddressCityPlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressStatePlaceHold__c,component);
        component.set("v.AddressStatePlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressZipPlaceHold__c,component);
        component.set("v.AddressZipPlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_MobileNumber__c,component);
        component.set("v.MobileNumber",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PhoneNumber__c,component);
        component.set("v.PhoneNumber",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_LegalCountry__c,component);
        component.set("v.LegalCountry",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_LegalCountryHelpTxt__c,component);
        component.set("v.LegalCountryHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PersonalDetailHeader__c,component);
        component.set("v.PersonalDetailHeader",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DOB__c,component);
        component.set("v.DOB",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DOBHelpTxt__c,component);
        component.set("v.DOBHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Gender__c,component);
        component.set("v.Gender",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_GenderHelpTxt__c,component);
        component.set("v.GenderHelpTxt",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Nationality__c,component);
        component.set("v.Nationality",component.get("v.LabelTempVal"));   
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_NationalityHelpTxt__c ,component);
        component.set("v.NationalityHelpTxt",component.get("v.LabelTempVal"));    
        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardian__c,component);
        component.set("v.ParentGuardian",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardianHelpTxt__c,component);
        component.set("v.ParentGuardianHelpTxt",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentName__c ,component);
        component.set("v.ParentName",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Email__c,component);
        component.set("v.Email",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhone__c,component);
        component.set("v.PrimaryPhone",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhoneHelpTxt__c,component);
        component.set("v.PrimaryPhoneHelpTxt",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Education__c,component);
        component.set("v.Education",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SchoolAttended__c,component);
        component.set("v.SchoolAttended",component.get("v.LabelTempVal"));        
//        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SchoolAttendedCity__c,component);
//        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SchoolAttendedZipCode__c,component);
//        component.set("v.SchoolAttendedCity",component.get("v.LabelTempVal"));       
//        component.set("v.SchoolAttendedZipCode",component.get("v.LabelTempVal"));               
//        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SchoolPicklist__c ,component);
//        component.set("v.SchoolPicklist",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_GraduationDate__c,component);
        component.set("v.GraduationDate",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_StudentDescribe__c,component);
        component.set("v.StudentDescribe",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AttendingUniversity__c,component);
        component.set("v.AreYouAttendingUniversity",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AttendUniversityName__c,component);
        component.set("v.AttendUniversityName",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_LanguageKnown__c ,component);
        component.set("v.LanguageKnown",component.get("v.LabelTempVal"));      
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Proficiency__c,component);
        component.set("v.Proficiency",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddLanguage__c,component);
        component.set("v.AddLanguage",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_CriminalConvictions__c,component);
        component.set("v.CriminalConvictions",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_HealthLifestyle__c ,component);
        component.set("v.HealthLifestyle",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PhyscalRestrictHeader__c,component);
        component.set("v.PhyscalRestrictHeader",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PRAdditionalComments__c,component);
        component.set("v.PRAdditionalComments",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DieteryRestrictHeader__c,component);
        component.set("v.DieteryRestrictHeader",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DRAdditionalComments__c,component);
        component.set("v.DRAdditionalComments",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_HealthIssuesPastYear__c,component);
        component.set("v.HealthIssuesPastYear",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DoYouSmoke__c,component);
        component.set("v.DoYouSmoke",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Describe__c,component);
        component.set("v.Describe",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SaveAndContinue__c,component);
        component.set("v.SaveAndContinue",component.get("v.LabelTempVal"));   
        
        helper.onBirthDateChange(component, event);
        helper.fetchPickListVal(component, 'What_school_do_you_attend__c,Do_you_smoke__c,Country_of_Legal_Residence__c,treated_for_any_health_issues_physical__c, Criminal_Convictions__c, NationalityCitizenship__c , Language_4__c,Language_3__c,Language_2__c,Gender__c,AAre_you_attending_University_College_o__c,Language_1_Proficiency__c,Language_2_Proficiency__c,Language_3_Proficiency__c,Language_4_Proficiency__c,Language_1__c','Contact'); 
        helper.fetchPickListVal(component, 'npe4__Type__c','npe4__Relationship__c'); 
        helper.getSchoolsWrapper(component,event);
        
        var ContactInfo = component.get("v.ContactInfo");
        var AppWrapper = component.get("v.applicationWrapper")
        helper.fetchSchool(component, AppWrapper.applicationObj);
        helper.evaluateFlagshipPrograms(component);
        
        var addressLines = [];
        if(ContactInfo.MailingStreet != undefined){
            addressLines = ContactInfo.MailingStreet.split(";");
        }
        if(addressLines.length > 1){
            component.set("v.addressLine1",addressLines[0]);
            component.set("v.addressLine2",addressLines[1]);
        }else{
            component.set("v.addressLine1",ContactInfo.MailingStreet);
        }
        
        var count = 1;
        var noneVal = $A.get("$Label.c.AfsLbl_Picklist_None");
		if(configObj.PersonalDetail_Fld_Language2Speak__c != 'Hide'){
            if(ContactInfo.Language_2__c != undefined && ContactInfo.Language_2__c != noneVal){
                component.set("v.SecondLanguageShow",true);
                count++;
            }
        }else{
            count++;
        }
        
        if(configObj.PersonalDetail_Fld_Language3Speak__c != 'Hide'){
            if(ContactInfo.Language_3__c != undefined && ContactInfo.Language_3__c != noneVal){
                component.set("v.ThirdLanguageShow",true);
                count++;
            }
        }else{
            count++;
        }
        
        if(configObj.PersonalDetail_Fld_Language4Speak__c != 'Hide'){
            if(ContactInfo.Language_4__c != undefined && ContactInfo.Language_4__c != noneVal){
                component.set("v.FourthLanguageShow",true);
                count++;
            }
        }else{
            count++;
        }

		if(configObj.State_pre_fill__c == 'Active'){
            helper.getStatesPreFill(component);
        }
        component.set("v.LangCount",count);
        window.scrollTo(0,0);
        component.set("v.Spinner","false");     
    },
    
    AddLanguageSection : function(component, event, helper){
        var count =  1;
        var configObj = component.get("v.configWrapper");
        var flag = true;
        if(configObj.PersonalDetail_Fld_Language2Speak__c != 'Hide'){
            if(component.get("v.SecondLanguageShow") === undefined || component.get("v.SecondLanguageShow") == false || component.get("v.SecondLanguageShow") == "false")
            {
                component.set("v.SecondLanguageShow",true);
                flag = false;
            }  count++; 
        }else{
            if(flag){
               count++; 
            }
        }
        
        if(configObj.PersonalDetail_Fld_Language3Speak__c != 'Hide' && flag){
            if(component.get("v.ThirdLanguageShow") === undefined || component.get("v.ThirdLanguageShow") == false || component.get("v.ThirdLanguageShow") == "false")
            {
                component.set("v.ThirdLanguageShow",true);
                flag = false;
            }  count++;
        }else{
            if(flag){
               count++; 
            }
        }
        
        if(configObj.PersonalDetail_Fld_Language4Speak__c != 'Hide' && flag){
            if(component.get("v.FourthLanguageShow") === undefined || component.get("v.FourthLanguageShow") == false || component.get("v.FourthLanguageShow") == "false")
            {
                component.set("v.FourthLanguageShow",true);
               
                flag = false;
            }  count++;
        }else{
            if(flag){
               count++; 
            }
        }
        
        component.set("v.LangCount",count); 
    },
    
    birthDateChange : function(component, event, helper) {
        helper.onBirthDateChange(component, event);
    },
    
//    schoolChanged : function(component, event, helper) {
//        var idComp = component.get("v.SchoolAttendedWrapperNew");
//        var schoolNameMap = component.get("v.schoolNameMap");
//        var noneOpt = $A.get("$Label.c.AfsLbl_General_None");
//        if(idComp != noneOpt){
//            component.set("v.SchoolAttendedWrapper", schoolNameMap[idComp]);
//        }        
//    },
    
    SaveRecord : function(component, event, helper) {
        // Check for validations
        var School = JSON.parse(JSON.stringify(component.get("v.School")))
            var SchoolAppObj = JSON.parse(JSON.stringify(component.get("v.SchoolApplication")))
        var ContactInfo = component.get("v.ContactInfo");
        var configWrapper = component.get("v.configWrapper");
        var hasflagshipProgram = component.get("v.flagshipProgramPreSelected");
        var messageTemplateStr;
        if(ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
            ContactInfo.Country_of_Legal_Residence__c = "";
        }
        
        if(ContactInfo.Gender__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectGender")){
            ContactInfo.Gender__c = "";
        }
        
        if(ContactInfo.NationalityCitizenship__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectNationality")){
            ContactInfo.NationalityCitizenship__c = "";
        }
        
        
        if(configWrapper.PersonalDetail_Fld_StreetAddress__c == 'Required'){
        	if((component.get("v.addressLine1") === undefined || component.get("v.addressLine1") == "")){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AddressRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Apartment__c == 'Required'){
        	if((component.get("v.addressLine2") === undefined || component.get("v.addressLine2") == "")){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AddressRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressCity__c == 'Required'){
        	if(ContactInfo.MailingCity === undefined || ContactInfo.MailingCity == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressState__c == 'Required'){
            if(ContactInfo.MailingState === undefined || ContactInfo.MailingState == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PinCode__c == 'Required'){
        	if(ContactInfo.MailingPostalCode === undefined || ContactInfo.MailingPostalCode == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }
            }
        }
        
        
        
        if(configWrapper.PersonalDetail_Fld_MobileNumber__c == 'Required'){
            if(ContactInfo.MobilePhone === undefined || ContactInfo.MobilePhone == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }
            }
        }
        
        if(ContactInfo.MobilePhone === 'invalid') {
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PhoneNumber__c == 'Required'){
        	if(ContactInfo.Phone === undefined || ContactInfo.Phone == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }
            }
        }
        
        if(ContactInfo.Phone === 'invalid') {
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_LegalCountry__c == 'Required'){
            if(ContactInfo.Country_of_Legal_Residence__c === undefined ||
               ContactInfo.Country_of_Legal_Residence__c == "" ||
               ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_DOB__c == 'Required'){
            if(ContactInfo.Birthdate === undefined || ContactInfo.Birthdate == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }
            }
        }
        
        if(ContactInfo.Birthdate != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(ContactInfo.Birthdate) > currentDate){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }
            }
        }
        
        if(ContactInfo.Gender__c === undefined || 
           ContactInfo.Gender__c == "" ||
           ContactInfo.Gender__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectGender")){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Nationality__c == 'Required'){
            if(ContactInfo.NationalityCitizenship__c === undefined ||
               ContactInfo.NationalityCitizenship__c == "" ||
               ContactInfo.NationalityCitizenship__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectNationality")){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Nationality");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Nationality");
                }
            }
        }
        
        var isAdult = component.get("v.isAdult");
        
        if(configWrapper.PersonalDetail_Fld_ParentGuardian__c == 'Required' && hasflagshipProgram){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += helper.validatedParentComp(component, event);
            }else{
                messageTemplateStr = helper.validatedParentComp(component, event);
            }
            
        }
        
        //Parent Guardian Validation for same email or phone
        var parentList = component.get('v.ParentGuardianWrapper');
        var phonesArray = [ContactInfo.MobilePhone];
        var emailArray = [ContactInfo.Email];
        for(var  i= 0 ;i < parentList.length ; i++ ){
            var parentPhone = parentList[i].relatedContact.Phone;
            var parentEmail = parentList[i].relatedContact.Email;
            if(phonesArray.includes(parentPhone))
            {
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneRepeated");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneRepeated");
                }
                if(emailArray.includes(parentEmail)){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianMailRepeated");
                }
                break;
            }else{
                phonesArray.push(parentPhone);
            }
            if(emailArray.includes(parentEmail)){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianMailRepeated");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_ParentGuardianMailRepeated");
                }
                break;
            }else{
                emailArray.push(parentEmail);
            }
        }
        
        if (configWrapper.PersonalDetail_Fld_WhatSchoolAttended__c == 'Required' && hasflagshipProgram) {
            if (School.GMPlace_Id__c  === undefined || School.GMPlace_Id__c  == "" || !School.GMPlace_Id__c  ||
               	School.Name  === undefined || School.Name  == "" || !School.Name      
               )  {
                if (messageTemplateStr != undefined) {
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
                } else {
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
                }
            }
        }
        
//        if(ContactInfo.What_school_do_you_attend__c == $A.get("$Label.c.AfsLbl_YourApplication_HighSchool")){
//            if(configWrapper.PersonalDetail_Fld_School__c == 'Required'){
//                if(component.get("v.SchoolAttendedWrapperNew") === undefined || component.get("v.SchoolAttendedWrapperNew") == "" || component.get("v.SchoolAttendedWrapperNew") == null){
//                    if(messageTemplateStr != undefined){
//                        messageTemplateStr += "\n";
//                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
//                    }else{
//                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SchoolRequired");
//                    }
//                } 
//            }
//        }
        
        if (configWrapper.PersonalDetail_Fld_WhenGraduate__c == 'Required' && hasflagshipProgram) {
            if (SchoolAppObj.Graduation_Date__c  === undefined || SchoolAppObj.Graduation_Date__c  == "" || !SchoolAppObj.Graduation_Date__c ) {
                if (messageTemplateStr != undefined) {
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_GraduationDateRequired");
                } else {
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_GraduationDateRequired");
                }
            }
        }
        
        /*if(ContactInfo.When_would_did_you_graduate__c != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(ContactInfo.When_would_did_you_graduate__c) > currentDate){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_GraduateDateNoFuture");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_GraduateDateNoFuture");
                }
            }
        }*/
        
        if (configWrapper.PersonalDetail_Fld_School_Average_Grade__c == 'Required' && hasflagshipProgram) {
            
            if (SchoolAppObj.Grade_Average__c === undefined || SchoolAppObj.Grade_Average__c == "" ||  SchoolAppObj.Grade_Average__c  > 10 || !SchoolAppObj.Grade_Average__c )  {
                if (messageTemplateStr != undefined) {
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SchoolAverageGrade");
                } else {
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_SchoolAverageGrade");
                }
            }
        }
        
        if (configWrapper.PersonalDetail_Fld_DescribeStudent__c == 'Required' && hasflagshipProgram) {
            if (ContactInfo.Describe_yourself_as_a_student__c === undefined || ContactInfo.Describe_yourself_as_a_student__c == "") {
                if (messageTemplateStr != undefined) {
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DescriptionStudentRequired");
                } else {
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DescriptionStudentRequired");
                }
            }
        }
        
//        if(isAdult){
//            /*if(ContactInfo.AAre_you_attending_University_College_o__c === undefined || ContactInfo.AAre_you_attending_University_College_o__c == ""){
//                if(messageTemplateStr != undefined){
//                    messageTemplateStr += "\n";
//                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_AttendingCOllgeRequired");
//                }else{
//                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AttendingCOllgeRequired");
//                }
//            }*/
//            if(ContactInfo.AAre_you_attending_University_College_o__c == 'Yes'){
//                if(configWrapper.PersonalDetail_Fld_NameUniversity__c == 'Required'){
//                    if(ContactInfo.Name_of_University_College_School__c === undefined || ContactInfo.Name_of_University_College_School__c == ""){
//                        if(messageTemplateStr != undefined){
//                            messageTemplateStr += "\n";
//                            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CollegeNameRequired");
//                        }else{
//                            messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CollegeNameRequired");
//                        }
//                    }
//                }
//            }
//        }
        
        
            
            
        
        
        
        if(ContactInfo.Language_1__c === undefined || ContactInfo.Language_1__c == "" || ContactInfo.Language_1__c == "None"){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language1Required");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language1Required");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Language1Prof__c == 'Required'){
            if(ContactInfo.Language_1_Proficiency__c === undefined || ContactInfo.Language_1_Proficiency__c == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language1ProfRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language1ProfRequired");
                }
            }
        }
        
        if(component.get("v.SecondLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language2Speak__c == 'Required'){
                if(ContactInfo.Language_2__c === undefined || ContactInfo.Language_2__c == "" || ContactInfo.Language_2__c == "None"){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language2Required");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language2Required");
                    }
                }
            }
            if(configWrapper.PersonalDetail_Fld_Language2Prof__c == 'Required'){
                if(ContactInfo.Language_2_Proficiency__c === undefined || ContactInfo.Language_2_Proficiency__c == ""){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language2ProfRequired");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language2ProfRequired");
                    }
                }
            }
        }
        
        if(component.get("v.ThirdLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language3Speak__c == 'Required'){
                if(ContactInfo.Language_3__c === undefined || ContactInfo.Language_3__c == "" || ContactInfo.Language_3__c == "None"){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language3Required");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language3Required");
                    }
                }
            }
            
            if(configWrapper.PersonalDetail_Fld_Language3Prof__c == 'Required'){
                if(ContactInfo.Language_3_Proficiency__c === undefined || ContactInfo.Language_3_Proficiency__c == ""){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language3ProfRequired");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language3ProfRequired");
                    }
                }
            }
        }
        
        if(component.get("v.FourthLanguageShow")){
            if(configWrapper.PersonalDetail_Fld_Language4Speak__c == 'Required'){
                if(ContactInfo.Language_4__c === undefined || ContactInfo.Language_4__c == "" || ContactInfo.Language_4__c == "None"){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language4Required");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language4Required");
                    }
                }
            }
            if(configWrapper.PersonalDetail_Fld_Language4Prof__c == 'Required'){
                if(ContactInfo.Language_4_Proficiency__c === undefined || ContactInfo.Language_4_Proficiency__c == ""){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Language4ProfRequired");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Language4ProfRequired");
                    }
                }
            }
        }
        
        if(isAdult == true || isAdult == "true"){
           /* if(ContactInfo.Criminal_Convictions__c === undefined || ContactInfo.Criminal_Convictions__c == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CriminalConictionRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CriminalConictionRequired");
                }
            } */
        }
        
        
        if(configWrapper.PersonalDetail_Txt_PhysicalRestrictions__c == 'Required'){
            var flag = false;
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_HaveAlergies__c != 'Hide'){
                    flag = ContactInfo.Have_allergies__c;
                } 
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_CantLiveWithPets__c != 'Hide'){
                    flag = ContactInfo.Can_t_live_with_pets_s__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_LimitedIntheActivity__c != 'Hide'){
                    flag = ContactInfo.Limited_in_the_activities_I_can_do__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_TakeMedications__c != 'Hide'){
                    flag = ContactInfo.Take_medications__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_CantLiveWithSmokers__c != 'Hide'){
                    flag = ContactInfo.Can_t_live_with_a_smoker__c;
                } 
            }
            
            //If Flag is True comments are required            
            if(flag == true && configWrapper.PersonalDetail_Fld_AdditionalCommentsPR__c != 'Hide'){
                if(!(component.get('v.ContactInfo.Medical_Condition_Comments__c'))){
                	if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_AdditionalCommentsPhysicalRestrictions");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AdditionalCommentsPhysicalRestrictions");
                    }
                }
            }
            
            if(flag != true && configWrapper.PersonalDetail_Fld_NoRestrictionsOrAlerg__c != 'Hide'){
                flag = ContactInfo.No_restrictions_or_allergies__c;
            }
            
            if(flag === undefined || flag == false){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PhysicalRestrictionsRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PhysicalRestrictionsRequired");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Txt_DieteryRestrictions__c == 'Required'){
            var flag = false;            

            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Vegetarian__c != 'Hide'){
                    flag = ContactInfo.Vegetarian__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Kosher__c != 'Hide'){
                    flag = ContactInfo.Kosher__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Celiac__c != 'Hide'){
                    flag = ContactInfo.Celiac__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_HaveFoodAllergies__c != 'Hide'){
                    flag = ContactInfo.Have_food_allergies__c;
                }
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Vegan__c != 'Hide'){
                    flag = ContactInfo.Vegan__c;
                } 
            }
            
            
            if(flag != true){
                if(configWrapper.PersonalDetail_Fld_Halal__c != 'Hide'){
                    flag = ContactInfo.Halal__c;
                }
            }
            
			 //If Flag is True comments are required            
            if(flag == true && configWrapper.PersonalDetail_Fld_DietaryComments__c != 'Hide'){
                if(!(component.get('v.ContactInfo.Dietary_Comments__c'))){
                	if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_AdditionalCommentsDietery");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AdditionalCommentsDietery");
                    }
                }
            }
            
            if(flag != true && configWrapper.PersonalDetail_Fld_NoDieteryRestrictions__c != 'Hide'){
                flag = ContactInfo.No_dietary_restrictions__c;
            }
            
            if(flag === undefined || flag == false){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DieteryRestrictRequired");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DieteryRestrictRequired");
                }
            }
        }
        
        if(ContactInfo.treated_for_any_health_issues_physical__c === undefined || ContactInfo.treated_for_any_health_issues_physical__c == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_HealthIssueRequired");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_HealthIssueRequired");
            }
        }
        
        if(ContactInfo.Do_you_smoke__c === undefined || ContactInfo.Do_you_smoke__c == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DoYouSmokeRequired");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DoYouSmokeRequired");
            }
        }
        
        if(messageTemplateStr != undefined && messageTemplateStr != ""){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
        }
        if(messageTemplateStr === undefined || messageTemplateStr === ''){
                helper.doSaveRecord(component);
        }
    },
    
    setParentAsFirst : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Parent_legal_Guardian__c = event.currentTarget.id;
        if(contactObj.Parent_legal_Guardian__c != "Other"){
            contactObj.Parent_Legal_Guardian_Other__c = "";
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setSchoolAttended : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.What_school_do_you_attend__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setAttendedUniversity : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.AAre_you_attending_University_College_o__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setLangProf1 : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Language_1_Proficiency__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setLangProf2 : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Language_2_Proficiency__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setLangProf3 : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Language_3_Proficiency__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setLangProf4 : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Language_4_Proficiency__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setNoRestrictions : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.No_restrictions_or_allergies__c == true || contactObj.No_restrictions_or_allergies__c == "true"){
            contactObj.No_restrictions_or_allergies__c = false;
        }else{
            contactObj.No_restrictions_or_allergies__c = true;
        }
        
        component.set("v.ContactInfo",contactObj); 
    },
    
    setHaveAllergies : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Have_allergies__c == true || contactObj.Have_allergies__c == "true"){
            contactObj.Have_allergies__c = false;
        }else{
            contactObj.Have_allergies__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setCantLiveWidPets : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Can_t_live_with_pets_s__c == true || contactObj.Can_t_live_with_pets_s__c == "true"){
            contactObj.Can_t_live_with_pets_s__c = false;
        }else{
            contactObj.Can_t_live_with_pets_s__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setLimitedActivites : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Limited_in_the_activities_I_can_do__c == true || contactObj.Limited_in_the_activities_I_can_do__c == "true"){
            contactObj.Limited_in_the_activities_I_can_do__c = false;
        }else{
            contactObj.Limited_in_the_activities_I_can_do__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setTakeMedicines : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Take_medications__c == true || contactObj.Take_medications__c == "true"){
            contactObj.Take_medications__c = false;
        }else{
            contactObj.Take_medications__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setNoSmokers : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Can_t_live_with_a_smoker__c == true || contactObj.Can_t_live_with_a_smoker__c == "true"){
            contactObj.Can_t_live_with_a_smoker__c = false;
        }else{
            contactObj.Can_t_live_with_a_smoker__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setHalal : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Halal__c == true || contactObj.Halal__c == "true"){
            contactObj.Halal__c = false;
        }else{
            contactObj.Halal__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setVegan : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Vegan__c == true || contactObj.Vegan__c == "true"){
            contactObj.Vegan__c = false;
        }else{
            contactObj.Vegan__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setHaveFoodAlergy : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Have_food_allergies__c == true || contactObj.Have_food_allergies__c == "true"){
            contactObj.Have_food_allergies__c = false;
        }else{
            contactObj.Have_food_allergies__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setCeliac : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Celiac__c == true || contactObj.Celiac__c == "true"){
            contactObj.Celiac__c = false;
        }else{
            contactObj.Celiac__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setKosher : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Kosher__c == true || contactObj.Kosher__c == "true"){
            contactObj.Kosher__c = false;
        }else{
            contactObj.Kosher__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setVegetarian : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.Vegetarian__c == true || contactObj.Vegetarian__c == "true"){
            contactObj.Vegetarian__c = false;
        }else{
            contactObj.Vegetarian__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setNoRestriction :  function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        if(contactObj.No_dietary_restrictions__c == true || contactObj.No_dietary_restrictions__c == "true"){
            contactObj.No_dietary_restrictions__c = false;
        }else{
            contactObj.No_dietary_restrictions__c = true;
        }
        component.set("v.ContactInfo",contactObj); 
    },
    
    setCriminalConvictions : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Criminal_Convictions__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setHealthIssue : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.treated_for_any_health_issues_physical__c =event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    setDoYouSmoke : function (component, event, helper){
        var contactObj = component.get("v.ContactInfo");
        contactObj.Do_you_smoke__c = event.currentTarget.id;
        component.set("v.ContactInfo",contactObj); 
    },
    
    del2LanguageSection : function (component, event, helper){
        var count = component.get("v.LangCount");
        component.set("v.LangCount",count-1); 
        var contactObj = component.get("v.ContactInfo");
        var noInstance;
        contactObj.Language_2__c = noInstance;
        contactObj.Language_2_Proficiency__c = noInstance;
        component.set("v.ContactInfo",contactObj); 
        component.set("v.SecondLanguageShow",false); 
    },
    
    del3LanguageSection : function (component, event, helper){
        var count = component.get("v.LangCount");
        component.set("v.LangCount",count-1);
        var contactObj = component.get("v.ContactInfo");
        var noInstance;
        contactObj.Language_3__c = noInstance;
        contactObj.Language_3_Proficiency__c = noInstance;
        component.set("v.ContactInfo",contactObj); 
        component.set("v.ThirdLanguageShow",false); 
    },
    
    del4LanguageSection : function (component, event, helper){
        var count = component.get("v.LangCount");
        component.set("v.LangCount",count-1);
        var contactObj = component.get("v.ContactInfo");
        var noInstance;
        contactObj.Language_4__c = noInstance;
        contactObj.Language_4_Proficiency__c = noInstance;
        component.set("v.ContactInfo",contactObj); 
        component.set("v.FourthLanguageShow",false); 
    },    
  
    ChangeAdressStates : function (component, event, helper){
        var States = component.find("AdressStates");
        component.set("v.ContactInfo.MailingState", States.get("v.value"));
    },
    
    deSelectCheckboxPhysical : function (component, event, helper){
        component.set("v.ContactInfo.Have_allergies__c", false);
        component.set("v.ContactInfo.Can_t_live_with_pets_s__c", false);
        component.set("v.ContactInfo.Limited_in_the_activities_I_can_do__c", false);
        component.set("v.ContactInfo.Take_medications__c", false);
        component.set("v.ContactInfo.Can_t_live_with_a_smoker__c", false);
        component.set("v.ContactInfo.Medical_Condition_Comments__c", '');
    },
    deSelectCheckboxDietary : function (component, event, helper){
        component.set("v.ContactInfo.Vegetarian__c", false);
        component.set("v.ContactInfo.Kosher__c", false);
        component.set("v.ContactInfo.Celiac__c", false);
        component.set("v.ContactInfo.Have_food_allergies__c", false);
        component.set("v.ContactInfo.Vegan__c", false);
        component.set("v.ContactInfo.Halal__c", false);
        component.set("v.ContactInfo.Dietary_Comments__c", '');
    },
})