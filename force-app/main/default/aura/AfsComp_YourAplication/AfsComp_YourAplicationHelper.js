({
    fetchPickListVal: function(component, fieldName,objName) {
        var action = component.get("c.getPicklistValuesNoSort");
        action.setParams({
            "objObject": objName,
            "fld": fieldName
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
                if(objName == 'Contact'){
                    component.set("v.LegalCountryPicklist", allValues['Country_of_Legal_Residence__c']);
                    component.set("v.CriminalConvictionsPicklist", allValues['Criminal_Convictions__c']);
                    component.set("v.NatinalityPicklist", allValues['NationalityCitizenship__c']);
                    component.set("v.Language4List", allValues['Language_4__c']);
                    component.set("v.Language3List", allValues['Language_3__c']);
                    component.set("v.LanguageThirdProficiency", allValues['Language_3_Proficiency__c']);
                    component.set("v.AttendingUniversity", allValues['AAre_you_attending_University_College_o__c']);
                    component.set("v.LanguagefirstProficiency", allValues['Language_1_Proficiency__c']);
                    component.set("v.LanguageSecondProficiency", allValues['Language_2_Proficiency__c']);
                    component.set("v.LanguagefourthProficiency", allValues['Language_4_Proficiency__c']);
                    component.set("v.Language1List", allValues['Language_1__c']);
                    component.set("v.Language2List", allValues['Language_2__c']);
                    component.set("v.IdentifyMyGender", allValues['Gender__c']);
                    component.set("v.HealthIssueInPastYear", allValues['treated_for_any_health_issues_physical__c']);
                    component.set("v.DoYouSmokeList", allValues['Do_you_smoke__c']);
                    component.set("v.SchoolAttendedPicklist", allValues['What_school_do_you_attend__c']);
                }else{
                    component.set("v.ParentandLegalGardian", allValues['npe4__Type__c']);                    
                }
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    } ,
	fetchPickListValNoSort : function(component, fieldName,objName){
        var action = component.get("c.getPicklistValuesNoSort");
        action.setParams({
            "objObject": objName,
            "fld": fieldName
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
                if(objName == 'Contact'){
                    component.set("v.LanguagefirstProficiency", allValues['Language_1_Proficiency__c']);
                    component.set("v.LanguageSecondProficiency", allValues['Language_2_Proficiency__c']);
                    component.set("v.LanguageThirdProficiency", allValues['Language_3_Proficiency__c']);
                    component.set("v.LanguagefourthProficiency", allValues['Language_4_Proficiency__c']);
                    component.set("v.IdentifyMyGender", allValues['Gender__c']);
                }                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();                
            }              
        });
        $A.enqueueAction(action);
    },	
	
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    doSaveRecord : function (component){
        var ContactInfo = component.get("v.ContactInfo");
        var applicationObj = component.get("v.applicationWrapper").applicationObj;
        var School = JSON.parse(JSON.stringify(component.get("v.School")))
		var SchoolAppObj = JSON.parse(JSON.stringify(component.get("v.SchoolApplication")))
        var schoolWrapper = component.get("v.SchoolAttendedWrapper");
        if(schoolWrapper != null){
			ContactInfo.School__c = schoolWrapper.schoolId;
        }
        
        var addressLine1 = component.get("v.addressLine1");
        var addressLine2 = component.get("v.addressLine2");
        if(addressLine1 != undefined){
            ContactInfo.MailingStreet = addressLine1;
        }
        
        if(addressLine2 != undefined){
            if(ContactInfo.MailingStreet === undefined){
               ContactInfo.MailingStreet = ';' +addressLine2;
            }else{
                ContactInfo.MailingStreet += ';' + addressLine2;
            }
            
        }
        
        if(ContactInfo.treated_for_any_health_issues_physical__c != $A.get("$Label.c.AfsLbl_YourApplication_Yes")){
            ContactInfo.Health_Issue_Description__c = "";
        }
        
        var parentGuardianWrapperList  =  this.createRelationWrapper(component);
        component.set("v.Spinner", "true"); 
        
        var action =  component.get("c.saveCompleteProfile");
        action.setParams({
            "contactInfo": ContactInfo,
            "applicationObj": applicationObj,
            "schoolAppObj": JSON.stringify(SchoolAppObj),
            "school": School,
            "parentGuardianWrapperJSON": JSON.stringify(parentGuardianWrapperList)
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var navigateWrapper = component.get("v.navigateWrapper");
                navigateWrapper.isProgramDetail = false;
                navigateWrapper.isAboutYou = true;
                navigateWrapper.isAboutYouSideBar = true;
                component.set("v.navigateWrapper", navigateWrapper);
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({
                    type: "Error",
                    duration: 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner", "false");
            }
        });
        $A.enqueueAction(action);
    },
    
    getSchoolsWrapper : function (component){
        
        var action = component.get("c.getSchools");
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {                
               component.set("v.SchoolAttendedWrapperPicklist", response.getReturnValue());
                var ContactInfo = component.get("v.ContactInfo");
                var mapNew = {};
                for(var i=0; i< response.getReturnValue().length ; i++){
					mapNew[response.getReturnValue()[i].schoolName] = response.getReturnValue()[i];
                    if(ContactInfo.School__c == response.getReturnValue()[i].schoolId){
                        component.set("v.SchoolAttendedWrapper", response.getReturnValue()[i]);
                        component.set("v.SchoolAttendedWrapperNew", response.getReturnValue()[i].schoolName);
                    }
                }
               component.set("v.schoolNameMap", mapNew); 
                
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            } 
            
         });
        $A.enqueueAction(action);
    },
    
    fetchSchool: function(component, applicantObj) {
        var action = component.get("c.getSchoolApplication");
        action.setParams({
            "applicantId": applicantObj.Applicant__c,
            "schoolId": applicantObj.Applicant_School_Association__c ? applicantObj.Applicant_School_Association__c : ""
        });

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
            	const {Applicant__c = null, School__c = null, Graduation_Date__c = null, Grade_Average__c = null, Id = null} = response.getReturnValue();
            	const schoolApp = {Applicant__c, School__c, Graduation_Date__c, Grade_Average__c, Id}
                component.set("v.SchoolApplication", schoolApp)
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({
                    type: "error",
                    duration: 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
			component.set("v.Spinner","false");
            component.set("v.isShowComponent", true);
        });												 
		$A.enqueueAction(action);
    },
    
    onBirthDateChange : function(component, event) {
        var ContactInfo = component.get("v.ContactInfo");
        var date = new Date();
        var month = date.getMonth()+1;
        var day = date.getDay();
        var year = date.getFullYear();
        
        var currentDate = new Date(year,month,day);
        var birthdate = new Date(ContactInfo.Birthdate);
        var diff =(currentDate.getTime() - birthdate.getTime()) / 1000;
        diff /= (60 * 60 * 24);
        var result = (diff/365.25);
        component.set("v.isAdult", result > 18);
    },
    
    createRelationWrapper :function (component){
      	var parentList = component.get('v.ParentGuardianWrapper');
        var contactData = component.get('v.ContactInfo');
   		        
        for(var  i= 0 ;i < parentList.length ; i++ ){
             if(parentList[i].parentSelected != 'Other'){
             	 parentList[i].relation.npe4__Type__c =  parentList[i].parentSelected;
                 
             }
            if($A.util.isEmpty(parentList[i].Id)){
                parentList[i].relation.npe4__Contact__c = contactData.Id;
                //parentList[i].relatedContact.AccountId = contactData.AccountId;
             }
        }
        return parentList;
    },
    
    validatedParentComp : function (component ,event){
        
        var messageTemplateStr ='';
        var parentList = component.get('v.ParentGuardianWrapper');
        var emergencyContact =  false;
        var parentName =''
        for(var  i= 0 ;i < parentList.length ; i++ ){
            if(parentList[i].relation.This_is_your_emergency_contact__c == true){
                emergencyContact = true; 
            }
            
            if(parentList[i].parentSelected  == ''){
                var num =i+1;
                parentName = 'Parent detail no ' + num ;
            }else{
                if(parentList[i].parentSelected != 'Other' ){
                    parentName = 'Your '+ parentList[i].parentSelected + '\'s';
                }else{
                    parentName = 'Your '+ parentList[i].relation.npe4__Type__c + '\'s';
                }
                
            }
              
            if($A.util.isEmpty(parentList[i].parentSelected)){
                messageTemplateStr += parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") +"\n";
            }else{
                if(parentList[i].parentSelected == 'Other' && $A.util.isEmpty(parentList[i].relation.npe4__Type__c)){
                    messageTemplateStr += parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") +"\n";
                }
            }
            if($A.util.isEmpty(parentList[i].relatedContact.FirstName)){
                messageTemplateStr +=    parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianName") +"\n"; 
            }
            if($A.util.isEmpty(parentList[i].relatedContact.LastName)){
                messageTemplateStr +=   parentName +' '+  $A.get("$Label.c.AfsLbl_Error_ParentGuardianLastName") +"\n"; 
            }
            //---------------------------------------------------------------------------
            if(!$A.util.isEmpty(parentList[i].relatedContact.Phone)){
                if(parentList[i].relatedContact.Phone === "invalid"){
                    messageTemplateStr +=   parentName +' '+ $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneNoValid") +"\n"; 
                }	
            }else if(component.get("v.configWrapper.Submission_Fld_ParentGuardian_Phone__c") =='Required'){
                messageTemplateStr +=   parentName +' '+  $A.get("$Label.c.AfsLbl_Error_PhoneRequire") +"\n"; 
            }
            
            if(parentList[i].relatedContact.Phone === 'invalid') {
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
                }
            }
            
            if(!$A.util.isEmpty(parentList[i].relatedContact.Email)){
                var validEmail = this.validateEmail(parentList[i].relatedContact.Email);
                if(validEmail != true){
                    messageTemplateStr +=  parentName +' '+  $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailNoValid") +"\n"; 
                }	
            }else if(component.get("v.configWrapper.Submission_Fld_ParentGuardian_Email__c") =='Required'){
                messageTemplateStr +=  parentName +' '+   $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailRequired") +"\n"; 
            } 
            
        }
        if(component.get("v.configWrapper.Submission_Fld_Parent_Emergency_contact__c") == 'Required' && emergencyContact == false){
            messageTemplateStr +=  $A.get("$Label.c.AfsLbl_Error_This_is_your_emergency_contact") +"\n"; 
        }
        
        return messageTemplateStr;
        
    },
    
    validateEmail : function (email) {
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(String(email).toLowerCase());
    },
    
    validatePhone : function (phone) {
    	var re = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    	return re.test(String(phone));
    },
	
	getStatesPreFill : function (component){
        var action = component.get("c.getStatesPreFill");
        var configWrapper = component.get("v.configWrapper");
        action.setParams({
            "afsCommunityId": component.get("v.configWrapper.Id")
													  
        });

        action.setCallback(this,function(response) {
																
            component.set("v.StatesToPortal", response.getReturnValue());
					
															 
																	 
									  
								  
								   
											
        });  
								  
			 
		  
        $A.enqueueAction(action);	
    },
    
    evaluateFlagshipPrograms : function(component){
        var app = component.get('v.applicationWrapper');
        var con = component.get('v.ContactInfo');
        
         var action = component.get("c.getHasFlagshipPrograms");
        action.setParams({
            "conId": con.Id,
            "appId": app.applicationObj.Id            
        });

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {            	
                component.set("v.flagshipProgramPreSelected", response.getReturnValue());
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({
                    type: "error",
                    duration: 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        })
        $A.enqueueAction(action);
    }
})