({
	doInit : function (cmp, event, helper) {
        cmp.set("v.Spinner", "true");
        helper.onInit(cmp,event);
        
    },
    
    handleUploadFinished : function (cmp, event, helper) {
        debugger;
        cmp.set("v.isOpen","false");
        cmp.set("v.Spinner", "true");
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        var fileIds = [];
        for(var i = 0; i < uploadedFiles.length ;i++){
            fileIds.push(uploadedFiles[i].documentId);
        }
        helper.getBase64String(cmp, event,fileIds);
    },
    
    closeModel: function(cmp, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        cmp.set("v.isOpen", "false");
    },
    
    openUploadModal: function(cmp, event, helper) {
        var showDp;
        if(event.currentTarget.id !=undefined){
        	var  sourceId = event.currentTarget.id;
            if(sourceId == "photo1Upload"){
                cmp.set("v.fileUploadTo", "documentUrl1");
                showDp = cmp.get("v.showDP1");
            }else if(sourceId == "photo2Upload"){
                cmp.set("v.fileUploadTo", "documentUrl2");
                showDp = cmp.get("v.showDP2");
            }else if(sourceId == "photo3Upload"){
                cmp.set("v.fileUploadTo", "documentUrl3");
                showDp = cmp.get("v.showDP3");
            }else{
                cmp.set("v.fileUploadTo", "documentUrl4");
                showDp = cmp.get("v.showDP4");
            }
        }
        if((showDp == "false" || showDp == false)){
            // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        	cmp.set("v.isOpen", "true");
        }
        
    },
    
    changeProfilePhoto: function(cmp, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"          
        if(event.currentTarget.id !=undefined){
        	var  sourceId = event.currentTarget.id;
            if(sourceId == "photo1"){
                var CDLId = cmp.get("v.CDLId1");
                if(CDLId != undefined && CDLId != ""){
                    cmp.set("v.Spinner", "true");
                    helper.removeProfilePhoto(cmp, event,CDLId);
                }
                cmp.set("v.CDLId1","");
                cmp.set("v.documentUrl1","");
                cmp.set("v.showDP1", "false");
            }else if(sourceId == "photo2"){
                var CDLId = cmp.get("v.CDLId2");
                if(CDLId != undefined && CDLId != ""){
                    cmp.set("v.Spinner", "true");
                    helper.removeProfilePhoto(cmp, event,CDLId);
                }
                cmp.set("v.CDLId2","");
                cmp.set("v.documentUrl2","");
                cmp.set("v.showDP2", "false");
            }else if(sourceId == "photo3"){
                var CDLId = cmp.get("v.CDLId3");
                if(CDLId != undefined && CDLId != ""){
                    cmp.set("v.Spinner", "true");
                    helper.removeProfilePhoto(cmp, event,CDLId);
                }
                cmp.set("v.CDLId3","");
                cmp.set("v.documentUrl3","");
                cmp.set("v.showDP3", "false");
            }else{
                var CDLId = cmp.get("v.CDLId4");
                if(CDLId != undefined && CDLId != ""){
                    cmp.set("v.Spinner", "true");
                    helper.removeProfilePhoto(cmp, event,CDLId);
                }
                cmp.set("v.CDLId4","");
                cmp.set("v.documentUrl4","");
                cmp.set("v.showDP4", "false");
            }
            cmp.set("v.allFilled", "false");
        }
    }
})