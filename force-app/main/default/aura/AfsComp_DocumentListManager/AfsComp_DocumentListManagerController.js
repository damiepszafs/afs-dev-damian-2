({
    doInit: function(component, event, helper) {
        console.log('DOCUMENT LIST MANAGER HELPER');
        component.set("v.Spinner",true);
        component.set("v.applicationStage",component.get("v.applicationWrapper").applicationObj.Status__c);
   
        helper.fetchDocumentList(component, event);
        window.scrollTo(0, 0);
    },
    
    displayAction : function(component, event, helper) {
        var docIndex = event.currentTarget.getAttribute("data-selectIndex");
        var docData = component.get("v.docListWrap")[docIndex];
        console.log('displayAction');
        
        if(docData.item.RecordType.Name == "Custom" && docData.item.Type__c == "Task"){
            component.set("v.Spinner",true);
            var docDataLst = component.get("v.docListWrap");
            helper.updateToDoItem(component, event,docDataLst[docIndex].item);
            docDataLst[docIndex].item.Status__c = "Completed";
            docDataLst[docIndex].blockAction = true;
            component.set("v.docListWrap",docDataLst);
        }else{
            if(docData.item.COMPLETE_YOUR_PROFILE_BY__c){
                docData.isDocumentAction = true;
            }
            
            if(docData.item.PAY_APPLICATION_FEE__c){
                component.set("v.Spinner",true);
                helper.updateGLpayment(component, event,docData.item);
                window.location = component.get("v.configWrapper").GL_Payment_URL__c+"/Appfee/"+component.get("v.applicationWrapper").applicationObj.GL_Service_Id__c+"?redirect_url="+window.location.pathname; 
            }else if(docData.item.PAY_PROGRAM_DEPOSIT__c){
                component.set("v.Spinner",true);
                helper.updateGLpayment(component, event,docData.item);
                window.location = component.get("v.configWrapper").GL_Payment_URL__c+"/Deposit/"+component.get("v.applicationWrapper").applicationObj.GL_Service_Id__c+"?redirect_url="+window.location.pathname;
            }else{
                component.set("v.docActionData",docData);
                component.set("v.showAction",true);  
            }
            
        }
        window.scrollTo(0, 0);                        
    },
    
    itemsChange : function(component, event, helper) {
        var showAction = component.get("v.showAction");
        if(showAction == "false" || showAction == false){
            component.set("v.Spinner",true);
            helper.fetchDocumentList(component, event);
            //$A.get('e.force:refreshView').fire();
        }
        window.scrollTo(0, 0);
    },
    
    backToList : function(component, event, helper) {
        component.set("v.showAction",false);
        window.scrollTo(0, 0);
    },
    
    goToProgramPayments : function(component, event){
        window.location = component.get("v.configWrapper").GL_Payment_URL__c+"/Payment/"+component.get("v.applicationWrapper").applicationObj.GL_Service_Id__c+"?redirect_url="+window.location.pathname;
    }
    
})