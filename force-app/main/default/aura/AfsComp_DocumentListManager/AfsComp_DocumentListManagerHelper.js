({
	fetchDocumentList : function(component, event) {
		var action = component.get("c.getDocumentList");
        
        component.set("v.isAllDocumentSubmitted",false);
        action.setParams({ appId : component.get("v.applicationWrapper").applicationObj.Id , stage : component.get("v.appStage") });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var lstDocs = response.getReturnValue();
                var count = 0;
                var isOnBoarding = false;
               
                for(var i = 0; i < lstDocs.length;i++){
                    if(lstDocs[i].item.Stage_of_portal__c == "- Extra documentation needed"){
                        component.set("v.extraDocuments",true);
                    }
                    if(lstDocs[i].item.Stage_of_portal__c == "4. Stage: Get Ready"){
                        component.set("v.getReady",true);
                        isOnBoarding = true;
                         component.set("v.photoProgressClass","");
                        var applicationWrapper = component.get("v.applicationWrapper");
                        applicationWrapper.applicationObj.Status__c = "Onboarding";
                        component.set("v.applicationWrapper",applicationWrapper);                                       
                    }
                    if(lstDocs[i].item.Stage_of_portal__c == "1. Stage: Pre-application"){
                        component.set("v.isParticipationDesire",true);
                    }
                    if((lstDocs[i].blockAction == true) || (lstDocs[i].blockAction == "true")){
                       count++; 
                    }
                }
                component.set("v.docListWrap",lstDocs);  
                component.set("v.Spinner",false);
                component.set("v.noRecordsFound",(lstDocs.length == 0));
                
                component.set("v.isAllDocumentSubmitted",(count == lstDocs.length));
                if(isOnBoarding && (count == lstDocs.length)){
                    component.set("v.photoProgressClass","c100 p100");
                }
                //console.log(lstDocs);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    updateToDoItem : function(component, event,recordId) {
		var action = component.get("c.completeTask");
        action.setParams({ recId : recordId });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.Spinner",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    updateGLpayment : function(component, event, toDoItem){
        var applicationId = component.get("v.applicationWrapper").applicationObj.Id;
        var action = component.get("c.updateGLpayments");
        action.setParams({ 
            toDoItem : toDoItem,
            applicationId : applicationId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                console.log('Success updating gl payment');
            }else{
                console.log('Failured updating gl payment');
            }
        });
        $A.enqueueAction(action);
    }
})