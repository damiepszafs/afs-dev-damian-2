({
    doInit : function (component, event, helper){
        component.set("v.Spinner","true"); 
        helper.isAppUnderProcess(component,event);
        helper.getProgramApplication(component,event);
        var configObj = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        
        if(!$A.util.isEmpty(configObj.Scholarship_Available_Stages__c)){
            if(configObj.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isScholarshipDisbaled",false);
            }else{
                component.set("v.isScholarshipDisbaled",true);
            }
        }else{
            component.set("v.isScholarshipDisbaled",true);
        }
        
        if(!$A.util.isEmpty(configObj.GCC_Available_Stages__c)){
            if(configObj.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                if(applicationWrapper.applicationObj.Is_GCC_Section_Completed__c){
                	component.set("v.isGCCDisbaled",true);
                }else{
                	component.set("v.isGCCDisbaled",false);
                }
            }else{
                component.set("v.isGCCDisbaled",true);
            }
        }else{
            component.set("v.isGCCDisbaled",true);
        }
        
        // Disable Your Profile if
        // Application stage is not in available stages
        if(!$A.util.isEmpty(configObj.Your_Profile_Available_Stages__c)){
            if(configObj.Your_Profile_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                component.set("v.isYourProfileDisbaled",false);
            }else{
                component.set("v.isYourProfileDisbaled",true);
            }
        }else{
            component.set("v.isYourProfileDisbaled",true);
        }
        
        helper.getDynamicCustomLabelVal(configObj.PreSelected_Txt_ApplicationFeeCover__c,component);
        component.set("v.PreSelected_Txt_ApplicationFeeCover",component.get("v.LabelTempVal"));
        // Get Application cover options
        helper.getDynamicCustomLabelVal(configObj.PreSelected_Txt_ApplicationFeeCoverOptio__c,component);
        var optionsLabel = component.get("v.LabelTempVal");
        if(optionsLabel != undefined){
            component.set("v.ApplicationFeeCover",component.get("v.LabelTempVal").split(';'));
        }
        helper.getDynamicCustomLabelVal(configObj.PreSelected_Txt_PayForApplication__c,component);
        component.set("v.AfsLbl_PreSelected_PayButton",component.get("v.LabelTempVal"));
		helper.onInit(component,event);   
        window.scrollTo(0, 0);
        
        
    },
    
    navigateComponent : function(component, event, helper) {
        var idNavigate = event.currentTarget.id;
        var config = component.get("v.configWrapper");
        var applicationWrapper = component.get("v.applicationWrapper");
        
        if(idNavigate == "@0@"){
            component.set("v.detailNavigationWrapper", '0');
         	window.scrollTo(0, 0);
        }else if(idNavigate == "@1@"){
            if(config.Your_Profile_Available_Stages__c != undefined && config.Your_Profile_Available_Stages__c != ''){
                if(config.Your_Profile_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                    component.set("v.detailNavigationWrapper", '1');
         			window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@2@"){
            if(config.Scholarship_Available_Stages__c != undefined && config.Scholarship_Available_Stages__c != ''){
                if(config.Scholarship_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                    component.set("v.detailNavigationWrapper", '2');
                    window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@3@"){
            if(config.GCC_Available_Stages__c != undefined && config.GCC_Available_Stages__c != ''){
                if(config.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1 && !applicationWrapper.applicationObj.Is_GCC_Section_Completed__c){
                    component.set("v.detailNavigationWrapper", '3');
                    window.scrollTo(0, 0);
                }
            }
        }else if(idNavigate == "@4@"){
            component.set("v.detailNavigationWrapper", '4');
         	window.scrollTo(0, 0);
        }
	},
    
    handleEventNavigation :function(component, event, helper) {
   			var navigateTo = event.getParam("navigateTo");
       		component.set("v.detailNavigationWrapper", navigateTo);
         	window.scrollTo(0, 0);
    },
    
    doPayAndSave : function(component, event, helper){
        component.set("v.Spinner","true"); 
        var wrapperList = component.get("v.wrapperList");
        var countNum = 0;
        for(var i=0;i < wrapperList.length;i++){
            if(wrapperList[i].isSelected == true || wrapperList[i].isSelected == "true"){
                countNum++;
            }
        }
        if(countNum > 1){
            component.set("v.Spinner","false"); 
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = $A.get("$Label.c.AfsLbl_Application_MoreThanOne");
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            return;
        }
        
        if(countNum == 0){
            component.set("v.Spinner","false"); 
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = $A.get("$Label.c.AfsLbl_Error_SelectOnePrgmMessage");
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            return;
        }
        helper.onPayAndSave(component, event);
    },
    
    openChangeModel : function(component, event, helper){
        component.set("v.showChangeModel",true); 
    },
    
    closeModel : function(component, event, helper){
        component.set("v.showChangeModel",false); 
    },
    
    changeProgram : function(component, event, helper){
        component.set("v.showChangeModel",false); 
        component.set("v.Spinner",true);
        helper.onRemoveProgram(component, event);
    }
    
})