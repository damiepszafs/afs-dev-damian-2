({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    getProgramApplication : function (component,event){
        
        var action = component.get("c.getProgramOffered");
        action.setParams({
            "prId": component.get("v.applicationWrapper").applicationObj.Program_Offer__c
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.programOffrd",response.getReturnValue()); 
                
                var programOffrd = component.get("v.programOffrd");
        		var totalCostString = component.get("v.totalCostString");

        		totalCostString = this.formatMoney(programOffrd.programObj.Program_Price_numeric__c, 0, " ", " ");
        		component.set("v.totalCostString", totalCostString);


            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    isAppUnderProcess : function (component,event){
        
        var action = component.get("c.isAppProcessed");
        action.setParams({
            "contactId": component.get("v.contactInfo").Id,
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.isAppUnderProcessFlag",response.getReturnValue()); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onInit : function (component,event){
        
        var action = component.get("c.getProgramWrapper");
        action.setParams({
            "contactId": component.get("v.contactInfo").Id,
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                if(component.get("v.isAppUnderProcessFlag") == "false" || component.get("v.isAppUnderProcessFlag") == false){
                    var navigateWrapper = component.get("v.navigateWrapper");
                    navigateWrapper.isProgramDetail = false;
                    navigateWrapper.isProgramDetailSideBar = true;
                    navigateWrapper.isAboutYou = false;
                    navigateWrapper.isAboutYouSideBar = true;
                    navigateWrapper.isPortalSubmissionConfirmation =true;
                    navigateWrapper.isPortalSubmissionConfirmationSideBar = true;
                    navigateWrapper.isPreSelected = false;
                    navigateWrapper.isPreSelectedSideBar = false;
                    navigateWrapper.isSelectProgram = false;
                    navigateWrapper.isSelectProgramSideBar = true;
                    component.set("v.navigateWrapper",navigateWrapper);
                	component.set("v.Spinner","false"); 
                }else{
                    component.set("v.wrapperList",response.getReturnValue());                 
                	component.set("v.Spinner","false"); 
                }
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onPayAndSave : function (component,event){
        
        var action = component.get("c.payAndSave");
        var wrapperList = component.get("v.wrapperList");
        var prId;
        for(var i=0;i < wrapperList.length;i++){
            if(wrapperList[i].isSelected == true || wrapperList[i].isSelected == "true"){
                prId = wrapperList[i].programObj.Id;
            }
        }
        action.setParams({
            "contactId": component.get("v.contactInfo").Id,
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id,
            "programOfferId": prId
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {
                var applicationWrapper = component.get("v.applicationWrapper");
                applicationWrapper.applicationObj.Program_Offer__c = prId;
                applicationWrapper.applicationObj.Status_App__c = "Pre-Selected";
            	applicationWrapper.applicationObj.Status__c = "Decision";
                applicationWrapper.currencyStr = response.getReturnValue();
                component.set("v.applicationWrapper",applicationWrapper);
                var navigateWrapper = component.get("v.navigateWrapper");
                navigateWrapper.isWaitlisted = false;
                navigateWrapper.isPreSelected = true;
                
                component.set("v.navigateWrapper",navigateWrapper);
                component.set("v.isDocumentCheckList",true);
                this.getProgramApplication(component,event);
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onRemoveProgram : function (component,event){
        var applicationWrapper = component.get("v.applicationWrapper");
        var applicationObj = applicationWrapper.applicationObj;
        var action = component.get("c.doChangeRequest");
        
        action.setParams({
            "applicationObj": applicationObj
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {
               var applicationWrapper = component.get("v.applicationWrapper");
                var navigateWrapper = component.get("v.navigateWrapper");
                navigateWrapper.isProgramDetail = false;
                navigateWrapper.isProgramDetailSideBar = false;
                navigateWrapper.isAboutYou = false;
                navigateWrapper.isAboutYouSideBar = false;
                navigateWrapper.isPortalSubmissionConfirmation =false;
                navigateWrapper.isPortalSubmissionConfirmationSideBar = false;
                navigateWrapper.isPreSelected = false;
                navigateWrapper.isPreSelectedSideBar = false;
                navigateWrapper.isSelectProgram = true;
                navigateWrapper.isSelectProgramSideBar = true;
                component.set("v.navigateWrapper",navigateWrapper);
                applicationWrapper.applicationObj = response.getReturnValue();
                component.set("v.applicationWrapper",applicationWrapper);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
	formatMoney : function(n, c, d, t) {
		  var c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;

  		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
})