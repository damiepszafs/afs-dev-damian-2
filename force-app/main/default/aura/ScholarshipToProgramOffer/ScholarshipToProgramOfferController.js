({
	doInit: function(component, event, helper) {
        helper.getProgram_Offers(component);      
     },
    
    create_Records: function(component, event, helper) {  
        helper.createRecords(component);
     },
    
    changeSelected: function(component, event, helper) {  
		var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.record;
        
        var setCodes = new Set();
        if (component.get('v.Program_Offers_Selected')) {
            setCodes = component.get('v.Program_Offers_Selected');
        }
        if (setCodes.has(id)) {            
            setCodes.delete(id);
        } else {
            setCodes.add(id);
        }
        component.set('v.Program_Offers_Selected', setCodes);        
     },

    checkAll: function(component, event, helper) {
        var master = document.getElementById('checkboxAll');
        var boxPack = document.getElementsByName('options');
        if(boxPack != null){
            var val = master.checked;
            var setCodes = new Set();
            for (var i = 0; i < boxPack.length; i++) {
                boxPack[i].checked = val;
                if(val == true){
                    setCodes.add(boxPack[i].dataset.record);
                }
            }
            component.set('v.Program_Offers_Selected', setCodes);
    	}
     },
})