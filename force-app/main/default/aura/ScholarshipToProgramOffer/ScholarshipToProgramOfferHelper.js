({
	getProgram_Offers: function(component) {
        var action = component.get('c.getProgram_Offers');
        
        action.setParams({
            recordId: component.get('v.recordId')
        });

        action.setCallback(this, function(actionResult) {            
         	component.set('v.Program_Offers', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }, 
    
    createRecords: function(component) {
        var action = component.get('c.createRecords');
		
         action.setParams({
             strJSONIds: JSON.stringify(Array.from(component.get('v.Program_Offers_Selected'))),             
             recordId: component.get('v.recordId')
        });
        
        action.setCallback(this, function(result) {
        	
            var response = new Array();
            response = result.getReturnValue();
            
            if(response.length > 0){
                if(response.length == 1){
                	alert(response.length + ' Scholarship in Program record was created');
                }else{
                    alert(response.length + ' Scholarship in Program records were created');
                }
            }
            var closePanel = $A.get("e.force:closeQuickAction");
            closePanel.fire();
        });

        $A.enqueueAction(action);
    },
})