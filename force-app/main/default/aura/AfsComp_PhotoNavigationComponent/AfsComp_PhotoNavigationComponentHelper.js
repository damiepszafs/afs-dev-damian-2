({
    onInit :  function(cmp, event) {
        var action = cmp.get("c.getProfilePhoto");
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                if(response.getReturnValue()[0] != "NoFile"){
                    cmp.set("v.documentUrl",response.getReturnValue()[1]);
                    cmp.set("v.showDP","true");
                    cmp.set("v.CDLId", response.getReturnValue()[0]);
                }
                
                cmp.set("v.Spinner", "false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
    },
    
	getBase64String : function(cmp, event, documentId) {
		
        var action = cmp.get("c.getEncodedString");
        action.setParams({
            "fileId": documentId,
            "isProfilePhoto": true
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                cmp.set("v.documentUrl",response.getReturnValue());
                cmp.set("v.showDP","true");
                cmp.set("v.Spinner", "false");
                var compEvent = cmp.getEvent("UpdateSideProfileImage");
                // A parameter’s name must match the name attribute
                // of one of the event’s <aura:attribute> tags
                compEvent.setParams({"imgUrl" : response.getReturnValue()});
                compEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    removeProfilePhoto : function(cmp, event) {
		var CDLId = cmp.get("v.CDLId");
        
        var action = cmp.get("c.deleteCDL");
        action.setParams({
            "documentId": CDLId
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                cmp.set("v.showDP","false");
                cmp.set("v.CDLId","");
                cmp.set("v.Spinner", "false");
                var compEvent = cmp.getEvent("UpdateSideProfileImage");
                var emptyString = '';
                // A parameter’s name must match the name attribute
                // of one of the event’s <aura:attribute> tags
                compEvent.setParams({"imgUrl" : emptyString});
                compEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
        
    }
})