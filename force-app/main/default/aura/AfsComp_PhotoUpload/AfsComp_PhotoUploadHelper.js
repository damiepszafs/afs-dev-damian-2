({
	getBase64String : function(cmp, event, documentId) {
		
        var action = cmp.get("c.getEncodedString");
        action.setParams({
            "fileId": documentId,
            "isProfilePhoto": true
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                cmp.set("v.documentUrl",response.getReturnValue());
                cmp.set("v.showDP","true");
                cmp.set("v.isOpen","false");
                cmp.set("v.Spinner", "false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }  
            
        });
        $A.enqueueAction(action);
    } 
})