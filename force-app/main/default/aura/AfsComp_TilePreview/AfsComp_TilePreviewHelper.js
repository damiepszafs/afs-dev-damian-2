({
	formatMoney : function(n, c, d, t) {
		  var c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                j = (j = i.length) > 3 ? j % 3 : 0;

  		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
},
    parseProgramMonth : function(monthNumber){
        var month;
        
        switch(monthNumber){
            case '01':
                month = $A.get("$Label.c.Month_January");
                break;
            case '02':
                month = $A.get("$Label.c.Month_February");
                break;
            case '03':
                month = $A.get("$Label.c.Month_March");
                break;
            case '04':
                month = $A.get("$Label.c.Month_April");
                break;
            case '05':
                month = $A.get("$Label.c.Month_May");
                break;
            case '06':
                month = $A.get("$Label.c.Month_June");
                break;
            case '07':
                month = $A.get("$Label.c.Month_July");
                break;
            case '08':
                month = $A.get("$Label.c.Month_August");
                break;
            case '09':
                month = $A.get("$Label.c.Month_September");
                break;
            case '10':
                month = $A.get("$Label.c.Month_October");
                break;
            case '11':
                month = $A.get("$Label.c.Month_November");
                break;
            case '12':
                month = $A.get("$Label.c.Month_December");
                break;
        }
        return month;
    },  
})