({
    openLink : function(component, event, helper) {
        event.stopImmediatePropagation();
        var wrapperList = component.get("v.wrapperObj");
        window.open(wrapperList.programObj.Program_Website_URL__c ,"_blank");
    },
    doSelect : function(component, event, helper) {
        var wrapperList = component.get("v.wrapperObj");
           
        if(wrapperList.isSelected === true){
            wrapperList.isSelected = false;
        }else{
            wrapperList.isSelected = true;
        }         
        
        /*var labelSubStr = component.get("v.labelTestString");
        var labelReference = $A.getReference("$Label.c." + labelSubStr);
        component.set("v.labelTestVal", labelReference);
        var dynamicLabel = component.get("v.labelTestVal");
        alert(dynamicLabel);
        var locale = $A.get("$Locale.userLocaleLang");

        alert(locale);*/
        component.set("v.wrapperObj",wrapperList);
	},
	
	doInit : function (component, event, helper){
		var wrapperObj = component.get("v.wrapperObj");
        var totalCostString = component.get("v.totalCostString");
        if(wrapperObj.programObj.Program_Price_numeric__c != 0 && wrapperObj.programObj.Program_Price_numeric__c !== undefined){
            totalCostString = wrapperObj.programObj.Currency__c + ' ' + helper.formatMoney(wrapperObj.programObj.Program_Price_numeric__c, 0, " ", " ");
        }
        else{
            totalCostString = "-";
        }
        component.set("v.totalCostString", totalCostString);
        //Parse the program date
        var month;
        var programDate;
        
        if(component.get("v.wrapperObj.programObj.From__c")){
           month = helper.parseProgramMonth(component.get("v.wrapperObj.programObj.From__c").substring(5, 7));
           programDate = month + ' ' + component.get("v.wrapperObj.programObj.From__c").substring(0, 4) + ' - '; 
        }
        if(component.get("v.wrapperObj.programObj.To__c")){
            month = helper.parseProgramMonth(component.get("v.wrapperObj.programObj.To__c").substring(5, 7));
            programDate = programDate + month + ' ' + component.get("v.wrapperObj.programObj.To__c").substring(0, 4);
        }
        component.set("v.ProgramDate", programDate);
	}
})