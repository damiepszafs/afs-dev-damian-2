({
	getDynamicCustomLabelVal :  function (labelStr,component){
        //var labelReference = $A.getReference("$Label.c." + labelStr);
        //component.set("v.LabelTempVal", labelReference);
        if(labelStr != undefined && labelStr != "" ){
            var labelReference = $A.getReference("$Label.c." + labelStr);
        	component.set("v.LabelTempVal", labelReference);
        }else{
            component.set("v.LabelTempVal", "");
        }
    },
    
    onInit : function (component,event){
        
        var action = component.get("c.getApplicationProgram");
        action.setParams({
            "applicationId": component.get("v.applicationWrapper").applicationObj.Program_Offer__c
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.programOffer",response.getReturnValue());                 
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            } 
        });
        $A.enqueueAction(action);
    },
    
    onSaveRecords : function (component,event){
        
        var action = component.get("c.updateApplication");
        action.setParams({
            "campURL": component.get("v.programOffer").Camp_URL__c,
            "applicationId": component.get("v.applicationWrapper").applicationObj.Id,
            "recId" : component.get("v.record").item.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var applicationWrapper = component.get("v.applicationWrapper");
				applicationWrapper.applicationObj.Camp_link_clicked__c = component.get("v.programOffer").Camp_URL__c;               
                //component.set("v.programOffer",response.getReturnValue());    
                //        
                component.set("v.parentToggle",false);     
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            } 
        });
        $A.enqueueAction(action);
    }
})