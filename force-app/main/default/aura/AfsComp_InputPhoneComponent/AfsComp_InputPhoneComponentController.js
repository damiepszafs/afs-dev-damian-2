({	
	doInit: function(component, event, helper) {
		const savedNumber = component.get("v.phone");
		helper.fetchPickListVal(component, savedNumber);
	},
    loadedScript: function(component, event, helper) {
        //After loading the library fill dynamically the combobox
        const savedNumber = component.get("v.phone");
        if (savedNumber !== undefined && savedNumber !== "" && savedNumber !== "invalid") {
        	try{
	        	const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
	        	const phone = phoneUtil.parse(savedNumber, "");
	        	const phoneRegion = phoneUtil.getRegionCodeForNumber(phone);
	        	component.set("v.countryCodeSelected", phoneRegion);
	        	component.find("InputSelectSingle").set("v.value", phoneRegion);
	        	const AsYouTypeFormatter = libphonenumber.AsYouTypeFormatter;
		        const formatter = new AsYouTypeFormatter(phoneRegion);
		        const arrayInput = [...phoneUtil.format(phone, libphonenumber.PhoneNumberFormat.INTERNATIONAL).replace("+" + phone.getCountryCode() + " ", "")];
		        arrayInput.filter(s => !(s === "-" || s === "(" || s === ")" || s === " ")).map(input => {
		            formatter.inputDigit(input)
		            return input
		        })
		        helper.validateNumber(phoneRegion, formatter.currentOutput_, component)
        	} catch (e) {
        		const [label, pRegion] = component.get("v.defaultCode").split(' - ');
        		component.set("v.countryCodeSelected", pRegion);
        		component.find("InputSelectSingle").set("v.value", pRegion);
        		component.set("v.phoneNumber", savedNumber);
        		const inputCmp = component.find("inputCmp");
        		inputCmp.set("v.errors", [{
                    message: "❌"
                }]);
        	}
        	
        } else {
        	const [label, phoneRegion] = component.get("v.defaultCode").split(' - ');
        	component.set("v.countryCodeSelected", phoneRegion);
        	component.find("InputSelectSingle").set("v.value", phoneRegion);
        }
        
        component.set("v.pnDisabled", false)	
        //Enable country codes field input.
        component.set("v.ccIsLoading", false);
    },
    countrySelected: function(component, evt, helper) {
        const componentSelect = component.find("InputSelectSingle");
        const countrySelected = componentSelect.get("v.value");
        component.set("v.countryCodeSelected", countrySelected)
        component.set("v.pnDisabled", false)
        const countries = component.find("InputSelectSingle").get("v.options");
        const countriesSelected = countries.map(c => ({
            label: c.label,
            value: c.value,
            selected: countrySelected === c.value ? true : false
        }));
        component.find("InputSelectSingle").set("v.options", countriesSelected);
        helper.validateNumber(countrySelected, component.get("v.phoneNumber"), component);
    },
    handleInputChange: function(component, evt, helper) {
        const AsYouTypeFormatter = libphonenumber.AsYouTypeFormatter;
        const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
        const countryCode = component.get("v.countryCodeSelected");
        //The formatter should be saved as static var.
        //Because this is not possible, every time the functions run, initiate the formatter again
        //and adds each digit as if they were when the user types.
        const formatter = new AsYouTypeFormatter(countryCode);
        const input = component.find("inputCmp").get("v.value");
        const arrayInput = [...input];
        arrayInput.filter(s => !(s === "-" || s === "(" || s === ")" || s === " ")).map(input => {
            formatter.inputDigit(input)
            return input
        })
        helper.validateNumber(countryCode, formatter.currentOutput_, component)
    }
})