({
    doInit :function(component, event, helper) {
		helper.setFields(component, event);
	},
	increase : function(component, event, helper) {
		helper.increase(component, event);
	},
    decrease : function(component, event, helper) {
		helper.decrease(component, event);
	}
    
})