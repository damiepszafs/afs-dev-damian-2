({
	doInit : function(component, event, helper) {
        	var view = component.get('v.detailNavigationWrapper');
        console.log('on change');
        if(view =='0'){
           helper.getSelectedPrograms(component,event); 
           
        }else if(view =='1' ){
           
           helper.populateNationalityHeader(component,event); 
        }
           
	},
    updateHeader : function(component, event, helper) {
        var view = component.get('v.detailNavigationWrapper');
        if(view =='0'){
           helper.getSelectedPrograms(component,event); 
        }else if(view =='1' ){
           helper.populateNationalityHeader(component,event); 
        }
        else if(view =='2' ){
           helper.populateScholaShipHeader(component,event); 
        }
        else if(view =='3' ){
           helper.populateGlobalCompentenceHeader(component,event); 
        }
    }
})