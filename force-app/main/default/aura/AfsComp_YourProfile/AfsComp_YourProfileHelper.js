({
    fetchPickListVal: function(component, fieldName, objName) {
        var NotSelc = $A.get("$Label.c.AfsLbl_YourProfile_NotSelected");
        var NotSelcObj = new Object();
        NotSelcObj.fieldVal = NotSelc;
        NotSelcObj.fieldLabel = NotSelc;
        component.set('v.NotSelectedObj', NotSelcObj);
        component.set('v.NotSelected', NotSelc);
        var action = component.get("c.getPicklistValues");

        action.setParams({
            "objObject": objName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (objName == 'Contact') {
                    component.set("v.NatinalityPicklist", allValues['NationalityCitizenship__c']);
                    component.set("v.DualCitizenShipPicklist", allValues['Dual_citizenship__c']);
																					  
																			   

                    var LGBTQ = allValues['LGBTQ_Member__c']
                    var notSelectdVal = component.get('v.NotSelectedObj');
                    LGBTQ.splice(0, 0, notSelectdVal);
                    component.set("v.LGBTQPickList", LGBTQ);
                    var Ethinicity = allValues['Ethnicity__c']
                    Ethinicity.splice(0, 0, notSelectdVal);
                    component.set("v.EthinicityPickList", Ethinicity);
                } else {
                    component.set("v.ParentandLegalGardian", allValues['npe4__Type__c']);
                }
            } else {
                console.log(response.getError());

            }
        });

        var actionApplication = component.get("c.getPicklistValues");
        actionApplication.setParams({
            "objObject": 'Application__c',
            "fld": 'Religious_affiliation__c,Access_to_religious_services__c,Ability_to_live_with_household_pets__c'
        });

        var optsApplication = [];
        actionApplication.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.ReligiousAffilationPickList", allValues['Religious_affiliation__c'])
                component.set("v.AccessToRegiousPickList", allValues['Access_to_religious_services__c'])
                component.set("v.HouseholdPetsPickList", allValues['Ability_to_live_with_household_pets__c'])
                component.set("v.Spinner", "false");
            } else {
                console.log(response.getError());
                component.set("v.Spinner", "false");

            }
        });

        $A.enqueueAction(action);
        $A.enqueueAction(actionApplication);
    },
    getDynamicCustomLabelVal: function(labelStr, component) {
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    setFields: function(component, event) {
        var ageUnder18 = true;
        var today = new Date();
        var past = component.get("v.contactInfo.Birthdate"); // remember this is equivalent to 06 01 2010 
        if (past != undefined) {
            var birthDate = new Date(past);
            var diff = Math.floor(today - birthDate);
            var day = 1000 * 60 * 60 * 24;
            var days = Math.floor(diff / day);
            var months = Math.floor(days / 31);
            var years = Math.floor(months / 12);
            if (years >= 18) {
                ageUnder18 = false;
            }
        }

        component.set("v.ageUnder18", ageUnder18);

        var counter = component.get("v.contactInfo.Home_Sister_Stepsister__c");
        var sisterAgeTemp = [];
        var sisterAge = '';
        if (component.get("v.contactInfo.Home_Sister_Stepsisters_Age__c") != undefined) {
            sisterAge = component.get("v.contactInfo.Home_Sister_Stepsisters_Age__c").split(";");
        }
        if (counter != undefined && counter != null) {
            for (var i = 0; i < counter; i++) {
                if (sisterAge != '' && sisterAge.length >= i) {
                    var rec = {
                        'age': sisterAge[i]
                    }
                    sisterAgeTemp.push(rec);
                } else {
                    var tmp = {
                        'age': ''
                    };
                    sisterAgeTemp.push(tmp);
                }
            }
        }

        component.set("v.sisterAgeTemp", sisterAgeTemp);


        var counter1 = component.get("v.contactInfo.Home_Brother_Stepbrother__c");
        var BrotherAgeTemp = [];
        var brotherAge = '';
        if (component.get("v.contactInfo.Home_Brother_Stepbrothers_Age__c") != undefined) {
            brotherAge = component.get("v.contactInfo.Home_Brother_Stepbrothers_Age__c").split(";");
        }

        if (counter1 != undefined && counter1 != null) {
            for (var i = 0; i < counter1; i++) {
                if (brotherAge != '' && brotherAge.length >= i) {
                    var rec = {
                        'age': brotherAge[i]
                    }
                    BrotherAgeTemp.push(rec);
                } else {
                    var tmp = {
                        'age': ''
                    };
                    BrotherAgeTemp.push(tmp);
                }
            }
        }
        component.set("v.brotherAgeTemp", BrotherAgeTemp);
    },
    saveCompleteProfile: function(component, helper) {
        var cmpEvent = component.getEvent("afscompnavigation");
        cmpEvent.setParam("navigateTo", "2");
        var vaidationResult = this.validateForm(component, helper);
        if (vaidationResult != '') {
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = vaidationResult;
            toastEvent.setParams({
                type: "Error",
                duration: 5000,
                message: messageTemplate,
                mode: "sticky"
            });
            toastEvent.fire();
            return;
        }
        
        var parentGuardianWrapperList = this.createRelationWrapper(component, helper);
        component.set("v.Spinner", "true");
        var action = component.get("c.saveCompleteProfile");
        var contactInfo = component.get("v.contactInfo");
        
        var wrapperObj = component.get("v.applicationWrapper");
		var selectedValFamily = component.get("v.selectedValuesFamilyDesc");
		wrapperObj.applicationObj.How_would_friends_family_describe_you__c = selectedValFamily;
		wrapperObj.applicationObj.Is_Profile_Section_Completed__c = true;
		
        var School = JSON.parse(JSON.stringify(component.get("v.School")))
		var SchoolAppObj = JSON.parse(JSON.stringify(component.get("v.SchoolApplication")))
        action.setParams({
            "contactInfo": contactInfo,
            "applicationObj": wrapperObj.applicationObj,
            "schoolAppObj": JSON.stringify(SchoolAppObj),
            "school": School,
            "parentGuardianWrapperJSON": JSON.stringify(parentGuardianWrapperList)
        });
												
											   
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var configWrapper = component.get("v.configWrapper");
                var isScholarshipDisbaled;
                if (configWrapper.Scholarship_Available_Stages__c != undefined && configWrapper.Scholarship_Available_Stages__c != '') {
                    if (configWrapper.Scholarship_Available_Stages__c.indexOf(wrapperObj.applicationObj.Status__c) != -1) {
                        isScholarshipDisbaled = false;
                    } else {
                        isScholarshipDisbaled = true;
                    }
                } else {
                    isScholarshipDisbaled = true;
                }
                if (!isScholarshipDisbaled) {
                    cmpEvent.fire();
                } else {
                    window.scrollTo(0, 0);
                }
                
                component.set("v.applicationWrapper", wrapperObj);
                component.set("v.Spinner", "false");
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError();
                toastEvent.setParams({
                    type: "Error",
                    duration: 5000,
                    message: messageTemplate,
                    mode: "sticky"
                });
                toastEvent.fire();
                component.set("v.Spinner", "false");
            }
        });
        $A.enqueueAction(action);
    },
    validatedParentComp: function(component, event) {

        var messageTemplateStr = '';
        var parentList = component.get('v.ParentGuardianWrapper');
        var emergencyContact = false;
        var parentName = ''
											 
        for (var i = 0; i < parentList.length; i++) {
																				   
										
			 

            if (parentList[i].parentSelected == '') {
                var num = i + 1;
                parentName = 'Parent detail no ' + num;
            } else {
                if (parentList[i].parentSelected != 'Other') {
                    parentName = 'Your ' + parentList[i].parentSelected + '\'s';
                } else {
                    parentName = 'Your ' + parentList[i].relation.npe4__Type__c + '\'s';
                }

            }

            if ($A.util.isEmpty(parentList[i].parentSelected)) {
                messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") + "\n";
            } else {
                if (parentList[i].parentSelected == 'Other' && $A.util.isEmpty(parentList[i].relation.npe4__Type__c)) {
                    messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") + "\n";
                }
            }
            if ($A.util.isEmpty(parentList[i].relatedContact.FirstName)) {
                messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianName") + "\n";
            }
            if ($A.util.isEmpty(parentList[i].relatedContact.LastName)) {
                messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianLastName") + "\n";
            }
            //---------------------------------------------------------------------------
            if (!$A.util.isEmpty(parentList[i].relatedContact.Phone)) {
                if (parentList[i].relatedContact.Phone === "invalid") {
                    messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneNoValid") + "\n";
                }
            } else if (component.get("v.configWrapper.Submission_Fld_ParentGuardian_Phone__c") == 'Required') {
                messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_PhoneRequire") + "\n";
            }

            if (!$A.util.isEmpty(parentList[i].relatedContact.Email)) {
                var validEmail = this.validateEmail(parentList[i].relatedContact.Email);
                if (validEmail != true) {
                    messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailNoValid") + "\n";
                }
            } else if (component.get("v.configWrapper.Submission_Fld_ParentGuardian_Email__c") == 'Required') {
                messageTemplateStr += parentName + ' ' + $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailRequired") + "\n";
            }

        }
																																	 
																										
		 


        /* if($A.util.isEmpty(component.get("v.contactInfo.Parent_legal_Guardian__c"))){
              messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianRequire") +"\n";
         }else if(component.get("v.contactInfo.Parent_legal_Guardian__c")){
             if( component.get("v.contactInfo.Parent_legal_Guardian__c") == 'Other' && $A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Other__c"))){
                  messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneNoValid")+"\n";
                
             }
         }
          if($A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Name__c"))){
              messageTemplateStr +=   $A.get("$Label.c.AfsLbl_Error_ParentGuardianName") +"\n"; 
         }
          if($A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Last_Name__c"))){
              messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianLastName")  +"\n"; 
         }
          if($A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Phone__c") || isNaN(component.get("v.contactInfo.Parent_Legal_Guardian_Phone__c")))){
              messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianPhoneNoValid") +"\n";
         }
          if($A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Email__c"))){
              messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ParentGuardianEmailRequired") +"\n";
         }
         
         if(!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_You_live_with_this_parent__c")) && component.get("v.configWrapper.Submission_Fld_You_live_with_this_parent__c") == 'Required'){
             if($A.util.isEmpty(component.get("v.contactInfo.You_live_with_this_Parent_Guardian__c") || component.get("v.contactInfo.You_live_with_this_Parent_Guardian__c") != true)) {
                	messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Live_with_guardian") +"\n";
               } 
         }  
         
         if(!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Parent_Emergency_contact__c")) && component.get("v.configWrapper.Submission_Fld_Parent_Emergency_contact__c") == 'Required'){
             if($A.util.isEmpty(component.get("v.contactInfo.Parent_Legal_Guardian_Emergency_Contact__c"))|| component.get("v.contactInfo.Parent_Legal_Guardian_Emergency_Contact__c") != true ) {
                	messageTemplateStr +=  $A.get("$Label.c.AfsLbl_Error_This_is_your_emergency_contact") +"\n";
               }
         }  */

        return messageTemplateStr;

    },
    validateForm: function(component, event) {
        var isUnder18 = component.get("v.ageUnder18");
        var messageTemplateStr = "";
        var configWrapper = component.get("v.configWrapper");
        var applicationObj = component.get("v.applicationWrapper.applicationObj");
        var selectedValFamily = component.get("v.selectedValuesFamilyDesc");
		if(configWrapper.PersonalDetail_Fld_Nationality__c == 'Required'){
            if ($A.util.isEmpty(component.get("v.contactInfo.NationalityCitizenship__c")) || component.get("v.contactInfo.NationalityCitizenship__c") == "-None-") {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Nationality") + "\n";
            }
        }
        
        if(configWrapper.Submission_Fld_Dual_Citizen__c == 'Required'){
            if ($A.util.isEmpty(component.get('v.contactInfo.Dual_citizenship__c'))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Dual_citizenship") + "\n";
            } else {
                if (component.get('v.contactInfo.Dual_citizenship__c') == 'Yes') {
                    if ($A.util.isEmpty(component.get("v.contactInfo.SecondCitizenship__c")) || component.get("v.contactInfo.SecondCitizenship__c") == "-None-") {
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Secondary_citizenship") + "\n";
                    }
                } else {
                    component.set("v.contactInfo.SecondCitizenship__c", null);
                }
            }
        }
        

        if (!$A.util.isEmpty(isUnder18) && isUnder18 == true) {
            if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Father__c")) && component.get("v.configWrapper.Submission_Fld_Home_Father__c") == 'Required') {
                if ($A.util.isEmpty(component.get("v.contactInfo.Home_Father__c"))) {
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Home_Father") + "\n";
                }
            }

            if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c")) && component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c") == 'Required') {
                if ($A.util.isEmpty(component.get("v.contactInfo.Home_Sister_Stepsister__c"))) {
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Home_Sister_Stepsister") + "\n";
                }
            }
            messageTemplateStr += this.validatedParentComp(component, event);
            messageTemplateStr += this.prepareFormData(component, event);

        }

        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Mother__c")) && component.get("v.configWrapper.Submission_Fld_Home_Mother__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Home_Mother__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Home_Mother") + "\n";
            }
        }

        if ($A.util.isEmpty(component.get('v.applicationWrapper.applicationObj.Ability_to_live_with_household_pets__c'))) {
            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Ability_To_live_with_household_pets") + "\n";;
        }

        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Instagram__c")) && component.get("v.configWrapper.Submission_Fld_Instagram__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Instagram__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Instagram") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Facebook__c")) && component.get("v.configWrapper.Submission_Fld_Facebook__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Facebook__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Facebook") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_LinkedIn__c")) && component.get("v.configWrapper.Submission_Fld_LinkedIn__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.LinkedIn__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_linkedIn") + "\n";
            }
        }

        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Twitter__c")) && component.get("v.configWrapper.Submission_Fld_Twitter__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Twitter__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Twitter") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Snapchat__c")) && component.get("v.configWrapper.Submission_Fld_Snapchat__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Snapchat__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SnapChat") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_YouTube__c")) && component.get("v.configWrapper.Submission_Fld_YouTube__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.YouTube__c"))) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Youtube") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Religious_af_liation__c")) && component.get("v.configWrapper.Submission_Fld_Religious_af_liation__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.applicationWrapper.applicationObj.Religious_affiliation__c")) || component.get("v.applicationWrapper.applicationObj.Religious_affiliation__c") == '-None-') {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Religious_af_liation") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Access_To_Religious_Servi__c")) && component.get("v.configWrapper.Submission_Fld_Access_To_Religious_Servi__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.applicationWrapper.applicationObj.Access_to_religious_services__c")) || component.get("v.applicationWrapper.applicationObj.Access_to_religious_services__c") == '-None-') {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Religious_Services") + "\n";
            }
        }



        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Are_You_Open_To__c")) && component.get("v.configWrapper.Submission_Fld_Are_You_Open_To__c") == 'Required') {
            var isSelected = false;
            if (!$A.util.isEmpty(component.get("v.applicationWrapper.applicationObj.Open_to_Same_Sex_Host_Parents__c")) && component.get("v.applicationWrapper.applicationObj.Open_to_Same_Sex_Host_Parents__c") == true) {
                isSelected = true;
            } else if (!$A.util.isEmpty(component.get("v.applicationWrapper.applicationObj.Open_to_Single_Host_Parent__c")) && component.get("v.applicationWrapper.applicationObj.Open_to_Single_Host_Parent__c") == true) {
                isSelected = true;
            } else if (!$A.util.isEmpty(component.get("v.applicationWrapper.applicationObj.Open_to_Sharing_a_host_family__c")) && component.get("v.applicationWrapper.applicationObj.Open_to_Sharing_a_host_family__c") == true) {
                isSelected = true;
            }
            if (isSelected == false) {

                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Open_To") + "\n";
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_DescribeYourself__c == "Required"){
            if(selectedValFamily === undefined){
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SelectOneDescribeYourSelf") + "\n";
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_WhatExperience__c == "Required"){
            if(applicationObj.What_do_you_want_out_of_this_experience__c === undefined || applicationObj.What_do_you_want_out_of_this_experience__c == "")
            {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_WriteAboutExperience") + "\n";
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_TellUsAboutYourself__c == "Required"){
            if(applicationObj.Tell_us_about_yourself__c === undefined || applicationObj.Tell_us_about_yourself__c == "")
            {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_WriteAboutYourself") + "\n";
            }
        }

        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_LGBTQ_Member__c")) && component.get("v.configWrapper.Submission_Fld_LGBTQ_Member__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.LGBTQ_Member__c")) || component.get("v.contactInfo.LGBTQ_Member__c") == component.get("v.NotSelected")) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_LGTBQ") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Ethnicity__c")) && component.get("v.configWrapper.Submission_Fld_Ethnicity__c") == 'Required') {
            if ($A.util.isEmpty(component.get("v.contactInfo.Ethnicity__c")) || component.get("v.contactInfo.Ethnicity__c") == component.get("v.NotSelected")) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Ethinicity") + "\n";
            }
        }
        if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Photos__c")) && component.get("v.configWrapper.Submission_Fld_Photos__c") == 'Required') {
            if (component.get("v.allFilled") == "false" || component.get("v.allFilled") == false || component.get("v.allFilled") === undefined) {
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_YourPhotosRequired") + "\n";
            }
        }

        return messageTemplateStr;

    },
    prepareFormData: function(component, event) {
        debugger;

        var messageTemplateStr = ''

        var contactInfo = component.get("v.contactInfo");
        var brotherTempAge = component.get("v.brotherAgeTemp");
        var sisteTempAge = component.get("v.sisterAgeTemp");
        var brotherAge = '';
        var sisterAge = '';
        for (var i = 0; i < brotherTempAge.length; i++) {
            if (!$A.util.isEmpty(brotherTempAge[i].age)) {
                if (!isNaN(brotherTempAge[i].age) && parseInt(brotherTempAge[i].age) < 99) {
                    if (brotherAge == '') {
                        brotherAge += brotherTempAge[i].age;
                    } else {
                        brotherAge += ';' + brotherTempAge[i].age;
                    }

                } else {
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Brother_Age") + "\n"
                    break;
                }
            }
        }
        for (var i = 0; i < sisteTempAge.length; i++) {
            if (!$A.util.isEmpty(sisteTempAge[i].age)) {
                if (!isNaN(sisteTempAge[i].age) && parseInt(sisteTempAge[i].age) < 99) {
                    if (sisterAge == '') {
                        sisterAge += sisteTempAge[i].age;
                    } else {
                        sisterAge += ';' + sisteTempAge[i].age;
                    }
                } else {
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Sister_Age") + "\n";
                    break;
                }
            }
        }
        if (contactInfo.Home_Sister_Stepsister__c != undefined && contactInfo.Home_Sister_Stepsister__c > 0) {
            if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c")) && component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c") == 'Required') {
                if (contactInfo.Home_Sister_Stepsister__c == undefined) {
                    component.set("v.contactInfo.Home_Sister_Stepsister__c", 0);
                } else {
                    if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Sister_Stepsisters_Age__c")) && component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c") == 'Required') {
                        if ($A.util.isEmpty(sisterAge)) {
                            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SisterAgeRequired") + "\n";
                        } else if (sisterAge.split(';').length != contactInfo.Home_Sister_Stepsister__c) {
                            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SisterAgeRequired") + "\n";
                        }
                    }
                }
            } else if (component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c") != 'Hide') {
                if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Sister_Stepsisters_Age__c")) && component.get("v.configWrapper.Submission_Fld_Home_Sister_Stepsister__c") == 'Required') {
                    if ($A.util.isEmpty(sisterAge)) {
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SisterAgeRequired") + "\n";
                    } else if (sisterAge.split(';').length != contactInfo.Home_Sister_Stepsister__c) {
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_SisterAgeRequired") + "\n";
                    }
                }
            }
        }
        if (contactInfo.Home_Brother_Stepbrother__c != undefined && contactInfo.Home_Brother_Stepbrother__c > 0) {
            if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Home_Stepbrother__c")) && component.get("v.configWrapper.Submission_Home_Stepbrother__c") == 'Required') {
                if (contactInfo.Home_Brother_Stepbrother__c == undefined) {
                    component.set("v.contactInfo.Home_Brother_Stepbrother__c", 0);
                } else {
                    if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Stepbrother_Age__c")) && component.get("v.configWrapper.Submission_Fld_Home_Stepbrother_Age__c") == 'Required') {
                        if ($A.util.isEmpty(brotherAge)) {
                            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_BrotherAgeRequired") + "\n";
                        } else if (brotherAge.split(';').length != contactInfo.Home_Brother_Stepbrother__c) {
                            messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_BrotherAgeRequired") + "\n";
                        }
                    }
                }
            } else if (component.get("v.configWrapper.Submission_Home_Stepbrother__c") != 'Hide') {

                if (!$A.util.isEmpty(component.get("v.configWrapper.Submission_Fld_Home_Stepbrother_Age__c")) && component.get("v.configWrapper.Submission_Fld_Home_Stepbrother_Age__c") == 'Required') {
                    if ($A.util.isEmpty(brotherAge)) {
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_BrotherAgeRequired") + "\n";
                    } else if (brotherAge.split(';').length != contactInfo.Home_Brother_Stepbrother__c) {
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_BrotherAgeRequired") + "\n";
                    }
                }
            }
        }
        component.set("v.contactInfo.Home_Brother_Stepbrothers_Age__c", brotherAge);
        component.set("v.contactInfo.Home_Sister_Stepsisters_Age__c", sisterAge);

        return messageTemplateStr;

    },
    createRelationWrapper: function(component, helper) {
        var parentList = component.get('v.ParentGuardianWrapper');
        var contactData = component.get('v.contactInfo');

        for (var i = 0; i < parentList.length; i++) {
            if (parentList[i].parentSelected != 'Other') {
                parentList[i].relation.npe4__Type__c = parentList[i].parentSelected;

            }
            if ($A.util.isEmpty(parentList[i].Id)) {
                parentList[i].relation.npe4__Contact__c = contactData.Id;
                parentList[i].relatedContact.AccountId = contactData.AccountId;
            }
        }
        return parentList;
    },
    validateEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    },
    validatePhone: function(phone) {
        var re = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
        return re.test(String(phone));
    }

})