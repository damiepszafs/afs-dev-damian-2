({
	doInit : function(component, event, helper) {
      helper.getPaymentPlan(component,helper);
      window.scrollTo(0, 0);
	},
    saveTask : function(component, event, helper) {
    	helper.updateTask(component,helper);
    },
    cancelPage : function(component, event, helper) {
    	component.set('v.parentToggle',false);
    }
})