({
	doResetPassword : function(component,event) {
		var action = component.get("c.resetPassword");
        
        action.setParams({
            "userNameStr" : component.get("v.userName")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                component.set("v.Spinner",false);
                var messageTemplate = response.getReturnValue();
                toastEvent.setParams({ 
                    type : "success",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                component.set("v.Spinner",false);
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    doLogin : function(component,event) {
        var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var comHomeURL = response.getReturnValue().Community_URL__c;
                var afslogin = comHomeURL + "/s/afslogin";
                window.location.href = afslogin;
            }else{
                var toastEvent = $A.get("e.force:showToast");
                component.set("v.Spinner",false);
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})