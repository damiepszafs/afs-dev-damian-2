({
	getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    fetchParents : function(component, contactId){
        var action = component.get("c.getParents");
        action.setParams({
            "contactId":  cId
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS"){
                var allValues = response.getReturnValue();
                component.set("v.parentEmails", allValues['Email']);
                component.set("v.parentPhones", allValues['Phone']);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListVal : function(component, fieldName) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "objObject": 'Contact',
            "fld": fieldName
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
				component.set("v.LegalCountryPicklist", allValues['Country_of_Legal_Residence__c']);
                component.set("v.NatinalityPicklist", allValues['NationalityCitizenship__c']);
                component.set("v.IdentifyMyGender", allValues['Gender__c']);
                component.set("v.hearABoutUsList", allValues['How_did_you_hear_about_AFS__c']);
                
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onPasswordChange : function(component, fieldName) {
        var action = component.get("c.changePassword");
        var pass = component.get("{!v.passwordVal}");
        action.setParams({
            "password": pass
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = $A.get("$Label.c.AfsLbl_Setting_PasswordChangeMsg");
                toastEvent.setParams({ 
                    type : "success",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    onSaveRecord : function(component, fieldName) {
        var action = component.get("c.saveRecord");
        var pass = component.get("{!v.passwordVal}");
        var ContactInfo = component.get("v.ContactInfo");
        
        var addressLine1 = component.get("v.addressLine1");
        var addressLine2 = component.get("v.addressLine2");
        if(addressLine1 != undefined){
            ContactInfo.MailingStreet = addressLine1;
        }
        
        if(addressLine2 != undefined){
            if(ContactInfo.MailingStreet === undefined){
               ContactInfo.MailingStreet = ';' +addressLine2;
            }else{
                ContactInfo.MailingStreet += ';' + addressLine2;
            }
            
        }
        
        action.setParams({
            "contactObj": ContactInfo,
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var applicationWrapper = component.get("v.applicationWrapper");
                var navigateWrapper = component.get("v.navigateWrapper");
                /*if(applicationWrapper.applicationObj.Status__c == 'Participation Desire'){
                    navigateWrapper.isSelectProgram = false;
                    navigateWrapper.isSelectProgramSideBar = true;
                    navigateWrapper.isProgramDetail = false;
                    navigateWrapper.isProgramDetailSideBar = true;
                    navigateWrapper.isAboutYou = false;
                    navigateWrapper.isAboutYouSideBar = true;
                    navigateWrapper.isPortalSubmissionConfirmation =false;
                    navigateWrapper.isPortalSubmissionConfirmationSideBar = true;
                    navigateWrapper.isPreSelected = false;
                    navigateWrapper.isPreSelectedSideBar = false;
                    navigateWrapper.isWaitlisted = false; 
                    component.set("v.navigateWrapper",navigateWrapper);
                }*/
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    validatePhone : function (phone) {
    	var re = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    	return re.test(String(phone));
    },
    
    validateEmail : function (phone) {
    	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    	return re.test(String(phone));
    },
})