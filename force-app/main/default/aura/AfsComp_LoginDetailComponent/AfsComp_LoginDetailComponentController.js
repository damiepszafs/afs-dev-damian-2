({
	doInit : function(component, event, helper) {
		component.set("v.Spinner",true);
		var configObj = component.get("v.configWrapper");

        var cLMap = component.get("v.customLabelMap");
        for (const [key, value] of cLMap.entries()) {
		  component.set(key, value);
		}
        component.set("v.isShowComponent", true);
        component.set("v.Spinner",false);
        
        // Changes made to add language selector
        //helper.doGetLanguageList(component,event);
	},
	
	updateLoginComponentCustomLabels : function(component, event) {
		var cLMap = component.get("v.customLabelMap");
        for (const [key, value] of cLMap.entries()) {
		  component.set(key, value);
		}
		
		var islanguageSelectedByUserBool = component.get("v.islanguageSelectedByUser");
		component.set("v.islanguageSelectedByUser", islanguageSelectedByUserBool);
		var languageActive = component.get("v.languageSelected");
		component.set("v.languageSelected", languageActive);
		
        component.set("v.isShowComponent", true);
	},
    
    signUpCommunity  : function(component, event, helper) {
        
        component.set("v.Spinner",true);
        var loginWrapper = component.get("v.loginWrapper");
        var messageTemplateStr;
        var configWrapper = component.get("v.configWrapper");
        
        if(loginWrapper.FirstName === undefined || loginWrapper.FirstName == ""){
            messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_FirstNameRequire");
        }
        
        if(loginWrapper.LastName === undefined || loginWrapper.LastName == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }
        }
        
        if(loginWrapper.Email === undefined || loginWrapper.Email == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }
        }else{
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(!re.test(loginWrapper.Email)){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }
            }
        }
        
        
        if(loginWrapper.Password === undefined || loginWrapper.Password == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PasswordRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PasswordRequire");
            }
        }else{
            if(loginWrapper.Password.length < 8){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PasswordWeek");
            }
        }
        
        
        
        if(loginWrapper.DOB === undefined || loginWrapper.DOB == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBRequire");
            }
        }
        
        if(loginWrapper.DOB != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(loginWrapper.DOB) > currentDate){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }
            }else{
                var date = new Date();
                var month = date.getMonth()+1;
                var day = date.getDay();
                var year = date.getFullYear();
                
                var currentDate = new Date(year,month,day);
                var birthdate = new Date(loginWrapper.DOB);
                var diff =(currentDate.getTime() - birthdate.getTime()) / 1000;
                diff /= (60 * 60 * 24);
                var result = Math.abs(Math.round(diff/365.25));
                if(result < 13){
                   if(messageTemplateStr != undefined){
                       messageTemplateStr += "\n";
                       messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBLessThan13");
                   }else{
                       messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBLessThan13");
                   } 
                }
            }
        }
        
        if(configWrapper.LoginComp_Fld_ZipCode__c == 'Required'){
            if(loginWrapper.ZipCode === undefined || loginWrapper.ZipCode == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ZipCodeRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_ZipCodeRequire");
                }
            }
        }
		
		if(configWrapper.LoginComp_Fld_ZipCode_Length__c != undefined && configWrapper.LoginComp_Fld_ZipCode_Length__c != ""){
            if(loginWrapper.ZipCode != undefined && loginWrapper.ZipCode.length != configWrapper.LoginComp_Fld_ZipCode_Length__c){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ZipCodeLength");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_ZipCodeLength");
                }
            }
        }
        
        if(configWrapper.LoginComp_Fld_Mobile__c == 'Required'){
            if(loginWrapper.MobileNumber === undefined || loginWrapper.MobileNumber == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }
            }
        }
        if(loginWrapper.MobileNumber === 'invalid') {
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        /*else{
            if(loginWrapper.MobileNumber != undefined){
                var re = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
                if(!re.test(loginWrapper.MobileNumber)){
                    if(messageTemplateStr != undefined){
                        messageTemplateStr += "\n";
                        messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberNoValid");
                    }else{
                        messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberNoValid");
                    }
                }
            }
        }*/
        
        if(configWrapper.LoginComp_Fld_PromoCode__c == 'Required'){
            if(loginWrapper.PromoCode === undefined || loginWrapper.PromoCode == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PromoCodeRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PromoCodeRequire");
                }
            }
        }
         var none = $A.get("$Label.c.AfsLbl_SignUp_None");
        if(configWrapper.LoginComp_Fld_HearAboutUs__c == 'Required'){
            if(loginWrapper.HearAboutUs === undefined || loginWrapper.HearAboutUs == none){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
                }
            }
        }
        
        if(loginWrapper.AgreeTerms == false){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
            }
        }
        
        if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            component.set("v.Spinner",false);
            return;
        }
        
        
		helper.createUserContact(component,event);
	},
    
    
    checkAgreeTerms : function(component, event, helper) {
		var loginWrapper = component.get("v.loginWrapper");
        if(loginWrapper.AgreeTerms){
            loginWrapper.AgreeTerms = false;
        }else{
            loginWrapper.AgreeTerms = true;
        }
        component.set("v.loginWrapper",loginWrapper);
	},
    
    checkKeepMeInformed : function(component, event, helper) {
        var loginWrapper = component.get("v.loginWrapper");
        if(loginWrapper.KeepMeInformed){
            loginWrapper.KeepMeInformed = false;
        }else{
            loginWrapper.KeepMeInformed = true;
        }
        component.set("v.loginWrapper",loginWrapper);
	}
    
})