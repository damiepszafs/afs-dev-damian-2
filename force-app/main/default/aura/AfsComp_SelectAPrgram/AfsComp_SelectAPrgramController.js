({
    doInit : function(component, event, helper) {
        // ASSIGN LABELS AND BUTTON TEXT USING CONFIG VAR
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_ProgramHeader__c,component);
        component.set("v.SelectProgramHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_DestinationHeader__c,component);
        component.set("v.AdditionalDestinationHeader",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_DestinationHelpTxt__c,component);
        component.set("v.AdditionalDestination_HelpText",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Btn_SaveContinue__c,component);
        component.set("v.Button_SaveAndContinue",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.SelectAProgram_Txt_DisplayPrgHeader__c,component);
        component.set("v.DisplayResult_SelectProgrm",component.get("v.LabelTempVal"));
        
        if(configObj.SelectAProgram_Fld_AdditionalDestn1__c == "Required" || configObj.SelectAProgram_Fld_AdditionalDestn2__c == "Required" || configObj.SelectAProgram_Fld_AdditionalDestn3__c == "Required"){
            component.set("v.isAddDestinationRequired","true");
        }
        
        helper.onInit(component, event);
        helper.getDestination(component, event);
        helper.getContactName(component,event);
        
        var wrapperList = component.get("v.wrapperList");
        var isSave = true;
        if(wrapperList){
            for(var i = 0; i < wrapperList.length ; i++){
                if(wrapperList[i].isSelected == true){
                    isSave = false;
                }
            }
        }
        
        var addDestination1 = component.get("v.addDestination1");
        var addDestination2 = component.get("v.addDestination2");
        var addDestination3 = component.get("v.addDestination3");
        if(addDestination1 && addDestination2 && addDestination3){
            isSave = false;
        }else{
            isSave = true; 
        }
        
        // disable enable save button based on criteria
        // 1. Required values are selected
        // 2. Atleast one prgram is selected
        component.set("v.isSaveValid",isSave);
    },
    
    onProgramSearch : function(component, event, helper) {
        component.set("v.Spinner","true");
        var MapofPiclistValues = event.getParam("MapofPiclistValues");
        component.set("v.MapofPiclistValues",MapofPiclistValues);
        component.set("v.isSearchPressed","true");
        helper.doProgramSearch(component, event);
    },
    
    onSaveAndContinueProgram : function(component, event, helper) {
        component.set("v.Spinner","true");
        var wrapperList = component.get("v.wrapperList");        
        var addDestination1 = component.get("v.addDestination1");
        var addDestination2 = component.get("v.addDestination2");
        var addDestination3 = component.get("v.addDestination3");
        
        var atleastOneProgramSelected = 0;
        var listToSave = [];
        var listToDelete = [];
        if(wrapperList){
            for(var i = 0; i < wrapperList.length ; i++){
                if(wrapperList[i].isSelected == true){
                    atleastOneProgramSelected++;
                    if(wrapperList[i].isSavedRecord != true){
                        listToSave.push(wrapperList[i].programObj.Id);
                    }
                    component.set("v.atLeastOneSelectedProgram", true);
                }else{
                    if(wrapperList[i].isSavedRecord == true){
                        listToDelete.push(wrapperList[i].programObj.Id);
                    }
                }
            }
        }
        var configObj = component.get("v.configWrapper");
        
        //fire validation and save
        var messageTemplate;
        if(atleastOneProgramSelected == 0){
            messageTemplate = $A.get("$Label.c.AfsLbl_Error_SelectOnePrgmMessage");
        }else if(configObj.SelectAProgram_Fld_AdditionalDestn1__c == "Required" && addDestination1 == null){
            if(messageTemplate !=undefined){
                messageTemplate += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination1Req");
            }else{
                messageTemplate = $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination1Req");
            }
        }else if(configObj.SelectAProgram_Fld_AdditionalDestn2__c == "Required" && addDestination2 == null){
            if(messageTemplate !=undefined){
                messageTemplate += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination2Req");
            }else{
                messageTemplate = $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination2Req");
            }
        }else if(configObj.SelectAProgram_Fld_AdditionalDestn3__c == "Required" && addDestination3 == null){
            if(messageTemplate !=undefined){
                messageTemplate += $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination3Req");
            }else{
                messageTemplate = $A.get("$Label.c.AfsLbl_SearchPrg_AddDestination3Req");
            }
        }
        console.log('messageTemplate=',messageTemplate);
        if(messageTemplate == undefined){
            helper.doSaveAndContinueProgram(component, event,listToSave,listToDelete);
        }else{
            component.set("v.Spinner","false");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type : "error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
        }
        
    }
})