import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

import yourAddress from "@salesforce/label/c.AfsLbl_YourApplication_Address";
import streetAddress from "@salesforce/label/c.AfsLbl_YourApplication_Street_Address";
import streetOptAddress from "@salesforce/label/c.AfsLbl_YourApplication_SreetAddressSecond";
import cityAddress from "@salesforce/label/c.AfsLbl_YourApplication_City";
import stateAddress from "@salesforce/label/c.AfsLbl_YourApplication_State";
import postalCodeAddress from "@salesforce/label/c.AfsLbl_YourApplication_PostalCode";
import countryAddress from "@salesforce/label/c.AfsLbl_YourApplication_Country";

import CONTACT_FIELD from "@salesforce/schema/User.ContactId";

import Id from "@salesforce/user/Id";

import ID_FIELD from "@salesforce/schema/Contact.Id";
import MAILING_STREET from "@salesforce/schema/Contact.MailingStreet";
import MAILING_CITY from "@salesforce/schema/Contact.MailingCity";
import MAILING_STATE from "@salesforce/schema/Contact.MailingState";
import MAILING_POSTALCODE from "@salesforce/schema/Contact.MailingPostalCode";

import { registerListener, unregisterAllListeners } from "c/pubsub";
import { getRecord, updateRecord } from "lightning/uiRecordApi";
import getContact from "@salesforce/apex/ParentGuardianController.getContact";
import { reduceErrors } from "c/ldsUtils";

export default class ContactAddress extends LightningElement {
  @api contact;
  @api visibilityStreet;
  @api visibilityCity;
  @api visibilityState;
  @api visibilityZip;
  @api visibilityCountry;

  @track error;
  @track record = { Id: "0035500000V4CxCAAV" };
  @track recordTypeId;
  @track picklist = false;

  userId = Id;

  labels = {
    yourAddress,
    streetAddress,
    streetOptAddress,
    cityAddress,
    stateAddress,
    postalCodeAddress,
    countryAddress
  };

  @wire(getRecord, { recordId: "$userId", fields: [CONTACT_FIELD] })
  handleUser({ error, data }) {
    if (data) this.record = { Id: "0035500000V4CxCAAV" }; //data.field.ContactId.value;
    if (error)
      this.dispatchEvent(
        new ShowToastEvent({
          title: "ERROR",
          message: reduceErrors(error).join(", "),
          variant: "error"
        })
      );
  }

  get streetVisible() {
    return this.visibilityStreet !== "hide";
  }

  get streetRequired() {
    return this.visibilityStreet === "required";
  }

  get cityVisible() {
    return this.visibilityCity !== "hide";
  }

  get cityRequired() {
    return this.visibilityCity === "required";
  }

  get stateVisible() {
    return this.visibilityState !== "hide";
  }

  get stateRequired() {
    return this.visibilityState === "required";
  }

  get zipVisible() {
    return this.visibilityZip !== "hide";
  }

  get zipRequired() {
    return this.visibilityZip === "required";
  }

  get street() {
    if (this.contact.MailingStreet) {
      const street = this.contact.MailingStreet.split(";");
      return street[0];
    }
    return "";
  }

  get streetOpt() {
    if (this.contact.MailingStreet) {
      const opt = this.contact.MailingStreet.split(";");
      return opt.length > 1 ? opt[1] : "";
    }
    return "";
  }

  connectedCallback() {
    registerListener("submitform", this.updateRecord, this);

    if (this.record) {
      getContact({ contactId: this.record.Id })
        .then(result => {
          this.record = result;
        })
        .catch(error => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "ERROR",
              message: reduceErrors(error).join(", "),
              variant: "error"
            })
          );
        });
    }
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  @api
  validateFields() {
    // [...this.template.querySelectorAll("lightning-input")].forEach(c =>
    //   c.reportValidity()
    // );
    const allValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    return allValid;
  }

  handleChange(event) {
    event.preventDefault();
    if (this.contact) {
      const updateAddress = new CustomEvent("updateaddress", {
        detail: {
          name: event.target.name,
          value: event.target.value
        }
      });
      this.dispatchEvent(updateAddress);
    } else {
      this.record = { ...this.record, [event.target.name]: event.target.value };
    }
  }

  handleChangeOpt(event) {
    event.preventDefault();
    event.target.value = this.street.concat(`;${event.target.value}`);
    this.handleChange(event);
  }

  updateRecord() {
    if (this.contact === undefined) {
      const fields = {};
      fields[ID_FIELD.fieldApiName] = this.record.Id;
      fields[MAILING_STREET.fieldApiName] = this.record.MailingStreet;
      fields[MAILING_CITY.fieldApiName] = this.record.MailingCity;
      fields[MAILING_STATE.fieldApiName] = this.record.MailingState;
      fields[MAILING_POSTALCODE.fieldApiName] = this.record.MailingPostalCode;

      const updateContact = { fields };

      updateRecord(updateContact)
        .then(updated => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "SUCCESS",
              message: `Contact updated, id: ${updated.id}`,
              variant: "success"
            })
          );
        })
        .catch(error => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "ERROR",
              message: reduceErrors(error).join(", "),
              variant: "error"
            })
          );
        });
    }
  }
}
