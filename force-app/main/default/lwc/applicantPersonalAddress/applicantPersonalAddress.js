/* eslint-disable no-console */

//https://developer.salesforce.com/docs/component-library/tools/playground/JoW9SEmLH/7/edit
import { LightningElement, api, wire, track } from "lwc";
import { getRecord, updateRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getPicklistValues } from "lightning/uiObjectInfoApi";

import { registerListener, unregisterAllListeners } from "c/pubsub";

import CONTACT_OBJECT from "@salesforce/schema/Contact";
import { getObjectInfo } from "lightning/uiObjectInfoApi";

// import contactId from "@salesforce/user/ContactId";

import yourAddress from "@salesforce/label/c.AfsLbl_YourApplication_Address";
import streetAddress from "@salesforce/label/c.AfsLbl_YourApplication_Street_Address";
import cityAddress from "@salesforce/label/c.AfsLbl_YourApplication_City";
import stateAddress from "@salesforce/label/c.AfsLbl_YourApplication_State";
import postalCodeAddress from "@salesforce/label/c.AfsLbl_YourApplication_PostalCode";
import countryAddress from "@salesforce/label/c.AfsLbl_YourApplication_Country";

import MAILING_STREET from "@salesforce/schema/Contact.MailingStreet";
import MAILING_CITY from "@salesforce/schema/Contact.MailingCity";
import MAILING_STATE from "@salesforce/schema/Contact.MailingState";
import MAILING_POSTALCODE from "@salesforce/schema/Contact.MailingPostalCode";
import MAILING_COUNTRY from "@salesforce/schema/Contact.MailingCountry";
import COUNTRY_LEGAL from "@salesforce/schema/Contact.Country_of_Legal_Residence__c";

const FIELDS = [
  MAILING_STREET,
  MAILING_CITY,
  MAILING_STATE,
  MAILING_POSTALCODE,
  MAILING_COUNTRY,
  COUNTRY_LEGAL
];

export default class ContactPersonalDetails extends LightningElement {
  //public (reactive) properties could be passed by parent component in the template or by config file filled by Builder.
  @api required = false;
  @api recordId = "0038A00000CpelJQAR";
  @api visibilityStreet;
  @api visibilityCity;
  @api visibilityState;
  @api visibilityZip;
  @api visibilityCountry;

  @track loading = true;
  @track error;
  @track record = undefined;
  @track recordTypeId;
  @track picklist = false;

  labels = {
    yourAddress,
    streetAddress,
    cityAddress,
    stateAddress,
    postalCodeAddress,
    countryAddress
  };

  @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
  wireInfo({ data, error }) {
    if (data) {
      this.recordTypeId = Object.keys(data.recordTypeInfos).filter(
        key => data.recordTypeInfos[key].name === "Applicant"
      )[0];
    }
    if (error) {
      this.error = error.message;
    }
  }

  @wire(getPicklistValues, {
    recordTypeId: "$recordTypeId",
    fieldApiName: COUNTRY_LEGAL
  })
  wirePick({ data, error }) {
    if (data) {
      this.picklist = data.values;
    } else if (error) {
      this.error = error.message;
    }
  }

  //data adapter to get info, $recordId means is dynamic.
  @wire(getRecord, {
    recordId: "$recordId",
    fields: FIELDS
  })
  wiredContact({ data, error }) {
    if (data) {
      this.record = data.fields;
      this.error = undefined;
    } else if (error) {
      this.error = error;
      this.record = undefined;
    }
    this.loading = false;
  }

  connectedCallback() {
    registerListener("submitform", this.updateContact, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  get streetVisible() {
    return this.visibilityStreet !== "hide";
  }

  get streetRequired() {
    return this.visibilityStreet === "required";
  }

  get cityVisible() {
    return this.visibilityCity !== "hide";
  }

  get cityRequired() {
    return this.visibilityCity === "required";
  }

  get stateVisible() {
    return this.visibilityState !== "hide";
  }

  get stateRequired() {
    return this.visibilityState === "required";
  }

  get zipVisible() {
    return this.visibilityZip !== "hide";
  }

  get zipRequired() {
    return this.visibilityZip === "required";
  }

  get countryVisible() {
    return this.visibilityCountry !== "hide";
  }

  get countryRequired() {
    return this.visibilityCountry === "required";
  }

  get Country_of_Legal_Residence__c() {
    return this.record.Country_of_Legal_Residence__c.value;
  }

  get MailingStreet() {
    return this.record.MailingStreet.value;
  }

  get MailingCity() {
    return this.record.MailingCity.value;
  }

  get MailingState() {
    return this.record.MailingState.value;
  }
  get MailingPostalCode() {
    return this.record.MailingPostalCode.value;
  }
  get MailingCountry() {
    return this.record.MailingCountry.value;
  }

  handleChange(event) {
    event.preventDefault();
    this.record = {
      ...this.record,
      [event.target.name]: { value: event.target.value }
    };
  }

  allFilled() {
    let init = true;
    Object.keys(this.record.field).forEach(k => {
      if (this.record.field[k].value.length === 0) init = false;
    });
    return init;
  }

  updateContact() {
    this.loading = true;

    let record = {
      fields: FIELDS.reduce(
        (acum, currentValue) => {
          acum = {
            ...acum,
            [currentValue.fieldApiName]: this.record[currentValue.fieldApiName]
              .value
          };
          return acum;
        },
        { Id: this.recordId }
      )
    };
    if (this.required) {
      if (this.allFilled) {
        updateRecord(record)
          .then(() => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "Success",
                message: "Record Is Updated",
                variant: "sucess"
              })
            );
            this.loading = false;
          })
          .catch(error => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "Error on data save",
                message: error.message.body,
                variant: "error"
              })
            );
            this.loading = false;
          });
      }
    } else {
      updateRecord(record)
        .then(() => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: "Record Is Updated",
              variant: "sucess"
            })
          );
          this.loading = false;
        })
        .catch(error => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Error on data save",
              message: error.message.body,
              variant: "error"
            })
          );
          this.loading = false;
        });
    }
  }
}
