import { LightningElement, track } from "lwc";
import getParentalConsent from "@salesforce/apex/utilCommunity.getParentalConsent";

export default class ParentalConsent extends LightningElement {
  @track parentalConsent;
  connectedCallback() {
    getParentalConsent()
      .then(parentsConsent => (this.parentalConsent = parentsConsent))
      .catch(error => console.log(error));
  }
}
