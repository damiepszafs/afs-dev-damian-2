import { LightningElement, track, api } from "lwc";
import getParents from "@salesforce/apex/ParentGuardianController.getParents";
import getContact from "@salesforce/apex/ParentGuardianController.getContact";
import updateRelationship from "@salesforce/apex/ParentGuardianController.updateRelationship";
import updateContact from "@salesforce/apex/ParentGuardianController.updateContact";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

import userId from "@salesforce/user/Id";

import { reduceErrors } from "c/ldsUtils";
import { registerListener, unregisterAllListeners, fireEvent } from "c/pubsub";

import ID_FIELD from "@salesforce/schema/Contact.Id";
import NAME_FIELD from "@salesforce/schema/Contact.FirstName";
import LASTNAME_FIELD from "@salesforce/schema/Contact.LastName";
import EMAIL_FIELD from "@salesforce/schema/Contact.Email";
import STREET_FIELD from "@salesforce/schema/Contact.MailingStreet";
import CITY_FIELD from "@salesforce/schema/Contact.MailingCity";
import STATE_FIELD from "@salesforce/schema/Contact.MailingState";
import POSTALCODE_FIELD from "@salesforce/schema/Contact.MailingPostalCode";
import BIRTHDATE_FIELD from "@salesforce/schema/Contact.Birthdate";
import POB_FIELD from "@salesforce/schema/Contact.Place_of_Birth__c";
import PHONE_FIELD from "@salesforce/schema/Contact.Phone";
import ACCOUNT_FIELD from "@salesforce/schema/Contact.AccountId";
import CONT_OWNER_FIELD from "@salesforce/schema/Contact.OwnerId";

import TYPE_REL_FIELD from "@salesforce/schema/npe4__Relationship__c.npe4__Type__c";
import CONTACT_REL_FIELD from "@salesforce/schema/npe4__Relationship__c.npe4__Contact__c";
import RELCONTACT_REL_FIELD from "@salesforce/schema/npe4__Relationship__c.npe4__RelatedContact__c";
import LIVETOG_REL_FIELD from "@salesforce/schema/npe4__Relationship__c.They_live_together__c";
import DESCRIPTION_FIELD from "@salesforce/schema/npe4__Relationship__c.npe4__Description__c";

import getCurrentContact from "@salesforce/apex/ParentGuardianController.getCurrentContact";
import getTypeOptions from "@salesforce/apex/ParentGuardianController.getTypeOptions";
import createContact from "@salesforce/apex/ParentGuardianController.createContact";
import createRelationship from "@salesforce/apex/ParentGuardianController.createRelationship";

export default class ApplicantGuardians extends LightningElement {
  @track state = [];
  @track error = [];
  @track isLoaded;
  @track options;
  @track currentContact;

  @api visibilityStreet;
  @api visibilityCity;
  @api visibilityState;
  @api visibilityZip;
  @api visibilityCountry;
  @api visibilityDob;
  @api visibilityPob;
  @api visibilityPhone;
  @api defaultCountryCode;

  connectedCallback() {
    registerListener("submitform", this.handleSubmit, this);
    registerListener("validatecomponents", this.fullValidation, this);

    fireEvent("valid", {
      name: "applicantGuardian",
      valid: false
    });

    //get dropdown values for relationship types
    getTypeOptions().then(res => {
      this.options = res.filter(
        opt =>
          opt.value === "Mother" ||
          opt.value === "Father" ||
          opt.value === "Other"
      );
    });

    //get contact associated with the current user
    getCurrentContact({ userId })
      .then(currentContact => {
        this.currentContact = currentContact;

        //this method returns an array of Relationships related to current contact
        getParents({ contactId: this.currentContact.Id })
          .then(guardians => {
            if (guardians.length > 0) {
              //if there is any relationship, get each contact information
              guardians.forEach(guardian => {
                //get related contact information
                getContact({ contactId: guardian.npe4__RelatedContact__c })
                  .then(contact => {
                    //create the local state as an array
                    this.state = this.state.concat([
                      {
                        ...guardian,
                        npe4__RelatedContact__c: contact
                      }
                    ]);
                  })
                  .catch(error => {
                    this.dispatchEvent(
                      new ShowToastEvent({
                        title: "ERROR",
                        message: reduceErrors(error).join(", "),
                        variant: "error"
                      })
                    );
                  });
              });
            } else {
              //no guardian create a new from the newGuardian template
              this.state = [this.newGuardian()];
            }
          })
          .then((this.isLoaded = true))
          .catch(error => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "ERROR",
                message: reduceErrors(error).join(", "),
                variant: "error"
              })
            );
          });
      })
      .catch(error => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "ERROR",
            message: reduceErrors(error).join(", "),
            variant: "error"
          })
        );
      });
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  //when relationship update
  handleUpdate(event) {
    const { relId, name, value } = event.detail;
    this.state = this.state.map(rel => {
      if (rel.Id === relId) return { ...rel, [name]: value };
      return rel;
    });
  }

  //when contact update
  handleUpdateContact(event) {
    //by contact Id get the contact to update and with name the property
    const { conId, name, value } = event.detail;

    if (name === "Email") {
      // check validity
      this.validateEmailNotUsed(conId, value);
    }
    this.state = this.state.map(rel => {
      if (rel.npe4__RelatedContact__c.Id === conId)
        return {
          ...rel,
          npe4__RelatedContact__c: {
            ...rel.npe4__RelatedContact__c,
            [name]: value
          }
        };
      return rel;
    });
  }

  validateEmailNotUsed(conId, value) {
    const valid = this.state.reduce((acum, currentValue) => {
      if (currentValue.npe4__RelatedContact__c.Email === value) return false;
      if (!acum) return false;
      return true;
    }, true);

    const rel = Array.from(
      this.template.querySelectorAll("c-applicant-relationship")
    ).filter(
      applicantRelationship =>
        applicantRelationship.guardian.npe4__RelatedContact__c.Id === conId
    )[0];

    if (valid) {
      rel.emailIsValid(
        valid,
        `Cannot use the same email address in the guardians`
      );
    }
  }

  //add a new guardian with the new
  addGuardian() {
    this.state = this.state.concat(this.newGuardian());
  }

  //helper created for templating a new guardian with random id to use in the iterator
  newGuardian() {
    return {
      Id: `newRel${Math.floor(Math.random() * 100)}`,
      action: "create",
      npe4__RelatedContact__c: {
        Id: `newCon${Math.floor(Math.random() * 100)}`
      }
    };
  }

  //delete a guardian
  removeGuardian(event) {
    const removedGuardianId = event.detail;
    this.state = this.state.map(guardian => {
      return guardian.Id === removedGuardianId
        ? { ...guardian, action: "delete" }
        : guardian;
    });
  }

  //custom list used in the HTML template not render guardians with delete action value
  get guardianNotDeleted() {
    return this.state.filter(guardian => guardian.action !== "delete");
  }

  //create visibility object to pass down the components
  get visibility() {
    return {
      street: this.visibilityStreet,
      city: this.visibilityCity,
      state: this.visibilityState,
      zip: this.visibilityZip,
      country: this.visibilityCountry,
      DOB: this.visibilityDob,
      POB: this.visibilityPob,
      phone: this.visibilityPhone
    };
  }

  // create/update concatenated calls
  handleSubmit() {
    //format for submit
    const formattedState = this.state.map(guardian => {
      const fieldsContact = {};
      fieldsContact[NAME_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.FirstName;
      fieldsContact[LASTNAME_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.LastName;
      fieldsContact[EMAIL_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.Email;
      fieldsContact[STREET_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.MailingStreet;
      fieldsContact[CITY_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.MailingCity;
      fieldsContact[STATE_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.MailingState;
      fieldsContact[POSTALCODE_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.MailingPostalCode;
      fieldsContact[BIRTHDATE_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.Birthdate;
      fieldsContact[PHONE_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.Phone;
      fieldsContact[POB_FIELD.fieldApiName] =
        guardian.npe4__RelatedContact__c.Place_of_Birth__c;
      fieldsContact[
        CONT_OWNER_FIELD.fieldApiName
      ] = this.currentContact.OwnerId;
      fieldsContact[ACCOUNT_FIELD.fieldApiName] = this.currentContact.AccountId;
      //if we are creating a contact set the field to null
      fieldsContact[ID_FIELD.fieldApiName] =
        guardian.action === "create"
          ? null
          : guardian.npe4__RelatedContact__c.Id;

      const fieldsRelationship = {};
      //if we are creating a contact set the field to null
      fieldsRelationship[ID_FIELD.fieldApiName] =
        guardian.action === "create" ? null : guardian.Id;
      fieldsRelationship[TYPE_REL_FIELD.fieldApiName] = guardian.npe4__Type__c;
      fieldsRelationship[
        CONTACT_REL_FIELD.fieldApiName
      ] = this.currentContact.Id;
      // fieldsRelationship[RELCONTACT_REL_FIELD.fieldApiName] = parentContact.Id;
      fieldsRelationship[LIVETOG_REL_FIELD.fieldApiName] =
        guardian.They_live_together__c;
      fieldsRelationship[DESCRIPTION_FIELD.fieldApiName] =
        guardian.npe4__Description__c;

      return { fieldsContact, fieldsRelationship, action: guardian.action };
    });

    //create an array of promises to then use the Promise all and know if any of them failed
    const promises = formattedState.map(fs => {
      return new Promise((res, rej) => {
        switch (fs.action) {
          case "create":
            {
              createContact({ contactRecord: fs.fieldsContact })
                .then(parentContact => {
                  //update the state
                  this.state = this.state.map(g => {
                    if (g.npe4__RelatedContact__c.Id === parentContact.Id)
                      return {
                        ...g,
                        npe4__RelatedContact__c: {
                          ...g.npe4__RelatedContact__c,
                          Id: parentContact.Id
                        }
                      };
                    return g;
                  });
                  fs.fieldsRelationship[RELCONTACT_REL_FIELD.fieldApiName] =
                    parentContact.Id;
                  createRelationship({
                    relationshipRecord: fs.fieldsRelationship
                  })
                    .then(relationship => {
                      this.state = this.state.map(g => {
                        if (g.Id === relationship.id)
                          return { ...g, Id: relationship.Id, action: null };
                        return g;
                      });
                      res("Succeded!");
                    })
                    .catch(er => rej(er));
                })
                .catch(er => rej(er));
            }
            break;
          case "delete":
            {
              //action needed when a parent is succesfully deleted
              this.state = this.state.filter(g => g.action !== "delete");
              res();
            }
            break;
          default: {
            updateContact({ contactRecord: fs.fieldsContact })
              .then(parentContact => {
                fs.fieldsRelationship[RELCONTACT_REL_FIELD.fieldApiName] =
                  parentContact.Id;
                updateRelationship({
                  relationshipRecord: fs.fieldsRelationship
                })
                  .then(rel => {
                    res(rel);
                  })
                  .catch(er => rej(er));
              })
              .catch(er => rej(er));
          }
        }
      });
    });

    Promise.all(promises)
      .then(rej => {
        fireEvent("onoperation", {
          name: "applicantGuardian",
          valid: true,
          submitted: true,
          messages: rej
        });
      })
      .catch(error => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "ERROR",
            message: reduceErrors(error).join(", "),
            variant: "error"
          })
        );
      });
  }

  renderedCallback() {}

  fullValidation() {
    const allValid = Array.from(
      this.template.querySelectorAll("c-applicant-relationship")
    ).reduce((validSoFar, currentAppRelCmp) => {
      const cmpValid = currentAppRelCmp.validateFields();
      return validSoFar && cmpValid;
    }, true);

    fireEvent("valid", {
      name: "applicantGuardian",
      valid: allValid
    });
  }
}
