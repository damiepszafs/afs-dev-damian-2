import { LightningElement, api, track } from "lwc";

import { loadScript } from "lightning/platformResourceLoader";
import libphonejs from "@salesforce/resourceUrl/libphone";

import getCountryCode from "@salesforce/apex/ParentGuardianController.getCountryCode";

export default class ContactPhone extends LightningElement {
  @api phone;
  @api countryCodeDefault;
  @api required;

  @track countryCode;
  @track error;
  @track countryOptions;
  @track libInitialized = false;
  @track phoneFormatted;

  connectedCallback() {
    if (!this.phone) this.countryCode = this.countryCodeDefault.split(" - ")[1];
    getCountryCode()
      .then(
        codes =>
          (this.countryOptions = codes.map(c => ({
            label: c.label.split(" - ")[0],
            value: c.label.split(" - ")[1]
          })))
      )
      // eslint-disable-next-line no-console
      .catch(error => console.error(error));
  }

  get isCountryOptionLoading() {
    if (this.countryOptions) return this.countryOptions.length > 0;
    return true;
  }

  renderedCallback() {
    if (!this.libInitialized)
      loadScript(this, libphonejs)
        .then(() => {
          this.setPhoneValues(this.phone);
          this.libInitialized = true;
        })
        .catch(error => {
          this.error = error;
        });

    if (this.countryCode && this.phoneFormatted) {
      const { E164, error } = this.validateNumber(
        this.countryCode,
        this.phoneFormatted
      );
      const onValidPhone = new CustomEvent("validphone", {
        detail: {
          phone: E164,
          error
        }
      });
      this.dispatchEvent(onValidPhone);

      const phoneCmp = this.template.querySelector(".phone-number");

      if (error) {
        phoneCmp.setCustomValidity(error);
      } else {
        phoneCmp.setCustomValidity("");
      }
      phoneCmp.reportValidity();
    }
  }

  setPhoneValues(rawPhone) {
    const phoneUtil = window.libphonenumber.PhoneNumberUtil.getInstance();
    const phone = phoneUtil.parse(rawPhone, "");
    const phoneRegion = phoneUtil.getRegionCodeForNumber(phone);

    this.phoneFormatted = phoneUtil.formatInOriginalFormat(phone, phoneRegion);
    this.countryCode = phoneRegion;
  }

  handleChange(event) {
    event.preventDefault();
    if (event.target.name === "CountryCode") {
      this.countryCode = event.target.value;
      const AsYouTypeFormatter = window.libphonenumber.AsYouTypeFormatter;
      const formatter = new AsYouTypeFormatter(this.countryCode);
      [...this.phoneFormatted]
        .filter(s => !(s === "-" || s === "(" || s === ")" || s === " "))
        .forEach(input => {
          formatter.inputDigit(input);
        });
      this.phoneFormatted = formatter.currentOutput_;
    }

    if (event.target.name === "Phone") {
      const AsYouTypeFormatter = window.libphonenumber.AsYouTypeFormatter;
      const formatter = new AsYouTypeFormatter(this.countryCode);
      [...event.target.value]
        .filter(s => !(s === "-" || s === "(" || s === ")" || s === " "))
        .forEach(input => {
          formatter.inputDigit(input);
        });
      this.phoneFormatted = formatter.currentOutput_;
    }
  }

  validateNumber(countryCode, phoneNumber) {
    const phoneUtil = window.libphonenumber.PhoneNumberUtil.getInstance();

    try {
      const number = phoneUtil.parseAndKeepRawInput(phoneNumber, countryCode);
      if (phoneUtil.isValidNumber(number)) {
        //dispatch custom action
        const E164 = phoneUtil.format(
          number,
          window.libphonenumber.PhoneNumberFormat.E164
        );
        return { E164, error: null };
      }
      //error
      return { E164: null, error: "❌" };
    } catch (e) {
      return { E164: null, error: e.message };
    }
  }

  @api
  validateFields() {
    const field = this.template.querySelector(".phone-number");
    field.reportValidity();
    return field.checkValidity();
  }
}
