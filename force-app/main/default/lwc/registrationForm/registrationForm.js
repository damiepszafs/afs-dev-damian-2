/* eslint-disable no-console */
import { LightningElement, api, track } from "lwc";
import AfsLbl_Login_SignUp from "@salesforce/label/c.AfsLbl_Login_SignUp";
import AfsLbl_SignUp_Gmail from "@salesforce/label/c.AfsLbl_SignUp_Gmail";
import AfsLbl_SignUp_SignUpwithEmail from "@salesforce/label/c.AfsLbl_SignUp_SignUpwithEmail";
import AfsLbl_SignUp_Facebook from "@salesforce/label/c.AfsLbl_SignUp_Facebook";
import AfsLbl_Login_Checkbox1 from "@salesforce/label/c.AfsLbl_Login_CheckBox1";
import AfsLbl_Login_Checkbox2 from "@salesforce/label/c.AfsLbl_Login_CheckBox2";
import AfsLbl_Login_PolicyHeader from "@salesforce/label/c.AfsLbl_Login_PolicyHeader";
import AfsLbl_Error_UserAlreadyExist from "@salesforce/label/c.AfsLbl_Error_UserAlreadyExist";
import FACEBOOK_LOGO from "@salesforce/resourceUrl/Facebook_Logo";
import GOOGLE_LOGO from "@salesforce/resourceUrl/Google_Logo";
import EMAIL_LOGO from "@salesforce/resourceUrl/EMAIL_Logo";

import getConsent from "@salesforce/apex/utilCommunity.getConsent";
import checkIfUserexist from "@salesforce/apex/utilCommunity.checkIfUserexist";

import generateSession from "@salesforce/apex/Afs_ParentComponentController.generateSession";
import resetPassword from "@salesforce/apex/Afs_ParentComponentController.resetPassword";

import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { reduceErrors } from "c/ldsUtils";

export default class RegistrationForm extends LightningElement {
  label = {
    AfsLbl_Login_SignUp,
    AfsLbl_SignUp_Gmail,
    AfsLbl_SignUp_Facebook,
    AfsLbl_Login_PolicyHeader,
    AfsLbl_Login_Checkbox1,
    AfsLbl_Login_Checkbox2,
    AfsLbl_Error_UserAlreadyExist,
    AfsLbl_SignUp_SignUpwithEmail
  };

  facebook_logo = FACEBOOK_LOGO;
  google_logo = GOOGLE_LOGO;
  email_logo = EMAIL_LOGO;
  @api showGoogleButton = false;
  @api googleCallback = "";
  @api showFacebookButton = false;
  @api facebookCallback = "";

  @track consentSelected;
  @track showSignUpForm = true;
  @track showConsent = false;
  @track showCheckEmail = false;
  @track signUpHeader;
  @track consentValue = [];
  @track firstName = "";
  @track lastName = "";
  @track email = "";
  @track consentimiento = "";
  @track emailInput;

  get showOr() {
    return this.showGoogleButton || this.showFacebookButton;
  }

  connectedCallback() {
    getConsent()
      .then(consent => (this.consentimiento = consent))
      .catch(error => console.log(error));
  }

  handleBack() {
    this.showSignUpForm = true;
    this.showConsent = false;
  }

  handleFacebook() {
    this.showSignUpForm = false;
    this.showConsent = true;
    this.consentSelected = this.facebookCallback;
    this.signUpHeader = this.label.AfsLbl_SignUp_Facebook;
  }

  handleGoogle() {
    this.showSignUpForm = false;
    this.showConsent = true;
    this.consentSelected = this.googleCallback;
    this.signUpHeader = this.label.AfsLbl_SignUp_Gmail;
  }

  get ChecboxOptions() {
    return [
      { label: this.label.AfsLbl_Login_Checkbox1, value: "consent1" },
      { label: this.label.AfsLbl_Login_Checkbox2, value: "consent2" }
    ];
  }

  handleConsentCheckbox(e) {
    this.consentValue = e.detail.value;
  }

  handleChange(event) {
    var emailInput = this.template.querySelector(".emailInput");
    const field = event.target.name;
    if (field === "firstName") {
      this.firstName = event.target.value;
    }
    if (field === "lastName") {
      this.lastName = event.target.value;
    } else if (field === "email") {
      this.email = event.target.value;
      emailInput.setCustomValidity("");
    }
  }

  @track value = "initial value";

  handleEmailSignUpValidation() {
    var emailInput = this.template.querySelector(".emailInput");
    console.log("click");
    const allValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);
    if (allValid) {
      //enter here if all 3 fields have been filled out.
      //now check if the user already exist
      checkIfUserexist({ email: this.email.toLowerCase() })
        .then(userexist => {
          if (userexist) {
            //user already exist
            emailInput.setCustomValidity(
              this.label.AfsLbl_Error_UserAlreadyExist
            );
          } else {
            //user doesn't exist. Show consent
            emailInput.setCustomValidity("");
            this.showSignUpForm = false;
            this.showConsent = true;
            this.consentSelected = "sign up with email";
            this.signUpHeader = this.label.AfsLbl_SignUp_SignUpwithEmail;
          }
          //render field error
          emailInput.reportValidity();
        })
        .catch(error => console.log(error));
    }
  }

  handleSignUp() {
    //check that both checkboxes have been checked
    if (this.consentValue.length >= 2) {
      // check if the user decided to sign up with an email
      if (this.consentSelected === "sign up with email") {
        this.handleEmailSignUp();
      } else {
        // enter the else statement if the user selected a social SSO
        //redirect the user to the URL stored on the component property
        window.location.href = this.consentSelected;
      }
    }
  }

  handleEmailSignUp() {
    generateSession({
      wrapObj: JSON.stringify({
        aboutUsPicklist: [
          {
            fieldLabel: "From a family member",
            fieldVal: "From a family member",
            isSelected: false
          },
          {
            fieldLabel: "From an AFS host family",
            fieldVal: "From an AFS host family",
            isSelected: false
          },
          {
            fieldLabel: "From a Friend",
            fieldVal: "From a Friend",
            isSelected: false
          },
          {
            fieldLabel: "Through my school",
            fieldVal: "Through my school",
            isSelected: false
          },
          {
            fieldLabel: "Through press, radio or TV",
            fieldVal: "Through press, radio or TV",
            isSelected: false
          },
          {
            fieldLabel: "At an Event or Conference",
            fieldVal: "At an Event or Conference",
            isSelected: false
          },
          {
            fieldLabel: "On a search Engine",
            fieldVal: "On a search Engine",
            isSelected: false
          },
          {
            fieldLabel: "On social media",
            fieldVal: "On social media",
            isSelected: false
          },
          {
            fieldLabel: "Other",
            fieldVal: "Other",
            isSelected: false
          }
        ],
        AgreeTerms: true,
        Email: this.email,
        FirstName: this.firstName,
        HearAboutUs: "At an Event or Conference",
        KeepMeInformed: true,
        languageSelected: "",
        LastName: this.lastName,
        MiddleName: "",
        MobileNumber: "+50662532545",
        Password: "ashdjklasmd65+",
        PromoCode: "",
        ZipCode: "",
        DOB: null
      }),
      prId: "",
      islanguageSelectedByUser: false,
      languageSelected: ""
    })
      .then(result => {
        //onced the new user has been created we send a password reset email
        //and show a check your email message
        resetPassword({ userNameStr: this.email });
        this.showCheckEmail = true;
        this.showConsent = false;
      })
      //Creoq ue este show toast no esta funcionando.. no estoy seguro
      .catch(error => {
        console.log(JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "ERROR",
            message: reduceErrors(error).join(", "),
            variant: "error"
          })
        );
      });
  }

  handleUpdateEmail() {
    this.showSignUpForm = true;
    this.showCheckEmail = false;
  }
}
