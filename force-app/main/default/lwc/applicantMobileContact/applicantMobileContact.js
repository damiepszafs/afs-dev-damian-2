import { LightningElement, api, wire, track } from "lwc";
import { getRecord, updateRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

import { registerListener, unregisterAllListeners } from "c/pubsub";

import mobilePhone from "@salesforce/label/c.AfsLbl_YourApplication_MobileNumber";

import MOBILE_PHONE from "@salesforce/schema/Contact.MobilePhone";

const FIELDS = [MOBILE_PHONE];

export default class MobileContact extends LightningElement {
  @api required = false;
  @api recordId = "0038A00000CpelJQAR";
  @api defaultCode;

  @track loading = true;
  @track error;
  @track record = undefined;

  labels = {
    mobilePhone
  };

  //data adapter to get info, $recordId means is dynamic.
  @wire(getRecord, {
    recordId: "$recordId",
    fields: FIELDS
  })
  wiredContact({ data, error }) {
    if (data) {
      this.record = data.fields;
      this.error = undefined;
    } else if (error) {
      this.error = error;
      this.record = undefined;
    }
    this.loading = false;
  }

  connectedCallback() {
    registerListener("submitform", this.updateContact, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  get MobilePhone() {
    // eslint-disable-next-line no-console
    console.log(this.record.MobilePhone.value);
    return this.record.MobilePhone.value;
  }

  handleChange(event) {
    event.preventDefault();
    this.record = {
      ...this.record,
      [event.target.name]: { value: event.target.value }
    };
  }

  allFilled() {
    let init = true;
    Object.keys(this.record.field).forEach(k => {
      if (this.record.field[k].value.length === 0) init = false;
    });
    return init;
  }

  updateContact() {
    this.loading = true;

    let record = {
      fields: FIELDS.reduce(
        (acum, currentValue) => {
          acum = {
            ...acum,
            [currentValue.fieldApiName]: this.record[currentValue.fieldApiName]
              .value
          };
          return acum;
        },
        { Id: this.recordId }
      )
    };
    if (this.required) {
      if (this.allFilled) {
        updateRecord(record)
          .then(() => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "Success",
                message: "Record Is Updated",
                variant: "sucess"
              })
            );
            this.loading = false;
          })
          .catch(error => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "Error on data save",
                message: error.message.body,
                variant: "error"
              })
            );
            this.loading = false;
          });
      }
    } else {
      updateRecord(record)
        .then(() => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: "Record Is Updated",
              variant: "sucess"
            })
          );
          this.loading = false;
        })
        .catch(error => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Error on data save",
              message: error.message.body,
              variant: "error"
            })
          );
          this.loading = false;
        });
    }
  }
}
