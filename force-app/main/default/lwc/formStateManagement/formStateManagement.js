import { LightningElement, track, api } from "lwc";
import { registerListener, fireEvent, unregisterAllListeners } from "c/pubsub";
import { NavigationMixin } from "lightning/navigation";

export default class FormStateManagement extends NavigationMixin(
  LightningElement
) {
  @track state = {
    validating: false,
    submitting: false,
    components: []
  };

  @api redirectTo;

  connectedCallback() {
    registerListener("valid", this.handleChange, this);
    registerListener("onoperation", this.handleChange, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleChange(component) {
    this.state = {
      ...this.state,
      components: this.state.components
        .filter(comp => comp.name !== component.name)
        .concat(component)
    };
  }
  renderedCallback() {
    //check if all valid
    console.log(JSON.stringify(this.state));
    if (this.state.submitting) {
      const allSubmitted = this.state.components.reduce(
        (submittedSofar, currentValue) => {
          return submittedSofar && currentValue.submitted;
        },
        true
      );
      if (allSubmitted) {
        //redirect!
        this.state = { ...this.state, submitting: false };
        this[NavigationMixin.Navigate]({
          type: "comm__namedPage",
          attributes: {
            pageName: this.redirectTo
          }
        });
      }
    }
    if (this.state.validating) {
      const allValid = this.state.components.reduce(
        (validSoFar, currentValue) => {
          return validSoFar && currentValue.valid;
        },
        true
      );
      if (allValid) {
        fireEvent("submitform");
        this.state = {
          validating: false,
          submitting: true,
          components: this.state.components.map(c => ({
            ...c,
            submitted: false
          }))
        };
      } else {
        this.state = { ...this.state, validating: false };
      }
    }
  }

  get isValidating() {
    return this.state.validating || this.state.submitting;
  }

  handleSubmit() {
    fireEvent("validatecomponents");
    this.state = { ...this.state, validating: true };
  }
}
