import { LightningElement, api, track } from "lwc";

import name from "@salesforce/label/c.AfsLbl_SignUp_Name";
import firstName from "@salesforce/label/c.AfsLbl_YourApplication_FirstName";
import lastName from "@salesforce/label/c.AfsLbl_YourApplication_LastName";
import relationship from "@salesforce/label/c.AfsLbl_Submission_Relationship";
import describeRelationship from "@salesforce/label/c.Afs_Describe_Relationship";
import email from "@salesforce/label/c.AfsLbl_YourApplication_Email";
import confirmEmail from "@salesforce/label/c.Afs_Confirm_Email";
import dateOfBirth from "@salesforce/label/c.AfsLbl_SignUp_DOB";
import placeOfBirth from "@salesforce/label/c.Afs_Place_of_birth";

export default class ApplicantRelationship extends LightningElement {
  @api guardian;
  @api options;
  @api visibility;
  @api defaultCode;

  label = {
    name,
    firstName,
    lastName,
    relationship,
    describeRelationship,
    email,
    confirmEmail,
    dateOfBirth,
    placeOfBirth
  };

  @track confirmEmail;

  connectedCallback() {
    if (this.guardian.npe4__RelatedContact__c.Email) {
      this.confirmEmail = this.guardian.npe4__RelatedContact__c.Email;
    }
  }

  handleConfirm(event) {
    event.preventDefault();
    this.confirmEmail = event.target.value;
    const confirmCmp = this.template.querySelector(".email-confirm");
    if (this.guardian.npe4__RelatedContact__c.Email === this.confirmEmail) {
      confirmCmp.setCustomValidity("");
    } else {
      confirmCmp.setCustomValidity(`The email addresses don't match`);
    }
    confirmCmp.reportValidity();
  }

  handleChange(event) {
    event.preventDefault();
    const changeRel = new CustomEvent("relchange", {
      detail: {
        relId: this.guardian.Id,
        name: event.target.name,
        value: event.target.value
      }
    });

    this.dispatchEvent(changeRel);
  }

  handlePhoneChange(event) {
    event.preventDefault();
    if (!event.detail.error) {
      const changePhone = new CustomEvent("contactchange", {
        detail: {
          conId: this.guardian.npe4__RelatedContact__c.Id,
          name: "Phone",
          value: event.detail.phone
        }
      });

      this.dispatchEvent(changePhone);
    }
  }

  handleContactChange(event) {
    event.preventDefault();
    if (event.detail.name) {
      event.target.name = event.detail.name;
      event.target.value = event.detail.value;
    }
    const changeCont = new CustomEvent("contactchange", {
      detail: {
        conId: this.guardian.npe4__RelatedContact__c.Id,
        name: event.target.name,
        value: event.target.value
      }
    });

    this.dispatchEvent(changeCont);
  }

  removeGuardian(event) {
    event.preventDefault();
    this.dispatchEvent(new CustomEvent("remove", { detail: this.guardian.Id }));
  }

  @api
  emailIsValid(valid, message) {
    const emailCmp = this.template.querySelector(".email");

    if (valid) {
      emailCmp.setCustomValidity("");
    } else {
      emailCmp.setCustomValidity(message);
    }

    emailCmp.reportValidity();
  }

  @api
  validateFields() {
    // [...this.template.querySelectorAll("lightning-input")].forEach(c =>
    //   c.reportValidity()
    // );
    const allValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, inputCmp) => {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    //validate phone and address
    const relationshipTypeFld = this.template.querySelector(
      "lightning-combobox"
    );
    relationshipTypeFld.reportValidity();

    const addressValid = this.template
      .querySelector("c-contact-address")
      .validateFields();
    let phoneValid = true;
    if (this.phoneIsRequired) {
      phoneValid = this.template
        .querySelector("c-contact-phone")
        .validateFields();
    }

    return (
      allValid &&
      relationshipTypeFld.checkValidity() &&
      addressValid &&
      phoneValid
    );
  }

  get isOptionOther() {
    return this.guardian.npe4__Type__c === "Other";
  }

  get dobIsVisible() {
    return this.visibility.DOB !== "hide";
  }
  get dobIsRequired() {
    return this.visibility.DOB === "required";
  }

  get pobIsVisible() {
    return this.visibility.POB !== "hide";
  }
  get pobIsRequired() {
    return this.visibility.POB === "required";
  }

  get phoneIsVisible() {
    return this.visibility.phone !== "hide";
  }
  get phoneIsRequired() {
    return this.visibility.phone === "required";
  }
}
