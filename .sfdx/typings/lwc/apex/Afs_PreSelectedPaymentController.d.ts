declare module "@salesforce/apex/Afs_PreSelectedPaymentController.getApplicationFee" {
  export default function getApplicationFee(param: {applicationId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedPaymentController.getApplicationProgram" {
  export default function getApplicationProgram(param: {applicationId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedPaymentController.updateApplication" {
  export default function updateApplication(param: {campURL: any, applicationId: any, recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedPaymentController.updateParent" {
  export default function updateParent(param: {recId: any}): Promise<any>;
}
