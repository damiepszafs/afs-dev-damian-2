declare module "@salesforce/apex/Afs_InputSchoolController.getSchoolAutoComplete" {
  export default function getSchoolAutoComplete(param: {input: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_InputSchoolController.getAddressAutoComplete" {
  export default function getAddressAutoComplete(param: {input: any, types: any, langug: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_InputSchoolController.getMySchool" {
  export default function getMySchool(param: {schoolId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_InputSchoolController.getAddressAutoCompleteMerge" {
  export default function getAddressAutoCompleteMerge(param: {input: any, types: any, langug: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_InputSchoolController.getAddressDetails" {
  export default function getAddressDetails(param: {PlaceId: any, lang: any}): Promise<any>;
}
