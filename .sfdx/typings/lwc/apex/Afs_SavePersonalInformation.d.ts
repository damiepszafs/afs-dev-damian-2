declare module "@salesforce/apex/Afs_SavePersonalInformation.savePersonalInfo" {
  export default function savePersonalInfo(param: {contactInfo: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getPicklistValues" {
  export default function getPicklistValues(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getPicklistValuesNoSort" {
  export default function getPicklistValuesNoSort(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getSchools" {
  export default function getSchools(): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.saveCompleteProfile" {
  export default function saveCompleteProfile(param: {contactInfo: any, applicationObj: any, schoolAppObj: any, school: any, parentGuardianWrapperJSON: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getSchoolApplication" {
  export default function getSchoolApplication(param: {applicantId: any, schoolId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getStatesPreFill" {
  export default function getStatesPreFill(param: {afsCommunityId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SavePersonalInformation.getHasFlagshipPrograms" {
  export default function getHasFlagshipPrograms(param: {conId: any, appId: any}): Promise<any>;
}
