declare module "@salesforce/apex/Afs_SettingController.getPicklistValues" {
  export default function getPicklistValues(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SettingController.getParents" {
  export default function getParents(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SettingController.changePassword" {
  export default function changePassword(param: {password: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_SettingController.saveRecord" {
  export default function saveRecord(param: {contactObj: any}): Promise<any>;
}
