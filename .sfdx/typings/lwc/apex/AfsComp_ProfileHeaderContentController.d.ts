declare module "@salesforce/apex/AfsComp_ProfileHeaderContentController.getSelectedProgramList" {
  export default function getSelectedProgramList(param: {applicant_Id: any}): Promise<any>;
}
