declare module "@salesforce/apex/Afs_AdvisorController.insertFeed" {
  export default function insertFeed(param: {ApplicationId: any, body: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_AdvisorController.insertComment" {
  export default function insertComment(param: {feedId: any, body: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_AdvisorController.getFeed" {
  export default function getFeed(param: {ApplicationId: any, ownerApplication: any, contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_AdvisorController.likeFeed" {
  export default function likeFeed(param: {feedId: any}): Promise<any>;
}
