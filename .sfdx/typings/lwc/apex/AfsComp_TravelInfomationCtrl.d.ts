declare module "@salesforce/apex/AfsComp_TravelInfomationCtrl.getPicklistValues" {
  export default function getPicklistValues(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/AfsComp_TravelInfomationCtrl.getProgramOfferDetails" {
  export default function getProgramOfferDetails(param: {programId: any}): Promise<any>;
}
declare module "@salesforce/apex/AfsComp_TravelInfomationCtrl.saveTravelInfomation" {
  export default function saveTravelInfomation(param: {contactInfo: any, taskID: any}): Promise<any>;
}
declare module "@salesforce/apex/AfsComp_TravelInfomationCtrl.deleteAttachment" {
  export default function deleteAttachment(param: {AttchmentId: any}): Promise<any>;
}
