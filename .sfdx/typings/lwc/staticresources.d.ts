declare module "@salesforce/resourceUrl/AFSLoginLoader" {
    var AFSLoginLoader: string;
    export default AFSLoginLoader;
}
declare module "@salesforce/resourceUrl/Afs_Asset" {
    var Afs_Asset: string;
    export default Afs_Asset;
}
declare module "@salesforce/resourceUrl/Afs_Asset_Toast" {
    var Afs_Asset_Toast: string;
    export default Afs_Asset_Toast;
}
declare module "@salesforce/resourceUrl/Afs_ExtraCSS" {
    var Afs_ExtraCSS: string;
    export default Afs_ExtraCSS;
}
declare module "@salesforce/resourceUrl/Afs_ProgramPayment" {
    var Afs_ProgramPayment: string;
    export default Afs_ProgramPayment;
}
declare module "@salesforce/resourceUrl/Email_Logo" {
    var Email_Logo: string;
    export default Email_Logo;
}
declare module "@salesforce/resourceUrl/Facebook_Logo" {
    var Facebook_Logo: string;
    export default Facebook_Logo;
}
declare module "@salesforce/resourceUrl/Google_Logo" {
    var Google_Logo: string;
    export default Google_Logo;
}
declare module "@salesforce/resourceUrl/NotFoundPic" {
    var NotFoundPic: string;
    export default NotFoundPic;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/libphone" {
    var libphone: string;
    export default libphone;
}
