declare module "@salesforce/label/c.AfsLbl_AboutYou_DescribeYourselfNew" {
    var AfsLbl_AboutYou_DescribeYourselfNew: string;
    export default AfsLbl_AboutYou_DescribeYourselfNew;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ExperienceHeader" {
    var AfsLbl_AboutYou_ExperienceHeader: string;
    export default AfsLbl_AboutYou_ExperienceHeader;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ExperienceHeaderHelpTxt" {
    var AfsLbl_AboutYou_ExperienceHeaderHelpTxt: string;
    export default AfsLbl_AboutYou_ExperienceHeaderHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ExperienceTextAreaHeader" {
    var AfsLbl_AboutYou_ExperienceTextAreaHeader: string;
    export default AfsLbl_AboutYou_ExperienceTextAreaHeader;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ExperienceTextAreaHeaderHelpTxt" {
    var AfsLbl_AboutYou_ExperienceTextAreaHeaderHelpTxt: string;
    export default AfsLbl_AboutYou_ExperienceTextAreaHeaderHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ExperienceWithAfsHeader" {
    var AfsLbl_AboutYou_ExperienceWithAfsHeader: string;
    export default AfsLbl_AboutYou_ExperienceWithAfsHeader;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_FamilyDecribeYouHeader" {
    var AfsLbl_AboutYou_FamilyDecribeYouHeader: string;
    export default AfsLbl_AboutYou_FamilyDecribeYouHeader;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_FamilyDecribeYouHeaderHelpTxt" {
    var AfsLbl_AboutYou_FamilyDecribeYouHeaderHelpTxt: string;
    export default AfsLbl_AboutYou_FamilyDecribeYouHeaderHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_FamilywentAbroad" {
    var AfsLbl_AboutYou_FamilywentAbroad: string;
    export default AfsLbl_AboutYou_FamilywentAbroad;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_HostedAAFSStudent" {
    var AfsLbl_AboutYou_HostedAAFSStudent: string;
    export default AfsLbl_AboutYou_HostedAAFSStudent;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_LivedAbroad" {
    var AfsLbl_AboutYou_LivedAbroad: string;
    export default AfsLbl_AboutYou_LivedAbroad;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_PageHeader" {
    var AfsLbl_AboutYou_PageHeader: string;
    export default AfsLbl_AboutYou_PageHeader;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_PageSubHeaderNew" {
    var AfsLbl_AboutYou_PageSubHeaderNew: string;
    export default AfsLbl_AboutYou_PageSubHeaderNew;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ParticipatedInAnotherPrgm" {
    var AfsLbl_AboutYou_ParticipatedInAnotherPrgm: string;
    export default AfsLbl_AboutYou_ParticipatedInAnotherPrgm;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_ProgramKindAndOrganization" {
    var AfsLbl_AboutYou_ProgramKindAndOrganization: string;
    export default AfsLbl_AboutYou_ProgramKindAndOrganization;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_SubmitBtn" {
    var AfsLbl_AboutYou_SubmitBtn: string;
    export default AfsLbl_AboutYou_SubmitBtn;
}
declare module "@salesforce/label/c.AfsLbl_AboutYou_TravelledAbroad" {
    var AfsLbl_AboutYou_TravelledAbroad: string;
    export default AfsLbl_AboutYou_TravelledAbroad;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Country" {
    var AfsLbl_Accepted_Country: string;
    export default AfsLbl_Accepted_Country;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Guide" {
    var AfsLbl_Accepted_Guide: string;
    export default AfsLbl_Accepted_Guide;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Handbook" {
    var AfsLbl_Accepted_Handbook: string;
    export default AfsLbl_Accepted_Handbook;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Language" {
    var AfsLbl_Accepted_Language: string;
    export default AfsLbl_Accepted_Language;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Learning" {
    var AfsLbl_Accepted_Learning: string;
    export default AfsLbl_Accepted_Learning;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_NoRecordsNew" {
    var AfsLbl_Accepted_NoRecordsNew: string;
    export default AfsLbl_Accepted_NoRecordsNew;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Participant" {
    var AfsLbl_Accepted_Participant: string;
    export default AfsLbl_Accepted_Participant;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_Resources" {
    var AfsLbl_Accepted_Resources: string;
    export default AfsLbl_Accepted_Resources;
}
declare module "@salesforce/label/c.AfsLbl_Accepted_YourResources" {
    var AfsLbl_Accepted_YourResources: string;
    export default AfsLbl_Accepted_YourResources;
}
declare module "@salesforce/label/c.AfsLbl_AdditionalDestination_HelpText" {
    var AfsLbl_AdditionalDestination_HelpText: string;
    export default AfsLbl_AdditionalDestination_HelpText;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_Advisor" {
    var AfsLbl_Adivisor_Advisor: string;
    export default AfsLbl_Adivisor_Advisor;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_Comment" {
    var AfsLbl_Adivisor_Comment: string;
    export default AfsLbl_Adivisor_Comment;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_Header" {
    var AfsLbl_Adivisor_Header: string;
    export default AfsLbl_Adivisor_Header;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_Like" {
    var AfsLbl_Adivisor_Like: string;
    export default AfsLbl_Adivisor_Like;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_Send" {
    var AfsLbl_Adivisor_Send: string;
    export default AfsLbl_Adivisor_Send;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_You" {
    var AfsLbl_Adivisor_You: string;
    export default AfsLbl_Adivisor_You;
}
declare module "@salesforce/label/c.AfsLbl_Adivisor_wrote" {
    var AfsLbl_Adivisor_wrote: string;
    export default AfsLbl_Adivisor_wrote;
}
declare module "@salesforce/label/c.AfsLbl_Application_Eligible" {
    var AfsLbl_Application_Eligible: string;
    export default AfsLbl_Application_Eligible;
}
declare module "@salesforce/label/c.AfsLbl_Application_MoreThanOne" {
    var AfsLbl_Application_MoreThanOne: string;
    export default AfsLbl_Application_MoreThanOne;
}
declare module "@salesforce/label/c.AfsLbl_Application_WaitListed" {
    var AfsLbl_Application_WaitListed: string;
    export default AfsLbl_Application_WaitListed;
}
declare module "@salesforce/label/c.AfsLbl_Button_CANCELCAP" {
    var AfsLbl_Button_CANCELCAP: string;
    export default AfsLbl_Button_CANCELCAP;
}
declare module "@salesforce/label/c.AfsLbl_Button_Cancel" {
    var AfsLbl_Button_Cancel: string;
    export default AfsLbl_Button_Cancel;
}
declare module "@salesforce/label/c.AfsLbl_Button_SaveAsDraft" {
    var AfsLbl_Button_SaveAsDraft: string;
    export default AfsLbl_Button_SaveAsDraft;
}
declare module "@salesforce/label/c.AfsLbl_Button_SearchPrograms" {
    var AfsLbl_Button_SearchPrograms: string;
    export default AfsLbl_Button_SearchPrograms;
}
declare module "@salesforce/label/c.AfsLbl_Button_YesChange" {
    var AfsLbl_Button_YesChange: string;
    export default AfsLbl_Button_YesChange;
}
declare module "@salesforce/label/c.AfsLbl_Custom_Button_Scholarship" {
    var AfsLbl_Custom_Button_Scholarship: string;
    export default AfsLbl_Custom_Button_Scholarship;
}
declare module "@salesforce/label/c.AfsLbl_DM_Accepted_OnBoarding_HeadDesc" {
    var AfsLbl_DM_Accepted_OnBoarding_HeadDesc: string;
    export default AfsLbl_DM_Accepted_OnBoarding_HeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_Accepted_OnBoarding_HeadTitle" {
    var AfsLbl_DM_Accepted_OnBoarding_HeadTitle: string;
    export default AfsLbl_DM_Accepted_OnBoarding_HeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_Accepted_Purchase_HeadDesc" {
    var AfsLbl_DM_Accepted_Purchase_HeadDesc: string;
    export default AfsLbl_DM_Accepted_Purchase_HeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_Accepted_Purchase_HeadTitle" {
    var AfsLbl_DM_Accepted_Purchase_HeadTitle: string;
    export default AfsLbl_DM_Accepted_Purchase_HeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_FileUploadErrorMsg" {
    var AfsLbl_DM_FileUploadErrorMsg: string;
    export default AfsLbl_DM_FileUploadErrorMsg;
}
declare module "@salesforce/label/c.AfsLbl_DM_FileUploadErrorTitle" {
    var AfsLbl_DM_FileUploadErrorTitle: string;
    export default AfsLbl_DM_FileUploadErrorTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_FileUploadMsg" {
    var AfsLbl_DM_FileUploadMsg: string;
    export default AfsLbl_DM_FileUploadMsg;
}
declare module "@salesforce/label/c.AfsLbl_DM_FileUploadTitle" {
    var AfsLbl_DM_FileUploadTitle: string;
    export default AfsLbl_DM_FileUploadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_ParticipationDesire_Desc" {
    var AfsLbl_DM_ParticipationDesire_Desc: string;
    export default AfsLbl_DM_ParticipationDesire_Desc;
}
declare module "@salesforce/label/c.AfsLbl_DM_ParticipationDesire_Head" {
    var AfsLbl_DM_ParticipationDesire_Head: string;
    export default AfsLbl_DM_ParticipationDesire_Head;
}
declare module "@salesforce/label/c.AfsLbl_DM_PreApplied_Desicion_HeadDesc" {
    var AfsLbl_DM_PreApplied_Desicion_HeadDesc: string;
    export default AfsLbl_DM_PreApplied_Desicion_HeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_PreApplied_Desicion_HeadTitle" {
    var AfsLbl_DM_PreApplied_Desicion_HeadTitle: string;
    export default AfsLbl_DM_PreApplied_Desicion_HeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_PreApplied_Extra_HeadDesc" {
    var AfsLbl_DM_PreApplied_Extra_HeadDesc: string;
    export default AfsLbl_DM_PreApplied_Extra_HeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_PreApplied_Extra_HeadTitle" {
    var AfsLbl_DM_PreApplied_Extra_HeadTitle: string;
    export default AfsLbl_DM_PreApplied_Extra_HeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionAcceptedHeadDesc" {
    var AfsLbl_DM_SubmissionAcceptedHeadDesc: string;
    export default AfsLbl_DM_SubmissionAcceptedHeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionAcceptedHeadTitle" {
    var AfsLbl_DM_SubmissionAcceptedHeadTitle: string;
    export default AfsLbl_DM_SubmissionAcceptedHeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionExtraDocHeadDesc" {
    var AfsLbl_DM_SubmissionExtraDocHeadDesc: string;
    export default AfsLbl_DM_SubmissionExtraDocHeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionExtraDocHeadTitle" {
    var AfsLbl_DM_SubmissionExtraDocHeadTitle: string;
    export default AfsLbl_DM_SubmissionExtraDocHeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionParticipationHeadDesc" {
    var AfsLbl_DM_SubmissionParticipationHeadDesc: string;
    export default AfsLbl_DM_SubmissionParticipationHeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionParticipationHeadTitle" {
    var AfsLbl_DM_SubmissionParticipationHeadTitle: string;
    export default AfsLbl_DM_SubmissionParticipationHeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionPreSelectedHeadDesc" {
    var AfsLbl_DM_SubmissionPreSelectedHeadDesc: string;
    export default AfsLbl_DM_SubmissionPreSelectedHeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionPreSelectedHeadTitle" {
    var AfsLbl_DM_SubmissionPreSelectedHeadTitle: string;
    export default AfsLbl_DM_SubmissionPreSelectedHeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionReadyHeadDesc" {
    var AfsLbl_DM_SubmissionReadyHeadDesc: string;
    export default AfsLbl_DM_SubmissionReadyHeadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DM_SubmissionReadyHeadTitle" {
    var AfsLbl_DM_SubmissionReadyHeadTitle: string;
    export default AfsLbl_DM_SubmissionReadyHeadTitle;
}
declare module "@salesforce/label/c.AfsLbl_DisplayResult_NoRecordsToDisplay" {
    var AfsLbl_DisplayResult_NoRecordsToDisplay: string;
    export default AfsLbl_DisplayResult_NoRecordsToDisplay;
}
declare module "@salesforce/label/c.AfsLbl_DisplayResult_SelectAtleastOneFilterErrorMsg" {
    var AfsLbl_DisplayResult_SelectAtleastOneFilterErrorMsg: string;
    export default AfsLbl_DisplayResult_SelectAtleastOneFilterErrorMsg;
}
declare module "@salesforce/label/c.AfsLbl_DisplayResult_SelectProgrm" {
    var AfsLbl_DisplayResult_SelectProgrm: string;
    export default AfsLbl_DisplayResult_SelectProgrm;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_AcademicDesc" {
    var AfsLbl_DocumentManager_AcademicDesc: string;
    export default AfsLbl_DocumentManager_AcademicDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_AcademicHeader" {
    var AfsLbl_DocumentManager_AcademicHeader: string;
    export default AfsLbl_DocumentManager_AcademicHeader;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_AcademicHistHelpTxt" {
    var AfsLbl_DocumentManager_AcademicHistHelpTxt: string;
    export default AfsLbl_DocumentManager_AcademicHistHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_Cancelled" {
    var AfsLbl_DocumentManager_Cancelled: string;
    export default AfsLbl_DocumentManager_Cancelled;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_DomesticReturnDesc" {
    var AfsLbl_DocumentManager_DomesticReturnDesc: string;
    export default AfsLbl_DocumentManager_DomesticReturnDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_DomesticReturnHead" {
    var AfsLbl_DocumentManager_DomesticReturnHead: string;
    export default AfsLbl_DocumentManager_DomesticReturnHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_DomesticReturnHelTxt" {
    var AfsLbl_DocumentManager_DomesticReturnHelTxt: string;
    export default AfsLbl_DocumentManager_DomesticReturnHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_DomesticTravelHead" {
    var AfsLbl_DocumentManager_DomesticTravelHead: string;
    export default AfsLbl_DocumentManager_DomesticTravelHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_DomesticTravelHelTxt" {
    var AfsLbl_DocumentManager_DomesticTravelHelTxt: string;
    export default AfsLbl_DocumentManager_DomesticTravelHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_Done" {
    var AfsLbl_DocumentManager_Done: string;
    export default AfsLbl_DocumentManager_Done;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthCertiDesc" {
    var AfsLbl_DocumentManager_HealthCertiDesc: string;
    export default AfsLbl_DocumentManager_HealthCertiDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthCertifHelpTxt" {
    var AfsLbl_DocumentManager_HealthCertifHelpTxt: string;
    export default AfsLbl_DocumentManager_HealthCertifHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthDocumentsHeader" {
    var AfsLbl_DocumentManager_HealthDocumentsHeader: string;
    export default AfsLbl_DocumentManager_HealthDocumentsHeader;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthUpdateDesc" {
    var AfsLbl_DocumentManager_HealthUpdateDesc: string;
    export default AfsLbl_DocumentManager_HealthUpdateDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthUpdateHeader" {
    var AfsLbl_DocumentManager_HealthUpdateHeader: string;
    export default AfsLbl_DocumentManager_HealthUpdateHeader;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HealthUpdateHelpTxt" {
    var AfsLbl_DocumentManager_HealthUpdateHelpTxt: string;
    export default AfsLbl_DocumentManager_HealthUpdateHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HomeInterviewDesc" {
    var AfsLbl_DocumentManager_HomeInterviewDesc: string;
    export default AfsLbl_DocumentManager_HomeInterviewDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HomeInterviewHead" {
    var AfsLbl_DocumentManager_HomeInterviewHead: string;
    export default AfsLbl_DocumentManager_HomeInterviewHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_HomeInterviewHelTxt" {
    var AfsLbl_DocumentManager_HomeInterviewHelTxt: string;
    export default AfsLbl_DocumentManager_HomeInterviewHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_InReview" {
    var AfsLbl_DocumentManager_InReview: string;
    export default AfsLbl_DocumentManager_InReview;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_LanguageTrainingDesc" {
    var AfsLbl_DocumentManager_LanguageTrainingDesc: string;
    export default AfsLbl_DocumentManager_LanguageTrainingDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_LanguageTrainingHead" {
    var AfsLbl_DocumentManager_LanguageTrainingHead: string;
    export default AfsLbl_DocumentManager_LanguageTrainingHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_LanguageTrainingHelTxt" {
    var AfsLbl_DocumentManager_LanguageTrainingHelTxt: string;
    export default AfsLbl_DocumentManager_LanguageTrainingHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_ParticipatAcadDesc" {
    var AfsLbl_DocumentManager_ParticipatAcadDesc: string;
    export default AfsLbl_DocumentManager_ParticipatAcadDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_ParticipatAcadHead" {
    var AfsLbl_DocumentManager_ParticipatAcadHead: string;
    export default AfsLbl_DocumentManager_ParticipatAcadHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_ParticipatAcadHelTxt" {
    var AfsLbl_DocumentManager_ParticipatAcadHelTxt: string;
    export default AfsLbl_DocumentManager_ParticipatAcadHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_PreDepartureDesc" {
    var AfsLbl_DocumentManager_PreDepartureDesc: string;
    export default AfsLbl_DocumentManager_PreDepartureDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_PreDepartureHead" {
    var AfsLbl_DocumentManager_PreDepartureHead: string;
    export default AfsLbl_DocumentManager_PreDepartureHead;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_PreDepartureHelTxt" {
    var AfsLbl_DocumentManager_PreDepartureHelTxt: string;
    export default AfsLbl_DocumentManager_PreDepartureHelTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_Rejected" {
    var AfsLbl_DocumentManager_Rejected: string;
    export default AfsLbl_DocumentManager_Rejected;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_SCHOLARSHIPDesc" {
    var AfsLbl_DocumentManager_SCHOLARSHIPDesc: string;
    export default AfsLbl_DocumentManager_SCHOLARSHIPDesc;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_SCHOLARSHIPHeader" {
    var AfsLbl_DocumentManager_SCHOLARSHIPHeader: string;
    export default AfsLbl_DocumentManager_SCHOLARSHIPHeader;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_ScholarDocHelpTxt" {
    var AfsLbl_DocumentManager_ScholarDocHelpTxt: string;
    export default AfsLbl_DocumentManager_ScholarDocHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_DocumentManager_ToDoList" {
    var AfsLbl_DocumentManager_ToDoList: string;
    export default AfsLbl_DocumentManager_ToDoList;
}
declare module "@salesforce/label/c.AfsLbl_Education_CantFindMySchoolBtnLBL" {
    var AfsLbl_Education_CantFindMySchoolBtnLBL: string;
    export default AfsLbl_Education_CantFindMySchoolBtnLBL;
}
declare module "@salesforce/label/c.AfsLbl_Education_SchoolAddressLBL" {
    var AfsLbl_Education_SchoolAddressLBL: string;
    export default AfsLbl_Education_SchoolAddressLBL;
}
declare module "@salesforce/label/c.AfsLbl_Education_SchoolGradeAverage" {
    var AfsLbl_Education_SchoolGradeAverage: string;
    export default AfsLbl_Education_SchoolGradeAverage;
}
declare module "@salesforce/label/c.AfsLbl_Education_SchoolNameLBL" {
    var AfsLbl_Education_SchoolNameLBL: string;
    export default AfsLbl_Education_SchoolNameLBL;
}
declare module "@salesforce/label/c.AfsLbl_Education_SchoolPlaceholder" {
    var AfsLbl_Education_SchoolPlaceholder: string;
    export default AfsLbl_Education_SchoolPlaceholder;
}
declare module "@salesforce/label/c.AfsLbl_Error_Ability_To_live_with_household_pets" {
    var AfsLbl_Error_Ability_To_live_with_household_pets: string;
    export default AfsLbl_Error_Ability_To_live_with_household_pets;
}
declare module "@salesforce/label/c.AfsLbl_Error_AboutYouSectionHeader" {
    var AfsLbl_Error_AboutYouSectionHeader: string;
    export default AfsLbl_Error_AboutYouSectionHeader;
}
declare module "@salesforce/label/c.AfsLbl_Error_AdditionalCommentsDietery" {
    var AfsLbl_Error_AdditionalCommentsDietery: string;
    export default AfsLbl_Error_AdditionalCommentsDietery;
}
declare module "@salesforce/label/c.AfsLbl_Error_AdditionalCommentsPhysicalRestrictions" {
    var AfsLbl_Error_AdditionalCommentsPhysicalRestrictions: string;
    export default AfsLbl_Error_AdditionalCommentsPhysicalRestrictions;
}
declare module "@salesforce/label/c.AfsLbl_Error_AddressRequire" {
    var AfsLbl_Error_AddressRequire: string;
    export default AfsLbl_Error_AddressRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_AgreeTermsRequire" {
    var AfsLbl_Error_AgreeTermsRequire: string;
    export default AfsLbl_Error_AgreeTermsRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_AttendingCOllgeRequired" {
    var AfsLbl_Error_AttendingCOllgeRequired: string;
    export default AfsLbl_Error_AttendingCOllgeRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_BrotherAgeRequired" {
    var AfsLbl_Error_BrotherAgeRequired: string;
    export default AfsLbl_Error_BrotherAgeRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Brother_Age" {
    var AfsLbl_Error_Brother_Age: string;
    export default AfsLbl_Error_Brother_Age;
}
declare module "@salesforce/label/c.AfsLbl_Error_CityRequire" {
    var AfsLbl_Error_CityRequire: string;
    export default AfsLbl_Error_CityRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_CollegeNameRequired" {
    var AfsLbl_Error_CollegeNameRequired: string;
    export default AfsLbl_Error_CollegeNameRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_CountryLegalRequire" {
    var AfsLbl_Error_CountryLegalRequire: string;
    export default AfsLbl_Error_CountryLegalRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Country_of_Legal_Residence" {
    var AfsLbl_Error_Country_of_Legal_Residence: string;
    export default AfsLbl_Error_Country_of_Legal_Residence;
}
declare module "@salesforce/label/c.AfsLbl_Error_CriminalConictionRequired" {
    var AfsLbl_Error_CriminalConictionRequired: string;
    export default AfsLbl_Error_CriminalConictionRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_DOBLessThan13" {
    var AfsLbl_Error_DOBLessThan13: string;
    export default AfsLbl_Error_DOBLessThan13;
}
declare module "@salesforce/label/c.AfsLbl_Error_DOBNoFuture" {
    var AfsLbl_Error_DOBNoFuture: string;
    export default AfsLbl_Error_DOBNoFuture;
}
declare module "@salesforce/label/c.AfsLbl_Error_DOBRequire" {
    var AfsLbl_Error_DOBRequire: string;
    export default AfsLbl_Error_DOBRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_DescriptionStudentRequired" {
    var AfsLbl_Error_DescriptionStudentRequired: string;
    export default AfsLbl_Error_DescriptionStudentRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_DesiredAmountRequire" {
    var AfsLbl_Error_DesiredAmountRequire: string;
    export default AfsLbl_Error_DesiredAmountRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_DieteryRestrictRequired" {
    var AfsLbl_Error_DieteryRestrictRequired: string;
    export default AfsLbl_Error_DieteryRestrictRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_DoYouSmokeRequired" {
    var AfsLbl_Error_DoYouSmokeRequired: string;
    export default AfsLbl_Error_DoYouSmokeRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Dual_citizenship" {
    var AfsLbl_Error_Dual_citizenship: string;
    export default AfsLbl_Error_Dual_citizenship;
}
declare module "@salesforce/label/c.AfsLbl_Error_EligibilityRequire" {
    var AfsLbl_Error_EligibilityRequire: string;
    export default AfsLbl_Error_EligibilityRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_EmailNoValid" {
    var AfsLbl_Error_EmailNoValid: string;
    export default AfsLbl_Error_EmailNoValid;
}
declare module "@salesforce/label/c.AfsLbl_Error_EmailRequire" {
    var AfsLbl_Error_EmailRequire: string;
    export default AfsLbl_Error_EmailRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Ethinicity" {
    var AfsLbl_Error_Ethinicity: string;
    export default AfsLbl_Error_Ethinicity;
}
declare module "@salesforce/label/c.AfsLbl_Error_Expiration_Date_Invalid" {
    var AfsLbl_Error_Expiration_Date_Invalid: string;
    export default AfsLbl_Error_Expiration_Date_Invalid;
}
declare module "@salesforce/label/c.AfsLbl_Error_Expiration_Date_Req" {
    var AfsLbl_Error_Expiration_Date_Req: string;
    export default AfsLbl_Error_Expiration_Date_Req;
}
declare module "@salesforce/label/c.AfsLbl_Error_Facebook" {
    var AfsLbl_Error_Facebook: string;
    export default AfsLbl_Error_Facebook;
}
declare module "@salesforce/label/c.AfsLbl_Error_FirstNameRequire" {
    var AfsLbl_Error_FirstNameRequire: string;
    export default AfsLbl_Error_FirstNameRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_GenderRequire" {
    var AfsLbl_Error_GenderRequire: string;
    export default AfsLbl_Error_GenderRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_GraduateDateNoFuture" {
    var AfsLbl_Error_GraduateDateNoFuture: string;
    export default AfsLbl_Error_GraduateDateNoFuture;
}
declare module "@salesforce/label/c.AfsLbl_Error_GraduationDateRequired" {
    var AfsLbl_Error_GraduationDateRequired: string;
    export default AfsLbl_Error_GraduationDateRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_HealthIssueRequired" {
    var AfsLbl_Error_HealthIssueRequired: string;
    export default AfsLbl_Error_HealthIssueRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_HearAboutUsRequire" {
    var AfsLbl_Error_HearAboutUsRequire: string;
    export default AfsLbl_Error_HearAboutUsRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Home_Mother" {
    var AfsLbl_Error_Home_Mother: string;
    export default AfsLbl_Error_Home_Mother;
}
declare module "@salesforce/label/c.AfsLbl_Error_Home_Sister_Stepsister" {
    var AfsLbl_Error_Home_Sister_Stepsister: string;
    export default AfsLbl_Error_Home_Sister_Stepsister;
}
declare module "@salesforce/label/c.AfsLbl_Error_Instagram" {
    var AfsLbl_Error_Instagram: string;
    export default AfsLbl_Error_Instagram;
}
declare module "@salesforce/label/c.AfsLbl_Error_InvalidBirthdate" {
    var AfsLbl_Error_InvalidBirthdate: string;
    export default AfsLbl_Error_InvalidBirthdate;
}
declare module "@salesforce/label/c.AfsLbl_Error_LGTBQ" {
    var AfsLbl_Error_LGTBQ: string;
    export default AfsLbl_Error_LGTBQ;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language1ProfRequired" {
    var AfsLbl_Error_Language1ProfRequired: string;
    export default AfsLbl_Error_Language1ProfRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language1Required" {
    var AfsLbl_Error_Language1Required: string;
    export default AfsLbl_Error_Language1Required;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language2ProfRequired" {
    var AfsLbl_Error_Language2ProfRequired: string;
    export default AfsLbl_Error_Language2ProfRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language2Required" {
    var AfsLbl_Error_Language2Required: string;
    export default AfsLbl_Error_Language2Required;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language3ProfRequired" {
    var AfsLbl_Error_Language3ProfRequired: string;
    export default AfsLbl_Error_Language3ProfRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language3Required" {
    var AfsLbl_Error_Language3Required: string;
    export default AfsLbl_Error_Language3Required;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language4ProfRequired" {
    var AfsLbl_Error_Language4ProfRequired: string;
    export default AfsLbl_Error_Language4ProfRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Language4Required" {
    var AfsLbl_Error_Language4Required: string;
    export default AfsLbl_Error_Language4Required;
}
declare module "@salesforce/label/c.AfsLbl_Error_LastNameRequire" {
    var AfsLbl_Error_LastNameRequire: string;
    export default AfsLbl_Error_LastNameRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_LinkMustBeClicked" {
    var AfsLbl_Error_LinkMustBeClicked: string;
    export default AfsLbl_Error_LinkMustBeClicked;
}
declare module "@salesforce/label/c.AfsLbl_Error_Live_with_guardian" {
    var AfsLbl_Error_Live_with_guardian: string;
    export default AfsLbl_Error_Live_with_guardian;
}
declare module "@salesforce/label/c.AfsLbl_Error_MobileNumberInvalid" {
    var AfsLbl_Error_MobileNumberInvalid: string;
    export default AfsLbl_Error_MobileNumberInvalid;
}
declare module "@salesforce/label/c.AfsLbl_Error_MobileNumberNoValid" {
    var AfsLbl_Error_MobileNumberNoValid: string;
    export default AfsLbl_Error_MobileNumberNoValid;
}
declare module "@salesforce/label/c.AfsLbl_Error_MobileNumberRequire" {
    var AfsLbl_Error_MobileNumberRequire: string;
    export default AfsLbl_Error_MobileNumberRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Nationality" {
    var AfsLbl_Error_Nationality: string;
    export default AfsLbl_Error_Nationality;
}
declare module "@salesforce/label/c.AfsLbl_Error_Open_To" {
    var AfsLbl_Error_Open_To: string;
    export default AfsLbl_Error_Open_To;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianEmailNoValid" {
    var AfsLbl_Error_ParentGuardianEmailNoValid: string;
    export default AfsLbl_Error_ParentGuardianEmailNoValid;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianEmailRequired" {
    var AfsLbl_Error_ParentGuardianEmailRequired: string;
    export default AfsLbl_Error_ParentGuardianEmailRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianLastName" {
    var AfsLbl_Error_ParentGuardianLastName: string;
    export default AfsLbl_Error_ParentGuardianLastName;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianMailRepeated" {
    var AfsLbl_Error_ParentGuardianMailRepeated: string;
    export default AfsLbl_Error_ParentGuardianMailRepeated;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianName" {
    var AfsLbl_Error_ParentGuardianName: string;
    export default AfsLbl_Error_ParentGuardianName;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianPhoneNoValid" {
    var AfsLbl_Error_ParentGuardianPhoneNoValid: string;
    export default AfsLbl_Error_ParentGuardianPhoneNoValid;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianPhoneRepeated" {
    var AfsLbl_Error_ParentGuardianPhoneRepeated: string;
    export default AfsLbl_Error_ParentGuardianPhoneRepeated;
}
declare module "@salesforce/label/c.AfsLbl_Error_ParentGuardianRequire" {
    var AfsLbl_Error_ParentGuardianRequire: string;
    export default AfsLbl_Error_ParentGuardianRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Passport_Country_of_Issue" {
    var AfsLbl_Error_Passport_Country_of_Issue: string;
    export default AfsLbl_Error_Passport_Country_of_Issue;
}
declare module "@salesforce/label/c.AfsLbl_Error_PasswordAddOn" {
    var AfsLbl_Error_PasswordAddOn: string;
    export default AfsLbl_Error_PasswordAddOn;
}
declare module "@salesforce/label/c.AfsLbl_Error_PasswordRequire" {
    var AfsLbl_Error_PasswordRequire: string;
    export default AfsLbl_Error_PasswordRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_PasswordSystem" {
    var AfsLbl_Error_PasswordSystem: string;
    export default AfsLbl_Error_PasswordSystem;
}
declare module "@salesforce/label/c.AfsLbl_Error_PasswordWeek" {
    var AfsLbl_Error_PasswordWeek: string;
    export default AfsLbl_Error_PasswordWeek;
}
declare module "@salesforce/label/c.AfsLbl_Error_PersonalSectionHeader" {
    var AfsLbl_Error_PersonalSectionHeader: string;
    export default AfsLbl_Error_PersonalSectionHeader;
}
declare module "@salesforce/label/c.AfsLbl_Error_PhoneNumberInvalid" {
    var AfsLbl_Error_PhoneNumberInvalid: string;
    export default AfsLbl_Error_PhoneNumberInvalid;
}
declare module "@salesforce/label/c.AfsLbl_Error_PhoneRequire" {
    var AfsLbl_Error_PhoneRequire: string;
    export default AfsLbl_Error_PhoneRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_PhysicalRestrictionsRequired" {
    var AfsLbl_Error_PhysicalRestrictionsRequired: string;
    export default AfsLbl_Error_PhysicalRestrictionsRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_PleaseAnswer" {
    var AfsLbl_Error_PleaseAnswer: string;
    export default AfsLbl_Error_PleaseAnswer;
}
declare module "@salesforce/label/c.AfsLbl_Error_PostalCodeRequire" {
    var AfsLbl_Error_PostalCodeRequire: string;
    export default AfsLbl_Error_PostalCodeRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_PromoCodeRequire" {
    var AfsLbl_Error_PromoCodeRequire: string;
    export default AfsLbl_Error_PromoCodeRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_Religious_Services" {
    var AfsLbl_Error_Religious_Services: string;
    export default AfsLbl_Error_Religious_Services;
}
declare module "@salesforce/label/c.AfsLbl_Error_Religious_af_liation" {
    var AfsLbl_Error_Religious_af_liation: string;
    export default AfsLbl_Error_Religious_af_liation;
}
declare module "@salesforce/label/c.AfsLbl_Error_SchoolAverageGrade" {
    var AfsLbl_Error_SchoolAverageGrade: string;
    export default AfsLbl_Error_SchoolAverageGrade;
}
declare module "@salesforce/label/c.AfsLbl_Error_SchoolRequired" {
    var AfsLbl_Error_SchoolRequired: string;
    export default AfsLbl_Error_SchoolRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_SchoolTypeRequired" {
    var AfsLbl_Error_SchoolTypeRequired: string;
    export default AfsLbl_Error_SchoolTypeRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Secondary_citizenship" {
    var AfsLbl_Error_Secondary_citizenship: string;
    export default AfsLbl_Error_Secondary_citizenship;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectAPrgmSectionHeader" {
    var AfsLbl_Error_SelectAPrgmSectionHeader: string;
    export default AfsLbl_Error_SelectAPrgmSectionHeader;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneASFExperience" {
    var AfsLbl_Error_SelectOneASFExperience: string;
    export default AfsLbl_Error_SelectOneASFExperience;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneDescribeYourSelf" {
    var AfsLbl_Error_SelectOneDescribeYourSelf: string;
    export default AfsLbl_Error_SelectOneDescribeYourSelf;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneExpirenceVal" {
    var AfsLbl_Error_SelectOneExpirenceVal: string;
    export default AfsLbl_Error_SelectOneExpirenceVal;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneFamilySayAboutVal" {
    var AfsLbl_Error_SelectOneFamilySayAboutVal: string;
    export default AfsLbl_Error_SelectOneFamilySayAboutVal;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneHowGlobalCompetance" {
    var AfsLbl_Error_SelectOneHowGlobalCompetance: string;
    export default AfsLbl_Error_SelectOneHowGlobalCompetance;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOnePrgmMessage" {
    var AfsLbl_Error_SelectOnePrgmMessage: string;
    export default AfsLbl_Error_SelectOnePrgmMessage;
}
declare module "@salesforce/label/c.AfsLbl_Error_SelectOneWhyGlobalCompetance" {
    var AfsLbl_Error_SelectOneWhyGlobalCompetance: string;
    export default AfsLbl_Error_SelectOneWhyGlobalCompetance;
}
declare module "@salesforce/label/c.AfsLbl_Error_SisterAgeRequired" {
    var AfsLbl_Error_SisterAgeRequired: string;
    export default AfsLbl_Error_SisterAgeRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Sister_Age" {
    var AfsLbl_Error_Sister_Age: string;
    export default AfsLbl_Error_Sister_Age;
}
declare module "@salesforce/label/c.AfsLbl_Error_SnapChat" {
    var AfsLbl_Error_SnapChat: string;
    export default AfsLbl_Error_SnapChat;
}
declare module "@salesforce/label/c.AfsLbl_Error_SpecifyProgramParticipated" {
    var AfsLbl_Error_SpecifyProgramParticipated: string;
    export default AfsLbl_Error_SpecifyProgramParticipated;
}
declare module "@salesforce/label/c.AfsLbl_Error_StateRequire" {
    var AfsLbl_Error_StateRequire: string;
    export default AfsLbl_Error_StateRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_This_is_your_emergency_contact" {
    var AfsLbl_Error_This_is_your_emergency_contact: string;
    export default AfsLbl_Error_This_is_your_emergency_contact;
}
declare module "@salesforce/label/c.AfsLbl_Error_To_Many_Scholarships" {
    var AfsLbl_Error_To_Many_Scholarships: string;
    export default AfsLbl_Error_To_Many_Scholarships;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_FirstName" {
    var AfsLbl_Error_Travel_FirstName: string;
    export default AfsLbl_Error_Travel_FirstName;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_LastName" {
    var AfsLbl_Error_Travel_LastName: string;
    export default AfsLbl_Error_Travel_LastName;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_MiddleName" {
    var AfsLbl_Error_Travel_MiddleName: string;
    export default AfsLbl_Error_Travel_MiddleName;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_PassPortNumber_Req" {
    var AfsLbl_Error_Travel_PassPortNumber_Req: string;
    export default AfsLbl_Error_Travel_PassPortNumber_Req;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_SecondCitizenship" {
    var AfsLbl_Error_Travel_SecondCitizenship: string;
    export default AfsLbl_Error_Travel_SecondCitizenship;
}
declare module "@salesforce/label/c.AfsLbl_Error_Travel_Tshirt" {
    var AfsLbl_Error_Travel_Tshirt: string;
    export default AfsLbl_Error_Travel_Tshirt;
}
declare module "@salesforce/label/c.AfsLbl_Error_Twitter" {
    var AfsLbl_Error_Twitter: string;
    export default AfsLbl_Error_Twitter;
}
declare module "@salesforce/label/c.AfsLbl_Error_Unselected_Scholarships" {
    var AfsLbl_Error_Unselected_Scholarships: string;
    export default AfsLbl_Error_Unselected_Scholarships;
}
declare module "@salesforce/label/c.AfsLbl_Error_UserAlreadyExist" {
    var AfsLbl_Error_UserAlreadyExist: string;
    export default AfsLbl_Error_UserAlreadyExist;
}
declare module "@salesforce/label/c.AfsLbl_Error_WriteAboutExperience" {
    var AfsLbl_Error_WriteAboutExperience: string;
    export default AfsLbl_Error_WriteAboutExperience;
}
declare module "@salesforce/label/c.AfsLbl_Error_WriteAboutYourself" {
    var AfsLbl_Error_WriteAboutYourself: string;
    export default AfsLbl_Error_WriteAboutYourself;
}
declare module "@salesforce/label/c.AfsLbl_Error_YourPhotosRequired" {
    var AfsLbl_Error_YourPhotosRequired: string;
    export default AfsLbl_Error_YourPhotosRequired;
}
declare module "@salesforce/label/c.AfsLbl_Error_Youtube" {
    var AfsLbl_Error_Youtube: string;
    export default AfsLbl_Error_Youtube;
}
declare module "@salesforce/label/c.AfsLbl_Error_ZipCodeLength" {
    var AfsLbl_Error_ZipCodeLength: string;
    export default AfsLbl_Error_ZipCodeLength;
}
declare module "@salesforce/label/c.AfsLbl_Error_ZipCodeRequire" {
    var AfsLbl_Error_ZipCodeRequire: string;
    export default AfsLbl_Error_ZipCodeRequire;
}
declare module "@salesforce/label/c.AfsLbl_Error_linkedIn" {
    var AfsLbl_Error_linkedIn: string;
    export default AfsLbl_Error_linkedIn;
}
declare module "@salesforce/label/c.AfsLbl_ForgetPass_NoUserExist" {
    var AfsLbl_ForgetPass_NoUserExist: string;
    export default AfsLbl_ForgetPass_NoUserExist;
}
declare module "@salesforce/label/c.AfsLbl_ForgetPass_PassworResetLinkSent" {
    var AfsLbl_ForgetPass_PassworResetLinkSent: string;
    export default AfsLbl_ForgetPass_PassworResetLinkSent;
}
declare module "@salesforce/label/c.AfsLbl_ForgetPass_ResetPassword" {
    var AfsLbl_ForgetPass_ResetPassword: string;
    export default AfsLbl_ForgetPass_ResetPassword;
}
declare module "@salesforce/label/c.AfsLbl_ForgetPass_Submit" {
    var AfsLbl_ForgetPass_Submit: string;
    export default AfsLbl_ForgetPass_Submit;
}
declare module "@salesforce/label/c.AfsLbl_ForgetPass_UserNameLbl" {
    var AfsLbl_ForgetPass_UserNameLbl: string;
    export default AfsLbl_ForgetPass_UserNameLbl;
}
declare module "@salesforce/label/c.AfsLbl_ForgotPass_UsernameRequire" {
    var AfsLbl_ForgotPass_UsernameRequire: string;
    export default AfsLbl_ForgotPass_UsernameRequire;
}
declare module "@salesforce/label/c.AfsLbl_General_Account" {
    var AfsLbl_General_Account: string;
    export default AfsLbl_General_Account;
}
declare module "@salesforce/label/c.AfsLbl_General_None" {
    var AfsLbl_General_None: string;
    export default AfsLbl_General_None;
}
declare module "@salesforce/label/c.AfsLbl_GlobalCompetancy_HowToBeGlobal" {
    var AfsLbl_GlobalCompetancy_HowToBeGlobal: string;
    export default AfsLbl_GlobalCompetancy_HowToBeGlobal;
}
declare module "@salesforce/label/c.AfsLbl_GlobalCompetancy_HowisGlobal" {
    var AfsLbl_GlobalCompetancy_HowisGlobal: string;
    export default AfsLbl_GlobalCompetancy_HowisGlobal;
}
declare module "@salesforce/label/c.AfsLbl_GlobalCompetancy_WhatisGlobal" {
    var AfsLbl_GlobalCompetancy_WhatisGlobal: string;
    export default AfsLbl_GlobalCompetancy_WhatisGlobal;
}
declare module "@salesforce/label/c.AfsLbl_GlobalCompetancy_WhyisGlobal" {
    var AfsLbl_GlobalCompetancy_WhyisGlobal: string;
    export default AfsLbl_GlobalCompetancy_WhyisGlobal;
}
declare module "@salesforce/label/c.AfsLbl_Home_Father" {
    var AfsLbl_Home_Father: string;
    export default AfsLbl_Home_Father;
}
declare module "@salesforce/label/c.AfsLbl_Introduce_yourself" {
    var AfsLbl_Introduce_yourself: string;
    export default AfsLbl_Introduce_yourself;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_ExcitingAFS_Error" {
    var AfsLbl_Introduction_ExcitingAFS_Error: string;
    export default AfsLbl_Introduction_ExcitingAFS_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_ExcitingAboutAFS" {
    var AfsLbl_Introduction_ExcitingAboutAFS: string;
    export default AfsLbl_Introduction_ExcitingAboutAFS;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FavoriteItem" {
    var AfsLbl_Introduction_FavoriteItem: string;
    export default AfsLbl_Introduction_FavoriteItem;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FavoriteItem_Error" {
    var AfsLbl_Introduction_FavoriteItem_Error: string;
    export default AfsLbl_Introduction_FavoriteItem_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FiveThings" {
    var AfsLbl_Introduction_FiveThings: string;
    export default AfsLbl_Introduction_FiveThings;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FiveThings_Error" {
    var AfsLbl_Introduction_FiveThings_Error: string;
    export default AfsLbl_Introduction_FiveThings_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FoodYouLike" {
    var AfsLbl_Introduction_FoodYouLike: string;
    export default AfsLbl_Introduction_FoodYouLike;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_FoodYouLike_Error" {
    var AfsLbl_Introduction_FoodYouLike_Error: string;
    export default AfsLbl_Introduction_FoodYouLike_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_Living_With_Host_Family_Header" {
    var AfsLbl_Introduction_Living_With_Host_Family_Header: string;
    export default AfsLbl_Introduction_Living_With_Host_Family_Header;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_ThingsCompleteYourDay" {
    var AfsLbl_Introduction_ThingsCompleteYourDay: string;
    export default AfsLbl_Introduction_ThingsCompleteYourDay;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_ThingsComplete_Error" {
    var AfsLbl_Introduction_ThingsComplete_Error: string;
    export default AfsLbl_Introduction_ThingsComplete_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_Video" {
    var AfsLbl_Introduction_Video: string;
    export default AfsLbl_Introduction_Video;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_Video_Error" {
    var AfsLbl_Introduction_Video_Error: string;
    export default AfsLbl_Introduction_Video_Error;
}
declare module "@salesforce/label/c.AfsLbl_Introduction_Video_PlaceHolder" {
    var AfsLbl_Introduction_Video_PlaceHolder: string;
    export default AfsLbl_Introduction_Video_PlaceHolder;
}
declare module "@salesforce/label/c.AfsLbl_Loading" {
    var AfsLbl_Loading: string;
    export default AfsLbl_Loading;
}
declare module "@salesforce/label/c.AfsLbl_LoginWith" {
    var AfsLbl_LoginWith: string;
    export default AfsLbl_LoginWith;
}
declare module "@salesforce/label/c.AfsLbl_LoginWithHelpText" {
    var AfsLbl_LoginWithHelpText: string;
    export default AfsLbl_LoginWithHelpText;
}
declare module "@salesforce/label/c.AfsLbl_Login_CheckBox2" {
    var AfsLbl_Login_CheckBox2: string;
    export default AfsLbl_Login_CheckBox2;
}
declare module "@salesforce/label/c.AfsLbl_Login_Checkbox1" {
    var AfsLbl_Login_Checkbox1: string;
    export default AfsLbl_Login_Checkbox1;
}
declare module "@salesforce/label/c.AfsLbl_Login_ErrMsgCheckbox" {
    var AfsLbl_Login_ErrMsgCheckbox: string;
    export default AfsLbl_Login_ErrMsgCheckbox;
}
declare module "@salesforce/label/c.AfsLbl_Login_ForgetPassword" {
    var AfsLbl_Login_ForgetPassword: string;
    export default AfsLbl_Login_ForgetPassword;
}
declare module "@salesforce/label/c.AfsLbl_Login_HavenotApplied" {
    var AfsLbl_Login_HavenotApplied: string;
    export default AfsLbl_Login_HavenotApplied;
}
declare module "@salesforce/label/c.AfsLbl_Login_Language" {
    var AfsLbl_Login_Language: string;
    export default AfsLbl_Login_Language;
}
declare module "@salesforce/label/c.AfsLbl_Login_LoginIn" {
    var AfsLbl_Login_LoginIn: string;
    export default AfsLbl_Login_LoginIn;
}
declare module "@salesforce/label/c.AfsLbl_Login_MiddleName" {
    var AfsLbl_Login_MiddleName: string;
    export default AfsLbl_Login_MiddleName;
}
declare module "@salesforce/label/c.AfsLbl_Login_PolicyHeader" {
    var AfsLbl_Login_PolicyHeader: string;
    export default AfsLbl_Login_PolicyHeader;
}
declare module "@salesforce/label/c.AfsLbl_Login_SignUp" {
    var AfsLbl_Login_SignUp: string;
    export default AfsLbl_Login_SignUp;
}
declare module "@salesforce/label/c.AfsLbl_Multiple_Departure_Dates" {
    var AfsLbl_Multiple_Departure_Dates: string;
    export default AfsLbl_Multiple_Departure_Dates;
}
declare module "@salesforce/label/c.AfsLbl_Multiple_Departure_Dates_Details" {
    var AfsLbl_Multiple_Departure_Dates_Details: string;
    export default AfsLbl_Multiple_Departure_Dates_Details;
}
declare module "@salesforce/label/c.AfsLbl_Multiple_Durations" {
    var AfsLbl_Multiple_Durations: string;
    export default AfsLbl_Multiple_Durations;
}
declare module "@salesforce/label/c.AfsLbl_Multiple_Durations_Detail" {
    var AfsLbl_Multiple_Durations_Detail: string;
    export default AfsLbl_Multiple_Durations_Detail;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_15minutes" {
    var AfsLbl_Navigation_15minutes: string;
    export default AfsLbl_Navigation_15minutes;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_ACCEPTED" {
    var AfsLbl_Navigation_ACCEPTED: string;
    export default AfsLbl_Navigation_ACCEPTED;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_AboutYou" {
    var AfsLbl_Navigation_AboutYou: string;
    export default AfsLbl_Navigation_AboutYou;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_ApplynowSideGetStarted" {
    var AfsLbl_Navigation_ApplynowSideGetStarted: string;
    export default AfsLbl_Navigation_ApplynowSideGetStarted;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_ApplynowSideHeader1" {
    var AfsLbl_Navigation_ApplynowSideHeader1: string;
    export default AfsLbl_Navigation_ApplynowSideHeader1;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_ApplynowSideHeader2" {
    var AfsLbl_Navigation_ApplynowSideHeader2: string;
    export default AfsLbl_Navigation_ApplynowSideHeader2;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_BONVOYAGE" {
    var AfsLbl_Navigation_BONVOYAGE: string;
    export default AfsLbl_Navigation_BONVOYAGE;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_GETREADY" {
    var AfsLbl_Navigation_GETREADY: string;
    export default AfsLbl_Navigation_GETREADY;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_LOGOUT" {
    var AfsLbl_Navigation_LOGOUT: string;
    export default AfsLbl_Navigation_LOGOUT;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_PRESELECTED" {
    var AfsLbl_Navigation_PRESELECTED: string;
    export default AfsLbl_Navigation_PRESELECTED;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_PersonalDetails" {
    var AfsLbl_Navigation_PersonalDetails: string;
    export default AfsLbl_Navigation_PersonalDetails;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_PreApplication" {
    var AfsLbl_Navigation_PreApplication: string;
    export default AfsLbl_Navigation_PreApplication;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_SelectaProgram" {
    var AfsLbl_Navigation_SelectaProgram: string;
    export default AfsLbl_Navigation_SelectaProgram;
}
declare module "@salesforce/label/c.AfsLbl_Navigation_Submitted" {
    var AfsLbl_Navigation_Submitted: string;
    export default AfsLbl_Navigation_Submitted;
}
declare module "@salesforce/label/c.AfsLbl_Opportunity_Delete_Error" {
    var AfsLbl_Opportunity_Delete_Error: string;
    export default AfsLbl_Opportunity_Delete_Error;
}
declare module "@salesforce/label/c.AfsLbl_Opportunity_Update_Error" {
    var AfsLbl_Opportunity_Update_Error: string;
    export default AfsLbl_Opportunity_Update_Error;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Amount_Paid" {
    var AfsLbl_PaymentPlan_Amount_Paid: string;
    export default AfsLbl_PaymentPlan_Amount_Paid;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Balance_Amount" {
    var AfsLbl_PaymentPlan_Balance_Amount: string;
    export default AfsLbl_PaymentPlan_Balance_Amount;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Due_Date" {
    var AfsLbl_PaymentPlan_Due_Date: string;
    export default AfsLbl_PaymentPlan_Due_Date;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payment_Amount" {
    var AfsLbl_PaymentPlan_Payment_Amount: string;
    export default AfsLbl_PaymentPlan_Payment_Amount;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payment_Date" {
    var AfsLbl_PaymentPlan_Payment_Date: string;
    export default AfsLbl_PaymentPlan_Payment_Date;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payment_Method" {
    var AfsLbl_PaymentPlan_Payment_Method: string;
    export default AfsLbl_PaymentPlan_Payment_Method;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payment_Number" {
    var AfsLbl_PaymentPlan_Payment_Number: string;
    export default AfsLbl_PaymentPlan_Payment_Number;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payment_Plan" {
    var AfsLbl_PaymentPlan_Payment_Plan: string;
    export default AfsLbl_PaymentPlan_Payment_Plan;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payments" {
    var AfsLbl_PaymentPlan_Payments: string;
    export default AfsLbl_PaymentPlan_Payments;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Payments_Installments" {
    var AfsLbl_PaymentPlan_Payments_Installments: string;
    export default AfsLbl_PaymentPlan_Payments_Installments;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_Save_Continue" {
    var AfsLbl_PaymentPlan_Save_Continue: string;
    export default AfsLbl_PaymentPlan_Save_Continue;
}
declare module "@salesforce/label/c.AfsLbl_PaymentPlan_your_payment_plan" {
    var AfsLbl_PaymentPlan_your_payment_plan: string;
    export default AfsLbl_PaymentPlan_your_payment_plan;
}
declare module "@salesforce/label/c.AfsLbl_Payment_ApplicationFee" {
    var AfsLbl_Payment_ApplicationFee: string;
    export default AfsLbl_Payment_ApplicationFee;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Description" {
    var AfsLbl_Payment_Description: string;
    export default AfsLbl_Payment_Description;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Header" {
    var AfsLbl_Payment_Header: string;
    export default AfsLbl_Payment_Header;
}
declare module "@salesforce/label/c.AfsLbl_Payment_MethodsHeader" {
    var AfsLbl_Payment_MethodsHeader: string;
    export default AfsLbl_Payment_MethodsHeader;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Payment1Text" {
    var AfsLbl_Payment_Payment1Text: string;
    export default AfsLbl_Payment_Payment1Text;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Payment2Text" {
    var AfsLbl_Payment_Payment2Text: string;
    export default AfsLbl_Payment_Payment2Text;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Payment3Text" {
    var AfsLbl_Payment_Payment3Text: string;
    export default AfsLbl_Payment_Payment3Text;
}
declare module "@salesforce/label/c.AfsLbl_Payment_Payment3Text1" {
    var AfsLbl_Payment_Payment3Text1: string;
    export default AfsLbl_Payment_Payment3Text1;
}
declare module "@salesforce/label/c.AfsLbl_Payment_PaymentMethodText1" {
    var AfsLbl_Payment_PaymentMethodText1: string;
    export default AfsLbl_Payment_PaymentMethodText1;
}
declare module "@salesforce/label/c.AfsLbl_Payment_PaymentMethodText2" {
    var AfsLbl_Payment_PaymentMethodText2: string;
    export default AfsLbl_Payment_PaymentMethodText2;
}
declare module "@salesforce/label/c.AfsLbl_Payment_PaymentMethodText3" {
    var AfsLbl_Payment_PaymentMethodText3: string;
    export default AfsLbl_Payment_PaymentMethodText3;
}
declare module "@salesforce/label/c.AfsLbl_PersonalDetail_MaidenName" {
    var AfsLbl_PersonalDetail_MaidenName: string;
    export default AfsLbl_PersonalDetail_MaidenName;
}
declare module "@salesforce/label/c.AfsLbl_PhotoProgress_CompleteYour" {
    var AfsLbl_PhotoProgress_CompleteYour: string;
    export default AfsLbl_PhotoProgress_CompleteYour;
}
declare module "@salesforce/label/c.AfsLbl_PhotoProgress_PHOTO" {
    var AfsLbl_PhotoProgress_PHOTO: string;
    export default AfsLbl_PhotoProgress_PHOTO;
}
declare module "@salesforce/label/c.AfsLbl_PhotoProgress_Profile" {
    var AfsLbl_PhotoProgress_Profile: string;
    export default AfsLbl_PhotoProgress_Profile;
}
declare module "@salesforce/label/c.AfsLbl_PhotoProgress_UploadA" {
    var AfsLbl_PhotoProgress_UploadA: string;
    export default AfsLbl_PhotoProgress_UploadA;
}
declare module "@salesforce/label/c.AfsLbl_Picklist_None" {
    var AfsLbl_Picklist_None: string;
    export default AfsLbl_Picklist_None;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_ApplicationFeeCover" {
    var AfsLbl_PreSelected_ApplicationFeeCover: string;
    export default AfsLbl_PreSelected_ApplicationFeeCover;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_ApplicationFeeCoverOption" {
    var AfsLbl_PreSelected_ApplicationFeeCoverOption: string;
    export default AfsLbl_PreSelected_ApplicationFeeCoverOption;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_ChangeProgram" {
    var AfsLbl_PreSelected_ChangeProgram: string;
    export default AfsLbl_PreSelected_ChangeProgram;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_ChangeProgramHeader" {
    var AfsLbl_PreSelected_ChangeProgramHeader: string;
    export default AfsLbl_PreSelected_ChangeProgramHeader;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_Header1" {
    var AfsLbl_PreSelected_Header1: string;
    export default AfsLbl_PreSelected_Header1;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_Header2" {
    var AfsLbl_PreSelected_Header2: string;
    export default AfsLbl_PreSelected_Header2;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_Header3" {
    var AfsLbl_PreSelected_Header3: string;
    export default AfsLbl_PreSelected_Header3;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_ProgramFee" {
    var AfsLbl_PreSelected_ProgramFee: string;
    export default AfsLbl_PreSelected_ProgramFee;
}
declare module "@salesforce/label/c.AfsLbl_PreSelected_SCHOLARSHIP" {
    var AfsLbl_PreSelected_SCHOLARSHIP: string;
    export default AfsLbl_PreSelected_SCHOLARSHIP;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_AppllyButtonTxt" {
    var AfsLbl_ScholarshipApply_AppllyButtonTxt: string;
    export default AfsLbl_ScholarshipApply_AppllyButtonTxt;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_ApplyBy" {
    var AfsLbl_ScholarshipApply_ApplyBy: string;
    export default AfsLbl_ScholarshipApply_ApplyBy;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_AppplicationFee" {
    var AfsLbl_ScholarshipApply_AppplicationFee: string;
    export default AfsLbl_ScholarshipApply_AppplicationFee;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_DesiredAmount" {
    var AfsLbl_ScholarshipApply_DesiredAmount: string;
    export default AfsLbl_ScholarshipApply_DesiredAmount;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_DesiredAmountDefault" {
    var AfsLbl_ScholarshipApply_DesiredAmountDefault: string;
    export default AfsLbl_ScholarshipApply_DesiredAmountDefault;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_DesiredAmountHelpTxt" {
    var AfsLbl_ScholarshipApply_DesiredAmountHelpTxt: string;
    export default AfsLbl_ScholarshipApply_DesiredAmountHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_ElegibilityDefault" {
    var AfsLbl_ScholarshipApply_ElegibilityDefault: string;
    export default AfsLbl_ScholarshipApply_ElegibilityDefault;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_ElegibilityHelpTxt" {
    var AfsLbl_ScholarshipApply_ElegibilityHelpTxt: string;
    export default AfsLbl_ScholarshipApply_ElegibilityHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_ElegibilityNewHeader" {
    var AfsLbl_ScholarshipApply_ElegibilityNewHeader: string;
    export default AfsLbl_ScholarshipApply_ElegibilityNewHeader;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_FooterTxt1" {
    var AfsLbl_ScholarshipApply_FooterTxt1: string;
    export default AfsLbl_ScholarshipApply_FooterTxt1;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_FooterTxt2" {
    var AfsLbl_ScholarshipApply_FooterTxt2: string;
    export default AfsLbl_ScholarshipApply_FooterTxt2;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_Full" {
    var AfsLbl_ScholarshipApply_Full: string;
    export default AfsLbl_ScholarshipApply_Full;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_HeaderDescription" {
    var AfsLbl_ScholarshipApply_HeaderDescription: string;
    export default AfsLbl_ScholarshipApply_HeaderDescription;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_MinimunGPA" {
    var AfsLbl_ScholarshipApply_MinimunGPA: string;
    export default AfsLbl_ScholarshipApply_MinimunGPA;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_NoRecordsFound" {
    var AfsLbl_ScholarshipApply_NoRecordsFound: string;
    export default AfsLbl_ScholarshipApply_NoRecordsFound;
}
declare module "@salesforce/label/c.AfsLbl_ScholarshipApply_Partial" {
    var AfsLbl_ScholarshipApply_Partial: string;
    export default AfsLbl_ScholarshipApply_Partial;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_AddDestination1Req" {
    var AfsLbl_SearchPrg_AddDestination1Req: string;
    export default AfsLbl_SearchPrg_AddDestination1Req;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_AddDestination2Req" {
    var AfsLbl_SearchPrg_AddDestination2Req: string;
    export default AfsLbl_SearchPrg_AddDestination2Req;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_AddDestination3Req" {
    var AfsLbl_SearchPrg_AddDestination3Req: string;
    export default AfsLbl_SearchPrg_AddDestination3Req;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_AreaOfInterest" {
    var AfsLbl_SearchPrg_AreaOfInterest: string;
    export default AfsLbl_SearchPrg_AreaOfInterest;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_DestinationSmall" {
    var AfsLbl_SearchPrg_DestinationSmall: string;
    export default AfsLbl_SearchPrg_DestinationSmall;
}
declare module "@salesforce/label/c.AfsLbl_SearchPrg_ProgramType" {
    var AfsLbl_SearchPrg_ProgramType: string;
    export default AfsLbl_SearchPrg_ProgramType;
}
declare module "@salesforce/label/c.AfsLbl_SecurityEnforceUtil_AccessError" {
    var AfsLbl_SecurityEnforceUtil_AccessError: string;
    export default AfsLbl_SecurityEnforceUtil_AccessError;
}
declare module "@salesforce/label/c.AfsLbl_SelectProg_HelpTextSearchCmp" {
    var AfsLbl_SelectProg_HelpTextSearchCmp: string;
    export default AfsLbl_SelectProg_HelpTextSearchCmp;
}
declare module "@salesforce/label/c.AfsLbl_SelectProg_ListThreeAdditional" {
    var AfsLbl_SelectProg_ListThreeAdditional: string;
    export default AfsLbl_SelectProg_ListThreeAdditional;
}
declare module "@salesforce/label/c.AfsLbl_SelectProg_ListThreeAdditional2" {
    var AfsLbl_SelectProg_ListThreeAdditional2: string;
    export default AfsLbl_SelectProg_ListThreeAdditional2;
}
declare module "@salesforce/label/c.AfsLbl_SelectProg_PageHeaderNew" {
    var AfsLbl_SelectProg_PageHeaderNew: string;
    export default AfsLbl_SelectProg_PageHeaderNew;
}
declare module "@salesforce/label/c.AfsLbl_SelectProg_SearchLabel" {
    var AfsLbl_SelectProg_SearchLabel: string;
    export default AfsLbl_SelectProg_SearchLabel;
}
declare module "@salesforce/label/c.AfsLbl_SelectionCamp_Description" {
    var AfsLbl_SelectionCamp_Description: string;
    export default AfsLbl_SelectionCamp_Description;
}
declare module "@salesforce/label/c.AfsLbl_SelectionCamp_Header" {
    var AfsLbl_SelectionCamp_Header: string;
    export default AfsLbl_SelectionCamp_Header;
}
declare module "@salesforce/label/c.AfsLbl_Setting_ChangeNow" {
    var AfsLbl_Setting_ChangeNow: string;
    export default AfsLbl_Setting_ChangeNow;
}
declare module "@salesforce/label/c.AfsLbl_Setting_ChangeyourPassword" {
    var AfsLbl_Setting_ChangeyourPassword: string;
    export default AfsLbl_Setting_ChangeyourPassword;
}
declare module "@salesforce/label/c.AfsLbl_Setting_PasswordChangeMsg" {
    var AfsLbl_Setting_PasswordChangeMsg: string;
    export default AfsLbl_Setting_PasswordChangeMsg;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_AlreadyApplied" {
    var AfsLbl_SignUp_AlreadyApplied: string;
    export default AfsLbl_SignUp_AlreadyApplied;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_CompleteYourRegistration" {
    var AfsLbl_SignUp_CompleteYourRegistration: string;
    export default AfsLbl_SignUp_CompleteYourRegistration;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_DOB" {
    var AfsLbl_SignUp_DOB: string;
    export default AfsLbl_SignUp_DOB;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_DOBHelpText" {
    var AfsLbl_SignUp_DOBHelpText: string;
    export default AfsLbl_SignUp_DOBHelpText;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Email" {
    var AfsLbl_SignUp_Email: string;
    export default AfsLbl_SignUp_Email;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_EmailPlaceHolder" {
    var AfsLbl_SignUp_EmailPlaceHolder: string;
    export default AfsLbl_SignUp_EmailPlaceHolder;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Facebook" {
    var AfsLbl_SignUp_Facebook: string;
    export default AfsLbl_SignUp_Facebook;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_FacebookOnly" {
    var AfsLbl_SignUp_FacebookOnly: string;
    export default AfsLbl_SignUp_FacebookOnly;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_FirstName" {
    var AfsLbl_SignUp_FirstName: string;
    export default AfsLbl_SignUp_FirstName;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Gmail" {
    var AfsLbl_SignUp_Gmail: string;
    export default AfsLbl_SignUp_Gmail;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_GmailOnly" {
    var AfsLbl_SignUp_GmailOnly: string;
    export default AfsLbl_SignUp_GmailOnly;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_HearAboutUs" {
    var AfsLbl_SignUp_HearAboutUs: string;
    export default AfsLbl_SignUp_HearAboutUs;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_HearAboutUsHelpText" {
    var AfsLbl_SignUp_HearAboutUsHelpText: string;
    export default AfsLbl_SignUp_HearAboutUsHelpText;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_LastName" {
    var AfsLbl_SignUp_LastName: string;
    export default AfsLbl_SignUp_LastName;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_LogIn" {
    var AfsLbl_SignUp_LogIn: string;
    export default AfsLbl_SignUp_LogIn;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_MobileHelpTxt" {
    var AfsLbl_SignUp_MobileHelpTxt: string;
    export default AfsLbl_SignUp_MobileHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_MobileNumber" {
    var AfsLbl_SignUp_MobileNumber: string;
    export default AfsLbl_SignUp_MobileNumber;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Name" {
    var AfsLbl_SignUp_Name: string;
    export default AfsLbl_SignUp_Name;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_None" {
    var AfsLbl_SignUp_None: string;
    export default AfsLbl_SignUp_None;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Or" {
    var AfsLbl_SignUp_Or: string;
    export default AfsLbl_SignUp_Or;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_Password" {
    var AfsLbl_SignUp_Password: string;
    export default AfsLbl_SignUp_Password;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_PasswordHelpTxt" {
    var AfsLbl_SignUp_PasswordHelpTxt: string;
    export default AfsLbl_SignUp_PasswordHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_PromoCode" {
    var AfsLbl_SignUp_PromoCode: string;
    export default AfsLbl_SignUp_PromoCode;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_PromoCodeHelpTxt" {
    var AfsLbl_SignUp_PromoCodeHelpTxt: string;
    export default AfsLbl_SignUp_PromoCodeHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_SignUpMe" {
    var AfsLbl_SignUp_SignUpMe: string;
    export default AfsLbl_SignUp_SignUpMe;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_SignUpWith" {
    var AfsLbl_SignUp_SignUpWith: string;
    export default AfsLbl_SignUp_SignUpWith;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_SignUpWithHelpTxt" {
    var AfsLbl_SignUp_SignUpWithHelpTxt: string;
    export default AfsLbl_SignUp_SignUpWithHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_SignUpwithEmail" {
    var AfsLbl_SignUp_SignUpwithEmail: string;
    export default AfsLbl_SignUp_SignUpwithEmail;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_ZipCode" {
    var AfsLbl_SignUp_ZipCode: string;
    export default AfsLbl_SignUp_ZipCode;
}
declare module "@salesforce/label/c.AfsLbl_SignUp_ZipCodeHelpTxt" {
    var AfsLbl_SignUp_ZipCodeHelpTxt: string;
    export default AfsLbl_SignUp_ZipCodeHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_SubmissionAdivisorHeader" {
    var AfsLbl_SubmissionAdivisorHeader: string;
    export default AfsLbl_SubmissionAdivisorHeader;
}
declare module "@salesforce/label/c.AfsLbl_SubmissionOpen_to_Sharing_a_host_family" {
    var AfsLbl_SubmissionOpen_to_Sharing_a_host_family: string;
    export default AfsLbl_SubmissionOpen_to_Sharing_a_host_family;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Add_ParentNew" {
    var AfsLbl_Submission_Add_ParentNew: string;
    export default AfsLbl_Submission_Add_ParentNew;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Age" {
    var AfsLbl_Submission_Age: string;
    export default AfsLbl_Submission_Age;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Are_you_open_to" {
    var AfsLbl_Submission_Are_you_open_to: string;
    export default AfsLbl_Submission_Are_you_open_to;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Brother" {
    var AfsLbl_Submission_Brother: string;
    export default AfsLbl_Submission_Brother;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Brother_stepbrother" {
    var AfsLbl_Submission_Brother_stepbrother: string;
    export default AfsLbl_Submission_Brother_stepbrother;
}
declare module "@salesforce/label/c.AfsLbl_Submission_CompleteYour" {
    var AfsLbl_Submission_CompleteYour: string;
    export default AfsLbl_Submission_CompleteYour;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Container_Message" {
    var AfsLbl_Submission_Container_Message: string;
    export default AfsLbl_Submission_Container_Message;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Destination" {
    var AfsLbl_Submission_Destination: string;
    export default AfsLbl_Submission_Destination;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Documentation_and_supporting" {
    var AfsLbl_Submission_Documentation_and_supporting: string;
    export default AfsLbl_Submission_Documentation_and_supporting;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Dual_CitizenShip" {
    var AfsLbl_Submission_Dual_CitizenShip: string;
    export default AfsLbl_Submission_Dual_CitizenShip;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Dual_Citizenship_Help_Text" {
    var AfsLbl_Submission_Dual_Citizenship_Help_Text: string;
    export default AfsLbl_Submission_Dual_Citizenship_Help_Text;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Emergency_Contact" {
    var AfsLbl_Submission_Emergency_Contact: string;
    export default AfsLbl_Submission_Emergency_Contact;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Facebook" {
    var AfsLbl_Submission_Facebook: string;
    export default AfsLbl_Submission_Facebook;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Father" {
    var AfsLbl_Submission_Father: string;
    export default AfsLbl_Submission_Father;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Grandfather" {
    var AfsLbl_Submission_Grandfather: string;
    export default AfsLbl_Submission_Grandfather;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Grandmother" {
    var AfsLbl_Submission_Grandmother: string;
    export default AfsLbl_Submission_Grandmother;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Help_Us" {
    var AfsLbl_Submission_Help_Us: string;
    export default AfsLbl_Submission_Help_Us;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Instagram" {
    var AfsLbl_Submission_Instagram: string;
    export default AfsLbl_Submission_Instagram;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Interested_in_apply_here" {
    var AfsLbl_Submission_Interested_in_apply_here: string;
    export default AfsLbl_Submission_Interested_in_apply_here;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Interested_in_scholarship" {
    var AfsLbl_Submission_Interested_in_scholarship: string;
    export default AfsLbl_Submission_Interested_in_scholarship;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Keep_going" {
    var AfsLbl_Submission_Keep_going: string;
    export default AfsLbl_Submission_Keep_going;
}
declare module "@salesforce/label/c.AfsLbl_Submission_LGBTQ" {
    var AfsLbl_Submission_LGBTQ: string;
    export default AfsLbl_Submission_LGBTQ;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Mother" {
    var AfsLbl_Submission_Mother: string;
    export default AfsLbl_Submission_Mother;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Open_to_Same" {
    var AfsLbl_Submission_Open_to_Same: string;
    export default AfsLbl_Submission_Open_to_Same;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Open_to_Single" {
    var AfsLbl_Submission_Open_to_Single: string;
    export default AfsLbl_Submission_Open_to_Single;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Optional_questions" {
    var AfsLbl_Submission_Optional_questions: string;
    export default AfsLbl_Submission_Optional_questions;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Other" {
    var AfsLbl_Submission_Other: string;
    export default AfsLbl_Submission_Other;
}
declare module "@salesforce/label/c.AfsLbl_Submission_PROFILE" {
    var AfsLbl_Submission_PROFILE: string;
    export default AfsLbl_Submission_PROFILE;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Program_Details" {
    var AfsLbl_Submission_Program_Details: string;
    export default AfsLbl_Submission_Program_Details;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Relationship" {
    var AfsLbl_Submission_Relationship: string;
    export default AfsLbl_Submission_Relationship;
}
declare module "@salesforce/label/c.AfsLbl_Submission_SecondCitizenshipHelpText" {
    var AfsLbl_Submission_SecondCitizenshipHelpText: string;
    export default AfsLbl_Submission_SecondCitizenshipHelpText;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Second_Citizenship" {
    var AfsLbl_Submission_Second_Citizenship: string;
    export default AfsLbl_Submission_Second_Citizenship;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Share_On_Twitter" {
    var AfsLbl_Submission_Share_On_Twitter: string;
    export default AfsLbl_Submission_Share_On_Twitter;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Share_on_facebook" {
    var AfsLbl_Submission_Share_on_facebook: string;
    export default AfsLbl_Submission_Share_on_facebook;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Sister" {
    var AfsLbl_Submission_Sister: string;
    export default AfsLbl_Submission_Sister;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Sister_stepsister" {
    var AfsLbl_Submission_Sister_stepsister: string;
    export default AfsLbl_Submission_Sister_stepsister;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Snapchat" {
    var AfsLbl_Submission_Snapchat: string;
    export default AfsLbl_Submission_Snapchat;
}
declare module "@salesforce/label/c.AfsLbl_Submission_StartDate" {
    var AfsLbl_Submission_StartDate: string;
    export default AfsLbl_Submission_StartDate;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Start_Global_Compentence" {
    var AfsLbl_Submission_Start_Global_Compentence: string;
    export default AfsLbl_Submission_Start_Global_Compentence;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Stepfather" {
    var AfsLbl_Submission_Stepfather: string;
    export default AfsLbl_Submission_Stepfather;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Stepmother" {
    var AfsLbl_Submission_Stepmother: string;
    export default AfsLbl_Submission_Stepmother;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Tell_your_friends" {
    var AfsLbl_Submission_Tell_your_friends: string;
    export default AfsLbl_Submission_Tell_your_friends;
}
declare module "@salesforce/label/c.AfsLbl_Submission_This_emergency_contact" {
    var AfsLbl_Submission_This_emergency_contact: string;
    export default AfsLbl_Submission_This_emergency_contact;
}
declare module "@salesforce/label/c.AfsLbl_Submission_This_will_help_us" {
    var AfsLbl_Submission_This_will_help_us: string;
    export default AfsLbl_Submission_This_will_help_us;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Twitter" {
    var AfsLbl_Submission_Twitter: string;
    export default AfsLbl_Submission_Twitter;
}
declare module "@salesforce/label/c.AfsLbl_Submission_UPLOAD_A" {
    var AfsLbl_Submission_UPLOAD_A: string;
    export default AfsLbl_Submission_UPLOAD_A;
}
declare module "@salesforce/label/c.AfsLbl_Submission_VisualGlimse" {
    var AfsLbl_Submission_VisualGlimse: string;
    export default AfsLbl_Submission_VisualGlimse;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Who_lives_at_home" {
    var AfsLbl_Submission_Who_lives_at_home: string;
    export default AfsLbl_Submission_Who_lives_at_home;
}
declare module "@salesforce/label/c.AfsLbl_Submission_YouTube" {
    var AfsLbl_Submission_YouTube: string;
    export default AfsLbl_Submission_YouTube;
}
declare module "@salesforce/label/c.AfsLbl_Submission_You_can_select_up_to_three" {
    var AfsLbl_Submission_You_can_select_up_to_three: string;
    export default AfsLbl_Submission_You_can_select_up_to_three;
}
declare module "@salesforce/label/c.AfsLbl_Submission_You_live_with_this_Parent" {
    var AfsLbl_Submission_You_live_with_this_Parent: string;
    export default AfsLbl_Submission_You_live_with_this_Parent;
}
declare module "@salesforce/label/c.AfsLbl_Submission_YourPhotos" {
    var AfsLbl_Submission_YourPhotos: string;
    export default AfsLbl_Submission_YourPhotos;
}
declare module "@salesforce/label/c.AfsLbl_Submission_YourPhotosHelpTxt" {
    var AfsLbl_Submission_YourPhotosHelpTxt: string;
    export default AfsLbl_Submission_YourPhotosHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Your_Family_Section" {
    var AfsLbl_Submission_Your_Family_Section: string;
    export default AfsLbl_Submission_Your_Family_Section;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Your_Global_Compentence_Course" {
    var AfsLbl_Submission_Your_Global_Compentence_Course: string;
    export default AfsLbl_Submission_Your_Global_Compentence_Course;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Your_social_accounts" {
    var AfsLbl_Submission_Your_social_accounts: string;
    export default AfsLbl_Submission_Your_social_accounts;
}
declare module "@salesforce/label/c.AfsLbl_Submission_Your_social_accounts_Help_Txt" {
    var AfsLbl_Submission_Your_social_accounts_Help_Txt: string;
    export default AfsLbl_Submission_Your_social_accounts_Help_Txt;
}
declare module "@salesforce/label/c.AfsLbl_Submission_ability_to_live_with_pets" {
    var AfsLbl_Submission_ability_to_live_with_pets: string;
    export default AfsLbl_Submission_ability_to_live_with_pets;
}
declare module "@salesforce/label/c.AfsLbl_Submission_duration" {
    var AfsLbl_Submission_duration: string;
    export default AfsLbl_Submission_duration;
}
declare module "@salesforce/label/c.AfsLbl_Submission_edit_your_account_settings" {
    var AfsLbl_Submission_edit_your_account_settings: string;
    export default AfsLbl_Submission_edit_your_account_settings;
}
declare module "@salesforce/label/c.AfsLbl_Submission_ethnicity" {
    var AfsLbl_Submission_ethnicity: string;
    export default AfsLbl_Submission_ethnicity;
}
declare module "@salesforce/label/c.AfsLbl_Submission_following_questions_are_optional" {
    var AfsLbl_Submission_following_questions_are_optional: string;
    export default AfsLbl_Submission_following_questions_are_optional;
}
declare module "@salesforce/label/c.AfsLbl_Submission_help_us_header" {
    var AfsLbl_Submission_help_us_header: string;
    export default AfsLbl_Submission_help_us_header;
}
declare module "@salesforce/label/c.AfsLbl_Submission_host_family" {
    var AfsLbl_Submission_host_family: string;
    export default AfsLbl_Submission_host_family;
}
declare module "@salesforce/label/c.AfsLbl_Submission_linkedin" {
    var AfsLbl_Submission_linkedin: string;
    export default AfsLbl_Submission_linkedin;
}
declare module "@salesforce/label/c.AfsLbl_Submission_of_your_application" {
    var AfsLbl_Submission_of_your_application: string;
    export default AfsLbl_Submission_of_your_application;
}
declare module "@salesforce/label/c.AfsLbl_Submission_photo" {
    var AfsLbl_Submission_photo: string;
    export default AfsLbl_Submission_photo;
}
declare module "@salesforce/label/c.AfsLbl_Submission_program" {
    var AfsLbl_Submission_program: string;
    export default AfsLbl_Submission_program;
}
declare module "@salesforce/label/c.AfsLbl_Submission_religious_af_liation" {
    var AfsLbl_Submission_religious_af_liation: string;
    export default AfsLbl_Submission_religious_af_liation;
}
declare module "@salesforce/label/c.AfsLbl_Submission_religious_services" {
    var AfsLbl_Submission_religious_services: string;
    export default AfsLbl_Submission_religious_services;
}
declare module "@salesforce/label/c.AfsLbl_Submission_save_Profile" {
    var AfsLbl_Submission_save_Profile: string;
    export default AfsLbl_Submission_save_Profile;
}
declare module "@salesforce/label/c.AfsLbl_Submission_step_by_step_guide" {
    var AfsLbl_Submission_step_by_step_guide: string;
    export default AfsLbl_Submission_step_by_step_guide;
}
declare module "@salesforce/label/c.AfsLbl_Submission_step_by_step_guide_Header" {
    var AfsLbl_Submission_step_by_step_guide_Header: string;
    export default AfsLbl_Submission_step_by_step_guide_Header;
}
declare module "@salesforce/label/c.AfsLbl_Submission_these_are_the_scholarships" {
    var AfsLbl_Submission_these_are_the_scholarships: string;
    export default AfsLbl_Submission_these_are_the_scholarships;
}
declare module "@salesforce/label/c.AfsLbl_Submission_you_completed" {
    var AfsLbl_Submission_you_completed: string;
    export default AfsLbl_Submission_you_completed;
}
declare module "@salesforce/label/c.AfsLbl_Submission_you_might_go_to" {
    var AfsLbl_Submission_you_might_go_to: string;
    export default AfsLbl_Submission_you_might_go_to;
}
declare module "@salesforce/label/c.AfsLbl_Submission_you_submitted_pre_app" {
    var AfsLbl_Submission_you_submitted_pre_app: string;
    export default AfsLbl_Submission_you_submitted_pre_app;
}
declare module "@salesforce/label/c.AfsLbl_Tile_COST" {
    var AfsLbl_Tile_COST: string;
    export default AfsLbl_Tile_COST;
}
declare module "@salesforce/label/c.AfsLbl_Tile_DESTINATION" {
    var AfsLbl_Tile_DESTINATION: string;
    export default AfsLbl_Tile_DESTINATION;
}
declare module "@salesforce/label/c.AfsLbl_Tile_DURATION" {
    var AfsLbl_Tile_DURATION: string;
    export default AfsLbl_Tile_DURATION;
}
declare module "@salesforce/label/c.AfsLbl_Tile_DurationSmall" {
    var AfsLbl_Tile_DurationSmall: string;
    export default AfsLbl_Tile_DurationSmall;
}
declare module "@salesforce/label/c.AfsLbl_Tile_ProgramDate" {
    var AfsLbl_Tile_ProgramDate: string;
    export default AfsLbl_Tile_ProgramDate;
}
declare module "@salesforce/label/c.AfsLbl_Tile_SELECT" {
    var AfsLbl_Tile_SELECT: string;
    export default AfsLbl_Tile_SELECT;
}
declare module "@salesforce/label/c.AfsLbl_Tile_Selected" {
    var AfsLbl_Tile_Selected: string;
    export default AfsLbl_Tile_Selected;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Country_Issue" {
    var AfsLbl_Travel_Country_Issue: string;
    export default AfsLbl_Travel_Country_Issue;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Country_Issue_Helptext" {
    var AfsLbl_Travel_Country_Issue_Helptext: string;
    export default AfsLbl_Travel_Country_Issue_Helptext;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Gender_in_Passport" {
    var AfsLbl_Travel_Gender_in_Passport: string;
    export default AfsLbl_Travel_Gender_in_Passport;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Complete_Document" {
    var AfsLbl_Travel_Info_Complete_Document: string;
    export default AfsLbl_Travel_Info_Complete_Document;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Immunization_Requirements" {
    var AfsLbl_Travel_Info_Immunization_Requirements: string;
    export default AfsLbl_Travel_Info_Immunization_Requirements;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Passport_Name" {
    var AfsLbl_Travel_Info_Passport_Name: string;
    export default AfsLbl_Travel_Info_Passport_Name;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_T_shit" {
    var AfsLbl_Travel_Info_T_shit: string;
    export default AfsLbl_Travel_Info_T_shit;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Travel_Information" {
    var AfsLbl_Travel_Info_Travel_Information: string;
    export default AfsLbl_Travel_Info_Travel_Information;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Unisex_Size" {
    var AfsLbl_Travel_Info_Unisex_Size: string;
    export default AfsLbl_Travel_Info_Unisex_Size;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Unisex_SizeHelpTxt" {
    var AfsLbl_Travel_Info_Unisex_SizeHelpTxt: string;
    export default AfsLbl_Travel_Info_Unisex_SizeHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Info_Visa_Requirements" {
    var AfsLbl_Travel_Info_Visa_Requirements: string;
    export default AfsLbl_Travel_Info_Visa_Requirements;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Passport_Expiration" {
    var AfsLbl_Travel_Passport_Expiration: string;
    export default AfsLbl_Travel_Passport_Expiration;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Passport_Expiration_Helptext" {
    var AfsLbl_Travel_Passport_Expiration_Helptext: string;
    export default AfsLbl_Travel_Passport_Expiration_Helptext;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Passport_Number" {
    var AfsLbl_Travel_Passport_Number: string;
    export default AfsLbl_Travel_Passport_Number;
}
declare module "@salesforce/label/c.AfsLbl_Travel_Passport_Number_HelpText" {
    var AfsLbl_Travel_Passport_Number_HelpText: string;
    export default AfsLbl_Travel_Passport_Number_HelpText;
}
declare module "@salesforce/label/c.AfsLbl_Travel_legal_residence" {
    var AfsLbl_Travel_legal_residence: string;
    export default AfsLbl_Travel_legal_residence;
}
declare module "@salesforce/label/c.AfsLbl_Travel_legal_residence_Helptext" {
    var AfsLbl_Travel_legal_residence_Helptext: string;
    export default AfsLbl_Travel_legal_residence_Helptext;
}
declare module "@salesforce/label/c.AfsLbl_Trigger_Scholarship" {
    var AfsLbl_Trigger_Scholarship: string;
    export default AfsLbl_Trigger_Scholarship;
}
declare module "@salesforce/label/c.AfsLbl_Waitlisted_Header1" {
    var AfsLbl_Waitlisted_Header1: string;
    export default AfsLbl_Waitlisted_Header1;
}
declare module "@salesforce/label/c.AfsLbl_Waitlisted_Header2" {
    var AfsLbl_Waitlisted_Header2: string;
    export default AfsLbl_Waitlisted_Header2;
}
declare module "@salesforce/label/c.AfsLbl_Waitlisted_Header3" {
    var AfsLbl_Waitlisted_Header3: string;
    export default AfsLbl_Waitlisted_Header3;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Additionalcomments" {
    var AfsLbl_YourApplication_Additionalcomments: string;
    export default AfsLbl_YourApplication_Additionalcomments;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Address" {
    var AfsLbl_YourApplication_Address: string;
    export default AfsLbl_YourApplication_Address;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_AddressHelpTxt" {
    var AfsLbl_YourApplication_AddressHelpTxt: string;
    export default AfsLbl_YourApplication_AddressHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_AddressSubHeader" {
    var AfsLbl_YourApplication_AddressSubHeader: string;
    export default AfsLbl_YourApplication_AddressSubHeader;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Button_AddLanguageNew" {
    var AfsLbl_YourApplication_Button_AddLanguageNew: string;
    export default AfsLbl_YourApplication_Button_AddLanguageNew;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Button_Save" {
    var AfsLbl_YourApplication_Button_Save: string;
    export default AfsLbl_YourApplication_Button_Save;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Button_SaveOnly" {
    var AfsLbl_YourApplication_Button_SaveOnly: string;
    export default AfsLbl_YourApplication_Button_SaveOnly;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Celiac" {
    var AfsLbl_YourApplication_Celiac: string;
    export default AfsLbl_YourApplication_Celiac;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_CitizenShip" {
    var AfsLbl_YourApplication_CitizenShip: string;
    export default AfsLbl_YourApplication_CitizenShip;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_CitizenShip1" {
    var AfsLbl_YourApplication_CitizenShip1: string;
    export default AfsLbl_YourApplication_CitizenShip1;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_City" {
    var AfsLbl_YourApplication_City: string;
    export default AfsLbl_YourApplication_City;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Country" {
    var AfsLbl_YourApplication_Country: string;
    export default AfsLbl_YourApplication_Country;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Criminalconvictions" {
    var AfsLbl_YourApplication_Criminalconvictions: string;
    export default AfsLbl_YourApplication_Criminalconvictions;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Describe" {
    var AfsLbl_YourApplication_Describe: string;
    export default AfsLbl_YourApplication_Describe;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_DietaryAdditionalcomments" {
    var AfsLbl_YourApplication_DietaryAdditionalcomments: string;
    export default AfsLbl_YourApplication_DietaryAdditionalcomments;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Education" {
    var AfsLbl_YourApplication_Education: string;
    export default AfsLbl_YourApplication_Education;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Email" {
    var AfsLbl_YourApplication_Email: string;
    export default AfsLbl_YourApplication_Email;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_FirstName" {
    var AfsLbl_YourApplication_FirstName: string;
    export default AfsLbl_YourApplication_FirstName;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_FoodAllergies" {
    var AfsLbl_YourApplication_FoodAllergies: string;
    export default AfsLbl_YourApplication_FoodAllergies;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_GenderHelpTxt" {
    var AfsLbl_YourApplication_GenderHelpTxt: string;
    export default AfsLbl_YourApplication_GenderHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Guardian" {
    var AfsLbl_YourApplication_Guardian: string;
    export default AfsLbl_YourApplication_Guardian;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Halal" {
    var AfsLbl_YourApplication_Halal: string;
    export default AfsLbl_YourApplication_Halal;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Health" {
    var AfsLbl_YourApplication_Health: string;
    export default AfsLbl_YourApplication_Health;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_HealthIssues" {
    var AfsLbl_YourApplication_HealthIssues: string;
    export default AfsLbl_YourApplication_HealthIssues;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_HighSchool" {
    var AfsLbl_YourApplication_HighSchool: string;
    export default AfsLbl_YourApplication_HighSchool;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Kosher" {
    var AfsLbl_YourApplication_Kosher: string;
    export default AfsLbl_YourApplication_Kosher;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Language" {
    var AfsLbl_YourApplication_Language: string;
    export default AfsLbl_YourApplication_Language;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_LastName" {
    var AfsLbl_YourApplication_LastName: string;
    export default AfsLbl_YourApplication_LastName;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_LegalRedencyHelpTxt" {
    var AfsLbl_YourApplication_LegalRedencyHelpTxt: string;
    export default AfsLbl_YourApplication_LegalRedencyHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_LimitedActivities" {
    var AfsLbl_YourApplication_LimitedActivities: string;
    export default AfsLbl_YourApplication_LimitedActivities;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_MiddleName" {
    var AfsLbl_YourApplication_MiddleName: string;
    export default AfsLbl_YourApplication_MiddleName;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_MobileNumber" {
    var AfsLbl_YourApplication_MobileNumber: string;
    export default AfsLbl_YourApplication_MobileNumber;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_NameOfSchool" {
    var AfsLbl_YourApplication_NameOfSchool: string;
    export default AfsLbl_YourApplication_NameOfSchool;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Nationality" {
    var AfsLbl_YourApplication_Nationality: string;
    export default AfsLbl_YourApplication_Nationality;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_NoDietaryRestriction" {
    var AfsLbl_YourApplication_NoDietaryRestriction: string;
    export default AfsLbl_YourApplication_NoDietaryRestriction;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Others" {
    var AfsLbl_YourApplication_Others: string;
    export default AfsLbl_YourApplication_Others;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_ParentGarduanHelpTxt" {
    var AfsLbl_YourApplication_ParentGarduanHelpTxt: string;
    export default AfsLbl_YourApplication_ParentGarduanHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_ParentName" {
    var AfsLbl_YourApplication_ParentName: string;
    export default AfsLbl_YourApplication_ParentName;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PersonalDetails" {
    var AfsLbl_YourApplication_PersonalDetails: string;
    export default AfsLbl_YourApplication_PersonalDetails;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PersonaldetailHeader" {
    var AfsLbl_YourApplication_PersonaldetailHeader: string;
    export default AfsLbl_YourApplication_PersonaldetailHeader;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PhoneNumber" {
    var AfsLbl_YourApplication_PhoneNumber: string;
    export default AfsLbl_YourApplication_PhoneNumber;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Physicalrestriction" {
    var AfsLbl_YourApplication_Physicalrestriction: string;
    export default AfsLbl_YourApplication_Physicalrestriction;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PostalCode" {
    var AfsLbl_YourApplication_PostalCode: string;
    export default AfsLbl_YourApplication_PostalCode;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PrimaryPhone" {
    var AfsLbl_YourApplication_PrimaryPhone: string;
    export default AfsLbl_YourApplication_PrimaryPhone;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PrimaryPhoneHelpText" {
    var AfsLbl_YourApplication_PrimaryPhoneHelpText: string;
    export default AfsLbl_YourApplication_PrimaryPhoneHelpText;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Pro_ciency" {
    var AfsLbl_YourApplication_Pro_ciency: string;
    export default AfsLbl_YourApplication_Pro_ciency;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_PrsonalAddress" {
    var AfsLbl_YourApplication_PrsonalAddress: string;
    export default AfsLbl_YourApplication_PrsonalAddress;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_School" {
    var AfsLbl_YourApplication_School: string;
    export default AfsLbl_YourApplication_School;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SchoolAttend" {
    var AfsLbl_YourApplication_SchoolAttend: string;
    export default AfsLbl_YourApplication_SchoolAttend;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SchoolCity" {
    var AfsLbl_YourApplication_SchoolCity: string;
    export default AfsLbl_YourApplication_SchoolCity;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SchoolZipCode" {
    var AfsLbl_YourApplication_SchoolZipCode: string;
    export default AfsLbl_YourApplication_SchoolZipCode;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SelectCountry" {
    var AfsLbl_YourApplication_SelectCountry: string;
    export default AfsLbl_YourApplication_SelectCountry;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SelectGender" {
    var AfsLbl_YourApplication_SelectGender: string;
    export default AfsLbl_YourApplication_SelectGender;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SelectNationality" {
    var AfsLbl_YourApplication_SelectNationality: string;
    export default AfsLbl_YourApplication_SelectNationality;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Smoke" {
    var AfsLbl_YourApplication_Smoke: string;
    export default AfsLbl_YourApplication_Smoke;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_SreetAddressSecond" {
    var AfsLbl_YourApplication_SreetAddressSecond: string;
    export default AfsLbl_YourApplication_SreetAddressSecond;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_State" {
    var AfsLbl_YourApplication_State: string;
    export default AfsLbl_YourApplication_State;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Street_Address" {
    var AfsLbl_YourApplication_Street_Address: string;
    export default AfsLbl_YourApplication_Street_Address;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_StudentDescribe" {
    var AfsLbl_YourApplication_StudentDescribe: string;
    export default AfsLbl_YourApplication_StudentDescribe;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_TakeMedication" {
    var AfsLbl_YourApplication_TakeMedication: string;
    export default AfsLbl_YourApplication_TakeMedication;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Title" {
    var AfsLbl_YourApplication_Title: string;
    export default AfsLbl_YourApplication_Title;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Vegan" {
    var AfsLbl_YourApplication_Vegan: string;
    export default AfsLbl_YourApplication_Vegan;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Vegetarian" {
    var AfsLbl_YourApplication_Vegetarian: string;
    export default AfsLbl_YourApplication_Vegetarian;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_WhenGraduate" {
    var AfsLbl_YourApplication_WhenGraduate: string;
    export default AfsLbl_YourApplication_WhenGraduate;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_Yes" {
    var AfsLbl_YourApplication_Yes: string;
    export default AfsLbl_YourApplication_Yes;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_allergies" {
    var AfsLbl_YourApplication_allergies: string;
    export default AfsLbl_YourApplication_allergies;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_attenduniversity" {
    var AfsLbl_YourApplication_attenduniversity: string;
    export default AfsLbl_YourApplication_attenduniversity;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_dietaryrestriction" {
    var AfsLbl_YourApplication_dietaryrestriction: string;
    export default AfsLbl_YourApplication_dietaryrestriction;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_gender" {
    var AfsLbl_YourApplication_gender: string;
    export default AfsLbl_YourApplication_gender;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_livewithpets" {
    var AfsLbl_YourApplication_livewithpets: string;
    export default AfsLbl_YourApplication_livewithpets;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_restriction" {
    var AfsLbl_YourApplication_restriction: string;
    export default AfsLbl_YourApplication_restriction;
}
declare module "@salesforce/label/c.AfsLbl_YourApplication_withsmoker" {
    var AfsLbl_YourApplication_withsmoker: string;
    export default AfsLbl_YourApplication_withsmoker;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AlergiesList" {
    var AfsLbl_YourHealthInformation_AlergiesList: string;
    export default AfsLbl_YourHealthInformation_AlergiesList;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AlergiesPets" {
    var AfsLbl_YourHealthInformation_AlergiesPets: string;
    export default AfsLbl_YourHealthInformation_AlergiesPets;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AlergiesPetsFreeRoom" {
    var AfsLbl_YourHealthInformation_AlergiesPetsFreeRoom: string;
    export default AfsLbl_YourHealthInformation_AlergiesPetsFreeRoom;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AlergiesPetsHairFree" {
    var AfsLbl_YourHealthInformation_AlergiesPetsHairFree: string;
    export default AfsLbl_YourHealthInformation_AlergiesPetsHairFree;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Allergies" {
    var AfsLbl_YourHealthInformation_Allergies: string;
    export default AfsLbl_YourHealthInformation_Allergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Asthma" {
    var AfsLbl_YourHealthInformation_Asthma: string;
    export default AfsLbl_YourHealthInformation_Asthma;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaDailyLifeAffect" {
    var AfsLbl_YourHealthInformation_AsthmaDailyLifeAffect: string;
    export default AfsLbl_YourHealthInformation_AsthmaDailyLifeAffect;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaDailyLifeAffectHow" {
    var AfsLbl_YourHealthInformation_AsthmaDailyLifeAffectHow: string;
    export default AfsLbl_YourHealthInformation_AsthmaDailyLifeAffectHow;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaMedication" {
    var AfsLbl_YourHealthInformation_AsthmaMedication: string;
    export default AfsLbl_YourHealthInformation_AsthmaMedication;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaMedicationDescribe" {
    var AfsLbl_YourHealthInformation_AsthmaMedicationDescribe: string;
    export default AfsLbl_YourHealthInformation_AsthmaMedicationDescribe;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaSeverity" {
    var AfsLbl_YourHealthInformation_AsthmaSeverity: string;
    export default AfsLbl_YourHealthInformation_AsthmaSeverity;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaSpecialConsideration" {
    var AfsLbl_YourHealthInformation_AsthmaSpecialConsideration: string;
    export default AfsLbl_YourHealthInformation_AsthmaSpecialConsideration;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaSpecialConsiderationDesc" {
    var AfsLbl_YourHealthInformation_AsthmaSpecialConsiderationDesc: string;
    export default AfsLbl_YourHealthInformation_AsthmaSpecialConsiderationDesc;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaSymptoms" {
    var AfsLbl_YourHealthInformation_AsthmaSymptoms: string;
    export default AfsLbl_YourHealthInformation_AsthmaSymptoms;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_AsthmaTrigger" {
    var AfsLbl_YourHealthInformation_AsthmaTrigger: string;
    export default AfsLbl_YourHealthInformation_AsthmaTrigger;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacDailyAffect" {
    var AfsLbl_YourHealthInformation_CeliacDailyAffect: string;
    export default AfsLbl_YourHealthInformation_CeliacDailyAffect;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacDietManagement" {
    var AfsLbl_YourHealthInformation_CeliacDietManagement: string;
    export default AfsLbl_YourHealthInformation_CeliacDietManagement;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacFoodSamePlate" {
    var AfsLbl_YourHealthInformation_CeliacFoodSamePlate: string;
    export default AfsLbl_YourHealthInformation_CeliacFoodSamePlate;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacFoodSamePot" {
    var AfsLbl_YourHealthInformation_CeliacFoodSamePot: string;
    export default AfsLbl_YourHealthInformation_CeliacFoodSamePot;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacHospitalization" {
    var AfsLbl_YourHealthInformation_CeliacHospitalization: string;
    export default AfsLbl_YourHealthInformation_CeliacHospitalization;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacHospitalizationHistory" {
    var AfsLbl_YourHealthInformation_CeliacHospitalizationHistory: string;
    export default AfsLbl_YourHealthInformation_CeliacHospitalizationHistory;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacLimitPhysicalActivity" {
    var AfsLbl_YourHealthInformation_CeliacLimitPhysicalActivity: string;
    export default AfsLbl_YourHealthInformation_CeliacLimitPhysicalActivity;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacMedications" {
    var AfsLbl_YourHealthInformation_CeliacMedications: string;
    export default AfsLbl_YourHealthInformation_CeliacMedications;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacMedicationsAdmin" {
    var AfsLbl_YourHealthInformation_CeliacMedicationsAdmin: string;
    export default AfsLbl_YourHealthInformation_CeliacMedicationsAdmin;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacMedicationsDetails" {
    var AfsLbl_YourHealthInformation_CeliacMedicationsDetails: string;
    export default AfsLbl_YourHealthInformation_CeliacMedicationsDetails;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacReactions" {
    var AfsLbl_YourHealthInformation_CeliacReactions: string;
    export default AfsLbl_YourHealthInformation_CeliacReactions;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacSymptoms" {
    var AfsLbl_YourHealthInformation_CeliacSymptoms: string;
    export default AfsLbl_YourHealthInformation_CeliacSymptoms;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_CeliacVisitDoctor" {
    var AfsLbl_YourHealthInformation_CeliacVisitDoctor: string;
    export default AfsLbl_YourHealthInformation_CeliacVisitDoctor;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Celiacdisease" {
    var AfsLbl_YourHealthInformation_Celiacdisease: string;
    export default AfsLbl_YourHealthInformation_Celiacdisease;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DaibetiesHelpTxt" {
    var AfsLbl_YourHealthInformation_DaibetiesHelpTxt: string;
    export default AfsLbl_YourHealthInformation_DaibetiesHelpTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Dental" {
    var AfsLbl_YourHealthInformation_Dental: string;
    export default AfsLbl_YourHealthInformation_Dental;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DentalCareProgram" {
    var AfsLbl_YourHealthInformation_DentalCareProgram: string;
    export default AfsLbl_YourHealthInformation_DentalCareProgram;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DentalNote" {
    var AfsLbl_YourHealthInformation_DentalNote: string;
    export default AfsLbl_YourHealthInformation_DentalNote;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Diabetes" {
    var AfsLbl_YourHealthInformation_Diabetes: string;
    export default AfsLbl_YourHealthInformation_Diabetes;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DiabetiesNeedsDoc" {
    var AfsLbl_YourHealthInformation_DiabetiesNeedsDoc: string;
    export default AfsLbl_YourHealthInformation_DiabetiesNeedsDoc;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DiagnosisDate" {
    var AfsLbl_YourHealthInformation_DiagnosisDate: string;
    export default AfsLbl_YourHealthInformation_DiagnosisDate;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DoYouHaveAllergies" {
    var AfsLbl_YourHealthInformation_DoYouHaveAllergies: string;
    export default AfsLbl_YourHealthInformation_DoYouHaveAllergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DoYouHaveAsthma" {
    var AfsLbl_YourHealthInformation_DoYouHaveAsthma: string;
    export default AfsLbl_YourHealthInformation_DoYouHaveAsthma;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Doallergiessignificantlyaffectyourdailylifeorlimity" {
    var AfsLbl_YourHealthInformation_Doallergiessignificantlyaffectyourdailylifeorlimity: string;
    export default AfsLbl_YourHealthInformation_Doallergiessignificantlyaffectyourdailylifeorlimity;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DoyouCeliacdisease" {
    var AfsLbl_YourHealthInformation_DoyouCeliacdisease: string;
    export default AfsLbl_YourHealthInformation_DoyouCeliacdisease;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DoyouDiabetes" {
    var AfsLbl_YourHealthInformation_DoyouDiabetes: string;
    export default AfsLbl_YourHealthInformation_DoyouDiabetes;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Doyouevertakemedicationforyourallergies" {
    var AfsLbl_YourHealthInformation_Doyouevertakemedicationforyourallergies: string;
    export default AfsLbl_YourHealthInformation_Doyouevertakemedicationforyourallergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_DoyouhaveallergieswhichrequireyoutocarryanEpiPen" {
    var AfsLbl_YourHealthInformation_DoyouhaveallergieswhichrequireyoutocarryanEpiPen: string;
    export default AfsLbl_YourHealthInformation_DoyouhaveallergieswhichrequireyoutocarryanEpiPen;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_EPiPenSubTxt" {
    var AfsLbl_YourHealthInformation_EPiPenSubTxt: string;
    export default AfsLbl_YourHealthInformation_EPiPenSubTxt;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Header" {
    var AfsLbl_YourHealthInformation_Header: string;
    export default AfsLbl_YourHealthInformation_Header;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Howdoyoudescribetheseverityofyourallergies" {
    var AfsLbl_YourHealthInformation_Howdoyoudescribetheseverityofyourallergies: string;
    export default AfsLbl_YourHealthInformation_Howdoyoudescribetheseverityofyourallergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Howisyourlifeaffectedoractivitieslimited" {
    var AfsLbl_YourHealthInformation_Howisyourlifeaffectedoractivitieslimited: string;
    export default AfsLbl_YourHealthInformation_Howisyourlifeaffectedoractivitieslimited;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Howoftendoyouusetheeippen" {
    var AfsLbl_YourHealthInformation_Howoftendoyouusetheeippen: string;
    export default AfsLbl_YourHealthInformation_Howoftendoyouusetheeippen;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_ManageCondition" {
    var AfsLbl_YourHealthInformation_ManageCondition: string;
    export default AfsLbl_YourHealthInformation_ManageCondition;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_PleaseFill" {
    var AfsLbl_YourHealthInformation_PleaseFill: string;
    export default AfsLbl_YourHealthInformation_PleaseFill;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_PleaseFillAllergies" {
    var AfsLbl_YourHealthInformation_PleaseFillAllergies: string;
    export default AfsLbl_YourHealthInformation_PleaseFillAllergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_PleaseFillAsthma" {
    var AfsLbl_YourHealthInformation_PleaseFillAsthma: string;
    export default AfsLbl_YourHealthInformation_PleaseFillAsthma;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_PleaseFillCeliac" {
    var AfsLbl_YourHealthInformation_PleaseFillCeliac: string;
    export default AfsLbl_YourHealthInformation_PleaseFillCeliac;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Select" {
    var AfsLbl_YourHealthInformation_Select: string;
    export default AfsLbl_YourHealthInformation_Select;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiers" {
    var AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiers: string;
    export default AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiers;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiersDescribe" {
    var AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiersDescribe: string;
    export default AfsLbl_YourHealthInformation_SpecialCOnisderationAlergiersDescribe;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Whatareyouallergicto" {
    var AfsLbl_YourHealthInformation_Whatareyouallergicto: string;
    export default AfsLbl_YourHealthInformation_Whatareyouallergicto;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Whatsymptomsdoyouexperience" {
    var AfsLbl_YourHealthInformation_Whatsymptomsdoyouexperience: string;
    export default AfsLbl_YourHealthInformation_Whatsymptomsdoyouexperience;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Whattriggersyourallergyies" {
    var AfsLbl_YourHealthInformation_Whattriggersyourallergyies: string;
    export default AfsLbl_YourHealthInformation_Whattriggersyourallergyies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_Whendoyoutypicallyexperienceyourallergies" {
    var AfsLbl_YourHealthInformation_Whendoyoutypicallyexperienceyourallergies: string;
    export default AfsLbl_YourHealthInformation_Whendoyoutypicallyexperienceyourallergies;
}
declare module "@salesforce/label/c.AfsLbl_YourHealthInformation_whatisthenameofthemedicationwhatisthedosageandhowof" {
    var AfsLbl_YourHealthInformation_whatisthenameofthemedicationwhatisthedosageandhowof: string;
    export default AfsLbl_YourHealthInformation_whatisthenameofthemedicationwhatisthedosageandhowof;
}
declare module "@salesforce/label/c.AfsLbl_YourProfile_AboutYou" {
    var AfsLbl_YourProfile_AboutYou: string;
    export default AfsLbl_YourProfile_AboutYou;
}
declare module "@salesforce/label/c.AfsLbl_YourProfile_DescribeYourselfStudent" {
    var AfsLbl_YourProfile_DescribeYourselfStudent: string;
    export default AfsLbl_YourProfile_DescribeYourselfStudent;
}
declare module "@salesforce/label/c.AfsLbl_YourProfile_NotSelected" {
    var AfsLbl_YourProfile_NotSelected: string;
    export default AfsLbl_YourProfile_NotSelected;
}
declare module "@salesforce/label/c.AfsLbl_YourProfile_TellAboutYourself" {
    var AfsLbl_YourProfile_TellAboutYourself: string;
    export default AfsLbl_YourProfile_TellAboutYourself;
}
declare module "@salesforce/label/c.AfsLbl_YourProfile_WhatExperience" {
    var AfsLbl_YourProfile_WhatExperience: string;
    export default AfsLbl_YourProfile_WhatExperience;
}
declare module "@salesforce/label/c.Afs_Google_Key" {
    var Afs_Google_Key: string;
    export default Afs_Google_Key;
}
declare module "@salesforce/label/c.DUE" {
    var DUE: string;
    export default DUE;
}
declare module "@salesforce/label/c.Error_in_processing_request_try_after_few_minutes" {
    var Error_in_processing_request_try_after_few_minutes: string;
    export default Error_in_processing_request_try_after_few_minutes;
}
declare module "@salesforce/label/c.Month_April" {
    var Month_April: string;
    export default Month_April;
}
declare module "@salesforce/label/c.Month_August" {
    var Month_August: string;
    export default Month_August;
}
declare module "@salesforce/label/c.Month_December" {
    var Month_December: string;
    export default Month_December;
}
declare module "@salesforce/label/c.Month_February" {
    var Month_February: string;
    export default Month_February;
}
declare module "@salesforce/label/c.Month_January" {
    var Month_January: string;
    export default Month_January;
}
declare module "@salesforce/label/c.Month_July" {
    var Month_July: string;
    export default Month_July;
}
declare module "@salesforce/label/c.Month_June" {
    var Month_June: string;
    export default Month_June;
}
declare module "@salesforce/label/c.Month_March" {
    var Month_March: string;
    export default Month_March;
}
declare module "@salesforce/label/c.Month_May" {
    var Month_May: string;
    export default Month_May;
}
declare module "@salesforce/label/c.Month_November" {
    var Month_November: string;
    export default Month_November;
}
declare module "@salesforce/label/c.Month_October" {
    var Month_October: string;
    export default Month_October;
}
declare module "@salesforce/label/c.Month_September" {
    var Month_September: string;
    export default Month_September;
}
declare module "@salesforce/label/c.Upload_your_travel_documents" {
    var Upload_your_travel_documents: string;
    export default Upload_your_travel_documents;
}
declare module "@salesforce/label/c.Upload_your_travel_documentsdesc" {
    var Upload_your_travel_documentsdesc: string;
    export default Upload_your_travel_documentsdesc;
}
